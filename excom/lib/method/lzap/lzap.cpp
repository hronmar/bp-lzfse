/*
 * File:       method/lzap/lzap.cpp
 * Purpose:    Implementation of the LZAP compression methods
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of LZAP compression module linked to the ExCom library.
 *
 * LZAP compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZAP compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZAP module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lzap.cpp
 *  \brief Implementation of the LZAP compression method
 */

#include "lzap.hpp"
#include "../universal_codes/UniversalCodes.h"

#include <iostream>

CompLZAP::CompLZAP(unsigned int handle) : 
	CompModule(handle)
	//sorter(DEFAULT_DICT_SIZE - (1 << DEFAULT_CHAR_BITSIZE), 1 << DEFAULT_CHAR_BITSIZE)
{
	reader = NULL; 
	writer = NULL; 
	dict_capacity = DEFAULT_DICT_SIZE;
	char_bitsize = DEFAULT_CHAR_BITSIZE;
	readSymbol = &CompLZAP::readByte;
	writeSymbol = &CompLZAP::writeByte;
	root_size = 1 << DEFAULT_CHAR_BITSIZE;
	decodedString.reserve(512);
}

int CompLZAP::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZAP::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompLZAP::setParameter(int parameter, void *value)
{
	if (value == NULL) 
		return EXCOM_ERR_MEMORY;

	unsigned int u = *((unsigned int*)value);

	switch (parameter) {
	case EXCOM_PARAM_LZAP_DICTIONARY_SIZE:
		if (u < (1 << char_bitsize) || u == ~0u) 
			return EXCOM_ERR_VALUE;
		dict_capacity = u;
		break;
	case EXCOM_PARAM_LZAP_CHAR_BITSIZE:
		if (u == 0 || u > 31 || dict_capacity < (1 << u))
			return EXCOM_ERR_VALUE;
		if ((char_bitsize = (unsigned char)u) == 8) {
			readSymbol = &CompLZAP::readByte;
			writeSymbol = &CompLZAP::writeByte;
		}	
		else {
			readSymbol = &CompLZAP::readNBits;
			writeSymbol = &CompLZAP::writeNBits;
		}
		root_size = 1 << char_bitsize;
		break;
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZAP::getValue(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_LZAP_DICTIONARY_SIZE:
		u = dict_capacity;
		break;
	case EXCOM_VALUE_LZAP_CHAR_BITSIZE:
		u = char_bitsize;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}

void CompLZAP::initializeDictionary() {
	encDict.clear();
	encDict.reserve(dict_capacity);

	lzw_node_enc_t lzwEncNode;
	lzwEncNode.first = ~0u;
	lzwEncNode.left = ~0u;
	lzwEncNode.right = ~0u;
	lzwEncNode.prefixIndex = ~0u;

	while (encDict.size() < root_size) {
		lzwEncNode.symbol = encDict.size();
		encDict.push_back(lzwEncNode);
	}

	bitsize = char_bitsize;
	threshold = 1 << char_bitsize;
}
/*
void remove(unsigned pos) {
	//if (pos > encDict.size() || pos <= root_size)
	//	return false;

	if (pos != encDict.size()) {
		lzw_node_enc_t &node = encDict[pos];
		
		unsigned index = encDict[node.prefixIndex].first;
	
		if (index == pos) {
			if (encDict[pos].left == ~0u)
				encDict[node.prefixIndex].first = encDict[pos].right;
			else {
				unsigned i = encDict[pos].left;
				while (encDict[i].right != ~0)
					i = encDict[i].right;
				encDict[node.prefixIndex].first = i;
			}
		}

		while (index != pos) {
			if (node.symbol > encDict[index].symbol)
				index = encDict[index].right;
			else index = encDict[index].left;
		} 		
	}
}
*/
// The code is based on an idea of Juha Nieminen
// http://warp.povusers.org/EfficientLZW/ 
unsigned CompLZAP::add(lzw_node_enc_t &node) {
	unsigned index = encDict[node.prefixIndex].first;

	if (index == ~0u) {
		encDict[node.prefixIndex].first = encDict.size();
		//sorter.InvalidateEntry(node.prefixIndex);
	} else {
		while (true) {
			if (encDict[index].symbol == node.symbol) 
				return index;

			if (encDict[index].symbol < node.symbol) {
				if (encDict[index].right == ~0u) {
					encDict[index].right = encDict.size();
					break; 
				}
				index = encDict[index].right;
			} else {
				if (encDict[index].left == ~0u) {
					encDict[index].left = encDict.size();
					break; 
				}        
				index = encDict[index].left;		
			}
		}
	}

	unsigned size = encDict.size();

	encDict.push_back(node);
	//sorter.InsertEntry(size, size);	

	return size;
}

unsigned CompLZAP::find(lzw_node_enc_t &node) const {
	unsigned index = encDict[node.prefixIndex].first;

	if (index != ~0u) {
		while (true) {
			if (encDict[index].symbol == node.symbol)
				return index;

			if (encDict[index].symbol < node.symbol) {
				if (encDict[index].right == ~0u) {
					break; 
				}
				index = encDict[index].right;
			} else {
				if (encDict[index].left == ~0u) {
					break; 
				}        
				index = encDict[index].left;		
			}
		}
	}

	return ~0u;
}

unsigned CompLZAP::searchInsertNode(lzw_node_enc_t& node) {
	if (node.prefixIndex == ~0u)
		return node.symbol;

	return add(node);
}
unsigned CompLZAP::searchNode(lzw_node_enc_t &node) const {
	if (node.prefixIndex == ~0u)
		return node.symbol;

	return find(node);
}

int CompLZAP::runCompress() {
	unsigned long c;	
	unsigned index;

	lzw_node_enc_t lzwNode;
	lzwNode.first = ~0u;
	lzwNode.right = ~0u;
	lzwNode.left = ~0u;
	lzwNode.prefixIndex = ~0u;

	initializeDictionary();

	unsigned index_of_phrase_G, index_of_phrase_F = ~0u;

	std::vector<unsigned> phrase_G;
	std::vector<unsigned>::const_iterator it;
	
	if ((this->*readSymbol)(&c) != EOF) {
		while (true) {
			lzwNode.symbol = (unsigned)c;

			if ((index = searchNode(lzwNode)) != ~0u) { //try to find node and if it exists...
				index_of_phrase_G = index; //remember it's index	
				lzwNode.prefixIndex = index; //G = G + c

				phrase_G.push_back(encDict[index].symbol); //buffer phrase G, it can be reconstructed later from index_of_phrase_G but it would take O(n) time

				if ((this->*readSymbol)(&c) == EOF)
					break;

				continue;
			}

			writer->writeNBits(bitsize, index_of_phrase_G); //output index of last valid phrase (G)

			//insert F + all prefixes of G
			lzwNode.prefixIndex = index_of_phrase_F; //F is already in dict so just set it as parent of new entry
			for (it = phrase_G.begin(); it != phrase_G.end(); ++it) { //and G is in our buffer
				lzwNode.symbol = *it;

				if (encDict.size() == dict_capacity) {
					initializeDictionary();
					goto skip_insert;
				}

				lzwNode.prefixIndex = searchInsertNode(lzwNode);
			}		

			//modify bitsize if and while neccessary
			if (encDict.size() != dict_capacity) {
				while (encDict.size() > threshold) { 
					threshold <<= 1;
					++bitsize;
				}
			}
		
			index_of_phrase_F = index_of_phrase_G;		
skip_insert:
			phrase_G.clear();
			lzwNode.prefixIndex = ~0u;
		}
		
		if (lzwNode.prefixIndex != ~0u) 
			writer->writeNBits(bitsize, lzwNode.prefixIndex); //output ID of last valid phrase if necessary
	}

	writer->eof();

	return EXCOM_ERR_OK;
}

int CompLZAP::runDecompress() {
	unsigned long input;

	std::vector<unsigned>::const_reverse_iterator it;
	unsigned index;
	unsigned index_of_phrase_G, index_of_phrase_F = ~0u;

	initializeDictionary();

	lzw_node_enc_t lzwNode;
	lzwNode.first = ~0u;
	lzwNode.right = ~0u;
	lzwNode.left = ~0u;

	while (reader->readNBits(bitsize, &input) != EOF) {
		if (input >= encDict.size())
			return EXCOM_ERR_FORMAT;

		decodedString.clear();
		index_of_phrase_G = index = (unsigned)input;
		do {
			lzw_node_enc_t &node = encDict[index];
			decodedString.push_back(node.symbol);
			index = node.prefixIndex;
		} while (index != ~0u);

		//insert F + all prefixes of G into dictionary

		lzwNode.prefixIndex = index_of_phrase_F; //start on position of phrase F			
		for (it = decodedString.rbegin(); it != decodedString.rend(); ++it) { //add new entry for every character read from phrase G
			(this->*writeSymbol)(*it);
			lzwNode.symbol = *it;

			if (encDict.size() == dict_capacity) {
				initializeDictionary();
				while (++it != decodedString.rend())
					(this->*writeSymbol)(*it); //output rest of the phrase
				goto skip_insert;
			}

			index = searchInsertNode(lzwNode);	
			lzwNode.prefixIndex = index;
		}			
		if (encDict.size() != dict_capacity) {
			while (encDict.size() > threshold) {
				threshold <<= 1;
				++bitsize;
			}
		}

		index_of_phrase_F = index_of_phrase_G; //basically F = G
skip_insert:;	
	}

	writer->eof();

	return EXCOM_ERR_OK;
}


int CompLZAP::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}


#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}
