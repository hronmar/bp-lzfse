#ifndef __METHOD_LZAP_HPP__
#define __METHOD_LZAP_HPP__

/*
 * File:       method/lzap/lzap.hpp
 * Purpose:    Declaration of the LZAP compression methods
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of LZAP compression module linked to the ExCom library.
 *
 * LZAP compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZAP compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZAP module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lzap.hpp
 *  \brief Declaration of the LZAP compression method
 */

#include <excom.h>
#include "../../compmodule.hpp"
#include <vector>

class CompLZAP : 
	public CompModule 
{
private:
	IOReader *reader;
	IOWriter *writer;

	int runCompress();
	int runDecompress();

	unsigned dict_size;
	unsigned dict_capacity;
	unsigned root_size;	
	unsigned char char_bitsize;

	int (CompLZAP::* readSymbol)(unsigned long *);
	int (CompLZAP::* writeSymbol)(unsigned long);

public:
	static const unsigned DEFAULT_DICT_SIZE = 4096;
	static const unsigned char DEFAULT_CHAR_BITSIZE = 8;

	CompLZAP(unsigned int handle);

	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();

private:
	typedef struct {
		unsigned prefixIndex; 
		unsigned first;
		unsigned left;
		unsigned right;	

		unsigned symbol;
	} lzw_node_enc_t;

	typedef std::vector <lzw_node_enc_t > lzwEncDictionary;
	lzwEncDictionary encDict;

	unsigned char bitsize;
	unsigned threshold;

	std::vector<unsigned> decodedString;

	int readNBits(unsigned long *value);
	int readByte(unsigned long *value);
	int writeNBits(unsigned long value);
	int writeByte(unsigned long value);
	
	void initializeDictionary();

	bool getCode(unsigned);

	unsigned add(lzw_node_enc_t&);
	unsigned find(lzw_node_enc_t&) const;

	unsigned searchNode(lzw_node_enc_t&) const;
	unsigned searchInsertNode(lzw_node_enc_t&);
};

inline int CompLZAP::checkConnection() {
	if (reader == NULL || writer == NULL) 
		return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

inline int CompLZAP::readNBits(unsigned long *value) {
	return reader->readNBits(char_bitsize, value);		
}
inline int CompLZAP::readByte(unsigned long *value) {
	int result = reader->readByte();
	if (result != EOF)
		*value = result;
	return result;
}
inline int CompLZAP::writeNBits(unsigned long value) {
	return writer->writeNBits(char_bitsize, value);
}
inline int CompLZAP::writeByte(unsigned long value) {
	return writer->writeByte((unsigned char)value);
}

#endif //ifndef __METHOD_LZAP_HPP__
