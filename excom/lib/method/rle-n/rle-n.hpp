#ifndef _COMP_RLE_N_H_
#define _COMP_RLE_N_H_

/*
 * File:       method/rle-n/rle-n.hpp
 * Purpose:    Declaration of the RLE-N compression method
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of RLE-N module linked to the ExCom library.
 *
 * RLE-N module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RLE-N module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RLE-N module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file rle-n.hpp
 *  \brief Declaration of the RLE-N compression method
 */

#include <excom.h>
#include "../../compmodule.hpp"

class CompRLE_N : 
	public CompModule 
{
private:
	IOReader *_reader;
	IOWriter *_writer;

protected:
	unsigned _threshold;

public:
	static const unsigned char DEFAULT_THRESHOLD = 4;
	
	CompRLE_N(unsigned int handle);

	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int checkConnection();
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int run();

	int Compress();
	int Decompress();

};

inline int CompRLE_N::checkConnection() {
	if (_reader == NULL || _writer == NULL) 
		return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

inline int CompRLE_N::setOperation(enum exc_oper_e operation) {
	if (operation == EXCOM_OP_COMPRESS || operation == EXCOM_OP_DECOMPRESS) {
		CompModule::operation = operation;
		return EXCOM_ERR_OK;
	} 
	return EXCOM_ERR_OPERATION;
}

#endif