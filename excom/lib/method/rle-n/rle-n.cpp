/*
 * File:       method/rle-n/rle-n.cpp
 * Purpose:    Implementation of the RLE-N compression method
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of RLE-N module linked to the ExCom library.
 *
 * RLE-N module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RLE-N module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RLE-N module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file rle-n.cpp
 *  \brief Implementation of the RLE-N compression method
 */

#include "rle-n.hpp"

CompRLE_N::CompRLE_N(unsigned int handle) : 
	CompModule(handle), _reader(NULL), _writer(NULL) 
{
	_threshold = DEFAULT_THRESHOLD;
}

int CompRLE_N::connectIOModule(IOModule *module, int connection_type) {
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (_reader != NULL) 
			return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) 
			return res;
		_reader = (IOReader *)ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (_writer != NULL) 
			return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) 
			return res;
		_writer = (IOWriter *)ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default: 
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompRLE_N::setParameter(int parameter, void *value) {
	if (value == NULL) 
		return EXCOM_ERR_MEMORY;

	if (parameter == EXCOM_PARAM_RLE_N_THRESHOLD) {
		unsigned &val = *(unsigned *)value;
		if (val < 4)
			return EXCOM_ERR_VALUE;
		_threshold = val;
		return EXCOM_ERR_OK;
	}
	return CompModule::setParameter(parameter, value);
}
int CompRLE_N::getValue(int parameter, void *value) {
	if (value == NULL) 
		return EXCOM_ERR_MEMORY;

	if (parameter == EXCOM_VALUE_RLE_N_THRESHOLD) {
		*(unsigned *)value = _threshold;
		return EXCOM_ERR_OK;
	}
	return CompModule::getValue(parameter, value);
}

int CompRLE_N::run() {
	int res = checkConnection();
	if (res != EXCOM_ERR_OK) 
		return res;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS)
		res = Compress();		
	else res = Decompress();

#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}

int CompRLE_N::Compress() {
	int c, prev;
	unsigned counter = 1;

	if ((prev = _reader->readByte()) != EOF) {
		while ((c = _reader->readByte()) != EOF) {
			if (c == prev) {
				if (++counter == _threshold) {
					_writer->writeByte(0xFF); //RLE-N mark
					_writer->writeByte(0x01); //RLE-N flag, any non-zero value will do
					_writer->writeByte((unsigned char)c);
					counter = 0;
				}
			}
			else {
				while (counter-- != 0) {
					_writer->writeByte((unsigned char)prev);
					if ((unsigned char)prev == 0xFF) //RLE-N mark
						_writer->writeByte(0x00); //symbol flag
				}
				prev = c;
				counter = 1;
			}
		}

		while (counter-- != 0) { //output the rest
			_writer->writeByte((unsigned char)prev);
			if ((unsigned char)prev == 0xFF) //RLE-N mark
				_writer->writeByte(0x00); //symbol flag
		}
	}

	_writer->eof();

	return EXCOM_ERR_OK;
}

int CompRLE_N::Decompress() {
	int c;
	unsigned i;

	while ((c = _reader->readByte()) != EOF) {
		if ((unsigned char)c == 0xFF) { //RLE-N mark
			c = _reader->readByte(); //read flag
			if ((unsigned char)c != 0x00) { //RLE-N flag
				c = _reader->readByte(); //read symbol
				for (i = 0; i < _threshold; ++i)
					_writer->writeByte((unsigned char)c);
			}
			else _writer->writeByte(0xFF);
		}
		else _writer->writeByte((unsigned char)c);
	}

	_writer->eof();

	return EXCOM_ERR_OK;
}
