/*
 * File:       method/lzfse/lzfse.cpp
 * Purpose:    Implementation of the LZFSE compression method
 * Author:     Martin Hron <hronmar2@fit.cvut.cz>, 2018
 *             bachelor thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Bachelor thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2018 Martin Hron
 *
 * This file is part of LZFSE coding module linked to the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZFSE module or with the ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "lzfse.hpp"

#include <cstdlib>
#include <vector>

#include "lzfse/lzfse.h"

int CompLZFSE::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZFSE::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompLZFSE::checkConnection()
{
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompLZFSE::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	res = performOp();

#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}

int CompLZFSE::performOp()
{
	//input buffer
	int src_allocated = SRC_BUFFER_INITIAL_SIZE;
	int src_size = 0;
	uint8_t * src_buffer = (uint8_t *) malloc(src_allocated);

	if (src_buffer == 0) {
		//allocation failed
		return EXCOM_ERR_MEMORY;
	}

	//read whole input into the source buffer
	while (true) {
		int available = src_allocated - src_size;
		int n = reader->readBlock(src_buffer + src_size, available);
		src_size += n;

		if (n < available)
			break;	//reached end of file

		//realloc
		if (src_allocated < SRC_BUFFER_REALLOC_THRESHOLD)
			src_allocated *= 2;	//buffer is lower than threshold -> double
		else
			src_allocated += SRC_BUFFER_REALLOC_THRESHOLD;	//increase linearly

		src_buffer = (uint8_t *) realloc(src_buffer, src_allocated);
		if (src_buffer == 0) {
			//reallocation failed
			return EXCOM_ERR_MEMORY;
		}
	}

	//allocate output buffer
	/* size of output is not known in advance, so if the allocated output buffer size 
	   is insufficient, the operation has to be run again with larger allocated buffer */
	size_t out_allocated = (operation == EXCOM_OP_COMPRESS) ? src_size
															: (4 * src_size);
	size_t out_size = 0;
	uint8_t * out_buffer = (uint8_t *) malloc(out_allocated);

	if (out_buffer == 0) {
		//allocation failed
		return EXCOM_ERR_MEMORY;
	}

	//allocate scratch buffer for encode/decode routine to use as a workspace
	size_t scratch_allocated = (operation == EXCOM_OP_COMPRESS) ? lzfse_encode_scratch_size(&m_Parameters) 
																: lzfse_decode_scratch_size();
	void * scratch = (scratch_allocated ? malloc(scratch_allocated) : 0);

	if (scratch == 0) {
		//allocation failed
		return EXCOM_ERR_MEMORY;
	}

	//now perform encode/decode operation
	while (true) {
		if (operation == EXCOM_OP_COMPRESS)
			out_size = lzfse_encode_buffer(out_buffer, out_allocated, src_buffer, src_size, scratch, &m_Parameters);
		else
			out_size = lzfse_decode_buffer(out_buffer, out_allocated, src_buffer, src_size, scratch);

		//if the output buffer was too small, double its size and run the operation again
		if (out_size == 0 || (operation == EXCOM_OP_DECOMPRESS && out_size == out_allocated)) {
			out_allocated *= 2;

			free(out_buffer);
			out_buffer = (uint8_t *) malloc(out_allocated);

			if (out_buffer == 0) {
				//allocation failed
				return EXCOM_ERR_MEMORY;
			}

			continue;
		}

		break;
	}

	//write contents of output buffer to output
	writer->writeBlock(out_buffer, out_size);

	writer->eof();

	//free memory
	if (scratch_allocated)
		free(scratch);

	free(src_buffer);
	free(out_buffer);

	return EXCOM_ERR_OK;
}

int CompLZFSE::setParameter(int parameter, void *value)
{
	unsigned int u;

	switch (parameter) {
	case EXCOM_PARAM_LZFSE_ENCODE_GOOD_MATCH:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);

		if (u < 4)
			return EXCOM_ERR_VALUE;
		m_Parameters.good_match = u;

		break;
	case EXCOM_PARAM_LZFSE_ENCODE_HASH_BITS:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);

		if (u < 10 || u > 16)
			return EXCOM_ERR_VALUE;
		m_Parameters.hash_bits = u;

		break;
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZFSE::getValue(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_LZFSE_ENCODE_GOOD_MATCH:
		u = m_Parameters.good_match;
		break;
	case EXCOM_VALUE_LZFSE_ENCODE_HASH_BITS:
		u = m_Parameters.hash_bits;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}

	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}
