#ifndef SORTEDCONTENT_H_
#define SORTEDCONTENT_H_

/*
 * File:       method/acb/SortedContent.hpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "BPlusTreeNode.hpp"

#include <string.h>
#include <stdint.h>

#define SC_ENABLED 0

#define SC_DEBUG 0
#if SC_DEBUG
    #include <stdlib.h>
    #include <iostream>
#endif

/*!
 * \brief Class for storing sorted contents with common context
 */
class SortedContents {
public:
	SortedContents(unsigned int maxSize);
	~SortedContents();

        /*!
         * \brief Current size of contents
         */
	unsigned int currentSize;

        /*!
         * \brief Maximum size of contents
         */
	unsigned int maxSize;

        /*!
         * \brief Array of sorted contents
         */
	long* contentPositions;

        /*!
         * \brief Search best matching content
         * \param data - Data in which is the content starting at \c position
         * \param position - Position at which content starts
         * \return Position in \c contentPositions at which best matching content is
         */
        unsigned int search(uint16_t* data, long position);

        /*!
         * \brief Inesrt new content starting at \c position into position \c slot
         * \param slot - Position in \c contentPositions to add content to
         * \param position - Starting position of content to add
         * \return true if it was successfull, false otherwise
         */
	bool insert(unsigned int slot, long position);

        /*!
         * \brief
         * \param data - Data in which is the content starting at \c position
         * \param position -  Starting position of content to add
         * \param contentLength - Common length of best matching content and new content
         * \param commonLength - Common length of two best matching contents surrounding new content
         * \param isLower - 1 if best content is lower than new content, 0 otherwise
         * \return index of best content
         */
        unsigned int findBestContentAndAdd(uint16_t* data, long position, unsigned int &contentLength, unsigned int &commonLength, unsigned int &isLower);

        /*!
         * \brief returns if this array of contents is full
         * \return true if this array of contents is full
         */
        bool isFull();
#if SC_DEBUG

        /*!
         * \brief Prints contents in this array
         * \param data - Data for which this array is for
         */
	void print(uint16_t* data);

        /*!
         * \brief Prints content at \c position
         * \param data - Data for which this array is for
         * \param position - Position at which content starts
         */
        void print(uint16_t* data, long position);
#endif
};

/*!
 * \brief Class representing node in LZW like tree
 *
 * The code is based on an idea of Juha Nieminen
 * http://warp.povusers.org/EfficientLZW/
 */
class SCInfoNode {
public:
	
	SCInfoNode();
	~SCInfoNode();

        /*!
         * \brief The index to the first string using this string as prefix.
         *        Initialized to the empty string index.
         */
	signed int first;

        /*!
         * \brief The index to the next string using the same prefix index as
         *        this one, and which has a smaller byte value than B.
         *        Initialized to the empty string index.
         */
	signed int left;

        /*!
         * \brief The index to the next string using the same prefix index as
         *        this one, and which has a larger byte value than B.
         *        Initialized to the empty string index.
         */
	signed int right;

        /*!
         * \brief Symbol value
         */
	unsigned char symbol;

        /*!
         * \brief Pointer to sorted contents for this context.
         *        Strings/contexts shorter than specified length have this field
         *        NULL.
         */
	SortedContents* contents;
};

/*!
 * \brief Class representing tree of contexts of specified size and encapsulating
 *        methods to search and update this tree.
 *
 * The code is based on an idea of Juha Nieminen
 * http://warp.povusers.org/EfficientLZW/ 
 */
class SortedContentsTree {
private:
        /*!
         * \brief LZW like tree holding mapping of common context and sorted
         *        contents of these contexts.
         */
	SCInfoNode** tree;
        
        /*!
         * \brief Current size of the \c tree.
         */
	int currentSize;
        
        /*!
         * \brief Maximum size of \c tree
         */
	unsigned int maxSize;
        
        /*!
         * \brief Specifies maximum size of sorted contents for one common context.
         */
        unsigned int maxDistance;

        /*!
         * \brief Specifies maximum content length.
         */
	unsigned int maxContentLength;
public: 
	SortedContentsTree(unsigned int maxSize);
	~SortedContentsTree();

        /*!
         * \brief Finds in \c tree common context to context starting at \c position
         *        in \c data.
         * \param data - Data for which this tree is created for
         * \param position - Position in \c data at which new context starts at
         * \return \c SortedContents for common context
         *
         * If this context is first of its kind, new empty \c SortedContents is returned.
         */
	SortedContents* getContents(uint16_t* data, long position);

        /*!
         * \brief Clears all contexts.
         */
	void clear();

        /*!
         * \brief Sets maximum content length.
         * \param maxContentLength - Maximum length of content
         */
	void setMaxContentLength(unsigned int maxContentLength);

        /*!
         * \brief Sets maximum size of sorted contents for one common context.
         * \param maxDistance - Maximum size of sorted contents for one common context
         */
        void setMaxDistance(unsigned int maxDistance);
};
#endif /* SORTEDCONTENT_H_ */
