#ifndef SUFFIXARRAY_H_
#define SUFFIXARRAY_H_

/*
 * File:       method/acb/SuffixArray.hpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "SuffixStructure.hpp"
#include <vector>
#define SA_DEBUG 0
#if SA_DEBUG
#include <iostream>
#endif

#define DEFAULT_INDEX_CAPACITY 1 << 10

typedef std::vector<long> dictionary_by_symbol_t;
typedef std::vector<dictionary_by_symbol_t* > dictionary_t;
typedef std::vector<dictionary_by_symbol_t* >::iterator dictionary_iterator_t;

class SuffixArray : public SuffixStructure {
private:
	/*
	* dynamic indexes with lcp- remove is not required so this implementation is
	* sufficient as it has :
	* 	- insert time O(1) or O(n) if its not inserted on last position.
	* 	- query time O(1) for direct access or O(n) for iteration O(log n) for binary search
	* 	- remove is O(n) but it is not a problem since index will only grow
	*/
	dictionary_t *index;

        long searchContext(dictionary_by_symbol_t* &data, long position);
#if SA_DEBUG
	void printIndex(long position);
#endif
public:

	SuffixArray();
	virtual ~SuffixArray();

	void search(long position, int &distance, unsigned int &contentLength);
	void translate(long position, int distance, unsigned int contentLength, unsigned char nextCharacter);
	void updateIndex(long position, long length);

        unsigned char* clear(long position, bool needData);
};

#endif /* SUFFIXARRAY_H_ */
