#ifndef BPLUSTREENODE_H_
#define BPLUSTREENODE_H_

/*
 * File:       method/acb/BPlusTreeNode.hpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#ifndef B_TREE_NODE_KEYS_BITS
#define B_TREE_NODE_KEYS_BITS 7
#endif

#ifndef ACB_MAX_CONTEXT_LENGTH
#define ACB_MAX_CONTEXT_LENGTH 10
#endif

#define _SIZE_OF_LONG BPlusTreeNode::_size_of_long
#define _SIZE_OF_POINTER BPlusTreeNode::_size_of_pointer
#define B_TREE_NODE_KEYS BPlusTreeNode::_b_tree_node_keys
#define B_TREE_NODE_KEYS_SIZE BPlusTreeNode::_b_tree_node_keys_size
#define B_TREE_NODE_POINTERS BPlusTreeNode::_b_tree_node_pointers
#define B_TREE_SPLIT_SIZE BPlusTreeNode::_b_tree_split_size
#define B_TREE_SPLIT_SIZE_POINTERS BPlusTreeNode::_b_tree_split_size_pointers

/*!
 * \brief Copy keys from source to destination.
 * \param dest - Keys to be copied to
 * \param src - Keys to be copied from
 * \param count - Count of keys to be copied
 */
#define MOVE_KEYS(dest, src, count) memmove((dest), (src), (count) * _SIZE_OF_LONG)
/*!
 * \brief Copy children from source to destination.
 * \param dest - Children to be copied to
 * \param src - Children to be copied from
 * \param count - Count of children to be copied
 */
#define MOVE_CHILDREN(dest, src, count) memmove((dest), (src), (count) * _SIZE_OF_POINTER)
/*!
 * \brief Copy keys from source to destination.
 * \param dest - Keys to be copied to
 * \param src - Keys to be copied from
 * \param count - Count of keys to be copied
 *
 * Caller must guarantee, that source and destination do not overlap so memcpy
 * can be used instead of memmove. Perfomance increase is not that significant but it is there.
 */
#define COPY_KEYS(dest, src, count) memcpy((dest), (src), (count) * _SIZE_OF_LONG)
/*!
 * \brief Copy children from source to destination.
 * \param dest - Children to be copied to
 * \param src - Children to be copied from
 * \param count - Count of children to be copied
 *
 * Caller must guarantee, that source and destination do not overlap so memcpy
 * can be used instead of memmove. Perfomance increase is not that significant but it is there.
 */
#define COPY_CHILDREN(dest, src, count) memcpy((dest), (src), (count) * _SIZE_OF_POINTER)

/*!
 * \brief Ancestor class representing node in a B+ Tree
 */
class BPlusTreeNode {
protected:

	unsigned int currentSize;
	long* keys;

        /*!
         * \brief Adds key to its position.
         * \param slot - Position in \c keys to add to
         * \param position - Position in source data
         *
         * This method should be implemented as inline so it can be reused without
         * method call performance issues.
         */
	void addKeyInline(unsigned int slot, long position);
public:
	static unsigned int _size_of_long;
	static unsigned int _size_of_pointer;
        
	static unsigned int _b_tree_node_keys;
	static unsigned int _b_tree_node_keys_size;
	static unsigned int _b_tree_node_pointers;
	static unsigned int _b_tree_split_size;
	static unsigned int _b_tree_split_size_pointers;

	BPlusTreeNode();
	virtual ~BPlusTreeNode();

        /*!
         * \brief Is this node a leaf node
         * \return true if this node is a leaf node
         */
	virtual bool isLeaf();
        /*!
         * \brief Returns if this node is full
         * \return true if node is full
         */
	bool isFull();

        /*!
         * \brief Increase node size by 1
         */
	void incSize();
        /*!
         * \brief Returns current size of node
         * \return current size of node
         */
	unsigned int getSize();
        /*!
         * \brief Sets size of this node
         * \param size - node size
         */
	void setSize(unsigned int size);

        /*!
         * \brief Returns keys stored in this node
         * \return array of keys pointing to source data
         */
	long* getKeys();

        /*!
         * \brief Adds key to its position.
         * \param slot - Position in \c keys to add to
         * \param position - Position in source data
         */
	void addKey(unsigned int slot, long position);

        /*!
         * \brief Returns first key in this node
         * \return first key of this node
         */
	long getFirstKey();

        /*!
         * \brief Finds best match of suffixes stored in \c keys using \c data and
         *        current \c position in \c data
         * \param data - Data to search in
         * \param position - Current position in \c data
         * \return
         */
	unsigned int search(uint16_t* data, long position);

        /*!
         * \brief Splits keys between this node and \c newNode from position \c slot
         *        and adds \c position either to this node or \c newNode depending
         *        on \c slot value.
         * \param slot - Position in \c keys to add to
         * \param position - Position in source data
         * \param newNode - New node to move \c keys to
         *
         * If slot is greater than \c B_TREE_SPLIT_SIZE than
         * \c position is added to \c newNode otherwise \c position
         * is added to this node.
         */
	void splitKeys(unsigned int slot, long position, BPlusTreeNode* newNode);
};

/*!
 * \brief Class representing inner node in a B+ Tree
 */
class BPlusTreeInnerNode : public BPlusTreeNode {
protected:
        /*!
         * \brief Array of children of this node
         */
	BPlusTreeNode** children;

public:
	BPlusTreeInnerNode();
	virtual ~BPlusTreeInnerNode();

        /*!
         * \brief Returns array of children
         * \return array of children
         */
	BPlusTreeNode** getChildren();
};

/*!
 * \brief Class representing leaf node in a B+ Tree
 */
class BPlusTreeLeafNode : public BPlusTreeNode {
protected:
        /*!
         * \brief Left sibling for this leaf node
         *
         * May be NULL if this node is left most node
         */
	BPlusTreeLeafNode* leftSibling;
        /*!
         * \brief Right sibling for this leaf node
         * 
         * May be NULL if this node is right most node
         */
	BPlusTreeLeafNode* rightSibling;

public:
	BPlusTreeLeafNode();
	virtual ~BPlusTreeLeafNode();

        /*!
         * \brief Is this node a leaf node
         * \return true if this node is a leaf node
         */
	bool isLeaf();

        /*!
         * \brief Returns left sibling of this node
         * \return left sibling of this node
         *
         * May be NULL if this node is left most node
         */
	BPlusTreeLeafNode* getLeftSibling();
        /*!
         * \brief Returns right sibling of this node
         * \return right sibling of this node
         *
         * May be NULL if this node is right most node
         */
	BPlusTreeLeafNode* getRightSibling();

        /*!
         * \brief Set right sibling to this node
         * \param node - Node which will become new right sibling
         *
         * This method alse takes care of setting left and right sibling of \c node.
         * Method for setting left sibling of this node is not required as
         * B+ Tree raises its size only to right.
         */
	void setRightSibling(BPlusTreeLeafNode* node);
};
#endif /* BPLUSTREENODE_H_ */
