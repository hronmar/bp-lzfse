/*
 * File:       method/acb/BPlusTree.cpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "BPlusTree.hpp"
#include <stdint.h>

BPlusTree::BPlusTree(){
	this->root = new BPlusTreeLeafNode();

	depth = 0;
	lastSearchPosition = -2;
	lastSearchSlot = 0;
	lastSearchNode = NULL;

	this->path = new BPlusTreeInnerNode*[256];
	this->slots = new unsigned int[256];

};

BPlusTree::~BPlusTree(){
	delete root;
	root = NULL;

	delete [] path;
	path = NULL;
	
	delete [] slots;
	slots = NULL;
};

void BPlusTree::init(long position) {
    insertKey((BPlusTreeLeafNode*) this->root, 0, position);
}

BPlusTreeLeafNode* BPlusTree::search(uint16_t* data, long position, unsigned int &contextIndex, uint16_t &contextLength){
	return searchInline(data, position, contextIndex, contextLength);
}

void BPlusTree::insert(uint16_t* data, long position, SortedContentsTree* contentsTree) {
	if(lastSearchPosition != position) {
		uint16_t contextLength = 0;
                unsigned int contextIndex = 0;
#if SC_ENABLED == 1
                // ContextIndex is position of best matching context
                BPlusTreeNode* toNode = searchInline(data, position, contextIndex, contextLength);
		if(contextLength >= ACB_MAX_CONTEXT_LENGTH) {
			SortedContents* contents = contentsTree->getContents(data, position);
			if(contents->currentSize == 0) {                                                                
				long context = toNode->getKeys()[contextIndex];
				contents->insert(0, context + 1);
			}
                        if(!contents->isFull()) {                            
                            unsigned int contentSlot = contents->search(data, position + 1);
                            // Add new content to its position in sorted array
                            contents->insert(contentSlot, position + 1);
                        }

		}
#else
                // ContextIndex is position of best matching context
                searchInline(data, position, contextIndex, contextLength);
#endif
	}

	// Insert new Key
        // lastSearchSlot contains position to which new context should be added
        // middleKey is position which is left most childs first element
        long middleKey = 0;
	BPlusTreeNode* newNode = insertKey((BPlusTreeLeafNode*) lastSearchNode, lastSearchSlot, position);

	// If neccessary traverse up and update inner nodes
	if(newNode != NULL) {

                middleKey = newNode->getFirstKey();
		while(depth > 0) {
			depth--;
			BPlusTreeInnerNode* leftNode = path[depth];
			unsigned int slot = slots[depth];
			if(leftNode->isFull()) {
				BPlusTreeInnerNode* rightNode = new BPlusTreeInnerNode();
				unsigned int newMiddleKey = middleKey;
				long* leftKeys = leftNode->getKeys();
				long* rightKeys = rightNode->getKeys();
				BPlusTreeNode** leftChildren = leftNode->getChildren();
				BPlusTreeNode** rightChildren = rightNode->getChildren();
				if(slot < B_TREE_SPLIT_SIZE) {
					// Last key in left node will become new middle key
					newMiddleKey = leftKeys[B_TREE_SPLIT_SIZE - 1];
					// Move half keys to right node
					COPY_KEYS(rightKeys, leftKeys + B_TREE_SPLIT_SIZE, B_TREE_SPLIT_SIZE);
					// Add new key to its place
					MOVE_KEYS(leftKeys + slot + 1, leftKeys + slot, B_TREE_SPLIT_SIZE - slot);
					leftKeys[slot] = middleKey;

					// Move pointers to children
					COPY_CHILDREN(
						rightChildren,
						leftChildren + B_TREE_SPLIT_SIZE,
						B_TREE_SPLIT_SIZE + 1);
					unsigned int leftSlot = slot + 1;
					if(leftSlot != B_TREE_SPLIT_SIZE) {
						MOVE_CHILDREN(
							leftChildren + leftSlot + 1,
							leftChildren + leftSlot,
							B_TREE_SPLIT_SIZE - leftSlot);
					}
					leftChildren[leftSlot] = newNode;
				} else {
					if(slot > B_TREE_SPLIT_SIZE) {
						// First key in right node will become new middle key
						newMiddleKey = leftKeys[B_TREE_SPLIT_SIZE];
						// Move half - 1 keys to right node
						COPY_KEYS(rightKeys, leftKeys + B_TREE_SPLIT_SIZE + 1, B_TREE_SPLIT_SIZE - 1);
						// Add new key to its place
						unsigned int rightSlot = slot - (B_TREE_SPLIT_SIZE + 1);
						MOVE_KEYS(rightKeys + rightSlot + 1, rightKeys + rightSlot, B_TREE_NODE_KEYS - slot);
						rightKeys[rightSlot] = middleKey;
					} else {
						// Middle key is not changed 
						// Move half keys to right node
						COPY_KEYS(rightKeys, leftKeys + B_TREE_SPLIT_SIZE, B_TREE_SPLIT_SIZE);
					}

					// Move pointers to children
					COPY_CHILDREN(
						rightChildren,
						leftChildren + B_TREE_SPLIT_SIZE + 1,
						B_TREE_SPLIT_SIZE);
					unsigned int rightSlot = slot - B_TREE_SPLIT_SIZE;
					if(rightSlot != B_TREE_SPLIT_SIZE) {
						MOVE_CHILDREN(
							rightChildren + rightSlot + 1,
							rightChildren + rightSlot,
							B_TREE_NODE_KEYS - slot);
					}
					rightChildren[rightSlot] = newNode;
				}
	
				leftNode->setSize(B_TREE_SPLIT_SIZE);
				rightNode->setSize(B_TREE_SPLIT_SIZE);
	
				middleKey = newMiddleKey;				
				newNode = rightNode;
			} else {
                                // Parent node is not full so just add
				unsigned int childSlot = slot + 1;
				unsigned int count = leftNode->getSize() - slot;
				long* keys = leftNode->getKeys();
				BPlusTreeNode** children = leftNode->getChildren();
				MOVE_KEYS(keys + slot + 1, keys + slot, count);
				MOVE_CHILDREN(children + childSlot + 1, children + childSlot, count);
				keys[slot] = middleKey;
				children[childSlot] = newNode;
				leftNode->incSize();
				newNode = NULL;
				return;
			}
		}
	}

	// I have to create new ROOT
	if(newNode != NULL) {
		BPlusTreeInnerNode* newRoot = new BPlusTreeInnerNode();
		newRoot->getChildren()[0] = this->root;
		newRoot->getChildren()[1] = newNode;
		newRoot->getKeys()[0] = middleKey;
                newRoot->setSize(1);
		this->root = newRoot;
	}
}

BPlusTreeLeafNode* BPlusTree::getMostLeftLeafNode() {
	BPlusTreeNode* node = this->root;
	while(!node->isLeaf()) {
		BPlusTreeInnerNode* innerNode = (BPlusTreeInnerNode*) node;
		node = innerNode->getChildren()[0];
	}
	return (BPlusTreeLeafNode*) node;
}

//============================================================
//======================== private ===========================
//============================================================
inline BPlusTreeLeafNode* BPlusTree::searchInline(uint16_t* data, long position, unsigned int &contextIndex, uint16_t &contextLength){
	BPlusTreeNode* node = root;
	depth = 0;
	while (!node->isLeaf()) {
		BPlusTreeInnerNode* innerNode = (BPlusTreeInnerNode*) node;		
		unsigned int slot = innerNode->search(data, position);
		path[depth] = innerNode;
		slots[depth] = slot;		

		node = innerNode->getChildren()[slot];
		depth++;
        }
	lastSearchSlot = node->search(data, position);
        lastSearchPosition = position;
	lastSearchNode = node;
        
        contextIndex = lastSearchSlot;

        // Compute context length
        if(contextIndex == node->getSize() && node->getSize()) {
            contextIndex--;
        }

        contextLength = 0;
        uint16_t* current = data + position;
        uint16_t* context = data + node->getKeys()[contextIndex];
        while (!(*current - *context)
                && contextLength < ACB_MAX_CONTEXT_LENGTH
                ){
                contextLength++;
                context--, current--;
        }

        if(contextIndex > 0) {
            unsigned int contextLengthLower = 0;
            current = data + position;
            context = data + node->getKeys()[contextIndex - 1];
            while (!(*current - *context)
                    && contextLengthLower < ACB_MAX_CONTEXT_LENGTH
                    ){
                    contextLengthLower++;
                    context--, current--;
            }
            if(contextLengthLower > contextLength) {
                contextIndex--;
                contextLength = contextLengthLower;
            }
        }
        return (BPlusTreeLeafNode*) node;
}

inline BPlusTreeLeafNode* BPlusTree::insertKey(BPlusTreeLeafNode* leaf, unsigned int slot, long position) {
	BPlusTreeLeafNode* node = NULL;
	if(leaf->isFull()) {
		node = new BPlusTreeLeafNode();
		leaf->splitKeys(slot, position, node);
                node->setRightSibling(leaf->getRightSibling());
                leaf->setRightSibling(node);
        } else {
		leaf->addKey(slot, position);
	}
	return node;
}
