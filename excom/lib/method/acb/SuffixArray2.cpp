/*
 * File:       method/acb/SuffixArray2.cpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "SuffixArray2.hpp"

SuffixArray2::SuffixArray2(){
        this->contentsTree = NULL;
	this->tree = new BPlusTree();
	// Add empty context
        this->tree->init(-1);
	this->currentSize = 0;
};

SuffixArray2::~SuffixArray2(){
	delete this->tree;
	this->tree = NULL;

#if SC_ENABLED == 1
	delete this->contentsTree;
	this->contentsTree = NULL;
#endif
};

inline BPlusTreeLeafNode* SuffixArray2::searchContext(long position, unsigned int &contextIndex) {
#if SUFFIX_STRUCTURE_PERF
	searchContextCount++;
	clock_t method_begin = clock ();
	uint16_t contextLength = 0;
	BPlusTreeLeafNode* node = tree->search(data, position, contextIndex, contextLength);
	searchContextTime += (clock() - method_begin);
	return node;
#else
        uint16_t contextLength = 0;
	return tree->search(data, position, contextIndex, contextLength);
#endif
}

inline void SuffixArray2::searchInline(long position, int& distance, unsigned int& contentLength, BPlusTreeLeafNode* contextNode, unsigned int contextIndex) {
    	// To search only in content and not last char of context
	position++;

        uint16_t* currentContentPosition = data + position;

	distance = 0;
	contentLength = 0;

	// search for content lower than context
	unsigned int currentDistance = 0;
	BPlusTreeLeafNode* contentNode = contextNode;
	long* keys = contentNode->getKeys();
	int contentIndex = contextIndex;
	while (currentDistance <= maxDistance) {
		if (contentIndex < 0) {
			contentNode = contentNode->getLeftSibling();
			if(contentNode != NULL) {
				keys = contentNode->getKeys();
				contentIndex = contentNode->getSize() - 1;
			} else {
				break;
			}
		}
		long length = 0;                
		uint16_t* content = data + keys[contentIndex] + 1;
		uint16_t* current = currentContentPosition;

		// Check how many characters are the same
		while (*content++ == *current++
			&& (length < maxContentLength) // max length is defined, because storing data using DHuff has maximum number upper bound
			) {
			length++;
		}
		// If there are more characters identical, change best found
		if (length > contentLength) {
			contentLength = length;
			distance = currentDistance;
		}
		contentIndex--;
		currentDistance++;
	}

	distance = -distance;

	// search for content greater than context
	currentDistance = 1;
	contentNode = contextNode;
	keys = contentNode->getKeys();
	unsigned int contentSize = contentNode->getSize();
	contentIndex = contextIndex + 1;
	while (currentDistance <= maxDistance) {
		if (contentIndex == contentSize) {
			contentNode = contentNode->getRightSibling();
			if(contentNode != NULL) {
				contentSize = contentNode->getSize();
				keys = contentNode->getKeys();
				contentIndex = 0;
			} else {
				break;
			}
		}
		long length = 0;                
		uint16_t* content = data + keys[contentIndex] + 1;
		uint16_t* current = currentContentPosition;

		// Check how many characters are the same
		while (*content++ == *current++
			&& (length < maxContentLength) // max length is defined, because storing data using DHuff has maximum number upper bound
			) {
			length++;
		}
		// If there are more characters identical, change best found
		if (length > contentLength) {
			contentLength = length;
			distance = currentDistance;
		}
		contentIndex++;
		currentDistance++;
	}
}

void SuffixArray2::search(long position, int &distance, unsigned int &contentLength) {
	unsigned int contextIndex = 0;
	BPlusTreeLeafNode* contextNode = searchContext(position, contextIndex);
#if SUFFIX_STRUCTURE_PERF
	searchContentCount++;
	clock_t method_begin = clock ();
#endif        

        searchInline(position, distance, contentLength, contextNode, contextIndex);

#if SUFFIX_STRUCTURE_PERF
	searchContentTime += (clock() - method_begin);
#endif
}

void SuffixArray2::translate(long position, int distance, unsigned int contentLength, unsigned char nextCharacter) {
#if SUFFIX_STRUCTURE_PERF
	translateCount++;
	clock_t method_begin = clock();
#endif

	// If content length is 0, there is no need to search in context, nor content
	// since I am only adding one new character
	if(contentLength > 0) {
		unsigned int contextIndex = 0;
    		BPlusTreeLeafNode* contextNode = searchContext(position, contextIndex);

		int contentIndex = contextIndex + distance;
		unsigned int nodeSize = contextNode->getSize();

		// Search left from context
		while(contentIndex < 0) {
			contextNode = contextNode->getLeftSibling();
			nodeSize = contextNode->getSize();
			contentIndex += nodeSize;
		}
		// Search right from context
		while(contentIndex >= nodeSize) {
			contextNode = contextNode->getRightSibling();
			contentIndex -= nodeSize;
			nodeSize = contextNode->getSize();
		}
		long contentPosition = contextNode->getKeys()[contentIndex];
		addData(contentPosition + 1 , contentLength);
	}
	addData(nextCharacter);
	updateIndex(position, contentLength + 1);

#if SUFFIX_STRUCTURE_PERF
	translateTime += (clock() - method_begin);
#endif
}

void SuffixArray2::updateIndex(long position, long length) {
#if SUFFIX_STRUCTURE_PERF
	updateIndexCount++;
	clock_t method_begin = clock ();
#endif
        // is full check
        if(IS_FULL) return;
	this->currentSize += length;
        long to = position + length;
	for(long i = position; i < to; i++) {
		this->tree->insert(data, i, this->contentsTree);
	}
//      printNode(tree->getMostLeftLeafNode());
#if SUFFIX_STRUCTURE_PERF
	updateIndexTime += (clock() - method_begin);
#endif
}

unsigned char* SuffixArray2::clear(long position, bool needData) {
#if SC_ENABLED == 1
        this->contentsTree->clear();
#endif
	delete this->tree;
	this->tree = new BPlusTree();
	this->tree->init(-1);

        return SuffixStructure::clear(position, needData);
}

// ===============================================================
// ================== Sorted contents methods ====================
// ===============================================================
#if SC_ENABLED
unsigned int SuffixArray2::search(long position, int &distance, unsigned int &contentLength, int &zzzLength) {
	unsigned int contextIndex = 0;
	uint16_t contextLength = 0;
	BPlusTreeLeafNode* contextNode = tree->search(data, position, contextIndex, contextLength);
#if SA_DEBUG
/*        if(contextLength >= ACB_MAX_CONTEXT_LENGTH) {
            std::cout << " ContextIndex : " << contextIndex << std::endl;
            std::cout << " ContextLength : " << contextLength << std::endl;
            printPosition(position);
            printNode(contextNode);
        }*/
#endif
#if SUFFIX_STRUCTURE_PERF
	searchContentCount++;
	clock_t method_begin = clock ();
#endif
	if(contextLength >= ACB_MAX_CONTEXT_LENGTH) {
		SortedContents* contents = contentsTree->getContents(data, position);
		if(contents->currentSize == 0) {
			long context = contextNode->getKeys()[contextIndex];
			contents->insert(0, context + 1);
		}
		unsigned int commonLength = 0;
		unsigned int isLower = 0;
		distance = contents->findBestContentAndAdd(data, position + 1, contentLength, commonLength, isLower);

                // if encoded length is zero, try encoding using
                // the normal way
                if(contentLength > 0) {
                    zzzLength = contentLength - commonLength;
                    // if zzzLength would be greater than maxContentLength
                    // i have to shorten it
                    // using this and not making contentLength = maxContentLength
                    // i can encode more characters at once
                    if(zzzLength > maxContentLength) {
                        contentLength = contentLength - (zzzLength - maxContentLength);
                        zzzLength = maxContentLength;
                    }
                    if(!isLower) {
                            zzzLength = -zzzLength;
                    }
                    //isBitWise = true;
                    return 1;
                }
	}

        searchInline(position, distance, contentLength, contextNode, contextIndex);

#if SUFFIX_STRUCTURE_PERF
	searchContentTime += (clock() - method_begin);
#endif
	// isBitWise = false;
	return 0;
}

void SuffixArray2::setMaxContentLength(unsigned int maxContentLength) {            
        SuffixStructure::setMaxContentLength(maxContentLength);
    	this->contentsTree->setMaxContentLength(maxContentLength);
}

void SuffixArray2::setMaxDistance(unsigned int maxDistance) {
    SuffixStructure::setMaxDistance(maxDistance);
    this->contentsTree->setMaxDistance((maxDistance << 1) + 1);
}

void SuffixArray2::setSize(unsigned int sizeInBits, unsigned int checkSizeBits) {
        SuffixStructure::setSize(sizeInBits, checkSizeBits);
	this->contentsTree = new SortedContentsTree(maxSize);
}
#endif

// ===============================================================
// ====================== Debug methods ==========================
// ===============================================================
#if SA_DEBUG
void SuffixArray2::printPosition(long position) {
    	std::cout << "Position : " << position << std::endl;
        std::cout << " " << position << "     : ";

        uint16_t* currentData = data + position - ACB_MAX_CONTEXT_LENGTH + 1;
        for(int j = 0; j < ACB_MAX_CONTEXT_LENGTH; j++) {
                std::cout << (int) *currentData++ << ",";
        }
        std::cout << "|";
        currentData = data + position + 1;
        for(int j = 0; j < 20; j++) {
                std::cout << (int) *currentData++ << ",";
        }
        std::cout << std::endl;
}

void SuffixArray2::printNode(BPlusTreeLeafNode* node) {
        std::cout << "Node : (" << node->getSize() << ")" << std::endl;
        unsigned int count = 0;
        for (int i = 0; i < node->getSize(); i++) {
                long currentPosition = node->getKeys()[i];
                std::cout  << " " << count++ << " : " << currentPosition << " : " ;
                uint16_t* currentData = data + currentPosition - ACB_MAX_CONTEXT_LENGTH + 1;
                for(int j = 0; j < ACB_MAX_CONTEXT_LENGTH; j++) {
                        printf("%c,", (int) *currentData++);
                }
                std::cout << "|";
                currentData = data + currentPosition + 1;
                for(int j = 0; j < 20; j++) {
                        printf("%c,", (int) *currentData++);
                }
                std::cout << std::endl;
        }
        std::cout << std::endl;
}
#endif