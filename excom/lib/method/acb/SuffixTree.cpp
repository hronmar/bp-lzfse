/*
 * File:       method/acb/SuffixTree.cpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

using namespace std;

#include "SuffixTree.hpp"

SuffixTree::SuffixTree(){
	this->root = new SuffixTreeNode();
	this->root->leaf = 0;
};

SuffixTree::~SuffixTree(){
	delete this->root;
	this->root = NULL;
};

SuffixTreeNode* SuffixTree::searchContext(long position) {
#if SUFFIX_STRUCTURE_PERF
	searchContextCount++;
	clock_t method_begin = clock ();
#endif

	unsigned char character = data[position];
	children_iterator_t it = root->children->upper_bound(character);
	it--;
	SuffixTreeNode* node = it->second;

	long nodePosition = node->nodeStartPosition;
	long currentPosition = position;
	while(nodePosition >= node->nodeEndPosition) {
		if(data[nodePosition] != data[currentPosition]) {
			// No more same characters - node is best Context
			break;
		}
		nodePosition--;
		currentPosition--;
		if(nodePosition < node->nodeEndPosition) {
			if(!node->isLeaf()) {
				it = node->children->upper_bound(data[currentPosition]);
				if(it != node->children->begin()) {
					it--;
				}
				node = it->second;
				nodePosition = node->nodeStartPosition;
			}
		}
	}
	// If current node has more children
	while(!node->isLeaf()) {
		node = node->children->rbegin()->second;
	}
#if SUFFIX_STRUCTURE_PERF
	searchContextTime += (clock() - method_begin);
#endif
	return node;
}

void SuffixTree::search(long position, int &distance, unsigned int &contentLength) {

	SuffixTreeNode* contextNode = searchContext(position);
	SuffixTreeNode* node = contextNode;

#if SUFFIX_STRUCTURE_PERF
	searchContentCount++;
	clock_t method_begin = clock ();
#endif
	
	// To search only in content and not last char of context
	position++;
	distance = 0;
	contentLength = 0;
	uint16_t* currentDataPosition = data + position;

	// search for content lower than context
	unsigned int currentDistance = 0;
	while(currentDistance <= maxDistance && node != NULL) {
		long length = 0;
		long contentStart = node->position + 1;

		uint16_t* content = data + contentStart;
		uint16_t* current = currentDataPosition;

		// Check how many characters are the same
		while ((*content++ == *current++)
			&& (length < maxContentLength) // max length is defined, because storing data using DHuff has maximum number upper bound
			) {
			length++;
		}
		// If there are more characters identical, change best found
		if (length > contentLength) {
			contentLength = length;
			distance = currentDistance;
		}
		currentDistance++;
		node = node->leftSibling;
	}

        distance = -distance;
	// search for content greater than context
	currentDistance = 1;
	node = contextNode;
	node = node->rightSibling;
	while(currentDistance <= maxDistance && node != NULL) {
		long length = 0;
		long contentStart = node->position + 1;

		uint16_t* content = data + contentStart;
		uint16_t* current = currentDataPosition;

		// Check how many characters are the same
		while ((*content++ == *current++)
			&& (length < maxContentLength) // max length is defined, because storing data using DHuff has maximum number upper bound
			) {
			length++;
		}
		// If there are more characters identical, change best found
		if (length > contentLength) {
			contentLength = length;
			distance = currentDistance;
		}
		currentDistance++;
		node = node->rightSibling;
	}

#if SUFFIX_STRUCTURE_PERF
	searchContentTime += (clock() - method_begin);
#endif
}

void SuffixTree::translate(long position, int distance, unsigned int contentLength, unsigned char nextCharacter) {
#if SUFFIX_STRUCTURE_PERF
	translateCount++;
	clock_t method_begin = clock();
#endif
	// If content length is 0, there is no need to search in context, nor content
	// since I am only adding one new character
	if(contentLength > 0) {

		SuffixTreeNode* node = searchContext(position);

		while(distance > 0) {
			node = node->rightSibling;
			distance--;
		}
		while(distance < 0) {
			node = node->leftSibling;
			distance++;
		}
			
		long contentPosition = node->position;
		addData(contentPosition + 1 , contentLength);
	}
	addData(nextCharacter);
	updateIndex(position, contentLength + 1);

#if SUFFIX_STRUCTURE_PERF
	translateTime += (clock() - method_begin);
#endif
}

void SuffixTree::updateIndex(long position, long length) {
#if SUFFIX_STRUCTURE_PERF
	updateIndexCount++;
	clock_t method_begin = clock ();
#endif
        if(IS_FULL) return;
        currentSize += length;
	for(long i = position; i < position + length; i++) {
		root->addNode(data, i, i);
//              printTree();
	}

#if SUFFIX_STRUCTURE_PERF
	updateIndexTime += (clock() - method_begin);
#endif
}

unsigned char* SuffixTree::clear(long position, bool needData) {
        delete this->root;
        this->root = new SuffixTreeNode();
        this->root->leaf = false;
        this->currentSize = 0;        
        return SuffixStructure::clear(position, needData);
}

#if ST_DEBUG
void SuffixTree::printTree() {
	cout << endl;
	cout << "Suffix tree : " << endl;
	printTree(root->children, 1, false);
}

void SuffixTree::printTree(children_map_t* nodes, unsigned int depth, bool showSiblings) {
	if(nodes == NULL) {
		return;	
	}
	for(children_iterator_t it = nodes->begin(); it != nodes->end(); it++) {           
		SuffixTreeNode* node = it->second;
                int mult = 3;
		for(int i = 0; i < mult * depth; i++) {
                    if (i % mult == 0 && i > 0) {
                        cout << '|';
                    } else if (i > (depth * mult) - mult){
                        cout << '_';
                    } else {
                        cout << " ";
                    }
			
		}
		cout << "\'";
		for(int i = node->nodeStartPosition; i >= node->nodeEndPosition; i--){
			cout << (char)data[i];
		}
		cout << "\'";
		if(node->isLeaf() && showSiblings) {
			cout << ", l: ";
			SuffixTreeNode* tmp = node->leftSibling;
			if(tmp != NULL ) {
				cout << "\'";
				for(int i = tmp->position; i >= tmp->nodeEndPosition; i--){
					cout << (char)data[i];
				}
				cout << "\'";
			}		
			cout << ", r: ";
			tmp = node->rightSibling;
			if(tmp != NULL ) {
				cout << "\'";
				for(int i = tmp->position; i >= tmp->nodeEndPosition; i--){
					cout << (char)data[i];
				}
				cout << "\'";
			}
		}
		cout << endl;
		printTree(node->children, depth + 1, showSiblings);
	}
}
#endif