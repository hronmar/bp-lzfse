/*
 * File:       method/acb/SortedContent.cpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "SortedContent.hpp"

// ===============================================================
// ======================= SortedContent =========================
// ===============================================================
SortedContents::SortedContents(unsigned int maxSize) {
	this->maxSize = maxSize;
	this->currentSize = 0;
	contentPositions = new long[maxSize];
}

SortedContents::~SortedContents() {
	delete [] contentPositions;
        contentPositions = NULL;
}

unsigned int SortedContents::findBestContentAndAdd(uint16_t* data, long position, unsigned int &contentLength, unsigned int &commonLength, unsigned int &isLower) {
        unsigned int slotToAdd = search(data, position);

	contentLength = 0;
	commonLength = 0;
        isLower = 0;
	
        // Compute content length
        unsigned int contextIndex = slotToAdd;
        bool edgeContent = false;
        if(contextIndex == currentSize) {
            edgeContent = true;
            contextIndex--;
            isLower = 1;
        }

        uint16_t* bestContent = data + contentPositions[contextIndex];
        uint16_t* currentContent = data + position;
        while(!(*bestContent - *currentContent)) {
                contentLength++;
                bestContent++;
                currentContent++;
        }

        if(edgeContent) {
#if SC_DEBUG
            std::cout <<"ContextIndex   : " << contextIndex << std::endl;
            std::cout <<"NextClosest    : " << -1 << std::endl;
            std::cout <<"isLower        : " << isLower << std::endl;
            std::cout <<"ContentLength  : " << contentLength << std::endl;
            std::cout <<"CommonLength   : " << commonLength << std::endl;
            print(data, position);
            print(data);
#endif
            insert(slotToAdd, position);
            return contextIndex;
        }

        // Compute common length
        unsigned int nextClosest = contextIndex;
        if (nextClosest > 0) {
                nextClosest--;
        } else {
#if SC_DEBUG
                std::cout <<"ContextIndex   : " << contextIndex << std::endl;
                std::cout <<"NextClosest    : " << -1 << std::endl;
                std::cout <<"isLower        : " << isLower << std::endl;
                std::cout <<"ContentLength  : " << contentLength << std::endl;
                std::cout <<"CommonLength   : " << commonLength << std::endl;
                print(data, position);
                print(data);
#endif
                // not enough data to compute common length;
                insert(slotToAdd, position);
                return contextIndex;
        }

        bestContent = data + contentPositions[nextClosest];
        currentContent = data + position;
        while(!(*bestContent - *currentContent)) {
                commonLength++;
                bestContent++;
                currentContent++;
        }

        if(commonLength > contentLength) {
            unsigned int _length = contentLength;
            contentLength = commonLength;
            commonLength = _length;
            int _contextIndex = contextIndex;
            contextIndex = nextClosest;
            nextClosest = _contextIndex;
            isLower = 1;
        }

#if SC_DEBUG
            std::cout <<"ContextIndex   : " << contextIndex << std::endl;
            std::cout <<"NextClosest    : " << nextClosest << std::endl;
            std::cout <<"isLower        : " << isLower << std::endl;
            std::cout <<"ContentLength  : " << contentLength << std::endl;
            std::cout <<"CommonLength   : " << commonLength << std::endl;
            print(data, position);
            print(data);
#endif
        insert(slotToAdd, position);
        return contextIndex;
}

unsigned int SortedContents::search(uint16_t* data, long position) {
	unsigned int begin = 0;
	unsigned int end = currentSize;
        while(begin < end) {
		unsigned int middle = (begin + end) >> 1;
                uint16_t* content = data + contentPositions[middle];
                uint16_t* current = data + position;
		int16_t ret = 0;
                while (!(ret = *current - *content)){
                        content++, current++;
                }
                if(ret <= 0) {			
			end = middle;
		} else {			
			begin = middle + 1;
		}
	}
	return end;
}

bool SortedContents::insert(unsigned int slot, long position) {
	if(isFull()) {
            // TODO co delat kdyz se zaplni
            return false;
	}	
	memmove(contentPositions + slot + 1, contentPositions + slot, (currentSize - slot) * BPlusTreeNode::_size_of_long);
	contentPositions[slot] = position;
	currentSize++;	
	return true;
}

bool SortedContents::isFull() {
    return currentSize == maxSize;
}

#if SC_DEBUG
void SortedContents::print(uint16_t* data) {
	std::cout << "Instance size :  " << currentSize << std::endl;
	for(int i = 0 ; i < currentSize; i++) {
		long position = contentPositions[i];		
		std::cout << " " << position << " " << i << " : ";
		
		uint16_t* currentData = data + position - ACB_MAX_CONTEXT_LENGTH;
		
		for(int j = 0; j < ACB_MAX_CONTEXT_LENGTH; j++) {
			std::cout << (int) *currentData++ << ",";
		}
		std::cout << "|";
		currentData = data + position;
		for(int j = 0; j < 20; j++) {
			std::cout << (int) *currentData++ << ",";
		}
		std::cout << std::endl;
	}
}

void SortedContents::print(uint16_t* data, long position) {
    	std::cout << "Position : " << position << std::endl;

        std::cout << " " << position << " : ";

        uint16_t* currentData = data + position - ACB_MAX_CONTEXT_LENGTH;

        for(int j = 0; j < ACB_MAX_CONTEXT_LENGTH; j++) {
                std::cout << (int) *currentData++ << ",";
        }
        std::cout << "|";
        currentData = data + position;
        for(int j = 0; j < 20; j++) {
                std::cout << (int) *currentData++ << ",";
        }
        std::cout << std::endl;
}
#endif

// ===============================================================
// ========================= SCInfoNode ==========================
// ===============================================================
SCInfoNode::SCInfoNode() {
	contents = NULL;
	first = -1;
	left = -1;
	right = -1;
}

SCInfoNode::~SCInfoNode() {
	if(contents != NULL) {
		delete contents;
		contents = NULL;
	}
}

// ===============================================================
// ==================== SortedContentsTree =======================
// ===============================================================
SortedContentsTree::SortedContentsTree(unsigned int maxSize) {
	this->maxSize = maxSize << 1;
	// should be set using setMaxContentLength, setMaxDistance
	this->maxContentLength = 255;
        this->maxDistance = 127;

	tree = new SCInfoNode* [this->maxSize];
	currentSize = 256;
	for(int i = 0; i < 256; i++) {
		SCInfoNode* node = new SCInfoNode();
		node->symbol = (unsigned char) i;
		tree[i] = node;
	}
}

SortedContentsTree::~SortedContentsTree() {
	for(int i = 0; i < currentSize; i++) {
		delete tree[i];
		tree[i] = NULL;
	}
	delete [] tree;
	tree = NULL;
}

void SortedContentsTree::clear() {
	for(int i = 256; i < currentSize; i++) {
		delete tree[i];
		tree[i] = NULL;
	}
	currentSize = 256;
	for(int i = 0; i < currentSize; i++) {
		SCInfoNode* node = tree[i];
		node->first = -1;
		node->left = -1;
		node->right = -1;
	}
}

// The code is based on an idea of Juha Nieminen
// http://warp.povusers.org/EfficientLZW/ 
SortedContents* SortedContentsTree::getContents(uint16_t* data, long position) {
	uint16_t* currentData = data + position;
	unsigned char currentChar = (unsigned char) *currentData;
	signed int index = tree[currentChar]->first;
	int contextLength = 1;
	// First character of suffix has already successor
	if (index != -1) {
		// Go to next char of suffix
		currentData--;
		currentChar = (unsigned char) *currentData;                
		// Traverse the tree to find suffix
		while (contextLength < ACB_MAX_CONTEXT_LENGTH) {
			if (tree[index]->symbol == currentChar) {
				currentData--;
				currentChar = (unsigned char) *currentData;
                                contextLength++;
				// There are always contexts with the same size
				// so first successor for searched suffix always exists, 
				// but it may not be the desired character
                                if(contextLength < ACB_MAX_CONTEXT_LENGTH) {
                                    index = tree[index]->first;
                                }
				
			} else if (tree[index]->symbol < currentChar) {
				if (tree[index]->right == -1) {
					// not found
					tree[index]->right = currentSize;
					break; 
				}      
				index = tree[index]->right;
			} else {
				if (tree[index]->left == -1) {
					// not found
					tree[index]->left = currentSize;
					break; 
				}        
				index = tree[index]->left;
			}
		}

		// Did not find the desired suffix but only its part.
		// I must add one new char to current found suffix part
		// because it is in left or right pointer of the current suffix part
		// and not the first pointer.
		if(contextLength < ACB_MAX_CONTEXT_LENGTH) {
			index = currentSize++;
			
			SCInfoNode* node = new SCInfoNode();
			node->symbol = currentChar;
			tree[index] = node;

			currentData--;
			currentChar = (unsigned char) *currentData;
			contextLength++;

			if(contextLength == ACB_MAX_CONTEXT_LENGTH) {
				// It is last character of context so i will
				// create new instance of sorted contents.
				SortedContents* contents = new SortedContents(maxDistance);
				tree[index]->contents = contents;
			}
		}
	} else {
		// It is first successor.
		index = (signed int) currentChar;
	}

	// There is only part of the suffix in the dictionary
	// so i will insert the rest of it.
	if(contextLength < ACB_MAX_CONTEXT_LENGTH) {
		while(contextLength < ACB_MAX_CONTEXT_LENGTH) {	
			tree[index]->first = currentSize;
			index = currentSize++;

			currentData--;
			currentChar = (unsigned char) *currentData;

			SCInfoNode* node = new SCInfoNode();
			node->symbol = currentChar;
			tree[index]  = node;

			contextLength++;
		}
		
		// It is last character of context so i will
		// create new instance of sorted contents.
		SortedContents* contents = new SortedContents(maxDistance);
		tree[index]->contents = contents;
	}

	return tree[index]->contents;
}

void SortedContentsTree::setMaxContentLength(unsigned int maxContentLength) {
	this->maxContentLength = maxContentLength;
}

void SortedContentsTree::setMaxDistance(unsigned int maxDistance) {
    this->maxDistance = maxDistance;
}
