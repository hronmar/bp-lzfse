#ifndef __METHOD_LZMW_HPP__
#define __METHOD_LZMW_HPP__

/*
 * File:       method/lzmw/lzmw.hpp
 * Purpose:    Declaration of the LZW compression methods
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of LZMW compression module linked to the ExCom library.
 *
 * LZMW compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZMW compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZMW module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lzmw.hpp
 *  \brief Declaration of the LZMW compression method
 */

#include <excom.h>
#include "../../compmodule.hpp"
#include <vector>

class CompLZMW : 
	public CompModule 
{
private:
	IOReader *reader;
	IOWriter *writer;

	int runCompress();
	int runDecompress();

	unsigned dict_size;
	unsigned dict_capacity;
	unsigned root_size;	
	unsigned char char_bitsize;

	int (CompLZMW::* readSymbol)(unsigned long *);
	int (CompLZMW::* writeSymbol)(unsigned long);

public:
	static const unsigned DEFAULT_DICT_SIZE = 4096;
	static const unsigned char DEFAULT_CHAR_BITSIZE = 8;

	CompLZMW(unsigned int handle);
	~CompLZMW();

	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();

private:
	static const unsigned INVALID_ID = ~0u;	

	typedef struct {
		unsigned prefixIndex; 
		unsigned first;
		unsigned left;
		unsigned right;
			
		unsigned ID;		

		unsigned symbol;
	} lzw_node_enc_t;

	typedef std::vector <lzw_node_enc_t > lzwEncDictionary;
	lzwEncDictionary encDict;
	unsigned *indices;

	unsigned char bitsize;
	unsigned threshold;

	std::vector<unsigned> decodedString;

	int readNBits(unsigned long *value);
	int readByte(unsigned long *value);
	int writeNBits(unsigned long value);
	int writeByte(unsigned long value);

	/*! \brief Initialize the LZW dictionary
	 *  \param op Type of operation - encode or decode
	 *  
	 *  All input symbols are added
	 */
	void initializeDictionary();

	bool getCode(unsigned);

	unsigned add(lzw_node_enc_t&);
	unsigned find(lzw_node_enc_t&) const;

	unsigned searchNode(lzw_node_enc_t&) const;
	unsigned searchInsertNode(lzw_node_enc_t&);
};

inline CompLZMW::~CompLZMW() {
	delete[] indices;
}

inline int CompLZMW::checkConnection() {
	if (reader == NULL || writer == NULL) 
		return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

inline int CompLZMW::readNBits(unsigned long *value) {
	return reader->readNBits(char_bitsize, value);		
}
inline int CompLZMW::readByte(unsigned long *value) {
	int result = reader->readByte();
	if (result != EOF)
		*value = result;
	return result;
}
inline int CompLZMW::writeNBits(unsigned long value) {
	return writer->writeNBits(char_bitsize, value);
}
inline int CompLZMW::writeByte(unsigned long value) {
	return writer->writeByte((unsigned char)value);
}

#endif //ifndef __METHOD_LZW_HPP__
