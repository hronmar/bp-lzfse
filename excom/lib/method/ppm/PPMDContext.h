#ifndef __PPMD_CONTEXT_H__
#define __PPMD_CONTEXT_H__

/*
 * File:       method/ppm/PPMDContext.h
 * Purpose:    Implementation of PPM's contextual compression model
 *
 * Based on PPMD implementation from p7zip, which is based
 * on Dmitry Shkarin's PPMdH
 *
 * Copyright (C) 1999-2002 Dmitry Shkarin
 * Copyright (C) 1999-2007 Igor Pavlov
 * Copyright (C) 2009 Filip Simek <simekf1@fel.cvut.cz>
 *
 * This file is part of PPM module linked to the ExCom library.
 *
 * PPM module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PPM module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PPM module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "PPMDSubAlloc.h"

namespace ppmd {

const int INT_BITS = 7, PERIOD_BITS = 7, TOT_BITS = INT_BITS + PERIOD_BITS,
	INTERVAL = 1 << INT_BITS, BIN_SCALE = 1 << TOT_BITS, MAX_FREQ = 124;
const int MAX_O = 255; /* maximum allowed model order */

template <class T>
inline void _PPMD_SWAP(T& t1, T& t2)
{
	T tmp = t1;
	t1 = t2;
	t2 = tmp;
}

struct SEE2_CONTEXT {
	// SEE-contexts for PPM-contexts with masked symbols
	uint16_t Summ;
	uint8_t Shift, Count;
	void init(int InitVal) {
		Summ = (uint16_t)(InitVal << (Shift = PERIOD_BITS - 4));
		Count = 4;
	}
	unsigned int getMean() {
		unsigned int RetVal = (Summ >> Shift);
		Summ = (uint16_t)(Summ - RetVal);
		return RetVal + (RetVal == 0);
	}
	void update() {
		if (Shift < PERIOD_BITS && --Count == 0) {
			Summ <<= 1;
			Count = (uint8_t)(3 << Shift++);
		}
	}
};

struct PPM_CONTEXT {
	uint16_t NumStats; // sizeof(uint16_t) > sizeof(uint8_t)
	uint16_t SummFreq;

	struct STATE {
		uint8_t Symbol, Freq;
		uint16_t SuccessorLow;
		uint16_t SuccessorHigh;

		uint32_t GetSuccessor() const {
			return SuccessorLow | ((uint32_t)SuccessorHigh << 16);
		}
		void SetSuccessor(uint32_t v) {
			SuccessorLow = (uint16_t)(v & 0xFFFF);
			SuccessorHigh = (uint16_t)((v >> 16) & 0xFFFF);
		}
	};

	uint32_t Stats;
	uint32_t Suffix;

	PPM_CONTEXT* createChild(CSubAllocator &subAllocator, STATE* pStats, STATE& FirstState) {
		PPM_CONTEXT* pc = (PPM_CONTEXT*) subAllocator.AllocContext();
		if (pc) {
			pc->NumStats = 1;
			pc->oneState() = FirstState;
			pc->Suffix = subAllocator.GetOffset(this);
			pStats->SetSuccessor(subAllocator.GetOffsetNoCheck(pc));
		}
		return pc;
	}

	STATE& oneState() const {
		return (STATE&) SummFreq;
	}
};

/////////////////////////////////

const uint16_t InitBinEsc[] = {0x3CDD, 0x1F3F, 0x59BF, 0x48F3, 0x64A1, 0x5ABC, 0x6632, 0x6051};

struct CInfo {
	CSubAllocator SubAllocator;
	SEE2_CONTEXT SEE2Cont[25][16], DummySEE2Cont;
	PPM_CONTEXT * MinContext, * MaxContext;

	PPM_CONTEXT::STATE* FoundState;      // found next state transition
	int NumMasked, InitEsc, OrderFall, RunLength, InitRL, MaxOrder;
	uint8_t CharMask[256], NS2Indx[256], NS2BSIndx[256], HB2Flag[256];
	uint8_t EscCount, PrintCount, PrevSuccess, HiBitsFlag;
	uint16_t BinSumm[128][64];               // binary SEE-contexts

	uint16_t &GetBinSumm(const PPM_CONTEXT::STATE &rs, int numStates) {
		HiBitsFlag = HB2Flag[FoundState->Symbol];
		return BinSumm[rs.Freq - 1][
		           PrevSuccess + NS2BSIndx[numStates - 1] +
		           HiBitsFlag + 2 * HB2Flag[rs.Symbol] +
		           ((RunLength >> 26) & 0x20)];
	}

	PPM_CONTEXT *GetContext(uint32_t offset) const {
		return (PPM_CONTEXT *)SubAllocator.GetPtr(offset);
	}
	PPM_CONTEXT *GetContextNoCheck(uint32_t offset) const {
		return (PPM_CONTEXT *)SubAllocator.GetPtrNoCheck(offset);
	}
	PPM_CONTEXT::STATE *GetState(uint32_t offset) const {
		return (PPM_CONTEXT::STATE *)SubAllocator.GetPtr(offset);
	}
	PPM_CONTEXT::STATE *GetStateNoCheck(uint32_t offset) const {
		return (PPM_CONTEXT::STATE *)SubAllocator.GetPtr(offset);
	}

	void RestartModelRare() {
		int i, k, m;
		memset(CharMask, 0, sizeof(CharMask));
		SubAllocator.InitSubAllocator();
		InitRL = -((MaxOrder < 12) ? MaxOrder : 12) - 1;
		MinContext = MaxContext = (PPM_CONTEXT*) SubAllocator.AllocContext();
		MinContext->Suffix = 0;
		OrderFall = MaxOrder;
		MinContext->SummFreq = (uint16_t)((MinContext->NumStats = 256) + 1);
		FoundState = (PPM_CONTEXT::STATE*)SubAllocator.AllocUnits(256 / 2);
		MinContext->Stats = SubAllocator.GetOffsetNoCheck(FoundState);
		PrevSuccess = 0;
		for (RunLength = InitRL, i = 0; i < 256; i++) {
			PPM_CONTEXT::STATE &state = FoundState[i];
			state.Symbol = (uint8_t)i;
			state.Freq = 1;
			state.SetSuccessor(0);
		}
		for (i = 0; i < 128; i++)
			for (k = 0; k < 8; k++)
				for (m = 0; m < 64; m += 8)
					BinSumm[i][k + m] = (uint16_t)(BIN_SCALE - InitBinEsc[k] / (i + 2));
		for (i = 0; i < 25; i++)
			for (k = 0; k < 16; k++)
				SEE2Cont[i][k].init(5*i + 10);
	}

	void StartModelRare(int maxOrder) {
		int i, k, m , Step;
		EscCount = PrintCount = 1;
		if (maxOrder < 2) {
			memset(CharMask, 0, sizeof(CharMask));
			OrderFall = MaxOrder;
			MinContext = MaxContext;
			while (MinContext->Suffix != 0) {
				MinContext = GetContextNoCheck(MinContext->Suffix);
				OrderFall--;
			}
			FoundState = GetState(MinContext->Stats);
			MinContext = MaxContext;
		} else {
			MaxOrder = maxOrder;
			RestartModelRare();
			NS2BSIndx[0] = 2 * 0;
			NS2BSIndx[1] = 2 * 1;
			memset(NS2BSIndx + 2, 2 * 2, 9);
			memset(NS2BSIndx + 11, 2 * 3, 256 - 11);
			for (i = 0; i < 3; i++)
				NS2Indx[i] = (uint8_t)i;
			for (m = i, k = Step = 1; i < 256; i++) {
				NS2Indx[i] = (uint8_t)m;
				if (!--k) {
					k = ++Step;
					m++;
				}
			}
			memset(HB2Flag, 0, 0x40);
			memset(HB2Flag + 0x40, 0x08, 0x100 - 0x40);
			DummySEE2Cont.Shift = PERIOD_BITS;
		}
	}

	PPM_CONTEXT* CreateSuccessors(bool skip, PPM_CONTEXT::STATE* p1) {
		// static UpState declaration bypasses IntelC bug
		// static PPM_CONTEXT::STATE UpState;
		PPM_CONTEXT::STATE UpState;

		PPM_CONTEXT *pc = MinContext;
		PPM_CONTEXT *UpBranch = GetContext(FoundState->GetSuccessor());
		PPM_CONTEXT::STATE * p, * ps[MAX_O], ** pps = ps;
		if (!skip) {
			*pps++ = FoundState;
			if (!pc->Suffix)
				goto NO_LOOP;
		}
		if (p1) {
			p = p1;
			pc = GetContext(pc->Suffix);
			goto LOOP_ENTRY;
		}
		do {
			pc = GetContext(pc->Suffix);
			if (pc->NumStats != 1) {
				if ((p = GetStateNoCheck(pc->Stats))->Symbol != FoundState->Symbol)
					do {
						p++;
					} while (p->Symbol != FoundState->Symbol);
			} else
				p = &(pc->oneState());
LOOP_ENTRY:
			if (GetContext(p->GetSuccessor()) != UpBranch) {
				pc = GetContext(p->GetSuccessor());
				break;
			}
			*pps++ = p;
		} while (pc->Suffix);
NO_LOOP:
		if (pps == ps)
			return pc;
		UpState.Symbol = *(uint8_t*) UpBranch;
		UpState.SetSuccessor(SubAllocator.GetOffset(UpBranch) + 1);
		if (pc->NumStats != 1) {
			if ((p = GetStateNoCheck(pc->Stats))->Symbol != UpState.Symbol)
				do {
					p++;
				} while (p->Symbol != UpState.Symbol);
			unsigned int cf = p->Freq - 1;
			unsigned int s0 = pc->SummFreq - pc->NumStats - cf;
			UpState.Freq = (uint8_t)(1 + ((2 * cf <= s0) ? (5 * cf > s0) :
			                           ((2 * cf + 3 * s0 - 1) / (2 * s0))));
		} else
			UpState.Freq = pc->oneState().Freq;
		do {
			pc = pc->createChild(SubAllocator, *--pps, UpState);
			if (!pc)
				return NULL;
		} while (pps != ps);
		return pc;
	}

	void UpdateModel() {
		PPM_CONTEXT::STATE fs = *FoundState, * p = NULL;
		PPM_CONTEXT* pc, * Successor;
		unsigned int ns1, ns, cf, sf, s0;
		if (fs.Freq < MAX_FREQ / 4 && MinContext->Suffix != 0) {
			pc = GetContextNoCheck(MinContext->Suffix);

			if (pc->NumStats != 1) {
				if ((p = GetStateNoCheck(pc->Stats))->Symbol != fs.Symbol) {
					do {
						p++;
					} while (p->Symbol != fs.Symbol);
					if (p[0].Freq >= p[-1].Freq) {
						_PPMD_SWAP(p[0], p[-1]);
						p--;
					}
				}
				if (p->Freq < MAX_FREQ - 9) {
					p->Freq += 2;
					pc->SummFreq += 2;
				}
			} else {
				p = &(pc->oneState());
				p->Freq = (uint8_t)(p->Freq + ((p->Freq < 32) ? 1 : 0));
			}
		}
		if (!OrderFall) {
			MinContext = MaxContext = CreateSuccessors(true, p);
			FoundState->SetSuccessor(SubAllocator.GetOffset(MinContext));
			if (MinContext == 0)
				goto RESTART_MODEL;
			return;
		}
		*SubAllocator.pText++ = fs.Symbol;
		Successor = (PPM_CONTEXT*) SubAllocator.pText;
		if (SubAllocator.pText >= SubAllocator.UnitsStart)
			goto RESTART_MODEL;
		if (fs.GetSuccessor() != 0) {
			if ((uint8_t *)GetContext(fs.GetSuccessor()) <= SubAllocator.pText) {
				PPM_CONTEXT* cs = CreateSuccessors(false, p);
				fs.SetSuccessor(SubAllocator.GetOffset(cs));
				if (cs == NULL)
					goto RESTART_MODEL;
			}
			if (!--OrderFall) {
				Successor = GetContext(fs.GetSuccessor());
				SubAllocator.pText -= (MaxContext != MinContext);
			}
		} else {
			FoundState->SetSuccessor(SubAllocator.GetOffsetNoCheck(Successor));
			fs.SetSuccessor(SubAllocator.GetOffsetNoCheck(MinContext));
		}
		s0 = MinContext->SummFreq - (ns = MinContext->NumStats) - (fs.Freq - 1);
		for (pc = MaxContext; pc != MinContext; pc = GetContext(pc->Suffix)) {
			if ((ns1 = pc->NumStats) != 1) {
				if ((ns1 & 1) == 0) {
					void *ppp = SubAllocator.ExpandUnits(GetState(pc->Stats), ns1 >> 1);
					pc->Stats = SubAllocator.GetOffset(ppp);
					if (!ppp)
						goto RESTART_MODEL;
				}
				pc->SummFreq = (uint16_t)(pc->SummFreq + (2 * ns1 < ns) + 2 * ((4 * ns1 <= ns) &
				                        (pc->SummFreq <= 8 * ns1)));
			} else {
				p = (PPM_CONTEXT::STATE*) SubAllocator.AllocUnits(1);
				if (!p)
					goto RESTART_MODEL;
				*p = pc->oneState();
				pc->Stats = SubAllocator.GetOffsetNoCheck(p);
				if (p->Freq < MAX_FREQ / 4 - 1)
					p->Freq <<= 1;
				else
					p->Freq  = MAX_FREQ - 4;
				pc->SummFreq = (uint16_t)(p->Freq + InitEsc + (ns > 3));
			}
			cf = 2 * fs.Freq * (pc->SummFreq + 6);
			sf = s0 + pc->SummFreq;
			if (cf < 6 * sf) {
				cf = 1 + (cf > sf) + (cf >= 4 * sf);
				pc->SummFreq += 3;
			} else {
				cf = 4 + (cf >= 9 * sf) + (cf >= 12 * sf) + (cf >= 15 * sf);
				pc->SummFreq = (uint16_t)(pc->SummFreq + cf);
			}
			p = GetState(pc->Stats) + ns1;
			p->SetSuccessor(SubAllocator.GetOffset(Successor));
			p->Symbol = fs.Symbol;
			p->Freq = (uint8_t)cf;
			pc->NumStats = (uint16_t)++ns1;
		}
		MaxContext = MinContext = GetContext(fs.GetSuccessor());
		return;
RESTART_MODEL:
		RestartModelRare();
		EscCount = 0;
		PrintCount = 0xFF;
	}

	void ClearMask() {
		EscCount = 1;
		memset(CharMask, 0, sizeof(CharMask));
		// if (++PrintCount == 0)
		//   PrintInfo(DecodedFile,EncodedFile);
	}

	void update1(PPM_CONTEXT::STATE* p) {
		(FoundState = p)->Freq += 4;
		MinContext->SummFreq += 4;
		if (p[0].Freq > p[-1].Freq) {
			_PPMD_SWAP(p[0], p[-1]);
			FoundState = --p;
			if (p->Freq > MAX_FREQ)
				rescale();
		}
	}


	void update2(PPM_CONTEXT::STATE* p) {
		(FoundState = p)->Freq += 4;
		MinContext->SummFreq += 4;
		if (p->Freq > MAX_FREQ)
			rescale();
		EscCount++;
		RunLength = InitRL;
	}

	SEE2_CONTEXT* makeEscFreq2(int Diff, uint32_t &scale) {
		SEE2_CONTEXT* psee2c;
		if (MinContext->NumStats != 256) {
			psee2c = SEE2Cont[NS2Indx[Diff-1]] +
			         (Diff < (GetContext(MinContext->Suffix))->NumStats - MinContext->NumStats) +
			         2 * (MinContext->SummFreq < 11 * MinContext->NumStats) +
			         4 * (NumMasked > Diff) +
			         HiBitsFlag;
			scale = psee2c->getMean();
		} else {
			psee2c = &DummySEE2Cont;
			scale = 1;
		}
		return psee2c;
	}



	void rescale() {
		int OldNS = MinContext->NumStats, i = MinContext->NumStats - 1, Adder, EscFreq;
		PPM_CONTEXT::STATE* p1, * p;
		PPM_CONTEXT::STATE *stats = GetStateNoCheck(MinContext->Stats);
		for (p = FoundState; p != stats; p--)
			_PPMD_SWAP(p[0], p[-1]);
		stats->Freq += 4;
		MinContext->SummFreq += 4;
		EscFreq = MinContext->SummFreq - p->Freq;
		Adder = (OrderFall != 0);
		p->Freq = (uint8_t)((p->Freq + Adder) >> 1);
		MinContext->SummFreq = p->Freq;
		do {
			EscFreq -= (++p)->Freq;
			p->Freq = (uint8_t)((p->Freq + Adder) >> 1);
			MinContext->SummFreq = (uint16_t)(MinContext->SummFreq + p->Freq);
			if (p[0].Freq > p[-1].Freq) {
				PPM_CONTEXT::STATE tmp = *(p1 = p);
				do {
					p1[0] = p1[-1];
				} while (--p1 != stats && tmp.Freq > p1[-1].Freq);
				*p1 = tmp;
			}
		} while (--i);
		if (p->Freq == 0) {
			do {
				i++;
			} while ((--p)->Freq == 0);
			EscFreq += i;
			MinContext->NumStats = (uint16_t)(MinContext->NumStats - i);
			if (MinContext->NumStats == 1) {
				PPM_CONTEXT::STATE tmp = *stats;
				do {
					tmp.Freq = (uint8_t)(tmp.Freq - (tmp.Freq >> 1));
					EscFreq >>= 1;
				} while (EscFreq > 1);
				SubAllocator.FreeUnits(stats, (OldNS + 1) >> 1);
				*(FoundState = &MinContext->oneState()) = tmp;
				return;
			}
		}
		EscFreq -= (EscFreq >> 1);
		MinContext->SummFreq = (uint16_t)(MinContext->SummFreq + EscFreq);
		int n0 = (OldNS + 1) >> 1, n1 = (MinContext->NumStats + 1) >> 1;
		if (n0 != n1)
			MinContext->Stats = SubAllocator.GetOffset(SubAllocator.ShrinkUnits(stats, n0, n1));
		FoundState = GetState(MinContext->Stats);
	}

	void NextContext() {
		PPM_CONTEXT *c = GetContext(FoundState->GetSuccessor());
		if (!OrderFall && (uint8_t *)c > SubAllocator.pText)
			MinContext = MaxContext = c;
		else {
			UpdateModel();
			if (EscCount == 0)
				ClearMask();
		}
	}
};

// Tabulated escapes for exponential symbol distribution
const uint8_t ExpEscape[16] = { 25, 14, 9, 7, 5, 5, 4, 4, 4, 3, 3, 3, 2, 2, 2, 2 };
#define GET_MEAN(SUMM,SHIFT,ROUND) ((SUMM+(1 << (SHIFT-ROUND))) >> (SHIFT))

} // namespace ppmd

#endif // ifndef __PPMD_CONTEXT_H__
