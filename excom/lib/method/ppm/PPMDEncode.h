#ifndef __PPMD_ENCODE_H__
#define __PPMD_ENCODE_H__

/*
 * File:       method/ppm/PPMDEncode.h
 * Purpose:    Methods for encoding symbols using PPM model
 *
 * Based on PPMD implementation from p7zip, which is based
 * on Dmitry Shkarin's PPMdH
 *
 * Copyright (C) 1999-2002 Dmitry Shkarin
 * Copyright (C) 1999-2007 Igor Pavlov
 * Copyright (C) 2009 Filip Simek <simekf1@fel.cvut.cz>
 *
 * This file is part of PPM module linked to the ExCom library.
 *
 * PPM module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PPM module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PPM module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "PPMDContext.h"
#include "RangeCoder.h"

namespace ppmd {

struct CEncodeInfo: public CInfo {

	void EncodeBinSymbol(int symbol, rangecoder::CEncoder *rangeEncoder) {
		PPM_CONTEXT::STATE& rs = MinContext->oneState();
		uint16_t &bs = GetBinSumm(rs, GetContextNoCheck(MinContext->Suffix)->NumStats);
		if (rs.Symbol == symbol) {
			FoundState = &rs;
			rs.Freq = (uint8_t)(rs.Freq + (rs.Freq < 128 ? 1 : 0));
			rangeEncoder->EncodeBit(bs, TOT_BITS, 0);
			bs = (uint16_t)(bs + INTERVAL - GET_MEAN(bs, PERIOD_BITS, 2));
			PrevSuccess = 1;
			RunLength++;
		} else {
			rangeEncoder->EncodeBit(bs, TOT_BITS, 1);
			bs = (uint16_t)(bs - GET_MEAN(bs, PERIOD_BITS, 2));
			InitEsc = ExpEscape[bs >> 10];
			NumMasked = 1;
			CharMask[rs.Symbol] = EscCount;
			PrevSuccess = 0;
			FoundState = NULL;
		}
	}

	void EncodeSymbol1(int symbol, rangecoder::CEncoder *rangeEncoder) {
		PPM_CONTEXT::STATE* p = GetStateNoCheck(MinContext->Stats);
		if (p->Symbol == symbol) {
			PrevSuccess = (2 * (p->Freq) > MinContext->SummFreq);
			RunLength += PrevSuccess;
			rangeEncoder->Encode(0, p->Freq, MinContext->SummFreq);
			(FoundState = p)->Freq += 4;
			MinContext->SummFreq += 4;
			if (p->Freq > MAX_FREQ)
				rescale();
			return;
		}
		PrevSuccess = 0;
		int LoCnt = p->Freq, i = MinContext->NumStats - 1;
		while ((++p)->Symbol != symbol) {
			LoCnt += p->Freq;
			if (--i == 0) {
				HiBitsFlag = HB2Flag[FoundState->Symbol];
				CharMask[p->Symbol] = EscCount;
				i = (NumMasked = MinContext->NumStats) - 1;
				FoundState = NULL;
				do {
					CharMask[(--p)->Symbol] = EscCount;
				} while (--i);
				rangeEncoder->Encode(LoCnt, MinContext->SummFreq - LoCnt, MinContext->SummFreq);
				return;
			}
		}
		rangeEncoder->Encode(LoCnt, p->Freq, MinContext->SummFreq);
		update1(p);
	}

	void EncodeSymbol2(int symbol, rangecoder::CEncoder *rangeEncoder) {
		int hiCnt, i = MinContext->NumStats - NumMasked;
		uint32_t scale;
		SEE2_CONTEXT* psee2c = makeEscFreq2(i, scale);
		PPM_CONTEXT::STATE* p = GetStateNoCheck(MinContext->Stats) - 1;
		hiCnt = 0;
		do {
			do {
				p++;
			} while (CharMask[p->Symbol] == EscCount);
			hiCnt += p->Freq;
			if (p->Symbol == symbol)
				goto SYMBOL_FOUND;
			CharMask[p->Symbol] = EscCount;
		} while (--i);

		rangeEncoder->Encode(hiCnt, scale, hiCnt + scale);
		scale += hiCnt;

		psee2c->Summ = (uint16_t)(psee2c->Summ + scale);
		NumMasked = MinContext->NumStats;
		return;
SYMBOL_FOUND:

		uint32_t highCount = hiCnt;
		uint32_t lowCount = highCount - p->Freq;
		if (--i) {
			PPM_CONTEXT::STATE* p1 = p;
			do {
				do {
					p1++;
				} while (CharMask[p1->Symbol] == EscCount);
				hiCnt += p1->Freq;
			} while (--i);
		}
		// SubRange.scale += hiCnt;
		scale += hiCnt;
		rangeEncoder->Encode(lowCount, highCount - lowCount, scale);
		psee2c->update();
		update2(p);
	}

	void EncodeSymbol(int c, rangecoder::CEncoder *rangeEncoder) {
		if (MinContext->NumStats != 1)
			EncodeSymbol1(c, rangeEncoder);
		else
			EncodeBinSymbol(c, rangeEncoder);
		while (!FoundState) {
			do {
				OrderFall++;
				MinContext = GetContext(MinContext->Suffix);
				if (MinContext == 0)
					return; //  S_OK;
			} while (MinContext->NumStats == NumMasked);
			EncodeSymbol2(c, rangeEncoder);
		}
		NextContext();
	}
};

} // namespace ppmd


#endif // ifndef __PPMD_ENCODE_H__
