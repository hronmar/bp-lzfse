#ifndef __PPMD_CODER_H__
#define __PPMD_CODER_H__

/*
 * File:       method/ppm/PPMDcoder.h
 * Purpose:    Declaration of PPMD class
 *
 * Based on PPMD implementation from p7zip
 *
 * Copyright (C) 1999-2007 Igor Pavlov
 * Copyright (C) 2009 Filip Simek <simekf1@fel.cvut.cz>
 *
 * This file is part of PPM module linked to the ExCom library.
 *
 * PPM module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PPM module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PPM module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "PPMDEncode.h"
#include "PPMDDecode.h"

class CompPPM;

namespace ppmd {

class PPMDcoder {
	friend class ::CompPPM;
private:
	rangecoder::CEncoder _rangeEncoder;
	CRangeDecoder _rangeDecoder;
	CEncodeInfo _einfo;
	CDecodeInfo _dinfo;

	// common parameters
	uint32_t _usedMemorySize;
	uint8_t _order;
	bool _raw;

	// decoder parameters
	int _remainLen;

	// class for automatic stream flushing upon finish
	class PPMDcoderFlusher {
		IOWriter *outstream;
		rangecoder::CEncoder *coder;
	public:
		PPMDcoderFlusher(IOWriter *_outstream,
			rangecoder::CEncoder *_coder = NULL):
			outstream(_outstream), coder(_coder) {}
		~PPMDcoderFlusher()
		{
			if (coder != NULL)
				coder->FlushData();
			outstream->eof();
		}
	};

private:
	// coder methods
	// decoder methods
	int CodeSpec(uint32_t num, IOWriter *outStream);

public:
	PPMDcoder();

	int compress  (IOReader *inStream, IOWriter *outStream);
	int decompress(IOReader *inStream, IOWriter *outStream);

	//this method seems to be useless
	void WriteCoderProperties (IOWriter *outStream);
};

}

#endif // ifndef __PPMD_CODER_H__
