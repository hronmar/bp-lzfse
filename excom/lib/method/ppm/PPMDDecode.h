#ifndef __PPMD_DECODE_H__
#define __PPMD_DECODE_H__

/*
 * File:       method/ppm/PPMDDecode.h
 * Purpose:    Functions for decoding symbols using PPM model
 *
 * Based on PPMD implementation from p7zip, which is based
 * on Dmitry Shkarin's PPMdH
 *
 * Copyright (C) 1999-2002 Dmitry Shkarin
 * Copyright (C) 1999-2007 Igor Pavlov
 * Copyright (C) 2009 Filip Simek <simekf1@fel.cvut.cz>
 *
 * This file is part of PPM module linked to the ExCom library.
 *
 * PPM module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PPM module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PPM module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "PPMDContext.h"
#include "RangeCoder.h"

namespace ppmd {

class CRangeDecoderVirt
{
public:
	virtual uint32_t GetThreshold(uint32_t total) = 0;
	virtual void Decode(uint32_t start, uint32_t size) = 0;
	virtual uint32_t DecodeBit(uint32_t size0, uint32_t numTotalBits) = 0;
};

class CRangeDecoder: public CRangeDecoderVirt, public rangecoder::CDecoder
{
	uint32_t GetThreshold(uint32_t total) {
		return rangecoder::CDecoder::GetThreshold(total);
	}
	void Decode(uint32_t start, uint32_t size) {
		rangecoder::CDecoder::Decode(start, size);
	}
	uint32_t DecodeBit(uint32_t size0, uint32_t numTotalBits) {
		return rangecoder::CDecoder::DecodeBit(size0, numTotalBits);
	}
};

struct CDecodeInfo: public CInfo {
	void DecodeBinSymbol(CRangeDecoderVirt *rangeDecoder) {
		PPM_CONTEXT::STATE& rs = MinContext->oneState();
		uint16_t& bs = GetBinSumm(rs, GetContextNoCheck(MinContext->Suffix)->NumStats);
		if (rangeDecoder->DecodeBit(bs, TOT_BITS) == 0) {
			FoundState = &rs;
			rs.Freq = (uint8_t)(rs.Freq + (rs.Freq < 128 ? 1 : 0));
			bs = (uint16_t)(bs + INTERVAL - GET_MEAN(bs, PERIOD_BITS, 2));
			PrevSuccess = 1;
			RunLength++;
		} else {
			bs = (uint16_t)(bs - GET_MEAN(bs, PERIOD_BITS, 2));
			InitEsc = ExpEscape[bs >> 10];
			NumMasked = 1;
			CharMask[rs.Symbol] = EscCount;
			PrevSuccess = 0;
			FoundState = NULL;
		}
	}

	void DecodeSymbol1(CRangeDecoderVirt *rangeDecoder) {
		PPM_CONTEXT::STATE* p = GetStateNoCheck(MinContext->Stats);
		int i, count, hiCnt;
		if ((count = rangeDecoder->GetThreshold(MinContext->SummFreq)) < (hiCnt = p->Freq)) {
			PrevSuccess = (2 * hiCnt > MinContext->SummFreq);
			RunLength += PrevSuccess;
			rangeDecoder->Decode(0, p->Freq); // MinContext->SummFreq);
			(FoundState = p)->Freq = (uint8_t)(hiCnt += 4);
			MinContext->SummFreq += 4;
			if (hiCnt > MAX_FREQ)
				rescale();
			return;
		}
		PrevSuccess = 0;
		i = MinContext->NumStats - 1;
		while ((hiCnt += (++p)->Freq) <= count)
			if (--i == 0) {
				HiBitsFlag = HB2Flag[FoundState->Symbol];
				rangeDecoder->Decode(hiCnt, MinContext->SummFreq - hiCnt); // , MinContext->SummFreq);
				CharMask[p->Symbol] = EscCount;
				i = (NumMasked = MinContext->NumStats) - 1;
				FoundState = NULL;
				do {
					CharMask[(--p)->Symbol] = EscCount;
				} while (--i);
				return;
			}
		rangeDecoder->Decode(hiCnt - p->Freq, p->Freq); // , MinContext->SummFreq);
		update1(p);
	}


	void DecodeSymbol2(CRangeDecoderVirt *rangeDecoder) {
		int count, hiCnt, i = MinContext->NumStats - NumMasked;
		uint32_t freqSum;
		SEE2_CONTEXT* psee2c = makeEscFreq2(i, freqSum);
		PPM_CONTEXT::STATE* ps[256], ** pps = ps, * p = GetStateNoCheck(MinContext->Stats) - 1;
		hiCnt = 0;
		do {
			do {
				p++;
			} while (CharMask[p->Symbol] == EscCount);
			hiCnt += p->Freq;
			*pps++ = p;
		} while (--i);

		freqSum += hiCnt;
		count = rangeDecoder->GetThreshold(freqSum);

		p = *(pps = ps);
		if (count < hiCnt) {
			hiCnt = 0;
			while ((hiCnt += p->Freq) <= count)
				p = *++pps;
			rangeDecoder->Decode(hiCnt - p->Freq, p->Freq); // , freqSum);

			psee2c->update();
			update2(p);
		} else {
			rangeDecoder->Decode(hiCnt, freqSum - hiCnt); // , freqSum);

			i = MinContext->NumStats - NumMasked;
			pps--;
			do {
				CharMask[(*++pps)->Symbol] = EscCount;
			} while (--i);
			psee2c->Summ = (uint16_t)(psee2c->Summ + freqSum);
			NumMasked = MinContext->NumStats;
		}
	}

	int DecodeSymbol(CRangeDecoderVirt *rangeDecoder) {
		if (MinContext->NumStats != 1)
			DecodeSymbol1(rangeDecoder);
		else
			DecodeBinSymbol(rangeDecoder);
		while (!FoundState) {
			do {
				OrderFall++;
				MinContext = GetContext(MinContext->Suffix);
				if (MinContext == 0)
					return -1;
			} while (MinContext->NumStats == NumMasked);
			DecodeSymbol2(rangeDecoder);
		}
		uint8_t symbol = FoundState->Symbol;
		NextContext();
		return symbol;
	}
};

} // namespace ppmd

#endif // ifndef __PPMD_DECODE_H__
