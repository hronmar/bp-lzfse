#ifndef __METHOD_SHUFF_HPP__
#define __METHOD_SHUFF_HPP__

/*
 * File:       method/shuff/shuff.hpp
 * Purpose:    Declaration of the Static Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Static Huffman coding module linked to the ExCom library.
 *
 * Static Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Static Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Shannon-Fano module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file shuff.hpp
 *  \brief Declaration of Static Huffman coding method.
 *
 *  This module provides the Static Huffman compression method, which is based on 
 *	frequencies of occurrence of particular symbols. The binary tree is constructed 
 *	and new, shorter codes are assigned for encountered characters.  
 */

#include <excom.h>
#include "../../compmodule.hpp"

class CompSHuff: public CompModule {
private:
	IOReader *reader;
	IOWriter *writer;

	int runCompress();
	int runDecompress();
public:
	CompSHuff(unsigned int handle): CompModule(handle), reader(NULL), writer(NULL) {}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int checkConnection();
	int run();
};

#endif //ifndef __METHOD_SHUFF_HPP__
