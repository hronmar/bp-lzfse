/*
 * File:       method/shuff/shuff.cpp
 * Purpose:    Declaration of Shannon-Fano statistical compression method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Static Huffman coding module linked to the ExCom library.
 *
 * Static Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Static Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Static Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "shuff.hpp"
#include "SHuffNode.h"
#include "SHuffTree.h"
#include "../universal_codes/UniversalCodes.h"

#include <iostream>

using namespace std;

int CompSHuff::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompSHuff::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompSHuff::checkConnection()
{
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompSHuff::runCompress() {
	unsigned long fileLength;

	int i;
	int c;

	string x;

	UniversalCodes* uc = new UniversalCodes();
	SHuffTree* sht = new SHuffTree();

	fileLength = 0;

	// The first pass - get symbol's frequencies
	while (c = reader->readByte(), c != EOF) {
		sht->addSymbol(c);
		fileLength++;
	}

	if (sht->buildTree()) {	
		writer->writeNBits(32, fileLength);

		// Get tree as a string
		x = sht->getTreeAsString();
                
		// Split by 8 bits and output
		for (i = 0; i < (x.size() / 8); i++) {
			writer->writeByte(uc->binToDec(x.substr(i * 8, 8)));
		}

		int tmp = x.size() % 8;
		if (tmp != 0) {
			writer->writeNBits(tmp, uc->binToDec(x.substr(x.size()-tmp)));
		}

		// Rewind a file to a beggining is necessary for the second pass
		reader->rewind();

		// The second pass - encode all symbols now
		while (c = reader->readByte(), c != EOF) {
			unsigned int length = 0;
			unsigned int code = sht->getSymbolCode(c, length);
                        writer->writeNBits(length, code);
                }
	}

	delete sht;
	sht = NULL;

	delete uc;
	uc = NULL;

	writer->eof();

	return EXCOM_ERR_OK;
}

int CompSHuff::runDecompress() {
	unsigned long bits;
	unsigned long bitsLeft;
	unsigned long c;
	unsigned long fileLength;

	int i;
	int j;

	string x;

	UniversalCodes* uc = new UniversalCodes();
	SHuffTree* sht = new SHuffTree();

	// Get file length
	i = reader->readNBits(32, &fileLength);

	if (i != EOF) {
		// Number of leaves in Huffman tree
		i = reader->readNBits(8, &bits);
		// EOFs!!!

		// Because of only 8 bits
		bits++; 

		// Number of bits need to be read to reconstruct the Huffman tree
		if (bits == 1) {
			// Only root and one leaf
			bits = 2 + 8; 
		} else {
			bits = (2 * bits - 1) + bits * 8;
		}

		// Get the tree from string
		for (i = 0; i < (bits / 8); i++) {
			j = reader->readNBits(8, &c);
			x += uc->getBeta(c, 8);
		}

		bitsLeft = bits % 8;
		if (bitsLeft != 0) {
			j = reader->readNBits(bitsLeft, &c);
			x += uc->getBeta(c, bitsLeft);
		}

		sht->reconstructTree(x);

		if (!sht->isErr()) {
			while (fileLength > 0) {
				do {
					i = reader->readNBits(1, &c);
					// EOF
				} while (!sht->isLeaf(c));

				if (sht->isErr()) {
					return EXCOM_ERR_FORMAT;
				}

				writer->writeByte(sht->getActualNodeSymbol());

				// Traverse up to root
				sht->traverseUp();
				fileLength--;	  
			}
		} else {
			return EXCOM_ERR_FORMAT;
		}
	}

	delete sht;
	sht = NULL;

	delete uc;
	uc = NULL;

	writer->eof();

	return EXCOM_ERR_OK;
}

int CompSHuff::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}


#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}
