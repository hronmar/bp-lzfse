#ifndef _LOOK_AHEAD_BUFFER_H
#define	_LOOK_AHEAD_BUFFER_H

/*
 * File:       method/lz_common/LookaheadBuffer.h
 * Purpose:    Declaration of the Lookahead Buffer for LZ compression methods
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of LZ77 and LZSS coding module linked to the ExCom library.
 *
 * LZ77 and LZSS compression methods ares free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZ77 and LZSS compression methods are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZ77 and LZSS module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file LookaheadBuffer.h
 *  \brief Declaration of the Lookahead buffer for LZ compression methods
 */

#include <string>
#include <vector>
#include <deque>

using namespace std;

class LookaheadBuffer {
private:
	unsigned int length;

	vector<unsigned char> lBuffer; 
	vector<unsigned char>::iterator it;
public:
	// Constructors
	LookaheadBuffer(int);

	// Destructor
	~LookaheadBuffer();

	void insert(unsigned char); 
	void remove(unsigned int); 
	void fill(deque<unsigned char>&);

	unsigned char getByte(unsigned int); 

	// Getters
	unsigned int getSize();
};

#endif	/* _LOOK_AHEAD_BUFFER_H */

