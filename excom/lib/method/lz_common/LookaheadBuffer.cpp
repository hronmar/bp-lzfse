/*
 * File:       method/lz_common/LookaheadBuffer.cpp
 * Purpose:    Declaration of the Lookahead Buffer for LZ compression methods
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of LZ77 and LZSS coding module linked to the ExCom library.
 *
 * LZ77 and LZSS compression methods ares free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZ77 and LZSS compression methods are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZ77 and LZSS module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file LookaheadBuffer.cpp
 *  \brief Implementation of the Lookahead buffer for LZ compression methods
 */

#include "LookaheadBuffer.h"

#include <iostream>
#include <vector>

LookaheadBuffer::LookaheadBuffer(int size) {
	lBuffer.reserve(size);
	length = size;
}

LookaheadBuffer::~LookaheadBuffer() {
}

void LookaheadBuffer::insert(unsigned char data) {
	lBuffer.push_back(data);
}

unsigned char LookaheadBuffer::getByte(unsigned int index) {
	return lBuffer.at(index);
}


void LookaheadBuffer::fill(deque<unsigned char>& data) {
	if (data.size() > 0) {
		int count;

		count = (data.size() > (length - lBuffer.size())) ? (length - lBuffer.size()) : data.size();

		for (int i = 0; i < count; ++i) {
			lBuffer.push_back(data.at(i));
		}
		data.erase(data.begin(), data.begin()+count);
	} 
	
	return;
}

unsigned int LookaheadBuffer::getSize() {
	return lBuffer.size();
}

void LookaheadBuffer::remove(unsigned int count) {
	int cnt;

	cnt = (count > lBuffer.size()) ? lBuffer.size() : count;

	lBuffer.erase(lBuffer.begin(), lBuffer.begin()+cnt);
}
