/*
 * File:       method/lz_common/SearchBuffer.cpp
 * Purpose:    Declaration of the Search Buffer for LZ compression methods
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of LZ77 and LZSS coding module linked to the ExCom library.
 *
 * LZ77 and LZSS compression methods ares free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZ77 and LZSS compression methods are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZ77 and LZSS module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file SearchBuffer.cpp
 *  \brief Implementation of the Search buffer for LZ compression methods
 */

#include "SearchBuffer.h"

#include <iostream>
#include <map>
#include <deque>

using namespace std;

// Constructor
SearchBuffer::SearchBuffer(int size) {
	sBuffer = new unsigned char [size];
	for (int i=0; i< size; i++) sBuffer[i] = NULL;
	index = 0;
	length = size;
	count = 0;
}

// Destructor
SearchBuffer::~SearchBuffer() {
	delete [] sBuffer;
	sBuffer = NULL;
}

int SearchBuffer::getLength() {
	return length;
}

int SearchBuffer::getCount() {
	return count;
}

void SearchBuffer::shiftData(LookaheadBuffer* lBuffer, map <unsigned char, deque<unsigned int> >&indices, unsigned int cnt) {
	unsigned char symbol;
	unsigned char indexByte;

	for (unsigned int i = 0; i < cnt; ++i) {
		symbol = lBuffer->getByte(i);

		indexByte = getByte(index);
		if (!indices[indexByte].empty()) {
			indices[indexByte].pop_front();    
		}	
	
		indices[symbol].push_back(index);    

		insert(symbol);
	}

	lBuffer->remove(cnt);
}

void SearchBuffer::insert(unsigned char data) {
	sBuffer[index] = data;

	if (count < length) count++;

	index = (index + 1) % length;
}

// Returns the real indices where the search buffer starts and where ends
buffer_bounds_t SearchBuffer::getBounds() {
	buffer_bounds_t bounds;

	if (count == 0) {
		bounds.begin = bounds.end = 0;
	} else {
		bounds.begin = (index >= 1) ? (index - 1) : (length - 1); 
		bounds.end = (count < length) ? 0 : index;
	}

	return bounds;
}

unsigned char SearchBuffer::getByte(unsigned int index) {
	return sBuffer[index];
}
