#ifndef _COMP_BWT_H_
#define _COMP_BWT_H_

/*
 * File:       method/bwt/bwt.hpp
 * Purpose:    Declaration of the BWT
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of BWT module linked to the ExCom library.
 *
 * BWT module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BWT module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BWT module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file bwt.hpp
 *  \brief Declaration of the BWT
 */

#include <excom.h>
#include "../../compmodule.hpp"

namespace BWTHelper {

template <typename T>
inline void Swap(T &x, T &y) {
	T t = x;
	x = y;
	y = t;
}

template <typename T>
inline void XorSwap(T &x, T &y) {
	x ^= y ^= x ^= y;
}

}

class CompBWT : 
	public CompModule 
{
private:
	IOReader *_reader;
	IOWriter *_writer;

protected:
	unsigned char _block_size;
	unsigned char *_block_data;

public:
	static const unsigned char DEFAULT_BLOCK_SIZE = 10;
	
	CompBWT(unsigned int handle);
	~CompBWT();

	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int checkConnection();
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int run();

	int Encode();
	int Decode();

};

inline CompBWT::~CompBWT() {
	delete[] _block_data;
}

inline int CompBWT::checkConnection() {
	if (_reader == NULL || _writer == NULL) 
		return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

inline int CompBWT::setOperation(enum exc_oper_e operation) {
	if (operation == EXCOM_OP_COMPRESS || operation == EXCOM_OP_DECOMPRESS) {
		CompModule::operation = operation;
		return EXCOM_ERR_OK;
	} 
	return EXCOM_ERR_OPERATION;
}

#endif