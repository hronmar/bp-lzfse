/*
 * File:       method/bwt/bwt.cpp
 * Purpose:    Implementation of the BWT
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of BWT module linked to the ExCom library.
 *
 * BWT module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BWT module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BWT module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file bwt.cpp
 *  \brief Implementation of the BWT
 */

#include <cstring> //memcpy, memcmp etc
#include "bwt.hpp"

CompBWT::CompBWT(unsigned int handle) : 
	CompModule(handle), _reader(NULL), _writer(NULL) 
{
	_block_size = DEFAULT_BLOCK_SIZE;
	_block_data = new unsigned char[_block_size << 1];
}

int CompBWT::connectIOModule(IOModule *module, int connection_type) {
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (_reader != NULL) 
			return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) 
			return res;
		_reader = (IOReader *)ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (_writer != NULL) 
			return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) 
			return res;
		_writer = (IOWriter *)ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default: 
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompBWT::setParameter(int parameter, void *value) {
	if (value == NULL) 
		return EXCOM_ERR_MEMORY;

	if (parameter == EXCOM_PARAM_BWT_BLOCK_SIZE) {
		unsigned &val = *(unsigned *)value;
		if (val < 2 || val > 255)
			return EXCOM_ERR_VALUE;
		if (val != _block_size) {
			_block_size = val;
			delete[] _block_data;
			_block_data = new unsigned char[_block_size << 1];
		}
		return EXCOM_ERR_OK;
	}
	return CompModule::setParameter(parameter, value);
}
int CompBWT::getValue(int parameter, void *value) {
	if (value == NULL) 
		return EXCOM_ERR_MEMORY;

	if (parameter == EXCOM_VALUE_BWT_BLOCK_SIZE) {
		*(unsigned *)value = _block_size;
		return EXCOM_ERR_OK;
	}
	return CompModule::getValue(parameter, value);
}

int CompBWT::run() {
	int res = checkConnection();
	if (res != EXCOM_ERR_OK) 
		return res;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS)
		res = Encode();		
	else res = Decode();

#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}

int CompBWT::Encode() {
	int input;
	unsigned char i;
	int j;
	unsigned char orig_idx;
	unsigned char **block_ptrs = new unsigned char *[_block_size];
	unsigned char sizedec = _block_size - 1;

	while (true) {
		for (i = 0; i < _block_size; ++i) {
			if ((input = _reader->readByte()) == EOF) { //not enough data to form a block?
				j = 0;
				while (j != i)
					_writer->writeByte(_block_data[j++]); //output raw data
				goto end;
			}
			_block_data[i] = _block_data[i + _block_size] = (unsigned char)input;
			block_ptrs[i] = _block_data + i;
		}

		orig_idx = 0;
		for (i = 1; i < _block_size; ++i) {
			j = i;
			while (j != 0 && memcmp(block_ptrs[j - 1], block_ptrs[j], _block_size) > 0) {
				BWTHelper::Swap(block_ptrs[j - 1], block_ptrs[j]);
				if (j - 1 == orig_idx)
					orig_idx = j;
				--j;
			}
		}
		
		for (i = 0; i < _block_size; ++i) 
			_writer->writeByte(block_ptrs[i][sizedec]);
		_writer->writeByte(orig_idx);
	}
end:	
	_writer->eof();
	delete[] block_ptrs;

	return EXCOM_ERR_OK;
}

int CompBWT::Decode() {
	unsigned long input;
	unsigned char i, prev;
	int j;
	unsigned char idx;
	unsigned char *sorted_data = _block_data + _block_size;
	unsigned char I[256];
	unsigned char *V = new unsigned char[_block_size];

	while (true) {
		for (i = 0; i < _block_size; ++i) {
			if ((input = _reader->readByte()) == EOF) { //not enough data to form a block?
				j = 0;
				while (j != i)
					_writer->writeByte(_block_data[j++]); //output raw data
				goto end;
			}
			_block_data[i] = (unsigned char)input;
			I[input] = 0;
		}

		memcpy(sorted_data, _block_data, _block_size);
		for (i = 0; i < _block_size; ++i) {
			V[i] = I[_block_data[i]]++;
			for (j = i + 1; j < _block_size; ++j) {
				if (sorted_data[i] > sorted_data[j])
					BWTHelper::XorSwap(sorted_data[i], sorted_data[j]);
			}
		}
		
		I[prev = *sorted_data] = 0;
		for (i = 1; i < _block_size; ++i) {
			if (prev != sorted_data[i])
				I[prev = sorted_data[i]] = i;
		}

		idx = (unsigned char)_reader->readByte();

		for (j = _block_size - 1; j >= 0; --j) {
			sorted_data[j] = _block_data[idx];
			idx = V[idx] + I[_block_data[idx]];
		}

		for (i = 0; i < _block_size; ++i) 
			_writer->writeByte(sorted_data[i]);

/*	slower variant:		
		idx = (unsigned char)_reader->readByte();

		for (i = 0; i < _block_size; ++i) {
			unsigned char &ref = sorted_data[idx];
			_writer->writeByte(ref);
			j = idx - 1;
			counter = 1;
			while (j >= 0 && sorted_data[j--] == ref)
				++counter;
			j = 0;
			while (counter != 0) {
				if (_block_data[j++] == ref)
					--counter;
			}
			idx = j - 1;
		}
*/
	}
end:
	_writer->eof();
	delete[] V;

	return EXCOM_ERR_OK;
}
