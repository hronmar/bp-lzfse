/*
 * File:       method/sfano/sfano.cpp
 * Purpose:    Declaration of Shannon-Fano statistical compression method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Shannon-Fano module linked to the ExCom library.
 *
 * Shannon-Fano module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Shannon-Fano module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Shannon-Fano module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "sfano.hpp"
#include "ShannonFanoTree.h"
#include "../universal_codes/UniversalCodes.h"
#include <iostream>

using namespace std;

int CompSFano::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompSFano::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompSFano::checkConnection()
{
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompSFano::runCompress() {
	int c;
	int i;
	unsigned long fileLength;
	string x;

	UniversalCodes* uc = new UniversalCodes();
	ShannonFanoTree* sft = new ShannonFanoTree();
	
	fileLength = 0;

	// The first pass - get symbol's frequencies
	while (c = reader->readByte(), c != EOF) {
		sft->addSymbol(c);
		fileLength++;
	}

	if (sft->buildTree()) {	
		// Write header
		writer->writeNBits(32, fileLength);

		// Get tree as a string
		x = sft->getTreeAsString();
		/* The decoder reads the file bit by bit
		 * therefore we have to plit by 8 bits and output */ 
		for (i = 0; i < (x.size() / 8); i++) {
			writer->writeByte(uc->binToDec(x.substr(i*8, 8)));
 		}

		int remainder = x.size() % 8;
		if (remainder != 0) {
			writer->writeNBits(remainder, uc->binToDec(x.substr(x.size()-remainder)));
		}

		// Rewind a file to a beggining is necessary for the second pass
		reader->rewind();

		// The second pass - encode all symbols now
		while (c = reader->readByte(), c != EOF) {
			x = sft->getSymbolCode(c);
	    		// Because of decompress we have to output bit by bit
			for (i = 0; i < x.size(); i++) {
				writer->writeNBits(1, (x.at(i) == '1' ? 1 : 0));
			}
		}

		writer->eof();
		
		// Clean up the data
		delete sft;
		sft = NULL;

		delete uc;
		uc = NULL;

		return EXCOM_ERR_OK;
	} else {
		delete sft;
		sft = NULL;

		delete uc;
		uc = NULL;

		return EXCOM_ERR_INCOMPLETE;
	}
}

int CompSFano::runDecompress() {
	int i;
	int j;
	unsigned long bits;
	unsigned long bitsLeft;
	unsigned long c;
	unsigned long fileLength;
	string x;

	UniversalCodes* uc = new UniversalCodes();
	ShannonFanoTree* sft = new ShannonFanoTree();

	// Get file length
	i = reader->readNBits(32, &fileLength);

	if (i != EOF) {
		// Number of leaves in Huffman tree
		i = reader->readNBits(8, &bits);
		// EOFs!!!

		/* Encoder outputted only 8 bits and 256 needs 9 bits
		 * we save one bit */ 
		bits++;

		// Number of bits need to be read to reconstruct the Shannon-Fano tree
		if (bits == 1) {
			bits = 2 + 8; // only root and one leaf
		} else {
			bits = (2*bits - 1) + bits*8;
		}

		// Get the tree from string
		for (i = 0; i < (bits / 8); i++) {
			j = reader->readNBits(8, &c);
			x += uc->getBeta(c, 8);
		}

		bitsLeft = bits % 8;
		if (bitsLeft != 0) {
			j = reader->readNBits(bitsLeft, &c);
			x += uc->getBeta(c, bitsLeft);
		}

		if (sft->reconstructTree(x)) {
			while (fileLength > 0) {
				// Read bits until some codeword is matched
				do {
					i = reader->readNBits(1, &c);
					// EOF
				} while (!sft->isLeaf(c));

				if (sft->isErr()) {
					delete sft;
					sft = NULL;

					delete uc;
					uc = NULL;

					return EXCOM_ERR_FORMAT;
				}

				writer->writeByte(sft->getActualNodeSymbol());

				// Traverse up to root, prepare for next search
				sft->traverseUp();
				fileLength--;	  
			}
		} else {
			delete sft;
			sft = NULL;

			delete uc;
			uc = NULL;

			writer->eof();

			return EXCOM_ERR_FORMAT;
		}
	}

	delete sft;
	sft = NULL;
	//sft->~ShannonFanoTree();

	delete uc;
	uc = NULL;

	writer->eof();

	return EXCOM_ERR_OK;
}

int CompSFano::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}


#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}


