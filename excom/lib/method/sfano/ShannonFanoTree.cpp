/*
 * File:       method/sfano/ShannonFanoTree.cpp
 * Purpose:    Declaration of Shannon-Fano statistical compression method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Shannon-Fano module linked to the ExCom library.
 *
 * Shannon-Fano module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Shannon-Fano module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Shannon-Fano module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file ShannonFanoTree.cpp
 *  \brief Concrete implementation of Shannon-Fano tree structure.
 *
 *  This class serves for a Shannon-Fano tree representation
 */

#include "ShannonFanoTree.h"
#include "ShannonFanoNode.h"

#include <algorithm>
#include <iostream>

#include <stdlib.h>
#include <math.h>

// Constructor
ShannonFanoTree::ShannonFanoTree() {
	leavesCount = 0;
	nodesCount = 0;
	root = new ShannonFanoNode();
	nodes.push_back(root);
	universalCodes = new UniversalCodes();
	err = false;
}

// Destructor
ShannonFanoTree::~ShannonFanoTree() {
	delete universalCodes;
	universalCodes = NULL;

	leaves.clear();

	for (nodeIter = nodes.begin(); nodeIter != nodes.end(); nodeIter++) {
		delete *nodeIter;
		*nodeIter = NULL;				
	}
/*
	for (vector<ShannonFanoNode*>::iterator nodes_iter = treeNodes.begin(); nodes_iter != treeNodes.end(); ++nodes_iter) {
		//(*nodes_iter)->~ShannonFanoNode();
		delete *nodes_iter;
		*nodes_iter = NULL;
	}
*/
	//delete root;
	//root = NULL;

	treeNodes.clear();

}

void ShannonFanoTree::addSymbolNode(ShannonFanoNode* node) {
	treeNodes.push_back(node);
	leavesCount++;
}

void ShannonFanoTree::addNode(ShannonFanoNode* node) {
	treeNodes.push_back(node);
	nodesCount++;
}

void ShannonFanoTree::createTree(ShannonFanoNode* node, int x, int y) {
	// Check when the leaves are reached
	if (x == y) {
		if (node->getParent()->getLeftChild() == node) {
			node->getParent()->setLeftChild(treeNodes[x]);
		} else {
			node->getParent()->setRightChild(treeNodes[x]);
		}

		treeNodes[x]->setParent(node->getParent());
		delete node;
		node = NULL;
	} else if (x == y-1) {
		treeNodes[x]->setParent(node);
		treeNodes[y]->setParent(node);

		node->setLeftChild(treeNodes[x]);
		node->setRightChild(treeNodes[y]);

		addNode(node);
	} else {
		ShannonFanoNode* leftChild = new ShannonFanoNode();
		ShannonFanoNode* rightChild = new ShannonFanoNode();

		leftChild->setParent(node);
		rightChild->setParent(node);

		node->setLeftChild(leftChild);
		node->setRightChild(rightChild);

		addNode(node);

		// Divide the current set to two parts
		int splitPoint = splitNodes(x, y);

		createTree(leftChild, x, splitPoint);
		createTree(rightChild, splitPoint+1, y);
	}

	return;
}

string ShannonFanoTree::traversePreorder(ShannonFanoNode* node) {
	string code = "";

	// Internal node
	if ((node->getLeftChild() != 0) || (node->getRightChild() != 0)) {
		code += INTERNAL_NODE_CODE; 
		if (node->getLeftChild() != 0) code += traversePreorder(node->getLeftChild());     
		if (node->getRightChild() != 0) code += traversePreorder(node->getRightChild());
	// Leaf
	} else {
		code = code + LEAF_NODE_CODE + universalCodes->getBeta(node->getSymbol(), BITS_PER_SYMBOL);
	}

	return code;
}

string ShannonFanoTree::getTreeAsString() {
	return universalCodes->getBeta(leavesCount-1, 8)
	  +traversePreorder(root);
}

string ShannonFanoTree::getSymbolCode(unsigned char s) {
	return leaves.find(s)->second->getCode();
}

bool ShannonFanoTree::isLeaf(int i) {
	if ((currentNode->getRightChild() != 0) && (currentNode->getLeftChild() != 0)) {
		currentNode = (i == 1) ? currentNode->getRightChild() : currentNode->getLeftChild();
		return (currentNode->getSymbol() != -1);
	} else {
		err = true;
		return true;
	}
}

bool ShannonFanoTree::isErr() {
	return err;
}

unsigned char ShannonFanoTree::getActualNodeSymbol() {
	return currentNode->getSymbol();
}

void ShannonFanoTree::traverseUp() {
	currentNode = root;
}

bool ShannonFanoTree::reconstructTree(string bits) {
	int index;
	int x;

	if (bits.size() != 0) {
		if (bits.at(0) == '0') {
			actualNode = new ShannonFanoNode();
			root  = currentNode = actualNode;
			index = 1;

			nodes.push_back(actualNode);

			while (index < bits.size()) {
				if (bits.at(index) == '0') {
					newNode = new ShannonFanoNode();
					newNode->setParent(actualNode);

					nodes.push_back(newNode);

					if (actualNode->getLeftChild() == 0) {
						actualNode->setLeftChild(newNode);
						actualNode = newNode;
					} else if (actualNode->getRightChild() == 0) {
						actualNode->setRightChild(newNode);
						actualNode = newNode;

					// Traverse up in the tree
					} else {
						x = 0;  
						while ((actualNode->getLeftChild() != 0) && (actualNode->getRightChild() != 0)) {
							if (actualNode->getParent() != 0) {
								actualNode = actualNode->getParent();	
								x++;
							} else {
								return false;
							}
						}
					}
					index++;
				} else {
					newNode = new ShannonFanoNode();
					newNode->setParent(actualNode);
					newNode->setSymbol((unsigned char) universalCodes->binToDec(bits.substr(index+1, 8)));

					addNode(newNode);

					nodes.push_back(newNode);

					if (actualNode->getLeftChild() == 0) {
						actualNode->setLeftChild(newNode);
						leaves.insert( make_pair(newNode->getSymbol(), newNode) );
					} else if (actualNode->getRightChild() == 0) {
						actualNode->setRightChild(newNode);
						leaves.insert( make_pair(newNode->getSymbol(), newNode) );
						x = 0;
						while ((actualNode->getLeftChild() != 0) && (actualNode->getRightChild() != 0) && (actualNode != root)) {
							if (actualNode->getParent() != 0) {
								actualNode = actualNode->getParent();	
								x++;
							} else {
								return false;
							}
						}
					} 

					index = index + 8 + 1;
				}
			}
		}

		//delete actualNode;
		//actualNode = NULL;
		//delete newNode;
		//newNode = NULL;

		return true;
	} else {
		return false;
	}
}

bool ShannonFanoTree::buildTree() {
	ShannonFanoNode* sfn;

	for (iterSymbols = symbolFrequency.begin(); iterSymbols != symbolFrequency.end(); iterSymbols++) {
		sfn = new ShannonFanoNode( (*iterSymbols).first, (*iterSymbols).second );
		leaves.insert( make_pair((*iterSymbols).first, sfn) );
		addSymbolNode(sfn);		
	}

	if (!leaves.empty()) { 
		sortNodes();

		if (leavesCount == 1) {
			root->setRightChild(treeNodes[0]);
			treeNodes[0]->setParent(root);
			addNode(root);
		} else {
			createTree(root, 0, leavesCount-1);
		}

		setSymbolCodes();

		return true;
	}

	return false;
}

void ShannonFanoTree::addSymbol(unsigned char s) { 
	if (symbolFrequency.find(s) != symbolFrequency.end()) {
		symbolFrequency.find(s)->second++;
	} else {
		symbolFrequency.insert(make_pair(s, 1));
	}
}

string ShannonFanoTree::getCode(ShannonFanoNode* node) {
	string bit;

	if (node->getParent() == root) {
		if ((root->getLeftChild() != 0) && (root->getLeftChild() == node)) {
			return "0";
		} else {
			return "1";
		}
	}

	if (node->getParent() != 0) {
		if (node->getParent()->getLeftChild() == node) {
			bit = "0";
		} else {
			bit = "1";
		}

		return getCode(node->getParent())+bit;
	} else {
		return "";
	}  
}

void ShannonFanoTree::setSymbolCodes() {
	for (iterNodes = leaves.begin(); iterNodes != leaves.end(); iterNodes++) {
		(*iterNodes).second->setCode(getCode((*iterNodes).second));
	}
}

int ShannonFanoTree::splitNodes(int x, int y) { 
	long probA1, probA2;
	long probB1, probB2;

	int i;

	for (i=x; i<y; i++) {
		if ((i+2) <= y) {
			probA1 = getFreqSum(x, i);
			probA2 = getFreqSum(i+1, y);

			probB1 = getFreqSum(x, i+1);
			probB2 = getFreqSum(i+2, y);

			/* We do not have divide by sum of all frequencies
			 * because less is better => both split parts are nearly the same */	      	
			if (abs(probA1-probA2) <= abs(probB1-probB2)) {
				break;	
			}
		} else {
			break;
		}
	} 

	return i;
}

long ShannonFanoTree::getFreqSum(int x, int y) {
	unsigned long freqSum = 0;

	for (int i=x; i<=y; i++) {
		freqSum +=  treeNodes[i]->getFreq();  
	}  

	return freqSum;
}

bool compare_nodes (ShannonFanoNode* a, ShannonFanoNode* b) {
	return a->getFreq() < b->getFreq();
}

void ShannonFanoTree::sortNodes() {
	sort(treeNodes.begin(), treeNodes.end(), compare_nodes);  
}
