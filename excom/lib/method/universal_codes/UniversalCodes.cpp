/* 
 * File:   UniversalCodes.cpp
 * Author: Jakub Reznicek
 */
#include <iostream>

#include "UniversalCodes.h"

UniversalCodes::UniversalCodes() {
}

UniversalCodes::~UniversalCodes() {
}

string UniversalCodes::interleave(string x, string y) {
  string buffer = "";

  for (unsigned i = 0; i < y.length(); i++) {
    buffer += x[i];
    buffer += y[i];
  }     

  buffer += x[x.length()-1];

  return buffer;
}

string UniversalCodes::getBetaSequence(unsigned int n) {
  string buffer;

  if ((n == 1) || (n == 0)) return "";

  buffer = getBeta(n);

  return getBetaSequence(buffer.length()-1)+buffer;
}

string UniversalCodes::getBetaSequence_(unsigned int n) {
  string buffer;
  string temp;
 
  if (n <= 7) {
    temp = getBeta(n);
    buffer.assign(3-temp.length(), '0');
    buffer += temp;

    return buffer;
  }

  buffer = getBeta(n);

  return getBetaSequence_(buffer.length()-1)+buffer;
}

string UniversalCodes::getOmegaPrime(unsigned int n) {
  string buffer;
  string temp;

  if (n == 1) return "00";

  buffer = getBetaSequence_(n)+'0';

  return buffer;
}

string UniversalCodes::getOmega(unsigned int n) {
  string buffer;

  buffer = getBetaSequence(n)+"0";

  return buffer;
}

string UniversalCodes::getDelta(unsigned int n) {
  string gammaCode;
  string betaCode_;

  gammaCode = getGamma(getBeta(n).length());
  betaCode_ = getBeta_(n);

  return gammaCode+betaCode_;
}

string UniversalCodes::getGamma(unsigned int n) {
  string alphaCode;
  string betaCode_;

  alphaCode = getAlpha(n);
  betaCode_ = getBeta_(n);

  if (n > 1) {
    return interleave(alphaCode, betaCode_);
  } else {
    return alphaCode;
  }
}


string UniversalCodes::getGammaPrime(unsigned int n) {
  string betaCode;
  string buffer;

  betaCode = getBeta(n);
  buffer.assign(betaCode.length() -1 , '0');
  buffer += betaCode;

  return buffer;
}

string UniversalCodes::getBeta_(unsigned int n) {
  string buffer;
  string betaCode;

  betaCode = getBeta(n);

  if (betaCode.length() > 1) {
    buffer = betaCode.substr(1, betaCode.length()-1);
  } else buffer = "";

  return buffer;
}

string UniversalCodes::getBeta(unsigned int n) {
  const size_t size = sizeof(n)*8;
  char result[size];

  int index = size;
  do {
    result[--index] = '0' + (n & 1);
  } while (n >>= 1);

  return string(result + index, result + size);
}

string UniversalCodes::getBeta(unsigned int n, int len) {
  string betaCode = getBeta(n);
  int size = betaCode.size();

  if (betaCode.size() < len) {
    for (int i = 1; i <= (len-size); i++) {
      betaCode = "0"+betaCode;
    }
  } 

  return betaCode;
}

string UniversalCodes::getAlpha(unsigned int n) {
  string alphaCode;

  if (n > 0) {
    alphaCode.assign(n-1, '0');
    alphaCode += '1';
  } else alphaCode = "";

  return alphaCode;
}

unsigned long UniversalCodes::binToDec(string s) {
  return bitset<32>(s).to_ulong();
}
