/* 
 * File:   UniversalCodes.h
 * Author: Jakub Reznicek
 */

#ifndef _UNIVERSALCODES_H
#define	_UNIVERSALCODES_H

#include <string>
#include <bitset>

using namespace std;

class UniversalCodes {
private:
  string interleave(string, string);	
  string getBetaSequence(unsigned int);
  string getBetaSequence_(unsigned int);
public:
  //constructors
  UniversalCodes();

  //destructor
  ~UniversalCodes();

  // methods
  string getAlpha(unsigned int);
  string getBeta(unsigned int);
  // Return a beta code of given number with '0's at the begging up to a given lentgh
  string getBeta(unsigned int, int);

  string getBeta_(unsigned int);
  string getGamma(unsigned int);	
  string getGammaPrime(unsigned int);	
  string getDelta(unsigned int);	
  string getOmega(unsigned int);
  string getOmegaPrime(unsigned int);
  unsigned long binToDec(string);
};

#endif	/* _UNIVERSALCODES_H */

