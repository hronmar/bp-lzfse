#ifndef __METHOD_LZW_HPP__
#define __METHOD_LZW_HPP__

/*
 * File:       method/lzw/lzw.hpp
 * Purpose:    Declaration of the LZW compression methods
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of LZW compression module linked to the ExCom library.
 *
 * LZW compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZW compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZW module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lzw.hpp
 *  \brief Declaration of the LZW compression method
 */

#include <excom.h>
#include "../../compmodule.hpp"

#include <map>
#include <vector>
#include <string>

#define OPERATION_COMPRESS 0
#define OPERATION_DECOMPRESS 1

class CompLZW : 
	public CompModule 
{
private:
	IOReader *reader;
	IOWriter *writer;

	int runCompress();
	int runDecompress();

	unsigned dict_size;
	unsigned dict_capacity;
	unsigned root_size;	
	unsigned char char_bitsize;

	int (CompLZW::* readSymbol)(unsigned long *);
	int (CompLZW::* writeSymbol)(unsigned long);

public:
	static const unsigned DEFAULT_DICT_SIZE = 4096;
	static const unsigned char DEFAULT_CHAR_BITSIZE = 8;

	CompLZW(unsigned int handle);
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();

private:
	typedef struct {
		unsigned prefixIndex; 
		unsigned first;
		unsigned left;
		unsigned right;

		unsigned char symbol;
	} lzw_node_enc_t;

	typedef struct {
		unsigned prefixIndex; 
		unsigned char symbol;
	} lzw_node_dec_t;

	typedef std::vector <lzw_node_enc_t > lzwEncDictionary;
	lzwEncDictionary encDict;

	typedef std::vector <lzw_node_dec_t > lzwDecDictionary;
	lzwDecDictionary decDict;

	unsigned char bitsize;
	unsigned threshold;

	std::vector<unsigned> decodedString;

	/*! \brief Initialize the LZW dictionary
	 *  \param op Type of operation - encode or decode
	 *  
	 *  All input symbols are added
	 */
	void initializeDictionary(int op);

	int readNBits(unsigned long *value);
	int readByte(unsigned long *value);
	int writeNBits(unsigned long value);
	int writeByte(unsigned long value);

	bool getCode(unsigned);

	unsigned add(lzw_node_enc_t&);
	unsigned find(lzw_node_enc_t&);

	/*! \brief Returns the number of bits required to store the given number
	 *  \param n A number
	 */
	int requiredBits(unsigned int n);

	bool searchForNode(lzw_node_enc_t&);
};

inline int CompLZW::readNBits(unsigned long *value) {
	return reader->readNBits(char_bitsize, value);		
}
inline int CompLZW::readByte(unsigned long *value) {
	int result = reader->readByte();
	if (result != EOF)
		*value = result;
	return result;
}
inline int CompLZW::writeNBits(unsigned long value) {
	return writer->writeNBits(char_bitsize, value);
}
inline int CompLZW::writeByte(unsigned long value) {
	return writer->writeByte((unsigned char)value);
}

#endif //ifndef __METHOD_LZW_HPP__
