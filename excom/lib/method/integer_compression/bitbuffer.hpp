#ifndef __BITBUFFER_HPP__
#define __BITBUFFER_HPP__
/*
 * File:       method/integer_compression/bitbuffer.hpp
 * Purpose:    Declaration of the bit buffers for IO handling
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file bitbuffer.hpp
 *  \brief Declaration of the bit buffers for IO handling
 *
 *  This class provides communication between the coding functors and
 *  the ExCom IO Module
 */

#include <excom.h>
#include "../../compmodule.hpp"
#include <string>

class BitBuffer {
	protected:
		IOWriter* _writer;
		IOReader* _reader;
		std::string _buffer;
		bool _eof;
		void writePart(void);
		void readPart(void);
	public:
		BitBuffer(IOWriter* writer, IOReader* reader);
		~BitBuffer(void);
		bool getBit(bool& bit);
		bool peekBit(bool& bit);
		void pushBit(char bit);
		void pushBit(int);
		void pushBits(const char* bits);
		void pushNBits(const char* bits);
		void pushULong(unsigned long bits, bool truncate = false);
		void pushULong(unsigned long bits, unsigned length);
		void pushTruncatedULong(unsigned long bits);
		void clear(void);
		bool ok(void);
		void write(void);
		IOWriter* writer(void) const { return _writer; }
		IOReader* reader(void) const { return _reader; }
};

class DummyBitBuffer: public BitBuffer {
	protected:
		unsigned long _bitsStored;
		void writePart(void);
	public:
		DummyBitBuffer(void);
		~DummyBitBuffer(void);
		void write(void);
		unsigned long bitsStored(void);
};

#endif
