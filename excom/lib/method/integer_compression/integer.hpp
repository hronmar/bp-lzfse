#ifndef __METHOD_INTEGER_HPP__
#define __METHOD_INTEGER_HPP__

/*
 * File:       method/integer_compression/integer.hpp
 * Purpose:    Declaration of a wrapper class for integer compression module
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file integer.hpp
 *  \brief Declaration of an integer compression module for ExCom library
 *
 *  This class provides communication between the ExCom library and the integer 
 *  compression module
 */

#include <excom.h>
#include "../../compmodule.hpp"

class CompInteger: public CompModule {
private:
	IOReader *reader;
	IOWriter *writer;
	unsigned method;
	unsigned param;
	int runCompress();
	int runDecompress();
public:
	CompInteger(unsigned int handle): CompModule(handle), reader(NULL), writer(NULL), method(0), param(0) {}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int checkConnection();
	int run();
};

#endif //ifndef __METHOD_INTEGER_HPP__
