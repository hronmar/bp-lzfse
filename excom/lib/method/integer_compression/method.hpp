#ifndef __METHOD_HPP__
#define __METHOD_HPP__
/*
 * File:       method/integer_compression/method.hpp
 * Purpose:    Declaration of the coding functors
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * Excom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Excom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file method.hpp
 *  \brief Declaration of the coding functors
 *
 */

#include "bitbuffer.hpp"
#include <vector>

#define AUTO			0
#define BLOCK			1
#define UNARY			2
#define BINARY			3
#define TERNARY_COMMA	4
#define TABOO			5
#define FIBONACCI		6
#define GOLDBACH		7
#define GOLOMB			8
#define RICE			9
#define ALPHA			10
#define BETA			11
#define BETAPRIME		12
#define GAMMA			13
#define GAMMAPRIME		14
#define DELTA			15
#define DELTAPRIME		16
#define OMEGA			17
#define OMEGAPRIME		18
#define METHODS			19

/**
 * Abstract coding functor class
 */
class Method {
	protected:
		/** BitBuffer class for IO operations */
		BitBuffer& bb;
	public:
		/**
		 * constructor
		 * @param bb	BitBuffer to use
		 */
		Method(BitBuffer& bb): bb(bb) {}
		/**
		 * desctructor
		 */
		virtual ~Method() {}
		/**
		 * encoding operator
		 * @param data	data to encode
		 */
		virtual void operator() (unsigned long) = 0;
		/**
		 * decoding operator
		 * @return 	decoded data
		 */
		virtual unsigned long operator() (void) = 0;
		/**
		 * number of used bits to encode the data
		 * @param data	data to encode
		 * @return		number of bits
		 */
		static unsigned long length (unsigned long);
		/**
		 * factory method for creating functors
		 * @param method	selected method
		 * @param param		parameter for the method
		 * @param bb		BitBuffer class to use to IO operations
		 * @return			pointer to the created functor class
		 */
		static Method* createMethod(unsigned method, unsigned param, BitBuffer& bb);
};

/**
 * Functor for input analysis
 */
class Heuristic: public Method {
	protected:
		/** function pointer for the length method */
		typedef unsigned long (*func_t)(unsigned long);
		/** length in bit for all methods */
		unsigned long length_bit[METHODS];
		/** length in Gb for all methods */
		unsigned long length_giga[METHODS];
		/** pointers for all methods */
		func_t length_func[METHODS];
	public:
		/**
		 * constructor
		 * @param bb	BitBuffer class for IO operations
		 */
		Heuristic(BitBuffer& bb);
		/**
		 * destructor
		 */
		virtual ~Heuristic();
		/**
		 * encoding operator -- performs the analysis
		 * @param data	value to analyse
		 */
		virtual void operator() (unsigned long);
		/**
		 * decoding operator -- not implemented, has no use
		 */
		virtual unsigned long operator() (void);
};

/**
 * Functor for unary code
 */
class Unary: public Method {
	public:
		Unary(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for Elias alpha code
 */
class Alpha: public Method {
	public:
		Alpha(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for Elias beta code
 */
class Beta: public Method {
	public:
		Beta(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
		/**
		 * extended decode operator
		 *
		 * beta code is decodable with known length
		 * @param length	number of bits
		 * @param dummy		dummy parameter, set it to NULL
		 * @return			decoded value
		 */
		unsigned long operator() (unsigned long, void*);
};

/**
 * Functor for Elias beta prime code
 */
class BetaPrime: public Method {
	public:
		BetaPrime(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
		/**
		 * extended decode operator
		 *
		 * beta prime code is decodable with known length
		 * @param length	number of bits
		 * @param dummy		dummy parameter, set it to NULL
		 * @return			decoded value
		 */
		unsigned long operator() (unsigned long, void*);
};

/**
 * Functor for Elias gamma code
 */
class Gamma: public Method {
	private:
		/**
		 * internal function for encoding via recursion
		 * @param data		number to encode
		 */
		virtual void internal(unsigned long);
	public:
		Gamma(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for Elias gamma prime code
 */
class GammaPrime: public Method {
	public:
		GammaPrime(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for Elias delta code
 */
class Delta: public Method {
	public:
		Delta(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for Elias delta prime code
 */
class DeltaPrime: public Method {
	public:
		DeltaPrime(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for Elias omega code
 */
class Omega: public Method {
	private:
		/**
		 * internal function for encoding via recursion
		 * @param data		number to encode
		 */
		void internal(unsigned long);
	public:
		Omega(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for Elias omega prime code
 */
class OmegaPrime: public Method {
	private:
		/**
		 * internal function for encoding via recursion
		 * @param data		number to encode
		 */
		void internal(unsigned long);
	public:
		OmegaPrime(BitBuffer& bb);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for block code
 */
class Block: public Method {
	public:
		Block(BitBuffer& bb);
		virtual void operator() (unsigned long data);
};

/**
 * Functor for ternary comma code
 */
class TernaryComma: public Method {
	public:
		TernaryComma(BitBuffer& bb);
		virtual void operator() (unsigned long data);
};

/**
 * Functor for Fibonacci code
 */
class Fibonacci: public Method {
	protected:
		/** order of the Fibonacci code */
		const unsigned order;
		/** vector of the Fibonacci numbers */
		static std::vector<unsigned long> fibs;
	public:
		/**
		 * constructor
		 * @param bb	BitBuffer to use
		 * @param order	order of the Fibonacci code
		 */
		Fibonacci(BitBuffer& bb, unsigned order = 2);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for the Golomb code
 */
class Golomb: public Method {
	protected:
		/** parameter for the Golomb code */
		const unsigned p;
		/** logarithm of the parameter p */
		const unsigned c;
	public:
		/**
		 * constructor
		 * @param bb	BitBuffer to use
		 * @param param	parameter for the Golomb code
		 */
		Golomb(BitBuffer& bb, unsigned param = 100000);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * Functor for the Rice code
 */
class Rice: public Method {
	protected:
		/** parameter for the Rice code */
		const unsigned p;
	public:
		/**
		 * constructor
		 * @param bb	BitBuffer to use
		 * @param param	parameter for the Rice code
		 */
		Rice(BitBuffer& bb, unsigned param = 65536);
		virtual void operator() (unsigned long);
		virtual unsigned long operator() (void);
		static unsigned long length (unsigned long);
};

/**
 * fast logarithm function of base two
 * @param number	number
 * @return			logarithm of the number
 */
static unsigned long fast_log2(float number) {
	// fast log2 magic
	unsigned long ix = (unsigned long&)number;
	unsigned long exp = (ix >> 23) & 0xFF;
	return (unsigned long)(exp) - 127;
}
#endif
