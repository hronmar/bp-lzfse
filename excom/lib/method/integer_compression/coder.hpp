#ifndef __CODER_HPP__
#define __CODER_HPP_

/*
 * File:       method/integer_compression/coder.hpp
 * Purpose:    The integer compression module
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * Excom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Excom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file coder.hpp
 *  \brief The integer compression module
 *
 *  This module is responsible for calling appropriate methods for encoding and
 *  decoding of the data. It communicates with the IOModule from the ExCom
 *  library.
 */

#include "method.hpp"

/**
 * The main Coder class
 */
class Coder {
	protected:
		/** pointer to the encoding/decoding functor */
		Method* fce;
		/** the associated BitBuffer class */
		BitBuffer bb;
	public:
		/**
		 * constructor -- constructs BitBuffer class and encoding/decoding functor
		 * @param writer	output IOModule
		 * @param reader	input IOModule
		 * @param method	selected method
		 * @param param		parameter for the method
		 */
		Coder(IOWriter* writer, IOReader* reader, unsigned method, unsigned param): bb(writer, reader) {
			fce = Method::createMethod(method, param, bb);
		}
		/**
		 * destructor -- deletes the encoding/decoding functor
		 */
		~Coder() {
			delete fce;
		}
		/**
		 * main generic encoding function
		 * @param data	number to encode
		 */
		void encode(unsigned long data) {
			(*fce)(data);
		}
		/**
		 * main generic decoding function
		 * @param data	decoded value
		 * @return	true on valid data
		 */
		bool decode(unsigned long& data) {
			bool ok = bb.ok();
			if (!ok) return ok;
			data = (*fce)();
			return ok;
		}
};

#endif
