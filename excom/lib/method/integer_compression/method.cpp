/*
 * File:       method/integer_compression/method.hpp
 * Purpose:    Implementation of generic functions
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file method.hpp
 *  \brief Implementation of generic functions
 *
 *  This class provides creating functors and defines some basic methods
 */

#include "method.hpp"

Method* Method::createMethod(unsigned method, unsigned param, BitBuffer& bb) {
	switch (method) {
		default:
			throw "Unknown method";
		case AUTO:
			return new Heuristic(bb);
		case BLOCK:
			//return new Block(bb);
		case UNARY:
			return new Unary(bb);
		case BINARY:
			throw "Method not yet implemented!";
		case TERNARY_COMMA:
			//return new TernaryComma(bb);
		case TABOO:
			throw "Method not yet implemented!";
		case FIBONACCI:
			return param?new Fibonacci(bb,param):new Fibonacci(bb);
		case GOLDBACH:
			throw "Method not yet implemented!";
		case GOLOMB:
			return param?new Golomb(bb,param):new Golomb(bb);
		case RICE:
			return param?new Rice(bb,param):new Rice(bb);
		case ALPHA:
			return new Alpha(bb);
		case BETA:
			return new Beta(bb);
		case BETAPRIME:
			return new BetaPrime(bb);
		case GAMMA:
			return new Gamma(bb);
		case GAMMAPRIME:
			return new GammaPrime(bb);
		case DELTA:
			return new Delta(bb);
		case DELTAPRIME:
			return new DeltaPrime(bb);
		case OMEGA:
			return new Omega(bb);
		case OMEGAPRIME:
			return new OmegaPrime(bb);
	}
}

unsigned long Method::length (unsigned long) {
	throw "Length unsupported!";
}

