/*
 * File:       method/integer_compression/bitbuffer.cpp
 * Purpose:    Implementation of the bit buffers
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file bitbuffer.cpp
 *  \brief Implementation of the bit buffers
 *
 */

#include <cstdlib>
#include <cstdio>
#include <bitset>
#include "bitbuffer.hpp"

#undef DEBUG_PRINT

static const unsigned int SIZE = sizeof(unsigned char) << 3;

void BitBuffer::writePart() {
	if (!_writer) return;
	while (_buffer.length() >= SIZE) {
		#ifdef DEBUG_PRINT
			printf("writing %s\n", std::bitset<SIZE>(_buffer.substr(0, SIZE)).to_string().c_str());
			printf("%lu\n", std::bitset<SIZE>(_buffer.substr(0, SIZE)).to_ulong());
		#endif
		_writer->writeByte(std::bitset<SIZE>(_buffer.substr(0, SIZE)).to_ulong());
		_buffer.erase(0, SIZE);
	}
}

void DummyBitBuffer::writePart() {
	while (_buffer.length() >= SIZE) {
		_bitsStored += SIZE;
		_buffer.erase(0, SIZE);
	}
}

void BitBuffer::readPart() {
	if (!_reader) return;
	while (_buffer.length() < SIZE) {
		int data = _reader->readByte();
		if (data == EOF) {
			_eof = true;
			return;
		}
		_buffer.append(std::bitset<SIZE>(data).to_string());
		#ifdef DEBUG_PRINT
			printf("reading %s\n", std::bitset<SIZE>(data).to_string().c_str());
			printf("in buffer %s\n", _buffer.c_str());
		#endif
	}
}

BitBuffer::BitBuffer(IOWriter* writer, IOReader* reader) {
	_writer = writer;
	_reader = reader;
	_eof = false;
}

DummyBitBuffer::DummyBitBuffer(): BitBuffer(NULL, NULL) {
	_bitsStored = 0;
}

BitBuffer::~BitBuffer() {
	write();
}

DummyBitBuffer::~DummyBitBuffer() {
	write();
}

bool BitBuffer::peekBit(bool& bit) {
	readPart();
	if (!_buffer.length())
		return false;
	bit = _buffer[0] == '1';
	return true;
}

bool BitBuffer::getBit(bool& bit) {
	readPart();
	if (!_buffer.length())
		return false;
	bit = _buffer[0] == '1';
	_buffer.erase(_buffer.begin());
	return true;
}

void BitBuffer::pushBit(char bit) {
	_buffer += bit;
	writePart();
}

void BitBuffer::pushBit(int bit) {
	if (bit) _buffer += '1';
	else _buffer += '0';
	writePart();
}

void BitBuffer::pushBits(const char* bits) {
	_buffer += bits;
	writePart();
}

void BitBuffer::pushNBits(const char* bits) {
	pushBits(bits);
}

void BitBuffer::pushULong(unsigned long bits, bool truncate) {
	if (!bits && !truncate) _buffer+= '0';
	else {
		std::string data = std::bitset<32>(bits).to_string();
		data.erase(0, data.find_first_of('1', 0));
		if (truncate) data.erase(0, 1);
		_buffer += data;
	}
	writePart();
}

void BitBuffer::pushULong(unsigned long bits, unsigned length) {
	std::string data = std::bitset<32>(bits).to_string();
	data.erase(0, 32-length);
	_buffer += data;
	writePart();
}

void BitBuffer::pushTruncatedULong(unsigned long bits) {
	pushULong(bits, true);
}

void BitBuffer::clear() {
	_buffer.erase();
}

bool BitBuffer::ok() {
	return !_eof || _buffer.length();
}

void BitBuffer::write() {
	if (!_writer) return;
	writePart();
	if (!_buffer.length()) return;
	for (unsigned i = _buffer.length(); i < SIZE; i++)
		_buffer += '0';
	writePart();
}

void DummyBitBuffer::write() {
	writePart();
	if (!_buffer.length()) return;
	_bitsStored += _buffer.length();
}

unsigned long DummyBitBuffer::bitsStored() {
	write();
	return _bitsStored;
}
