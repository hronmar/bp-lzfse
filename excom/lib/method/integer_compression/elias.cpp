/*
 * File:       method/integer_compression/elias.cpp
 * Purpose:    Implementation of the Elias coding functors
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file elias.cpp
 *  \brief Implementation of the Elias coding functors
 *
 */
#include "method.hpp"

Alpha::Alpha(BitBuffer& bb): Method(bb) {}
Beta::Beta(BitBuffer& bb): Method(bb) {}
BetaPrime::BetaPrime(BitBuffer& bb): Method(bb) {}
Gamma::Gamma(BitBuffer& bb): Method(bb) {}
GammaPrime::GammaPrime(BitBuffer& bb):Method(bb) {}
Delta::Delta(BitBuffer& bb): Method(bb) {}
DeltaPrime::DeltaPrime(BitBuffer& bb): Method(bb) {}
Omega::Omega(BitBuffer& bb): Method(bb) {}
OmegaPrime::OmegaPrime(BitBuffer& bb): Method(bb) {}

void Alpha::operator() (unsigned long data) {
	while (data--)
		bb.pushBit('0');
	bb.pushBit('1');
}

unsigned long Alpha::operator() () {
	unsigned long data = 0;
	bool bit;
	while (bb.getBit(bit) && !bit)
		data++;
	return data;
}

unsigned long Alpha::length (unsigned long data) {
	return data;
}

void Beta::operator() (unsigned long data) {
	bb.pushULong(data);
}

unsigned long Beta::operator() () {
	throw "Beta code is not uniquely decodable!";
}

unsigned long Beta::length (unsigned long data) {
	return fast_log2(data)+1;
}

unsigned long Beta::operator() (unsigned long length, void*) {
	bool bit;
	unsigned long data = 0;
	for (unsigned long i = 0; i < length; i++) {
		if (!bb.getBit(bit)) break;
		data <<= 1;
		data |= bit;
	}
	return data;
}

void BetaPrime::operator() (unsigned long data) {
	bb.pushULong(data, true);
}

unsigned long BetaPrime::operator() () {
	throw "Beta-comma code is not uniquely decodable!";
}

unsigned long BetaPrime::length (unsigned long data) {
	return fast_log2(data);
}

unsigned long BetaPrime::operator() (unsigned long length, void*) {
	bool bit;
	unsigned long data = 1;
	for (unsigned long i = 0; i < length; i++) {
		if (!bb.getBit(bit)) break;
		data <<= 1;
		data |= bit;
	}
	return data;
}

void Gamma::internal(unsigned long data) {
	if (data <= 1) return;
	if (data & 1) {
		internal(data >> 1);
		bb.pushBits("01");
	}
	else {
		internal(data >> 1);
		bb.pushBits("00");
	}
}

void Gamma::operator() (unsigned long data) {
	internal(data);
	bb.pushBit('1');
}

unsigned long Gamma::operator() () {
	bool bit;
	unsigned long data = 1;
	while (bb.getBit(bit)) {
		if (bit) break;
		if (!bb.getBit(bit)) break;
		data <<= 1;
		if (bit) data++;
	}
	return data;
}

unsigned long Gamma::length (unsigned long data) {
	return (fast_log2(data)<<1) + 1;
}

void GammaPrime::operator() (unsigned long data) {
	unsigned long length = fast_log2(data);
	Alpha a(bb);
	a(length);
	bb.pushULong(data, true);
}

unsigned long GammaPrime::operator() () {
	Alpha a(bb);
	unsigned long length = a();
	BetaPrime b(bb);
	return b(length,NULL);
}

unsigned long GammaPrime::length (unsigned long data) {
	return Alpha::length(fast_log2(data))+fast_log2(data)+1;
}

void Delta::operator() (unsigned long data) {
	unsigned long length = fast_log2(data);
	Gamma g(bb);
	g(length + 1);
	bb.pushULong(data, true);
}

unsigned long Delta::operator() () {
	Gamma g(bb);
	unsigned long length = g();
	BetaPrime b(bb);
	return b(length - 1,NULL);
}

unsigned long Delta::length (unsigned long data) {
	return Gamma::length(fast_log2(data)+1)+fast_log2(data);
}

void DeltaPrime::operator() (unsigned long data) {
	unsigned long length = fast_log2(data);
	GammaPrime g(bb);
	g(length + 1);
	bb.pushULong(data, true);
}

unsigned long DeltaPrime::operator() () {
	GammaPrime g(bb);
	unsigned long length = g();
	BetaPrime b(bb);
	return b(length - 1,NULL);
}

unsigned long DeltaPrime::length (unsigned long data) {
	return GammaPrime::length(fast_log2(data)+1)+fast_log2(data)+1;
}

void Omega::internal(unsigned long data) {
	if (data <= 1) return;
	internal(fast_log2(data));
	bb.pushULong(data);
}

void Omega::operator() (unsigned long data) {
	internal(data);
	bb.pushBit(0);
}

unsigned long Omega::operator() () {
	unsigned long data = 1;
	bool bit;
	while (bb.getBit(bit)) {
		if (!bit) return data;
		BetaPrime b(bb);
		data = b(data,NULL);
	}
}

unsigned long Omega::length (unsigned long data) {
	return ((5*fast_log2(data))>>1)+1;
}

void OmegaPrime::internal(unsigned long data) {
	if (data <= 3) bb.pushBit(0);
	if (data <= 1) return;
	if (data >= 8) internal(fast_log2(data));
	bb.pushULong(data);
}

void OmegaPrime::operator() (unsigned long data) {
	internal(data);
	bb.pushBit(0);
}

unsigned long OmegaPrime::operator() () {
	unsigned long data = 1;
	bool bit;
	if (!bb.getBit(bit)) return data;
	// if first bit is one, then read another two bits
	if (bit) {
		data <<= 1;
		if (!bb.getBit(bit)) return data;
		data |= bit;
		data <<= 1;
		if (!bb.getBit(bit)) return data;
		data |= bit;
	}
	// first bit is zero
	else {
		// second bit is zero, we are done
		if (!bb.getBit(bit) || !bit) return data;
		// second bit was one, read the third one
		data <<= 1;
		if (!bb.getBit(bit)) return data;
		data |= bit;
	}
	while (bb.getBit(bit)) {
		if (!bit) return data;
		BetaPrime b(bb);
		data = b(data,NULL);
	}
}

unsigned long OmegaPrime::length (unsigned long data) {
	return ((5*fast_log2(data))>>1)+1;
}
