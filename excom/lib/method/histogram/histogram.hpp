#ifndef __METHOD_HISTOGRAM_HPP__
#define __METHOD_HISTOGRAM_HPP__

/*
 * File:       method/histogram/histrogram.hpp
 * Purpose:    Declaration of pseudo-compression method which extracts statistical data from the input
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file histogram.hpp
 *  \brief Declaration of pseudo-compression method which extracts statistical information from
 *  the input data.
 *
 *  The main purpose of this method is to produce a chart of frequencies of different
 *  symbols, that are sent to its input.
 */

#include <excom.h>
#include "../../compmodule.hpp"

// FIXME: these should be set at configuration time (config.h)
#define HISTOGRAM_BPC 1
#define HISTOGRAM_INTERVALS 256
#define HISTOGRAM_RANGE_MIN -1
#define HISTOGRAM_RANGE_MAX -1
#define HISTOGRAM_OUTPUT EXCOM_HIST_OUTPUT_HUMAN

class CompHistogram: public CompModule {
private:
	IOReader *reader;
	IOWriter *writer;
	unsigned int bpc;        //! bytes per character (value)
	unsigned int intervals;  //! number of intervals
	unsigned int int_size;   //! size of an interval
	unsigned int *data;      //! array of frequencies for each interval
	int output;              //! output format
	int range_min, range_max;//! only values in the range [range_min, range_max] will be monitored
	unsigned int real_rmin, real_rmax;//! sanitized values of range_max and range_min
public:
	CompHistogram(unsigned int handle): CompModule(handle),
		reader(NULL), writer(NULL),
		bpc(HISTOGRAM_BPC), intervals(HISTOGRAM_INTERVALS),
		data(NULL), output(HISTOGRAM_OUTPUT),
		range_min(HISTOGRAM_RANGE_MIN),
		range_max(HISTOGRAM_RANGE_MAX) {
		operation = EXCOM_OP_HISTOGRAM;
	}
	~CompHistogram() {
		if (data != NULL) delete [] data;
	}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();
};

#endif //ifndef __METHOD_COPY_HPP__
