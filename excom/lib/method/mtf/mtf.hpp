#ifndef _COMP_MTF_H_
#define _COMP_MTF_H_

/*
 * File:       method/mtf/mtf.cpp
 * Purpose:    Declaration of the MTF transform
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of MTF module linked to the ExCom library.
 *
 * MTF module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MTF module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MTF module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file mtf.hpp
 *  \brief Declaration of the MTF transfrom
 */

#include <excom.h>
#include "../../compmodule.hpp"

namespace MTFHelper {

template <typename T>
inline void XorSwap(T &x, T &y) {
	x ^= y ^= x ^= y;
}

}

class CompMTF : 
	public CompModule 
{
private:
	IOReader *_reader;
	IOWriter *_writer;

	void _InitAlphabet();	

public:
#ifndef ALPHABET_SIZE
	static const unsigned ALPHABET_SIZE = 256;
#endif

protected:
	char _alphabet[ALPHABET_SIZE];

public:	
	CompMTF(unsigned int handle);
	~CompMTF();

	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int checkConnection();
	int run();

	int Encode();
	int Decode();

};

inline CompMTF::CompMTF(unsigned int handle) : 
	CompModule(handle), _reader(NULL), _writer(NULL) 
{}

inline int CompMTF::checkConnection() {
	if (_reader == NULL || _writer == NULL) 
		return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

inline int CompMTF::setOperation(enum exc_oper_e operation) {
	if (operation == EXCOM_OP_COMPRESS || operation == EXCOM_OP_DECOMPRESS) {
		CompModule::operation = operation;
		return EXCOM_ERR_OK;
	} 
	return EXCOM_ERR_OPERATION;
}

inline void CompMTF::_InitAlphabet() {
	for (unsigned i = 0; i < ALPHABET_SIZE; ++i)
		_alphabet[i] = i;
}

#endif