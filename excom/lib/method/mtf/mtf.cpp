/*
 * File:       method/mtf/mtf.cpp
 * Purpose:    Implementation of the MTF transform
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of MTF module linked to the ExCom library.
 *
 * MTF module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MTF module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MTF module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file mtf.cpp
 *  \brief Implementation of the MTF transfrom
 */

#include "mtf.hpp"

CompMTF::~CompMTF() {}

int CompMTF::connectIOModule(IOModule *module, int connection_type) {
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (_reader != NULL) 
			return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) 
			return res;
		_reader = (IOReader *)ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (_writer != NULL) 
			return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) 
			return res;
		_writer = (IOWriter *)ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default: 
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompMTF::run() {
	int res = checkConnection();
	if (res != EXCOM_ERR_OK) 
		return res;

	_InitAlphabet();

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS)
		res = Encode();		
	else res = Decode();

#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}

int CompMTF::Encode() {
	int input;
	unsigned char i;
	char c;
	unsigned x = 0;

	while ((input = _reader->readByte()) != EOF) {
		c = _alphabet[i = 0];
		while (c != (char)input)
			MTFHelper::XorSwap(c, _alphabet[++i]);
		_alphabet[0] = c;
		_writer->writeByte(i);
	}
	_writer->eof();

	return EXCOM_ERR_OK;
}

int CompMTF::Decode() {
	int input;

	while ((input = _reader->readByte()) != EOF) {
		_writer->writeByte(_alphabet[input]);
		while (input-- != 0)
			MTFHelper::XorSwap(_alphabet[input], _alphabet[input + 1]);
	}
	_writer->eof();

	return EXCOM_ERR_OK;
}

