/*
 * File:       method/lzy/lzy.cpp
 * Purpose:    Implementation of the LZY compression methods
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of LZY compression module linked to the ExCom library.
 *
 * LZY compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZY compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZY module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lzy.cpp
 *  \brief Implementation of the LZY compression method
 */

#include "lzy.hpp"
#include <list>
#include <iostream>

CompLZY::CompLZY(unsigned int handle) : 
	CompModule(handle), 
	reader(NULL), 
	writer(NULL), 
	dict_capacity(DEFAULT_DICT_SIZE)
{
	char_bitsize = DEFAULT_CHAR_BITSIZE;
	readSymbol = &CompLZY::readByte;
	writeSymbol = &CompLZY::writeByte;
	root_size = 1 << DEFAULT_CHAR_BITSIZE;
	decodedString.reserve(512);
}

int CompLZY::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZY::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompLZY::setParameter(int parameter, void *value)
{
	if (value == NULL) 
		return EXCOM_ERR_MEMORY;

	unsigned int u = *((unsigned int*)value);

	switch (parameter) {
	case EXCOM_PARAM_LZY_DICTIONARY_SIZE:
		if (u < (1 << char_bitsize) || u == ~0u) 
			return EXCOM_ERR_VALUE;
		dict_capacity = u;
		break;
	case EXCOM_PARAM_LZY_CHAR_BITSIZE:
		if (u == 0 || u > 31 || dict_capacity < (1 << u))
			return EXCOM_ERR_VALUE;
		if ((char_bitsize = (unsigned char)u) == 8) {
			readSymbol = &CompLZY::readByte;
			writeSymbol = &CompLZY::writeByte;
		}	
		else {
			readSymbol = &CompLZY::readNBits;
			writeSymbol = &CompLZY::writeNBits;
		}
		root_size = 1 << char_bitsize;
		break;
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZY::getValue(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_LZY_DICTIONARY_SIZE:
		u = dict_capacity;
		break;
	case EXCOM_VALUE_LZY_CHAR_BITSIZE:
		u = char_bitsize;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}

void CompLZY::initializeDictionary() {
	encDict.clear();
	encDict.reserve(dict_capacity);

	lzw_node_enc_t lzwEncNode;
	lzwEncNode.first = ~0u;
	lzwEncNode.left = ~0u;
	lzwEncNode.right = ~0u;
	lzwEncNode.suffix = ~0u;
	lzwEncNode.prefixIndex = ~0u;

	while (encDict.size() < root_size) {
		lzwEncNode.symbol = encDict.size();
		encDict.push_back(lzwEncNode);
	}

	bitsize = char_bitsize;
	threshold = 1 << char_bitsize;
}

// The code is based on an idea of Juha Nieminen
// http://warp.povusers.org/EfficientLZW/ 
unsigned CompLZY::add(lzw_node_enc_t &node, bool *was_inserted) {
	unsigned index = encDict[node.prefixIndex].first;

	unsigned new_index = encDict.size() == dict_capacity ? ~0u : encDict.size();

	if (index == ~0u) {
		encDict[node.prefixIndex].first = new_index;
	} else {
		while (true) {
			if (encDict[index].symbol == node.symbol) {
				*was_inserted = false;
				return index;
			}

			if (encDict[index].symbol < node.symbol) {
				if (encDict[index].right == ~0u) {
					encDict[index].right = new_index;
					break; 
				}
				index = encDict[index].right;
			} else {
				if (encDict[index].left == ~0u) {
					encDict[index].left = new_index;
					break; 
				}        
				index = encDict[index].left;		
			}
		}
	}

	*was_inserted = true;

	if (encDict.size() != dict_capacity)
		encDict.push_back(node);
	else initializeDictionary();

	return new_index;
}

unsigned CompLZY::find(lzw_node_enc_t &node) const {
	unsigned index = encDict[node.prefixIndex].first;

	if (index != ~0u) {
		while (true) {
			if (encDict[index].symbol == node.symbol)
				return index;

			if (encDict[index].symbol < node.symbol) {
				if (encDict[index].right == ~0u) {
					break; 
				}
				index = encDict[index].right;
			} else {
				if (encDict[index].left == ~0u) {
					break; 
				}        
				index = encDict[index].left;		
			}
		}
	}

	return ~0u;
}

unsigned CompLZY::searchInsertNode(lzw_node_enc_t &node, bool *was_inserted) {
	if (node.prefixIndex == ~0u) {
		*was_inserted = false;
		return node.symbol;
	}

	return add(node, was_inserted);
}
unsigned CompLZY::searchNode(lzw_node_enc_t &node) const {
	if (node.prefixIndex == ~0u)
		return node.symbol;

	return find(node);
}

int CompLZY::runCompress() {
	lzw_node_enc_t lzwNode;
	lzwNode.first = ~0u;
	lzwNode.right = ~0u;
	lzwNode.left = ~0u;

	lzw_node_enc_t dict_node;
	dict_node.first = ~0u;
	dict_node.right = ~0u;
	dict_node.left = ~0u;
	dict_node.prefixIndex = ~0u;

	initializeDictionary();

	unsigned long c;
	unsigned index;

	unsigned new_phrase_idx;
	bool was_inserted;
	unsigned suffix;
	std::vector<unsigned> buffer;
	buffer.reserve(256);
	std::vector<unsigned>::const_iterator it;

	if ((this->*readSymbol)(&c) != EOF) {
		lzwNode.prefixIndex = (unsigned)c;
		buffer.push_back((unsigned)c);

		while ((this->*readSymbol)(&c) != EOF) {	
			lzwNode.symbol = (unsigned)c;

			if ((index = searchNode(lzwNode)) != ~0u) { //find out where this new phrase should be living
				lzwNode.prefixIndex = index;
				buffer.push_back((unsigned)c);
				continue;
			}
			
			//phrase does not exist
			writer->writeNBits(bitsize, lzwNode.prefixIndex);

			for (it = buffer.begin(); it != buffer.end(); ++it) {	
				dict_node.symbol = *it; //read next character C from buffer

				new_phrase_idx = searchInsertNode(dict_node, &was_inserted); //find or insert new phrase
				if (!was_inserted)
					dict_node.prefixIndex = new_phrase_idx;
				else {
					if (new_phrase_idx == ~0u) { //check if dict was filled and erased in insert process
						dict_node.prefixIndex = ~0u; //it was so start from beginning
						goto skip_insert; //and skip all other insert actions
					}
					suffix = encDict[dict_node.prefixIndex].suffix; //get suffix index of parent of new entry
					while (suffix != ~0u) { //until suffix is valid
						dict_node.prefixIndex = suffix;
						//find or insert suffix of inserted phrase, set suffix index of last inserted phrase to this index and change last inserted phrase to this one
						new_phrase_idx = encDict[new_phrase_idx].suffix = searchInsertNode(dict_node, &was_inserted); 
						suffix = encDict[suffix].suffix; //get suffix of suffix for next iteration

						if (!was_inserted) { //suffix exists, all other suffixes must also exist so just leave
							dict_node.prefixIndex = new_phrase_idx;
							goto no_more_suffixes;
						}
						else if (new_phrase_idx == ~0u) { //check if dict was filled and erased in insert process
							dict_node.prefixIndex = ~0u; //it was so start from beginning
							goto skip_insert; //and skip all other insert actions
						}
					}
					dict_node.prefixIndex = *it; //parent phrase is set to character C
					encDict[new_phrase_idx].suffix = *it; //also set this phrase as suffix of last inserted phrase
no_more_suffixes:;
				}
			}
			
			//modify bitsize if and while necessary
			if (encDict.size() != dict_capacity) {
				while (encDict.size() > threshold) {
					threshold <<= 1;
					++bitsize;
				}
			}
skip_insert:
			buffer.clear();
			buffer.push_back((unsigned)c);
			lzwNode.prefixIndex = (unsigned)c;
		}
	}
	
	writer->writeNBits(bitsize, lzwNode.prefixIndex);
	writer->eof();

	return EXCOM_ERR_OK;
}

int CompLZY::runDecompress() {
	unsigned long input;

	lzw_node_enc_t lzwNode;
	lzwNode.first = ~0u;
	lzwNode.right = ~0u;
	lzwNode.left = ~0u;
	lzwNode.prefixIndex = ~0u;

	initializeDictionary();

	unsigned index;

	unsigned new_phrase_idx;
	bool was_inserted;
	unsigned suffix;

	std::vector<unsigned>::const_reverse_iterator it;

	while (reader->readNBits(bitsize, &input) != EOF) {
		if (input >= encDict.size())
			return EXCOM_ERR_FORMAT;

		decodedString.clear();
		index = (unsigned)input;
		do {
			lzw_node_enc_t &node = encDict[index];
			decodedString.push_back(node.symbol);
			index = node.prefixIndex;
		} while (index != ~0u);

		//insert F + G and all suffixes into dictionary

		for (it = decodedString.rbegin(); it != decodedString.rend(); ++it) {
			(this->*writeSymbol)(*it);		
			lzwNode.symbol = *it; //read next character C from buffer

			new_phrase_idx = searchInsertNode(lzwNode, &was_inserted); //find or insert new phrase
			if (!was_inserted) 
				lzwNode.prefixIndex = new_phrase_idx;
			else {
				if (new_phrase_idx == ~0u) { //check if dict was filled and erased in insert process
					while (++it != decodedString.rend()) //it was so output the rest of phrase...
						(this->*writeSymbol)(*it);
					lzwNode.prefixIndex = ~0u;
					goto skip_insert; //...and skip all other insert actions
				}
				suffix = encDict[lzwNode.prefixIndex].suffix; //get suffix index of parent of new entry
				while (suffix != ~0u) { //until suffix is valid
					lzwNode.prefixIndex = suffix;

					//find or insert suffix of inserted phrase, set suffix index of last inserted phrase to this index and change last inserted phrase to this one
					new_phrase_idx = encDict[new_phrase_idx].suffix = searchInsertNode(lzwNode, &was_inserted); 
					suffix = encDict[suffix].suffix; //get suffix of suffix for next iteration

					if (!was_inserted) { //suffix exists, all other suffixes must also exist so just leave
						lzwNode.prefixIndex = new_phrase_idx;
						goto no_more_suffixes;
					}
					else if (new_phrase_idx == ~0u) { //check if dict was filled and erased in insert process
						while (++it != decodedString.rend()) //it was so output the rest of phrase...
							(this->*writeSymbol)(*it);
						lzwNode.prefixIndex = ~0u;
						goto skip_insert; //...and skip all other insert actions
					}
				}
				lzwNode.prefixIndex = *it; //parent phrase is set to character C
				encDict[new_phrase_idx].suffix = *it; //also set this phrase as suffix of last inserted phrase
no_more_suffixes:;
			}
		}
			
		//modify bitsize if and while necessary
		if (encDict.size() != dict_capacity) {
			while (encDict.size() > threshold) {
				threshold <<= 1;
				++bitsize;
			}
		}
skip_insert:;
	}
	
	writer->eof();

	return EXCOM_ERR_OK;
}

int CompLZY::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}


#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}
