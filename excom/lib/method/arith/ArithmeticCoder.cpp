/*
 * File:       method/arith/ArithmeticCoder.cpp
 * Purpose:    Declaration of ACB compression method (Associative Coder of Buyanovsky)
 *
 * Based on implementation from Arithmetic Encoding and Decoding Library by
 * Michael Dipperstein, http://michael.dipperstein.com/arithmetic/
 * 
 * Copyright (C) 2004 Michael Dipperstein
 * Copyright (C) 2011 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "ArithmeticCoder.hpp"

namespace arithcoder {

AlphabetInfo::AlphabetInfo(unsigned int size) {
    this->size = size;
    this->upper_bound = size + 1;
    this->ranges = new probability_t[upper_bound + 1];
    this->symbolToIndexMapping = new int[upper_bound + 1];
    this->indexToSymbolMapping = new int[upper_bound + 1];

    this->cumulativeProb = 0;
    this->ranges[0] = 0;          // absolute lower range
    this->symbolToIndexMapping[0] = 0;
    this->indexToSymbolMapping[0] = 0;

    // assign upper and lower probability ranges assuming
    for (int c = 1; c <= upper_bound; c++) {
        this->ranges[c] = this->ranges[c - 1] + 1;
        this->symbolToIndexMapping[c] = c;
        this->indexToSymbolMapping[c] = c;
        this->cumulativeProb++;
    }
}

AlphabetInfo::~AlphabetInfo() {
    delete [] ranges;
    delete [] symbolToIndexMapping;
    delete [] indexToSymbolMapping;
}

// ===============================================================
// ===================== Arithmetic coder ========================
// ===============================================================
Coder::Coder()  {
    // initialize coder start with full probability range [0%, 100%)
    lower = 0;
    upper = ~0;                     // all ones
    underflowBits = 0;
    code = 0;

    _mask_bit_0 = MASK_BIT(0);
    _mask_bit_1 = MASK_BIT(1);
}

Coder::~Coder() {}

void Coder::updateLowerUpper(AlphabetInfo* alphabet, int symbol) {
    unsigned long range;        // must be able to hold max upper + 1
    unsigned long rescaled;     // range rescaled for range of new symbol
                                // must hold range * max upper

    // Calculate new upper and lower ranges.  Since the new upper range is
    // dependant of the old lower range, compute the upper range first.
    range = (unsigned long)(upper - lower) + 1;         // current range

    // as ranges are sorted from lowest to highest occurance
    // they are not sorted by symbol and have to be mapped
    probability_t symbolIndex = alphabet->symbolToIndexMapping[symbol];

    // scale upper range of the symbol being coded
    rescaled = (unsigned long)alphabet->ranges[UPPER(symbolIndex)] * range;
    rescaled /= (unsigned long)alphabet->cumulativeProb;

    // new upper = old lower + rescaled new upper - 1
    upper = lower + (probability_t)rescaled - 1;

    // scale lower range of the symbol being coded
    rescaled = (unsigned long)alphabet->ranges[LOWER(symbolIndex)] * range;
    rescaled /= (unsigned long)alphabet->cumulativeProb;

    // new lower = old lower + rescaled new upper
    lower = lower + (probability_t)rescaled;

#if BUILD_DEBUG_OUTPUT
    printf("Low: %d, High:%d\n", lower, upper);
    if (lower > upper) {
        /* compile this in when testing new models. */
        fprintf(stderr, "Panic: lower (%X)> upper (%X)\n", lower, upper);
    }
#endif
}

// ===============================================================
// ==================== Arithmetic encoder =======================
// ===============================================================
Encoder::Encoder(::IOWriter* writer): Coder() {
    this->writer = writer;
    init();
}

Encoder::~Encoder() {}

void Encoder::init() {}

void Encoder::writeEncodedBits() {
    for (;;) {
        if ((upper & _mask_bit_0) == (lower & _mask_bit_0)) {
            // MSBs match, write them to output file
            writer->writeNBits(1, (upper & _mask_bit_0) != 0);
            // we can write out underflow bits too
            while (underflowBits > 0) {
                writer->writeNBits(1, (upper & _mask_bit_0) == 0);
                underflowBits--;
            }
        } else
        if ((lower & _mask_bit_1) && !(upper & _mask_bit_1)) {
            // Possible underflow condition: neither MSBs nor second MSBs
            // match.  It must be the case that lower and upper have MSBs of
            // 01 and 10.  Remove 2nd MSB from lower and upper.
            underflowBits += 1;
            lower &= ~(_mask_bit_0 | _mask_bit_1);
            upper |= _mask_bit_1;

            // The shifts below make the rest of the bit removal work.  If
            // you don't believe me try it yourself.
        } else {
            // nothing left to do
            return;
        }

        // Shift out old MSB and shift in new LSB.  Remember that lower has
        // all 0s beyond it's end and upper has all 1s beyond it's end.
        lower <<= 1;
        upper <<= 1;
        upper |= 1;
    }
    return;
}

void Encoder::writeRemaining() {
    writer->writeNBits(1, (lower & _mask_bit_1) != 0);

    // write out any unwritten underflow bits
    for (underflowBits++; underflowBits > 0; underflowBits--) {
        writer->writeNBits(1, (lower & _mask_bit_1) == 0);
    }
}

void Encoder::add(AlphabetInfo* alphabet, int symbol) {
    insert(alphabet, symbol);
    writeEncodedBits();
}

void Encoder::flush(AlphabetInfo* alphabet) {
    // encode EOF
    insert(alphabet, alphabet->size);
    writeEncodedBits();

    // write out least significant bits
    writeRemaining();
    return;
}

// ===============================================================
// ==================== Arithmetic decoder =======================
// ===============================================================
Decoder::Decoder(::IOReader* reader): Coder() {
    this->reader = reader;
    init();
}

Decoder::~Decoder() {}

void Decoder::init() {
    code = 0;
    // read PERCISION MSBs of code one bit at a time
    for (int i = 0; i < PRECISION; i++) {
        code <<= 1;

        unsigned long b = 0;
        reader->readNBits(1, &b);
        // treat EOF like 0
        code |= b;
    }
}

probability_t Decoder::getUnscaledCode(AlphabetInfo* alphabet) {
    unsigned long range;        // must be able to hold max upper + 1
    unsigned long unscaled;

    range = (unsigned long)(upper - lower) + 1;

    // reverse the scaling operations from ApplySymbolRange
    unscaled = (unsigned long)(code - lower) + 1;
    unscaled = unscaled * (unsigned long)alphabet->cumulativeProb - 1;
    unscaled /= range;

    return ((probability_t) unscaled);
}

int Decoder::getSymbolFromProbability(AlphabetInfo* alphabet, probability_t probability) {
    int first = 0;
    int last = alphabet->upper_bound;
    int middle = last / 2;

    // binary search
    while (last >= first) {
        if (probability < alphabet->ranges[LOWER(middle)]) {
            // lower bound is higher than probability
            last = middle - 1;
            middle = first + ((last - first) / 2);
            continue;
        }

        if (probability >= alphabet->ranges[UPPER(middle)]) {
            // upper bound is lower than probability
            first = middle + 1;
            middle = first + ((last - first) / 2);
            continue;
        }

        // we must have found the right value
        return alphabet->indexToSymbolMapping[middle];
    }

    // error: none of the ranges include the probability
    fprintf(stderr, "Unknown Symbol: %d (max: %d)\n", probability,
        alphabet->ranges[alphabet->upper_bound]);
    return -1;
}

void Decoder::readEncodedBits() {
    for (;;) {
        if ((upper & _mask_bit_0) == (lower & _mask_bit_0)) {
            // MSBs match, allow them to be shifted out
        } else
        if ((lower & _mask_bit_1) && !(upper & _mask_bit_1)) {
            // Possible underflow condition: neither MSBs nor second MSBs
            // match.  It must be the case that lower and upper have MSBs of
            // 01 and 10.  Remove 2nd MSB from lower and upper.
            lower  &= ~(_mask_bit_0 | _mask_bit_1);
            upper  |= _mask_bit_1;
            code   ^= _mask_bit_1;

            // the shifts below make the rest of the bit removal work
        } else {
            // nothing to shift out
            return;
        }

        // Shift out old MSB and shift in new LSB.  Remember that lower has
        // all 0s beyond it's end and upper has all 1s beyond it's end.
        lower <<= 1;
        upper <<= 1;
        upper |= 1;
        code <<= 1;

        // next bit from encoded input
        unsigned long bit = 0;
        if (reader->readNBits(1, &bit) != EOF) {
            // add next encoded bit to code
            code |= bit;
        }
    }
    return;
}

int Decoder::read(AlphabetInfo* alphabet) {
    // get the unscaled probability of the current symbol
    probability_t unscaled = getUnscaledCode(alphabet);
    // figure out which symbol has the above probability
    int symbol = 0;
    if((symbol = getSymbolFromProbability(alphabet, unscaled)) == -1) {
        // error: unknown symbol
        return alphabet->size;
    }

    // factor out symbol
    insert(alphabet, symbol);
    readEncodedBits();
    return symbol;
}

// ===============================================================
// ================= Adaptive arithmetic coder ===================
// ===============================================================
ACoder::ACoder() {}

ACoder::~ACoder() {}

void ACoder::updateProbabilities(AlphabetInfo* alphabet, int symbol) {
    // for updating dynamic models
    int i;
    probability_t original;     // range value prior to rescale
    probability_t delta;        // range for individual symbol

    // add new symbol to model
    alphabet->cumulativeProb++;

    int symbolIndex = alphabet->symbolToIndexMapping[symbol];
    if(symbolIndex < alphabet->size) {
        int lowerIndex = LOWER(symbolIndex);
        int upperIndex = UPPER(symbolIndex);
        int newSymbolRange = alphabet->ranges[upperIndex] - alphabet->ranges[lowerIndex] + 1;
        do {
            lowerIndex += 1;    // lowerIndex = LOWER(symbolIndex) + 1;
            upperIndex += 1;    // upperIndex = UPPER(symbolIndex) + 1
            int rangeUpper = alphabet->ranges[upperIndex] - alphabet->ranges[lowerIndex];
            if(newSymbolRange > rangeUpper) {
                // probability of symbol is gearter then of the one on his right
                // so i will shift it to higher position
                // and update symbol to index and index to symbol mappings
                int oldSymbolToIndex = alphabet->symbolToIndexMapping[symbol];
                alphabet->symbolToIndexMapping[symbol] = lowerIndex;
                alphabet->symbolToIndexMapping[alphabet->indexToSymbolMapping[lowerIndex]] = oldSymbolToIndex;

                int oldIndexToSymbol = alphabet->indexToSymbolMapping[lowerIndex];
                alphabet->indexToSymbolMapping[lowerIndex] = symbol;
                alphabet->indexToSymbolMapping[symbolIndex] = oldIndexToSymbol;
                symbolIndex++;
            } else {
                // nothing more to move
                break;
            }
        } while(symbolIndex < alphabet->size);
    }

    for (i = UPPER(symbolIndex); i <= alphabet->upper_bound; i++) {
        alphabet->ranges[i] += 1;
    }

    // half current values if cumulativeProb is too large
    if (alphabet->cumulativeProb >= MAX_PROBABILITY) {
        alphabet->cumulativeProb = 0;
        original = 0;

        for (i = 1; i <= alphabet->upper_bound; i++) {
            delta = alphabet->ranges[i] - original;
            if (delta <= 2) {
                // prevent probability from being 0
                original = alphabet->ranges[i];
                alphabet->ranges[i] = alphabet->ranges[i - 1] + 1;
            } else {
                original = alphabet->ranges[i];
                alphabet->ranges[i] = alphabet->ranges[i - 1] + (delta / 2);
            }
            alphabet->cumulativeProb += (alphabet->ranges[i] - alphabet->ranges[i - 1]);
        }
    }
#if BUILD_DEBUG_OUTPUT
    // dump list of ranges
    for (int c = 0; c < UPPER(alphabet->size); c++) {
        int symbol = alphabet->indexToSymbolMapping[c];
        printf("%02X\t%02X\t%d\t%d\n", c, symbol, alphabet->ranges[LOWER(c)], alphabet->ranges[UPPER(c)]);
    }
#endif
}

// ===============================================================
// ================ Adaptive arithmetic encoder ==================
// ===============================================================
AEncoder::AEncoder(::IOWriter* writer): Encoder(writer), ACoder() {}

AEncoder::~AEncoder() {}

void AEncoder::insert(AlphabetInfo* alphabet, int symbol) {
    updateLowerUpper(alphabet, symbol);
    updateProbabilities(alphabet, symbol);
}

// ===============================================================
// ================ Adaptive arithmetic decoder ==================
// ===============================================================
ADecoder::ADecoder(::IOReader* reader): Decoder(reader), ACoder() {}

ADecoder::~ADecoder() {}

void ADecoder::insert(AlphabetInfo* alphabet, int symbol) {
    updateLowerUpper(alphabet, symbol);
    updateProbabilities(alphabet, symbol);
}

// ===============================================================
// ================== Static arithmetic coder ====================
// ===============================================================
SCoder::SCoder() {}

SCoder::~SCoder() {}

void SCoder::symbolCountToProbabilityRanges(AlphabetInfo* alphabet) {
    int c;

    // absolute lower bound is 0
    alphabet->ranges[0] = 0;
    // add 1 EOF character
    alphabet->ranges[alphabet->upper_bound] = 1;
    alphabet->cumulativeProb++;

    /* assign upper and lower probability ranges */
    for (c = 1; c <= alphabet->upper_bound; c++) {
        alphabet->ranges[c] += alphabet->ranges[c - 1];
    }

#if BUILD_DEBUG_OUTPUT
    // dump list of ranges
    for (c = 0; c < alphabet->upper_bound; c++) {
        printf("%02X\t%d\t%d\n", c, alphabet->ranges[LOWER(c)], alphabet->ranges[UPPER(c)]);
    }
#endif

    return;
}

// ===============================================================
// ================= Static arithmetic encoder ===================
// ===============================================================
SEncoder::SEncoder(::IOWriter* writer): Encoder(writer), SCoder() {}

SEncoder::~SEncoder() {}

void SEncoder::insert(AlphabetInfo* alphabet, int symbol) {
    updateLowerUpper(alphabet, symbol);
}

int SEncoder::buildProbabilityRangeList(AlphabetInfo* alphabet, ::IOReader* reader) {
    int c;

    // unsigned long is used to hold the largest counts we can have without
    // any rescaling.  Rescaling may take place before probability ranges
    // are computed.

    unsigned long countArray[alphabet->size];
    unsigned long totalCount = 0;
    unsigned long rescaleValue;

    // start with no symbols counted
    for (c = 0; c < alphabet->size; c++) {
        countArray[c] = 0;
    }

    unsigned int bitSize = BIT_SIZE(alphabet->size);
    unsigned long code = 0;
    while (reader->readNBits(bitSize, &code)!= EOF) {
        countArray[code]++;
        totalCount++;
    }

    // rescale counts to be < MAX_PROBABILITY
    if (totalCount >= MAX_PROBABILITY) {
        rescaleValue = (totalCount / MAX_PROBABILITY) + 1;

        for (c = 0; c < alphabet->size; c++) {
            if (countArray[c] > rescaleValue) {
                countArray[c] /= rescaleValue;
            } else if (countArray[c] != 0) {
                countArray[c] = 1;
            }
        }
    }

    // copy scaled symbol counts to range list
    alphabet->ranges[0] = 0;
    alphabet->cumulativeProb = 0;
    for (c = 0; c < alphabet->size; c++) {
        alphabet->ranges[UPPER(c)] = countArray[c];
        alphabet->cumulativeProb += countArray[c];
    }

    // convert counts to a range of probabilities
    symbolCountToProbabilityRanges(alphabet);
    reader->rewind();
    return EXCOM_ERR_OK;
}

int SEncoder::writeHeader(AlphabetInfo* alphabet, ::IOWriter* writer) {
#if BUILD_DEBUG_OUTPUT
    printf("HEADER:\n");
#endif
    // symbol count so far
    probability_t previous = 0;
    unsigned int bitSize = BIT_SIZE(alphabet->size);

    for(int c = 0; c <= (alphabet->size - 1); c++) {
        if (alphabet->ranges[UPPER(c)] > previous) {
            // some of these symbols will be encoded
            writer->writeNBits(bitSize, c);
            // symbol count
            previous = (alphabet->ranges[UPPER(c)] - previous);

#if BUILD_DEBUG_OUTPUT
            printf("%02X\t%d\n", c, previous);
#endif

            // write out PRECISION - 2 bit count
            writer->writeNBits((PRECISION - 2), previous);

            // current upper range is previous for the next character
            previous = alphabet->ranges[UPPER(c)];
        }
    }

    // now write end of table (zero count)
    writer->writeNBits(bitSize, 0);
    previous = 0;
    writer->writeNBits(PRECISION - 2, previous);
    return EXCOM_ERR_OK;
}

// ===============================================================
// ================= Static arithmetic decoder ===================
// ===============================================================
SDecoder::SDecoder(::IOReader* reader): Decoder(reader), SCoder() {}
SDecoder::~SDecoder() {}
void SDecoder::insert(AlphabetInfo* alphabet, int symbol) {
    updateLowerUpper(alphabet, symbol);
}

int SDecoder::readHeader(AlphabetInfo* alphabet, ::IOReader* reader) {
#if BUILD_DEBUG_OUTPUT
    printf("HEADER:\n");
#endif

    unsigned long count;
    alphabet->cumulativeProb = 0;

    for (int c = 0; c <= alphabet->upper_bound; c++) {
        alphabet->ranges[UPPER(c)] = 0;
    }

    unsigned int bitSize = BIT_SIZE(alphabet->size);
    // read [character, probability] sets
    for (;;) {
        unsigned long c = 0;
        reader->readNBits(bitSize, &c);
        count = 0;

       // read (PRECISION - 2) bit count */
       if (reader->readNBits((PRECISION - 2), &count) == EOF) {
            // premature EOF
            fprintf(stderr, "Error: unexpected EOF\n");
            return EXCOM_ERR_INCOMPLETE;
       }

#if BUILD_DEBUG_OUTPUT
        printf("%02X\t%d\n", c, count);
#endif

        if (count == 0) {
            // 0 count means end of header
            break;
        }

        alphabet->ranges[UPPER(c)] = count;
        alphabet->cumulativeProb += count;
    }

    // convert counts to range list
    symbolCountToProbabilityRanges(alphabet);
    return EXCOM_ERR_OK;
}
} // namespace arithcoder