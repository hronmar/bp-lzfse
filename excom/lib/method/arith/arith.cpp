/*
 * File:       method/arith/arith.hpp
 * Purpose:    Declaration of a arithmetic compression method
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2011
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Michal Valach
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "arith.hpp"

int CompArith::connectIOModule(IOModule *module, int connection_type) {
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompArith::setParameter(int parameter, void *value) {	
	switch (parameter) {
	case EXCOM_PARAM_ARITH_STATIC:
                if (value != NULL) return EXCOM_ERR_MEMORY;
                staticCoding = true;
		break;	
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompArith::getValue(int parameter, void *value) {
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_ARITH_STATIC:
		u = staticCoding;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}

int CompArith::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompArith::checkConnection()
{
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompArith::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

        if(operation == EXCOM_OP_COMPRESS) {
            res = compress();
        } else {
            res = decompress();
        }
#ifdef PERF_MON
	PERF_STOP
#endif

	return EXCOM_ERR_OK;
}

int CompArith::compress() {

    writer->writeNBits(1, staticCoding ? 1 : 0);
    unsigned int codeLength = BIT_SIZE(ALPHABET_SIZE);
    writer->writeNBits(4, codeLength);

    arithcoder::Encoder* encoder = NULL;
    arithcoder::AlphabetInfo* alphabet = new arithcoder::AlphabetInfo(ALPHABET_SIZE);

    if(staticCoding) {
        // initialize probabilities
        arithcoder::SEncoder::buildProbabilityRangeList(alphabet, reader);
        // store probabilities to output stream
        arithcoder::SEncoder::writeHeader(alphabet, writer);
        
        encoder = new arithcoder::SEncoder(writer);
    } else {
        encoder = new arithcoder::AEncoder(writer);
    }
    
    unsigned long data = 0;
    while (reader->readNBits(codeLength, &data) != EOF) {
        encoder->add(alphabet, data);
    }

    encoder->flush(alphabet);   
    writer->eof();

    delete encoder;
    delete alphabet;

    return EXCOM_ERR_OK;
}

int CompArith::decompress() {

    unsigned long value = 0;
    reader->readNBits(1, &value);
    staticCoding = value ? true : false;  
    reader->readNBits(4, &value);
    unsigned int alphabetSize = 1 << value;

    arithcoder::Decoder* decoder =  NULL;
    arithcoder::AlphabetInfo* alphabet = new arithcoder::AlphabetInfo(alphabetSize);

    if(staticCoding) {
        // init probabilities from output stream
        arithcoder::SDecoder::readHeader(alphabet, reader);
        decoder = new arithcoder::SDecoder(reader);
    } else {
        decoder = new arithcoder::ADecoder(reader);
    }

    int byte = 0;
    while ((byte = decoder->read(alphabet)) != alphabet->size) {
        writer->writeByte(byte);
    }
    
    writer->eof();

    delete decoder;
    delete alphabet;

    return EXCOM_ERR_OK;
}

