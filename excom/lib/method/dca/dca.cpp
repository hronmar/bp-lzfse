/*
 * File:       method/dca/dca.cpp
 * Purpose:    Implementation of a wrapper class for DCA compression method
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of DCA module linked to the ExCom library.
 *
 * DCA module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DCA module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DCA module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "dca.hpp"

int CompDCA::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		// the comp. module mustn't have a reader module
		// attached already
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	case EXCOM_CONN_DCA_EXC_INPUT:
		if (exc_rd != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		exc_rd = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DCA_EXC_OUTPUT:
		if (exc_wr != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		exc_wr = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompDCA::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompDCA::setParameter(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_PARAM_DCA_MAXLENGTH:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		//NOTE! if bigger values than 255 should be allowed,
		//it's also necessary to correct DCAcompressor::compress
		//and DCAcompressor::decompress to store this value
		//as more than one byte
		if (u < 1 || u > 255) return EXCOM_ERR_VALUE;
		compr->ad.maxlevel = u;
		break;
	case EXCOM_PARAM_DCA_RAW:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u == 0)
			compr->raw = false;
		else
			compr->raw = true;
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompDCA::getValue(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_DCA_MAXLENGTH:
		u = compr->ad.maxlevel;
		break;
	case EXCOM_VALUE_DCA_RAW:
		if (compr->raw) u = 1;
		else u = 0;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}

int CompDCA::checkConnection()
{
	// reader and writer are required for both operations
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	// for each operation one additional I/O module is required
	if (operation == EXCOM_OP_COMPRESS) {
		if (exc_wr == NULL) return EXCOM_ERR_CHAIN;
		// the writer must be seekable
		if (writer->seekable != 1) return EXCOM_ERR_SEEKABLE;
	}
	if (operation == EXCOM_OP_DECOMPRESS && exc_rd == NULL)
		return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompDCA::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = compr->compress(reader, writer, exc_wr);
	} else if (operation == EXCOM_OP_DECOMPRESS) {
		res = compr->decompress(reader, writer, exc_rd);
	}

#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}


