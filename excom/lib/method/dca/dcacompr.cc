/*
 *  ================================================
 *
 *    DCA (Data Compression using Antidictionaries)
 *    2006 Martin Fiala
 *    Diploma thesis
 *
 *    Faculty of Electrical Engineering
 *    Department of Computer Science and Engineering
 *    Diploma thesis leader: Ing. Jan Holub, Ph.D.
 *
 *  ================================================
 *
 *    dcacompr.cc - DCA compression & decompression class
 *
 *  Changes:
 *    090402 by Filip Simek <simekf1@fel.cvut.cz>
 *           incorporated as a module to ExCom library
 *
 *
 * This file is part of DCA module linked to the ExCom library.
 *
 * DCA module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DCA module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DCA module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <queue>
#include <stack>
#include <errno.h>

#include "dca.hpp"

using namespace dca;

inline DCAstate* DCAantidict::allocDCAstate(bool bb, int level)
{
	if (stPool == stPoolLimit) {
		stPool = new DCAstate[ALLOCATE_BURST];
		stPoolLimit = stPool + ALLOCATE_BURST - 1;
		statePool.push_back(stPool);
	} else {
		stPool++;
	}

	stPool->level = level;
	stPool->b = bb;
	stPool->next[0] = NULL;
	stPool->next[1] = NULL;
	return stPool;
}

/**
 * auxiliary function for trie building, returns appropriate
 * existing node or creates a new one
 * @param   p   node
 * @param   a   next input char
 * @return      node following p when input a
 */
DCAstate* DCAantidict::next(DCAstate* p, bool a, int level)
{
	if ((unsigned int)level > maxlevel) {
		p = p->fail;
		level--;
	}

	if (p->next[a] != NULL) {
		return p->next[a];
	} else {
		// DCAstate size is 52 bytes (but could be lowered)
		// first 15 level (1.7MB) - 32k nodes could be represented by heap
		// other nodes could be allocated in std::vector<array> pools

		// states are allocated in bursts
		DCAstate* q = allocDCAstate(a, level);
		totalNodes++;

		p->next[a] = q;

		if (p == trie) {
			q->fail = trie;
		} else {
			// can we remove this recursion?
			q->fail = next(p->fail, a, level - 1);
		}
		return q;
	}
}

int DCAantidict::compressData(IOReader *in, IOWriter *out, IOWriter *exc)
{

	unsigned char c;
	bool b;
	int i;
	DCAstate* cur;
	DCAstate* p;
	unsigned char chr = 0;
	int chri = 0;
	unsigned int except = 0, compressed = 0, passed = 0;
	int res;
	unsigned int distance = 0, maxdistance = 0, totaldistance = 0;

	trie = allocDCAstate(false);
	totalNodes++;
	trie->level = 0;
	trie->fail = trie;
	cur = trie;

	originalLen = 0;
	while (res = in->readByte(), res != EOF) {
		originalLen++;
		c = (unsigned char)res;
		for (i = 1; i <= 8; i++) {
			b = c & 0x80;

			p = cur;
			if ((unsigned int)p->level == maxlevel) {
				p = p->fail;
			}
			while (p->next[0] == NULL && p->next[1] == NULL && p != trie) {
				p = p->fail;
			}

			if (p->next[b] == NULL) {
				if (p->next[1-b] != NULL) { // exception
					except++;
					unsigned int exceptsize;
					fibCompress(distance, &exceptsize);
					if (distance > maxdistance)
						maxdistance = distance;
					totaldistance += distance;
					unsigned short dist = distance;
					// output the exception in little endian
					exc->writeByte((unsigned char)(dist & 0xff));
					exc->writeByte((unsigned char)((dist >> 8) & 0xff));
					distance = 0;
				} else {
					passed++;
					chr = (chr << 1) | b;
					chri++;
				}
			} else {
				if (p->next[1-b] != NULL) { // output and continue
					passed++;
					chr = (chr << 1) | b;
					chri++;
				} else { // erase this character
					compressed++;
					distance++;
				}
			}

			if (chri == 8) {
				chri = 0;
				out->writeByte(chr);
			}

			cur = next(cur, b, cur->level + 1);
			c = c << 1;
		}
	}

	if (chri > 0) {
		chr = chr << (8 - chri);
		out->writeByte(chr);
	}
	out->eof();
	exc->eof();
	return EXCOM_ERR_OK;
}

int DCAantidict::decompressData(IOReader *in, IOWriter *out, IOReader *exc)
{
	bool b;
	unsigned int i = 0, j;
	DCAstate* cur;
	DCAstate* p;
	unsigned char chr = 0;
	int chri = 0;
	int compressed = 0, passed = 0;
	unsigned short except = 0;
	int res, i1, i2;
	unsigned char c_in;
	int distance = 0;

	trie = allocDCAstate(false);
	totalNodes++;
	trie->level = 0;
	trie->fail = trie;
	cur = trie;

	i1 = exc->readByte();
	i2 = exc->readByte();
	if (i1 == EOF || i2 == EOF) {
		// can't read exception file, possible end
		except = 0x7fff;
	} else {
		except = ((unsigned short)i1) + (((unsigned short)i2) << 8);
	}

	// read first byte from input
	res = in->readByte();
	if (res == EOF) {
		return EXCOM_ERR_INCOMPLETE;
	}
	c_in = (unsigned char)res;
	while (i < originalLen) {
		i++;

		for (j = 1; j <= 8; j++) {
			p = cur;
			if ((unsigned int)p->level == maxlevel) {
				p = p->fail;
			}
			while (p->next[0] == NULL && p->next[1] == NULL && p != trie) {
				p = p->fail;
			}

			b = (p->next[0] == NULL) ^(p->next[1] == NULL);
			if (!b) {
				if (chri == 8) {
					// read next byte from input
					res = in->readByte();
					if (res == EOF) {
						out->eof();
						return EXCOM_ERR_INCOMPLETE;
					}
					c_in = (unsigned char)res;
					chri = 0;
				}

				b = c_in & 0x80;
				c_in = c_in << 1;
				chri++;

				passed++;
			} else {
				b = p->next[0] == NULL;
				if (except > distance) { // decompress
					compressed++;
					distance++;
				} else { // exception
					b = !b;
					i1 = exc->readByte();
					i2 = exc->readByte();
					if (i1 == EOF || i2 == EOF) {
						// can't read exception file, possible end
						except = 0x7fff;
					} else {
						except = ((unsigned short)i1) + (((unsigned short)i2) << 8);
					}
					distance = 0;
				}
			}

			chr = (chr << 1) | b;

			cur = next(cur, b, cur->level + 1);
		}

		out->writeByte(chr);
	}
	out->eof();

	return EXCOM_ERR_OK;
}

int DCAcompressor::compress(IOReader *reader, IOWriter *writer, IOWriter *exc_wr)
{
	int res;

	if (!raw) {
		// write header
		writer->writeByte(DCA_VERSION);
		writer->writeByte((unsigned char)ad.maxlevel);
	}
	// a placeholder for original file length, which will be known after
	// the compression
	writer->writeNBits(32, 0);

	res = ad.compressData(reader, writer, exc_wr);
	if (res != EXCOM_ERR_OK) return res;

	if (!raw) {
		writer->immWriteBytes(2, 4, ad.originalLen);
	} else {
		writer->immWriteBytes(0, 4, ad.originalLen);
	}

	return EXCOM_ERR_OK;
}

int DCAcompressor::decompress(IOReader *reader, IOWriter *writer, IOReader *exc_rd)
{
	int x;

	if (!raw) {
		// read header
		x = reader->readByte();
		if (x == EOF) return EXCOM_ERR_INCOMPLETE;
		if (x > DCA_VERSION) {
			return EXCOM_ERR_FORMAT;
		}

		x = reader->readByte();
		if (x == EOF) return EXCOM_ERR_INCOMPLETE;
		if (x < 1 || x > 255) {
			return EXCOM_ERR_FORMAT;
		}
		ad.maxlevel = (unsigned int)x;
	}

	// originalLen is important for DCA and is therefore present even in raw mode
	x = reader->readNBits(32, &ad.originalLen);
	if (x == EOF) return EXCOM_ERR_INCOMPLETE;

	x = ad.decompressData(reader, writer, exc_rd);
	if (x != EXCOM_ERR_OK) return x;

	return EXCOM_ERR_OK;
}


