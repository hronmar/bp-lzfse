#ifndef FIBCOMPR_H
#define FIBCOMPR_H

#define FIBSIZE 46
#define FIBINDEXSIZE 1024

void fibInit();
unsigned int fibDecompress(unsigned int num, unsigned int size);
unsigned int fibCompress(unsigned int num, unsigned int* size);

#endif
