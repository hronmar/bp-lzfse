#ifndef __DCACOMPR_H__
#define __DCACOMPR_H__

/*
 *  ================================================
 *
 *    DCA (Data Compression using Antidictionaries)
 *    2006 Martin Fiala
 *    Diploma thesis
 *
 *    Faculty of Electrical Engineering
 *    Department of Computer Science and Engineering
 *    Diploma thesis leader: Ing. Jan Holub, Ph.D.
 *
 *  ================================================
 *
 *    dcacompr.h - DCA compression & decompression class
 *
 *  Changes:
 *    090402 by Filip Simek <simekf1@fel.cvut.cz>
 *           incorporated as a module to ExCom library
 *
 *
 * This file is part of DCA module linked to the ExCom library.
 *
 * DCA module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DCA module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DCA module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <vector>
#include "fibcompr.h"

#define SUBTREE_NO 0
#define SUBTREE_RIGHT 1
#define SUBTREE_LEFT 2
#define SUBTREE_BOTH 3

class CompDCA;

namespace dca
{

/**
 * structure representing node with its attributes and pointers
 * */
struct DCAstate {
	/** node level */
	int level; // FIXME this could be removed
	/** how did we get here */
	bool b;
	/** fail function */
	DCAstate* fail; // FIXME could be joined with epsilon?
	/** child (delta) */
	DCAstate* next[2]; // FIXME could be removed in heap repre
};

/**
 * DCA handling class, antidictionary creation and using
 */
class DCAantidict
{
	friend class ::CompDCA;
public:

	/** allocated nodes */
	int totalNodes;

	/** next() times called */
	int nextTimes;

	/** uncompressed data length */
	unsigned long originalLen;

	/**
	 * do the data compression using automaton
	 * @param in            input I/O module
	 * @param out           output I/O module
	 * @param exc           output module for exceptions
	 * @return              \c EXCOM_ERR_OK on success
	 */
	int compressData(IOReader *in, IOWriter *out, IOWriter *exc);

	/**
	 * do the data compression using automaton
	 * @param in            input I/O module
	 * @param out           output I/O module
	 * @param exc           input module for exceptions
	 * @return              \c EXCOM_ERR_OK on success
	 */
	int decompressData(IOReader *in, IOWriter *out, IOReader *exc);

	/**
	 * contructor
	 * @param k maximal antiword length
	 */
	DCAantidict(int k) : totalNodes(0), nextTimes(0),
			maxlevel(k),
			stPool(NULL), stPoolLimit(NULL) {
		fibInit();
	}

	/**
	 * destructor - deletes all states allocated for the state pool
	 */
	~DCAantidict() {
		for (std::vector<DCAstate*>::iterator i = statePool.begin();
		        i != statePool.end(); i++) {
			delete [] *i;
		}
	}

	/** maximal antiword length */
	unsigned int maxlevel;

private:
	const static unsigned int ALLOCATE_BURST = 16192;

	/** trie root node */
	DCAstate* trie;

	/** allocated states */
	DCAstate* stPool;
	DCAstate* stPoolLimit;
	std::vector<DCAstate*> statePool;

	/**
	 * auxiliary function for trie building
	 * @param   p   node
	 * @param   a   next input char
	 * @return      node following p when input a
	 */
	inline DCAstate* next(DCAstate* p, bool a, int level);

	/** DCAstate allocator */
	inline DCAstate* allocDCAstate(bool bb, int level = 0);
};

/**
 * frontend class with user usable methods
 */
class DCAcompressor
{
	friend class ::CompDCA;
	DCAantidict ad;
	bool raw;
public:
	/**
	 * Compresses data using dynamic DCA, writes exceptions to a separate stream
	 * @param reader input stream
	 * @param writer output stream
	 * @param exc_wr output stream for storing exceptions
	 * @return      EXCOM_ERR_OK on success
	 */
	int compress(IOReader* reader, IOWriter* writer, IOWriter* exc_wr);

	/**
	 * Decompresses data using dynamic DCA, reads exceptions from separate stream
	 * @param reader input stream
	 * @param writer output stream
	 * @param exc_rd input stream for reading exceptions
	 * @return      EXCOM_ERR_OK on success
	 */
	int decompress(IOReader* reader, IOWriter* writer, IOReader* exc_rd);

	/**
	 * Constructor - sets maximal antiword length to a default value
	 */
	DCAcompressor(): ad(DCA_MAXLENGTH), raw(DCA_RAW) {};
};

}

#endif //ifndef __DCACOMPR_H__

