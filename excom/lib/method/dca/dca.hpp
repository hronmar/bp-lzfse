#ifndef __METHOD_DCA_HPP__
#define __METHOD_DCA_HPP__

/*
 * File:       method/dca/dca.hpp
 * Purpose:    Declaration of a wrapper class for DCA compression method
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of DCA module linked to the ExCom library.
 *
 * DCA module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DCA module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DCA module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file dca.hpp
 *  \brief Declaration of a wrapper class for the DCA compression method
 *  (Data Compression Using Antidictionaries).
 *
 *  \todo TODO: expand the description
 */

#include <excom.h>
#include "../../compmodule.hpp"

// version of the DCA output file
#define DCA_VERSION 0

// FIXME: these should be set at configuration time (config.h)
#define DCA_MAXLENGTH 30
#define DCA_RAW false

#include "dcacompr.h"

class CompDCA: public CompModule {
private:
	// data input and output
	IOReader *reader;
	IOWriter *writer;
	// dynamic DCA needs a special stream for exceptions
	IOReader *exc_rd;
	IOWriter *exc_wr;

	// compressor class
	dca::DCAcompressor *compr;
public:
	CompDCA(unsigned int handle):
		CompModule(handle), reader(NULL), writer(NULL),
		exc_rd(NULL), exc_wr(NULL) {
		compr = new dca::DCAcompressor();
	}
	~CompDCA() {
		if (compr != NULL) delete compr;
	}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();
};

#endif //ifndef __METHOD_DCA_HPP__
