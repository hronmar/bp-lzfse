/*
 * File:       method/dhuff/DHuffTree.cpp
 * Purpose:    Declaration of the Dymamic Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Dynamic Huffman coding module linked to the ExCom library.
 *
 * Dynamic Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dynamic Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Dynamic Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file DHuffTree.cpp
 *  \brief Implementation of the Dynamic Huffman tree.
 *
 *  This class serves for the Dynamic Huffman tree representation and manipulation
 */

#include "DHuffTree.h"

// Constructor
DHuffTree::DHuffTree() {
	init(ALPHABET_SIZE_BITS);
}

DHuffTree::DHuffTree(unsigned int codeLength) {
	init(codeLength);
}

void DHuffTree::init(unsigned int codeLength) {
	this->codeLength = codeLength;
	this->nodeIndex = 2 * (1 << codeLength);
	this->maxNodes = this->nodeIndex;
	HuffTree = new DHuffNode*[this->maxNodes + 1];
	memset(HuffTree, 0, (this->maxNodes + 1) * sizeof(DHuffNode*));
	symbPositions = new int[this->maxNodes + 1];
	memset(symbPositions, 0, (this->maxNodes + 1) * sizeof(int));
	symbols = new int[this->maxNodes + 1]; // init all symbols with 0 - never seen
	memset(symbols, 0, (this->maxNodes + 1) * sizeof(int));
	this->root = new DHuffNode();
	addNode(this->root);
	this->currentNode = this->root;
}

// Destructor
DHuffTree::~DHuffTree() {
	delete [] HuffTree;
	HuffTree = NULL;

	delete [] symbPositions;
	symbPositions = NULL;
	
	delete [] symbols;
	symbols = NULL;

	delete this->root;
	this->root == NULL;
}


inline void DHuffTree::addNode(DHuffNode *node) {
	node->setIndex(nodeIndex);
	HuffTree[nodeIndex--] = node;
}

inline void DHuffTree::addNode(DHuffNode *node, int index) {
	node->setIndex(index);
	HuffTree[index] = node;
}

inline void DHuffTree::addSymbol(unsigned int s) {
	symbols[s] = 1;
}

inline DHuffNode* DHuffTree::getNode(int index) {
	return HuffTree[index];
}

inline int DHuffTree::symbolSeen(unsigned int s) {
	return symbols[s];
}

void DHuffTree::swapNodes(int x, int y) {
	DHuffNode* a = getNode(x);
	DHuffNode* b = getNode(y);  
	DHuffNode* aParent = a->getParent();
	DHuffNode* bParent = b->getParent();

	// a and b are siblings, have a common parent	
	if (bParent == aParent) {
		if (aParent->getLeftChild() == a) {
			aParent->setLeftChild(b);
			aParent->setRightChild(a);

			a->setCode(RIGHT_CHILD_CODE); 
			b->setCode(LEFT_CHILD_CODE); 
		} else {
			aParent->setLeftChild(a);
			aParent->setRightChild(b);      

			a->setCode(LEFT_CHILD_CODE); 
			b->setCode(RIGHT_CHILD_CODE); 
		}
	} else {
		if (aParent->getLeftChild() == a) {
			aParent->setLeftChild(b);
			b->setCode(LEFT_CHILD_CODE); 
		} else { 
			aParent->setRightChild(b);    
			b->setCode(RIGHT_CHILD_CODE); 
		}

		if (bParent->getLeftChild() == b) {
			bParent->setLeftChild(a);
			a->setCode(LEFT_CHILD_CODE); 
		} else { 
			bParent->setRightChild(a);    
			a->setCode(RIGHT_CHILD_CODE); 
		}

		a->setParent(bParent);
		b->setParent(aParent);
	}

	if (a->isSymbolNode()) {
		symbPositions[a->getSymbol()] = y;
	}

	if (b->isSymbolNode()) {
		symbPositions[b->getSymbol()] = x;
	}

	a->setIndex(y);
	b->setIndex(x);

	HuffTree[x] = b;
	HuffTree[y] = a;
}

inline void DHuffTree::updateTree(int index) {
	unsigned long currentWeight;
	unsigned long indexToSwap;
	unsigned long currentIndex;

	DHuffNode* currentNode = getNode(index);

	currentIndex = index;
	indexToSwap = 0;

	while (currentNode != root) {
		currentWeight = currentNode->getWeight();

		if ((currentWeight == getNode(currentIndex+1)->getWeight()) && (getNode(currentIndex+1) != root)) {
			for (int i=currentIndex+1; i < this->maxNodes; i++) {
				if (currentWeight == getNode(i)->getWeight()) indexToSwap = i;
				else break;
			}

			if (currentNode->getParent() != getNode(indexToSwap)) {
				swapNodes(currentIndex, indexToSwap);
				currentIndex = indexToSwap;
			}	
		} 

		getNode(currentIndex)->incWeight();
		currentNode = getNode(currentIndex)->getParent();
		currentIndex = currentNode->getIndex(); 
	}

	root->incWeight();
}

inline int DHuffTree::getNodeIndex(unsigned int s) {
	return symbPositions[s];
}

unsigned int DHuffTree::getSymbolCode(int index, unsigned int &length) {
	DHuffNode* node = getNode(index);
	length = 0;
	unsigned int code = 0;
	while(node != root) {
		code = code + (node->getCode() << length);
		node = node->getParent();
		length++;
	}
	return code;
}

bool DHuffTree::isLeaf(int i) {
	if (!currentNode->isSymbolNode()) {
		currentNode = (i == 1) ? currentNode->getRightChild() : currentNode->getLeftChild();
		return (currentNode->getLeftChild() == 0);
	} else {
		return true;
	}
}

bool DHuffTree::isEsc() {
	return (currentNode->getIndex() == nodeIndex + 1);
}

bool DHuffTree::isLeaf() {
	return currentNode->isSymbolNode();
}

void DHuffTree::traverseUp() {
	currentNode = root;
}

unsigned int DHuffTree::getCurrentSymbol() {
	return currentNode->getSymbol();
}

// Caller must make sure, that s is not longer than codeLength
// It is not efficient to mask s using current max code length
unsigned int DHuffTree::insert(unsigned int s, unsigned int &codeLength) {
	unsigned int x = 0;
	codeLength = 0;
	if (!symbolSeen(s)) {
		x = getSymbolCode(nodeIndex + 1, codeLength);
		x = ( x << this->codeLength ) | s;
		codeLength += this->codeLength;

		addSymbol(s);
		DHuffNode* symbolNode = new DHuffNode(s);
		DHuffNode* zeroNode = new DHuffNode();

		symbolNode->setParent(getNode(nodeIndex + 1));
		zeroNode->setParent(getNode(nodeIndex + 1));

		addNode(symbolNode);
		addNode(zeroNode);

		getNode(nodeIndex + 3)->setLeftChild(zeroNode);
		getNode(nodeIndex + 3)->setRightChild(symbolNode);

		symbPositions[s] = nodeIndex + 2; 
		updateTree(nodeIndex + 3);
	} else {
		x = getSymbolCode(getNodeIndex(s), codeLength);
		updateTree(getNodeIndex(s));
	}
	return x;
}

void DHuffTree::insert(unsigned int s) {
	if (!symbolSeen(s)) {
		addSymbol(s);
		DHuffNode* symbolNode = new DHuffNode(s);
		DHuffNode* zeroNode = new DHuffNode();

		symbolNode->setParent(getNode(nodeIndex + 1));
		zeroNode->setParent(getNode(nodeIndex + 1));

		addNode(symbolNode);
		addNode(zeroNode);

		getNode(nodeIndex + 3)->setLeftChild(zeroNode);
		getNode(nodeIndex + 3)->setRightChild(symbolNode);

		symbPositions[s] = nodeIndex + 2; 
		updateTree(nodeIndex + 3);
	} else {
		updateTree(getNodeIndex(s));
	}
}

unsigned int DHuffTree::getEsc(unsigned int &codeLength) {
    return getSymbolCode(nodeIndex + 1, codeLength);
}

unsigned int DHuffTree::getCodeLength() {
	return this->codeLength;
}
