#ifndef __METHOD_DHUFF_HPP__
#define __METHOD_DHUFF_HPP__

/*
 * File:       method/dhuff/dhuff.hpp
 * Purpose:    Declaration of the Dynamic Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Dynamic Huffman coding module linked to the ExCom library.
 *
 * Dynamic Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dynamic Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Dynamic Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file dhuff.hpp
 *  \brief Declaration of the Dynamic Huffman coding method.
 *
 *  This module provides the Dynamic Huffman compression method, which is based on 
 *	building the binary tree as the input data are processed 
 */

#include <excom.h>
#include "../../compmodule.hpp"

class CompDHuff: public CompModule {
private:
	IOReader *reader;
	IOWriter *writer;

	int runCompress();
	int runDecompress();
public:
	CompDHuff(unsigned int handle): CompModule(handle), reader(NULL), writer(NULL) {}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int checkConnection();
	int run();
private:
	unsigned char getSymbol();

	int eofIndicator;
};

#endif //ifndef __METHOD_DHUFF_HPP__
