#ifndef _DYNAMIC_HUFF_TREE_H
#define	_DYNAMIC_HUFF_TREE_H

/*
 * File:       method/dhuff/DHuffTRee.h
 * Purpose:    Declaration of the Dymamic Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Dynamic Huffman coding module linked to the ExCom library.
 *
 * Dynamic Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dynamic Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Dynamic Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file DHuffTree.h
 *  \brief Declaration of Dynamic Huffman tree representation.
 *
 *  This class serves for Dynamic Huffman tree representation and manipulation
 */

#include "DHuffNode.h"

#include <string.h>

#define ALPHABET_SIZE_BITS 8
#define LEFT_CHILD_CODE 0
#define RIGHT_CHILD_CODE 1

using namespace std;

class DHuffTree {
private:
	int nodeIndex; //!< Next available index for a node
	int maxNodes;

	unsigned int codeLength;

	DHuffNode** HuffTree;
	int* symbPositions; //!< To quickly find an index of node according to its symbol
	int* symbols; //!< Easy way how to check whether the symbol was encountered or not

	DHuffNode *root;
	DHuffNode *currentNode;

	/*! \brief Inserts a new node to the tree
	 *  \param node Pointer to a Huffman node
	 */
	void addNode(DHuffNode * node);
	/*! \brief Inserts an internal node to the tree
	 *  \param node Pointer to a Huffman node
	 *  \param index Position where to insert the node
	 *
	 *  The node is inserted at given position
	 */
	void addNode(DHuffNode * node, int index);
	/*! \brief Inserts a symbol to a set of symbols
	 *  \param s A symbol
	 */
	void addSymbol(unsigned int s);
	/*! \brief Swap two nodes in the tree
	 *  \param x A position of the first node
	 *  \param y A position of the second node
	 *
	 *  This function swaps two nodes at given
	 *  positions, changes their parents and
	 *  set them either as a left or a right child 
	 *  according to the other node
	 */
	void swapNodes(int x, int y);
	/*! \brief Update the Huffman tree after every insertion
	 *  \param index 
	 *  
	 *  This function should be called whenever a new symbol
	 *  is inserted or the old one encoutered. This also checks
	 *  the Sibling property
	 */
	void updateTree(int index);

	/*! \brief Returns an index of node which holds a given string
	 *  \param s A symbol 
	 */
	int getNodeIndex(unsigned int s);

	/*! \brief Quickly checks whether the symbol was seen or not
	 *  \param s A symbol 
	 */
	int symbolSeen(unsigned int s); 

	/*! \brief Returns a binary codeword of a node at given index 
	 *  \param index An index of node 
	 */
	unsigned int getSymbolCode(int index, unsigned int &length);

	/*! \brief Returns a node at given index 
	 *  \param index An index of node 
	 */
	DHuffNode* getNode(int index);

	void init(unsigned int codeLength);
public:
	// Constructor
	DHuffTree();

	DHuffTree(unsigned int codeLength);

	// Destructor
	~DHuffTree();

	bool isLeaf();
	/*! \brief Returns true if the current node is a leaf 
	 *  \param index An index of node 
	 */
	bool isLeaf(int index);
	/*! \brief Returns true if the current node represents an ESC code
	 */
	bool isEsc();
	/*! \brief Sets the root as a current node
	 *
	 *  This function is called when a leaf during the traversal is reached
	 *  when the decoder reads a bit by bit with a view to find the match
	 *  for a codeword. In other words, it is some kind of reset 
	 */
	void traverseUp();
	/*! \brief Returns the symbol of the current node in a tree
	 */
	unsigned int getCurrentSymbol();

	/*! \brief Inserts a symbol
	 *  \param s Symbol to be inserted
	 *
	 *  This function checks if the symbol was already added or not
	 */
	unsigned int insert(unsigned int s, unsigned int &codeLength);

	void insert(unsigned int s);

        /*!
         * \brief Returns code for esc node
         * \param codeLength - length of esc symbol in bits
         * \return esc symbol
         */
        unsigned int getEsc(unsigned int &codeLength);
	/*!
	 *	 
	 */
	unsigned int getCodeLength();
};

#endif	/* _DYNAMIC_HUFF_TREE_H */

