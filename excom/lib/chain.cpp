/*
 * File:       chain.cpp
 * Purpose:    Implementation of classes related to compression chain
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "chain.hpp"
#include "handles.hpp"

CompressionChain::CompressionChain(unsigned int handle)
{
	hand_alloc.registerObject(handle, HANDLE_CHAIN, this);
	chain_handle = handle;
	running = 0;
	thread = NULL;
	thread_info = NULL;
	thread_num = 0;
	int res;
	res = pthread_mutex_init(&mutex_wait, NULL);
	if (res != 0) {
		//allocate the mutex statically
		pthread_mutex_t mutex_static = PTHREAD_MUTEX_INITIALIZER;
		mutex_wait = mutex_static;
	}
}

CompressionChain::~CompressionChain()
{
	hand_alloc.freeHandle(chain_handle);

	if (thread) delete [] thread;
	if (thread_info) delete [] thread_info;

	pthread_mutex_destroy(&mutex_wait);

	// destroy I/O and compression modules
	std::vector<CompModule*>::iterator iter_comp;
	std::vector<IOModule*>::iterator iter_io;
	for (iter_comp = comp_modules.begin(); iter_comp != comp_modules.end(); iter_comp++) {
		delete *iter_comp;
	}
	for (iter_io = io_modules.begin(); iter_io != io_modules.end(); iter_io++) {
		delete *iter_io;
	}
}

void CompressionChain::addCompModule(CompModule *module)
{
	comp_modules.push_back(module);
	module->chain = this;
}

void CompressionChain::addIOModule(IOModule *module)
{
	io_modules.push_back(module);
}

static void* run_thread(void *param)
{
	struct CompressionChain::run_thread_info *info;
	if (param == NULL) return NULL;
	info = (struct CompressionChain::run_thread_info*) param;
	// TODO: create a barrier that will stop the threads until all of them are created ?
	//       so that no input is read in case that we are unable to create all threads
	//       and are going to cancel those that were created

	// this call will block until the compression is complete
	info->comp->result = info->comp->run();
	info->finished = 1;
	return NULL;
}

int CompressionChain::run()
{
	int res;

	// isn't the chain already running?
	if (running) return EXCOM_ERR_BUSY;

	// isn't the chain broken?
	std::vector<CompModule*>::iterator iter_comp;
	for (iter_comp = comp_modules.begin(); iter_comp != comp_modules.end(); iter_comp++) {
		if ((*iter_comp)->checkConnection() != EXCOM_ERR_OK) {
			// the chain is broken
			return EXCOM_ERR_CHAIN;
		}
	}
	std::vector<IOModule*>::iterator iter_io;
	IOPipe* pipe;
	for (iter_io = io_modules.begin(); iter_io != io_modules.end(); iter_io++) {
		if ((*iter_io)->variant & IO_VAR_PIPE) {
			// it's a pipe, it has to have both end connected
			if ((*iter_io)->connected != 2) {
				return EXCOM_ERR_CHAIN;
			}
			// if it's connected properly, rewind the pipe
			pipe = (IOPipe*) ioGeneralToParticular(*iter_io, IO_VAR_PIPE);
			pipe->rewind();
		}
	}

#ifdef PERF_MON
	PERF_START
#endif

	// start one thread for each compression module in the chain
	if (thread != NULL) delete [] thread;
	if (thread_info != NULL) delete [] thread_info;
	thread_num = comp_modules.size();
	thread = new pthread_t[thread_num];
	thread_info = new run_thread_info[thread_num];

	int i = 0;
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	// start a single thread for each compression module
	for (iter_comp = comp_modules.begin(); iter_comp != comp_modules.end(); iter_comp++, i++) {
		thread_info[i].finished = 0;
		thread_info[i].comp = *iter_comp;

		res = pthread_create(&thread[i], &attr, run_thread, (void*)(&thread_info[i]));
		if (res != 0) {
			// destroy all threads, that have already been created
			for (int j = 0; j < i; j++) {
				pthread_cancel(thread[j]);
				pthread_join(thread[j], NULL);
			}

			thread_num = 0;
			delete [] thread;
			thread = NULL;
			delete [] thread_info;
			thread_info = NULL;
			return EXCOM_ERR_THREAD;
		}
	}

	running = 1;
	return EXCOM_ERR_OK;
}

int CompressionChain::wait()
{
	// this check is an overkill, because this situation is also checked in exc_wait,
	// but one can never be safe enough
	if (!running) return EXCOM_ERR_NOT_NOW;
	// this mutex prevents race conditions when wait and one or more tryWait
	// function calls are made at the same time
	pthread_mutex_lock(&mutex_wait);
	// wait for all the threads
	for (unsigned int i = 0; i < thread_num; i++) {
		if (thread_info[i].finished == 2) {
			// this thread has already been joined by tryWait method
			continue;
		}
		pthread_join(thread[i], NULL);
	}

#ifdef PERF_MON
	PERF_STOP
#endif

	delete [] thread;
	thread = NULL;
	delete [] thread_info;
	thread_info = NULL;
	thread_num = 0;
	running = 0;
	pthread_mutex_unlock(&mutex_wait);
	return EXCOM_ERR_OK;
}

int CompressionChain::tryWait()
{
	// this check is an overkill, because this situation is also checked in exc_tryWait,
	// but one can never be safe enough
	if (!running) return EXCOM_ERR_NOT_NOW;
	int done = 1;
	// this mutex prevents race conditions when wait or one or more tryWait
	// function calls are made at the same time
	pthread_mutex_lock(&mutex_wait);
	for (unsigned int i = 0; i < thread_num; i++) {
		int f = thread_info[i].finished;
		if (f == 0) {
			// this thread hasn't finished, yet
			done = 0;
		} else if (f == 1) {
			// join this thread
			pthread_join(thread[i], NULL);
			thread_info[i].finished = 2;
		}
	}

	if (done) {
#ifdef PERF_MON
		PERF_STOP
#endif
		delete [] thread;
		thread = NULL;
		delete [] thread_info;
		thread_info = NULL;
		thread_num = 0;
		running = 0;
		pthread_mutex_unlock(&mutex_wait);
		return EXCOM_ERR_OK;
	} else {
		pthread_mutex_unlock(&mutex_wait);
		return EXCOM_ERR_LATER;
	}
}


