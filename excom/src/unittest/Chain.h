#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/chain.hpp"
#include "../../lib/iomodule.hpp"
#include "../../lib/compmodule.hpp"
#include "../../lib/method/copy/copy.hpp"

#define HANDLE 1

unsigned int handles[5] = {2,3,4,5,6};

class ChainTestSuite: public CxxTest::TestSuite {
	CompressionChain *chain;
public:
	void setUp() {
		chain = new CompressionChain(HANDLE);
	}

	void tearDown() {
		delete chain;
	}

	void testRunningAndWaiting() {
		// an empty chain
		TS_ASSERT_EQUALS(chain->run(), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(chain->run(), EXCOM_ERR_BUSY);
		TS_ASSERT_EQUALS(chain->tryWait(), EXCOM_ERR_OK);
		// add a comp module
		CompCopy *copy = new CompCopy(handles[0]);
		chain->addCompModule(copy);
		// a broken chain
		TS_ASSERT_EQUALS(chain->run(), EXCOM_ERR_CHAIN);
		// complete the chain
		char buf[10];
		IOMemReader *ior = new IOMemReader(handles[1]);
		TS_ASSERT_EQUALS(ior->attachMemory(buf, 10), EXCOM_ERR_OK);
		IOMemWriter *iow = new IOMemWriter(handles[2]);
		TS_ASSERT_EQUALS(iow->attachMemory(buf, 10), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(copy->connectIOModule(ior, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(copy->connectIOModule(iow, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		chain->addIOModule(ior);
		chain->addIOModule(iow);
		// success
		TS_ASSERT_EQUALS(chain->run(), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(chain->wait(), EXCOM_ERR_OK);
		// the compression module and the I/O modules are destroyed by the destructor of the chain
	}

	void testBreakingPipes() {
		// add a comp module
		CompCopy *copy = new CompCopy(handles[0]);
		chain->addCompModule(copy);
		char buf[10];
		IOMemReader *ior = new IOMemReader(handles[1]);
		TS_ASSERT_EQUALS(ior->attachMemory(buf, 10), EXCOM_ERR_OK);
		// create a pipe
		IOMemPipe *iop = new IOMemPipe(handles[2]);
		TS_ASSERT_EQUALS(copy->connectIOModule(ior, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(copy->connectIOModule(iop, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		chain->addIOModule(ior);
		chain->addIOModule(iop);
		iop->connected = 1;
		// the chain is broken, it contains a pipe with one loose end
		TS_ASSERT_EQUALS(chain->run(), EXCOM_ERR_CHAIN);
		// complete the chain
		CompCopy *c2 = new CompCopy(handles[3]);
		chain->addCompModule(c2);
		IOMemWriter *iow = new IOMemWriter(handles[4]);
		TS_ASSERT_EQUALS(iow->attachMemory(buf, 10), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(c2->connectIOModule(iop, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(c2->connectIOModule(iow, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		chain->addIOModule(iow);
		iop->connected = 2;
		// success
		TS_ASSERT_EQUALS(chain->run(), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(chain->wait(), EXCOM_ERR_OK);
		// the compression modules and the I/O modules are destroyed by the destructor of the chain
	}
};

