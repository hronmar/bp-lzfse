#include <unistd.h>
#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/method/lzmw/lzmw.hpp"

#define HANDLE 1
#define HANDLE_IO_IN 2
#define HANDLE_IO_OUT 3
#define HANDLE_TMP 4

#define DATA_ORIG "data_comp"
#define TMP_LZMW "tmp_lzmw"
#define TMP_UNLZMW "tmp_unlzmw"

class CompLZWTestSuite: public CxxTest::TestSuite {
	CompLZMW *comp;

public:
	virtual void setUp() {
		comp = new CompLZMW(HANDLE);
	}
	virtual void tearDown() {
		delete comp;
	}

	void testParameters() {
		unsigned v;
		TS_ASSERT_EQUALS(comp->setParameter(-1, &v), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_DICTIONARY_SIZE, NULL), EXCOM_ERR_MEMORY);
		v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_DICTIONARY_SIZE, &v), EXCOM_ERR_VALUE);
		v = 255;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_DICTIONARY_SIZE, &v), EXCOM_ERR_VALUE);
		v = 256;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_DICTIONARY_SIZE, &v), EXCOM_ERR_OK);
		v = 4096;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_DICTIONARY_SIZE, &v), EXCOM_ERR_OK);
		v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_CHAR_BITSIZE, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_CHAR_BITSIZE, &v), EXCOM_ERR_VALUE);
		v = 8;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_CHAR_BITSIZE, &v), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(comp->getValue(-1, &v), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_LZMW_DICTIONARY_SIZE, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_LZMW_DICTIONARY_SIZE, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 4096u);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_LZMW_CHAR_BITSIZE, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_LZMW_CHAR_BITSIZE, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 8u);

		// a value common to all compression modules
		int u;
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_COMP_RESULT, &u), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(u, EXCOM_ERR_LATER);
	}

	void testIOInputModules() {
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		// invalid pointer to a module
		TS_ASSERT_EQUALS(comp->connectIOModule(NULL, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_MEMORY);
		// invalid connection type
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, -1), EXCOM_ERR_PARAM);
		// input module connected as an output module
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		delete reader;
	}

	void testIOOutputModules() {
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		// output module connected as an input module
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_DUPLICATE);
		delete writer;
	}

	void compareFiles(const char *a, const char *b)
	{
		long sizeA, sizeB;
		FILE *A, *B;
		unsigned char *dataA = NULL, *dataB = NULL;
		A = fopen(a, "rb");
		TS_ASSERT_DIFFERS(A, (FILE*)NULL);
		fseek(A, 0, SEEK_END);
		sizeA = ftell(A);
		fseek(A, 0, SEEK_SET);
		dataA = new unsigned char[sizeA];
		TS_ASSERT_EQUALS(fread(dataA, sizeA, 1, A), 1u);
		fclose(A);

		B = fopen(b, "rb");
		TS_ASSERT_DIFFERS(B, (FILE*)NULL);
		fseek(B, 0, SEEK_END);
		sizeB = ftell(B);
		fseek(B, 0, SEEK_SET);
		TS_ASSERT_EQUALS(sizeA, sizeB);
		if (sizeA == sizeB) {
			dataB = new unsigned char[sizeB];
			TS_ASSERT_EQUALS(fread(dataB, sizeB, 1, B), 1u);
			TS_ASSERT_SAME_DATA(dataA, dataB, sizeA);
		}
		fclose(B);

		if (dataA != NULL) delete dataA;
		if (dataB != NULL) delete dataB;
	}

	void testCompression() {
		unsigned v;

		// input and output modules
		IOFileReader *reader = new IOFileReader(HANDLE_IO_IN);
		IOFileWriter *writer = new IOFileWriter(HANDLE_IO_OUT);

		TS_ASSERT_EQUALS(reader->openFile(DATA_ORIG), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->openFile(TMP_LZMW), EXCOM_ERR_OK);

		// running shouldn't succeed without an input I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// ...or without output I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// set parameters
		v = CompLZMW::DEFAULT_DICT_SIZE;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_DICTIONARY_SIZE, &v), EXCOM_ERR_OK);
		v = CompLZMW::DEFAULT_CHAR_BITSIZE;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_LZMW_CHAR_BITSIZE, &v), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		delete reader;
		delete writer;

		// decompression
		CompLZMW *decomp = new CompLZMW(HANDLE_TMP);
		reader = new IOFileReader(HANDLE_IO_IN);
		writer = new IOFileWriter(HANDLE_IO_OUT);

		TS_ASSERT_EQUALS(reader->openFile(TMP_LZMW), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->openFile(TMP_UNLZMW), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(decomp->setOperation(EXCOM_OP_DECOMPRESS), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// set parameters
		v = CompLZMW::DEFAULT_DICT_SIZE;
		TS_ASSERT_EQUALS(decomp->setParameter(EXCOM_PARAM_LZMW_DICTIONARY_SIZE, &v), EXCOM_ERR_OK);
		v = CompLZMW::DEFAULT_CHAR_BITSIZE;
		TS_ASSERT_EQUALS(decomp->setParameter(EXCOM_PARAM_LZMW_CHAR_BITSIZE, &v), EXCOM_ERR_OK);

		// run the decompression
		TS_ASSERT_EQUALS(decomp->run(), EXCOM_ERR_OK);

		// check, whether output data matches input original
		compareFiles(TMP_UNLZMW, DATA_ORIG);

		delete reader;
		delete writer;
		delete decomp;

		// delete temporary files
		TS_ASSERT_EQUALS(unlink(TMP_LZMW), 0);
		TS_ASSERT_EQUALS(unlink(TMP_UNLZMW), 0);
	}

};
