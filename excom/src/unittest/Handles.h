#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/handles.hpp"

class HandlesTestSuite: public CxxTest::TestSuite {
	HandleAllocator *ha;
public:
	void setUp() {
		ha = new HandleAllocator();
	}

	void tearDown() {
		delete ha;
	}

	void test_allocationLimit() {
		// NOTE: this test is bound to size of the handles' pool that the handle allocator is using.
		// This number is hardcoded to the module right now.
		unsigned int handle[1024];
		// ivalid pointer
		TS_ASSERT_EQUALS(ha->getFreeHandle(NULL), EXCOM_ERR_HANDLE);
		// exhaust the pool completely
		for (int i = 0; i < 1023; i++) {
			TS_ASSERT_EQUALS(ha->getFreeHandle(&handle[i]), EXCOM_ERR_OK);
			TS_ASSERT_DIFFERS(handle[i], 0u);
			// every handle must be unique
			for (int j = 0; j < i; j++) {
				TS_ASSERT_DIFFERS(handle[j], handle[i]);
			}
		}
		// there shouldn't be any more handles in the pool
		TS_ASSERT_EQUALS(ha->getFreeHandle(&handle[0]), EXCOM_ERR_HANDLE);

		// free one particular handle
		ha->freeHandle(42);
		// obtain the same handle again
		TS_ASSERT_EQUALS(ha->getFreeHandle(&handle[0]), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(handle[0], 42u);
	}

	void test_invalidFree() {
		unsigned int handle;
		// this should do nothing (definitely not cause an error)
		ha->freeHandle(0);
		ha->freeHandle(1);
		TS_ASSERT_EQUALS(ha->getFreeHandle(&handle), EXCOM_ERR_OK);
		// double-free shouldn't be a problem
		ha->freeHandle(handle);
		ha->freeHandle(handle);
		// 1x allocation, 4x free, still shouldn't allow us allocate more than 1023 handles
		for (int i = 0; i < 1023; i++) {
			TS_ASSERT_EQUALS(ha->getFreeHandle(&handle), EXCOM_ERR_OK);
		}
		TS_ASSERT_EQUALS(ha->getFreeHandle(&handle), EXCOM_ERR_HANDLE);
	}

	void test_registeredObjects() {
		unsigned int h1, h2, h3;
		void *a = (void*) &h1, *b;
		enum handle_type_e t;
		// 0 is an invalid handle
		TS_ASSERT_EQUALS(ha->registerObject(0, HANDLE_IO, NULL), EXCOM_ERR_HANDLE);

		TS_ASSERT_EQUALS(ha->getFreeHandle(&h1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(ha->getFreeHandle(&h2), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(ha->getFreeHandle(&h3), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(ha->registerObject(h1, HANDLE_IO, a), EXCOM_ERR_OK);
		// already registered
		TS_ASSERT_EQUALS(ha->registerObject(h1, HANDLE_IO, a), EXCOM_ERR_HANDLE);
		// a different handle
		TS_ASSERT_EQUALS(ha->registerObject(h2, HANDLE_COMPRESSION, NULL), EXCOM_ERR_OK);

		// handle -> object translation
		// 0 is an invalid handle
		TS_ASSERT_EQUALS(ha->objectFromHandle(0, &t, &b), EXCOM_ERR_HANDLE);
		// invalid pointer
		TS_ASSERT_EQUALS(ha->objectFromHandle(h1, NULL, &b), EXCOM_ERR_MEMORY);
		// invalid pointer
		TS_ASSERT_EQUALS(ha->objectFromHandle(h1, &t, NULL), EXCOM_ERR_MEMORY);
		// h3 has never been registered
		TS_ASSERT_EQUALS(ha->objectFromHandle(h3, &t, &b), EXCOM_ERR_HANDLE);
		// h2 has been deleted
		ha->freeHandle(h2);
		TS_ASSERT_EQUALS(ha->objectFromHandle(h2, &t, &b), EXCOM_ERR_HANDLE);
		// h1 should be ok
		TS_ASSERT_EQUALS(ha->objectFromHandle(h1, &t, &b), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(t, HANDLE_IO);
		TS_ASSERT_EQUALS(b, a);
		ha->freeHandle(h1);
	}
};


