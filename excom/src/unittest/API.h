#include <unistd.h>
#include <cxxtest/TestSuite.h>
#include <excom.h>

#define DATA_FILE "data_reader"

class APITestSuite: public CxxTest::TestSuite {
public:
	void test_fileIOModule() {
		unsigned int mod_handle;
		// invalid pointer to file name
		TS_ASSERT_EQUALS(exc_fileIOModule(NULL, EXCOM_IO_READ, &mod_handle), EXCOM_ERR_MEMORY);
		// invalid type
		TS_ASSERT_EQUALS(exc_fileIOModule(DATA_FILE, (exc_iotype_e)1000, &mod_handle), EXCOM_ERR_PARAM);
		// invalid pointer to module handle
		TS_ASSERT_EQUALS(exc_fileIOModule(DATA_FILE, EXCOM_IO_READ, NULL), EXCOM_ERR_HANDLE);
		// file not found
		TS_ASSERT_EQUALS(exc_fileIOModule("notfound", EXCOM_IO_READ, &mod_handle), EXCOM_ERR_OPEN);
		// success
		TS_ASSERT_EQUALS(exc_fileIOModule(DATA_FILE, EXCOM_IO_READ, &mod_handle), EXCOM_ERR_OK);
		// destroy the IO module
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_OK);
		// won't be able to destroy the same module twice
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_HANDLE);
		// successful creation
		TS_ASSERT_EQUALS(exc_fileIOModule("create_me", EXCOM_IO_WRITE, &mod_handle), EXCOM_ERR_OK);
		// destroy the IO module and the file
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(unlink("create_me"), 0);
		// pipe doesn't need a file name...
		TS_ASSERT_EQUALS(exc_fileIOModule(NULL, EXCOM_IO_PIPE, &mod_handle), EXCOM_ERR_OK);
		// destroy the IO module
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_OK);
		// ...but won't argue if it gets one
		TS_ASSERT_EQUALS(exc_fileIOModule("create_me", EXCOM_IO_PIPE, &mod_handle), EXCOM_ERR_OK);
		// destroy the IO module and the file
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(unlink("create_me"), 0);
	}

	void test_memIOModule() {
		unsigned int mod_handle;
		char buf[10];
		// invalid pointer to buffer
		TS_ASSERT_EQUALS(exc_memIOModule(NULL, 10, EXCOM_IO_READ, &mod_handle), EXCOM_ERR_MEMORY);
		// invalid type
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, (exc_iotype_e)1000, &mod_handle), EXCOM_ERR_PARAM);
		// invalid pointer to module handle
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_READ, NULL), EXCOM_ERR_HANDLE);
		// success
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_READ, &mod_handle), EXCOM_ERR_OK);
		// destroy the IO module
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_OK);
		// won't be able to destroy the same module twice
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_HANDLE);
		// success attaching buffer for writing
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_WRITE, &mod_handle), EXCOM_ERR_OK);
		// destroy the IO module
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_OK);
		// pipe doesn't need a buffer...
		TS_ASSERT_EQUALS(exc_memIOModule(NULL, 0, EXCOM_IO_PIPE, &mod_handle), EXCOM_ERR_OK);
		// destroy the IO module
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_OK);
		// ...but won't argue if it gets one
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_PIPE, &mod_handle), EXCOM_ERR_OK);
		// destroy the IO module
		TS_ASSERT_EQUALS(exc_destroyIOModule(mod_handle), EXCOM_ERR_OK);
	}

	void test_copressionChain() {
		unsigned int chain_handle;
		// invalid pointer to chain handle
		TS_ASSERT_EQUALS(exc_compressionChain(NULL), EXCOM_ERR_HANDLE);
		// success
		TS_ASSERT_EQUALS(exc_compressionChain(&chain_handle), EXCOM_ERR_OK);
		// cleanup
		TS_ASSERT_EQUALS(exc_destroyChain(chain_handle), EXCOM_ERR_OK);
	}

	void test_compModule() {
		unsigned int chain_handle;
		unsigned int mod_handle;
		TS_ASSERT_EQUALS(exc_compressionChain(&chain_handle), EXCOM_ERR_OK);
		//invalid method
		TS_ASSERT_EQUALS(exc_compModule((exc_method_e)1000, EXCOM_OP_COMPRESS, chain_handle, &mod_handle), EXCOM_ERR_METHOD);
		//invalid operation
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, (exc_oper_e)1000, chain_handle, &mod_handle), EXCOM_ERR_OPERATION);
		//invalid chain handle
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, 0, &mod_handle), EXCOM_ERR_CHAIN);
		//invalid pointer to module handle
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain_handle, NULL), EXCOM_ERR_HANDLE);
		//success
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain_handle, &mod_handle), EXCOM_ERR_OK);
		//cleanup
		TS_ASSERT_EQUALS(exc_destroyChain(chain_handle), EXCOM_ERR_OK);

		//TODO: test creation of particular compression methods in their own test-cases
	}

	void test_getValue() {
		// test general behavior only, for particular parameters of the actual modules make other test-cases
		unsigned int io_handle;
		unsigned int mod_handle;
		unsigned int chain_handle;
		unsigned int value;
		char buf[10];
		TS_ASSERT_EQUALS(exc_compressionChain(&chain_handle), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_READ, &io_handle), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain_handle, &mod_handle), EXCOM_ERR_OK);
		// 0 is an invalid handle
		TS_ASSERT_EQUALS(exc_getValue(0, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_HANDLE);
		// invalid pointer
		TS_ASSERT_EQUALS(exc_getValue(io_handle, EXCOM_VALUE_IO_BUFFER_SIZE, NULL), EXCOM_ERR_MEMORY);
		// compression chains don't provide values
		TS_ASSERT_EQUALS(exc_getValue(chain_handle, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_HANDLE);
		// invalid parameter for a compression module
		TS_ASSERT_EQUALS(exc_getValue(mod_handle, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_PARAM);
		// success
		TS_ASSERT_EQUALS(exc_getValue(io_handle, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_OK);
		// cleanup
		TS_ASSERT_EQUALS(exc_destroyChain(chain_handle), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_destroyIOModule(io_handle), EXCOM_ERR_OK);
	}

	void test_setParameter() {
		// test general behavior only, for particular parameters of the actual modules make other test-cases
		unsigned int io_handle;
		unsigned int mod_handle;
		unsigned int chain_handle;
		unsigned int value = 3;
		TS_ASSERT_EQUALS(exc_compressionChain(&chain_handle), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_fileIOModule(DATA_FILE, EXCOM_IO_READ, &io_handle), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain_handle, &mod_handle), EXCOM_ERR_OK);
		// 0 is an invalid handle
		TS_ASSERT_EQUALS(exc_setParameter(0, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_HANDLE);
		// compression chains don't have parameters
		TS_ASSERT_EQUALS(exc_setParameter(chain_handle, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_HANDLE);
		// invalid parameter for a compression module
		TS_ASSERT_EQUALS(exc_setParameter(mod_handle, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_PARAM);
		// invalid pointer
		TS_ASSERT_EQUALS(exc_setParameter(io_handle, EXCOM_VALUE_IO_BUFFER_SIZE, NULL), EXCOM_ERR_MEMORY);
		// wrong value
		value = 0;
		TS_ASSERT_EQUALS(exc_setParameter(io_handle, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_VALUE);
		// success
		value = 64;
		TS_ASSERT_EQUALS(exc_setParameter(io_handle, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_OK);
		// cleanup
		TS_ASSERT_EQUALS(exc_destroyChain(chain_handle), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_destroyIOModule(io_handle), EXCOM_ERR_OK);
	}

	void test_connectModules() {
		unsigned int ior, iow;
		unsigned int pipe;
		unsigned int mod1, mod2;
		unsigned int chain1, chain2;
		char buf[10];
		TS_ASSERT_EQUALS(exc_compressionChain(&chain1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_READ, &ior), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_WRITE, &iow), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_memIOModule(NULL, 0, EXCOM_IO_PIPE, &pipe), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain1, &mod1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain1, &mod2), EXCOM_ERR_OK);
		// 0 is an invalid handle
		TS_ASSERT_EQUALS(exc_connectModules(0, mod1, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_HANDLE);
		TS_ASSERT_EQUALS(exc_connectModules(ior, 0, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_HANDLE);
		// invalid connection type
		TS_ASSERT_EQUALS(exc_connectModules(ior, mod1, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(exc_connectModules(iow, mod1, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_PARAM);
		// success
		TS_ASSERT_EQUALS(exc_connectModules(ior, mod1, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// duplicate
		TS_ASSERT_EQUALS(exc_connectModules(ior, mod1, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		TS_ASSERT_EQUALS(exc_connectModules(ior, mod2, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		// connect two compression modules using a pipe
		TS_ASSERT_EQUALS(exc_connectModules(pipe, mod1, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		TS_ASSERT_EQUALS(exc_connectModules(pipe, mod1, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_connectModules(pipe, mod2, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_DUPLICATE);
		TS_ASSERT_EQUALS(exc_connectModules(pipe, mod2, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_connectModules(iow, mod2, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		// destroy all
		TS_ASSERT_EQUALS(exc_destroyChain(chain1), EXCOM_ERR_OK);
		
		// connection between 2 compression chains using a pipe
		TS_ASSERT_EQUALS(exc_compressionChain(&chain1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_compressionChain(&chain2), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_memIOModule(NULL, 0, EXCOM_IO_PIPE, &pipe), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain1, &mod1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain2, &mod2), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_connectModules(pipe, mod1, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_connectModules(pipe, mod2, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_CHAIN);
		// destroy all
		TS_ASSERT_EQUALS(exc_destroyChain(chain1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_destroyChain(chain2), EXCOM_ERR_OK);
	}

	void test_destroyChain() {
		unsigned int io, mod, chain;
		char buf[10];
		unsigned int value;
		TS_ASSERT_EQUALS(exc_compressionChain(&chain), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_READ, &io), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain, &mod), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_connectModules(io, mod, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// make sure, that io and mod are valid handles
		TS_ASSERT_DIFFERS(exc_getValue(io, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_HANDLE);
		TS_ASSERT_DIFFERS(exc_getValue(mod, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_HANDLE);
		// 0 is an invalid handle
		TS_ASSERT_EQUALS(exc_destroyChain(0), EXCOM_ERR_HANDLE);
		// io is not a chain
		TS_ASSERT_EQUALS(exc_destroyChain(io), EXCOM_ERR_HANDLE);
		// success
		TS_ASSERT_EQUALS(exc_destroyChain(chain), EXCOM_ERR_OK);
		// verify, that compression and IO modules are also destroyed
		// these two calls, that succeeded above, should now fail
		TS_ASSERT_EQUALS(exc_getValue(io, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_HANDLE);
		TS_ASSERT_EQUALS(exc_getValue(mod, EXCOM_VALUE_IO_BUFFER_SIZE, &value), EXCOM_ERR_HANDLE);
	}

	void test_run() {
		unsigned int ior, iow, mod, chain;
		char buf[10];
		TS_ASSERT_EQUALS(exc_compressionChain(&chain), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_READ, &ior), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_memIOModule(buf, 10, EXCOM_IO_WRITE, &iow), EXCOM_ERR_OK);

		// 0 is an invalid handle
		TS_ASSERT_EQUALS(exc_run(0), EXCOM_ERR_CHAIN);
		// even though the chain does nothing, it should succeed
		TS_ASSERT_EQUALS(exc_run(chain), EXCOM_ERR_OK);
		// we'll need to wait for the chain before we can use it again
		TS_ASSERT_EQUALS(exc_run(chain), EXCOM_ERR_BUSY);
		// cleanup
		TS_ASSERT_EQUALS(exc_wait(chain), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(exc_compModule(EXCOM_MET_COPY, EXCOM_OP_COMPRESS, chain, &mod), EXCOM_ERR_OK);
		// the chain is broken
		TS_ASSERT_EQUALS(exc_run(chain), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(exc_connectModules(ior, mod, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_connectModules(iow, mod, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		// success
		TS_ASSERT_EQUALS(exc_run(chain), EXCOM_ERR_OK);
		// cleanup
		TS_ASSERT_EQUALS(exc_wait(chain), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_destroyChain(chain), EXCOM_ERR_OK);
	}

	void test_tryWait() {
		// note: this is quite a weak test
		unsigned int chain;
		TS_ASSERT_EQUALS(exc_compressionChain(&chain), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_run(chain), EXCOM_ERR_OK);
		// 0 is an invalid handle
		TS_ASSERT_EQUALS(exc_tryWait(0), EXCOM_ERR_CHAIN);
		// there are no threads running in the chain, so we shouldn't be waiting for any
		TS_ASSERT_EQUALS(exc_tryWait(chain), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_destroyChain(chain), EXCOM_ERR_OK);
	}

	void test_wait() {
		// note: this is quite a weak test
		unsigned int chain;
		TS_ASSERT_EQUALS(exc_compressionChain(&chain), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_run(chain), EXCOM_ERR_OK);
		// 0 is an invalid handle
		TS_ASSERT_EQUALS(exc_wait(0), EXCOM_ERR_CHAIN);
		// there are no threads running in the chain, so we shouldn't be waiting for any
		TS_ASSERT_EQUALS(exc_wait(chain), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc_destroyChain(chain), EXCOM_ERR_OK);
	}
};
