#!/bin/sh

source 'common.sh'

#putline <corpus_file> <parameter> <value> <tex_file> <tab_file> <orig_size>
putline() {
	t=`mintime <"$d/$2$3_1.txt"`
	xt=`separators $t`
	o=`wc -c "$d/$2$3" | grep -o '^[0-9][0-9]*'`
	rat=`echo "scale=7; $o / $6" | bc`
	xrat=`separators $rat`
	printf '%10s%16s%16s\\\\\n' "$3&" "$xt &" "$xrat" >>"$4"
	printf '%10s%16s%16s\n' "$3" "$t" "$rat" >>"$5"
}

# obtain sizes of the original corpus files to be able to compute ratio
size1=`wc -c corpus/kennedy.xls | grep -o '^[0-9][0-9]*'`
size2=`wc -c corpus/asyoulik.txt | grep -o '^[0-9][0-9]*'`

out=ppm_mem_kennedy.xls.txt
tex=ppm_mem_kennedy.xls.tex
# write header
echo '\hline' >$tex
printf 'Memory[B]&%16s%16s\\\\\n' "Time[\$\\mu\$s]&" "Ratio&" >>$tex
echo '\hline' >>$tex
printf 'Memory[B] %16s%16s\n' "Time[us]" "Ratio" >$out
echo -e "\n" >>$out

for mem in 2048 16384 65536 262144 1048576 4194304; do
	putline kennedy.xls m $mem "$tex" "$out" $size1
done

echo '\hline' >>$tex

out=ppm_ord_asyoulik.txt.txt
tex=ppm_ord_asyoulik.txt.tex
# write header
echo '\hline' >$tex
printf 'Order    &%16s%16s\\\\\n' "Time[\$\\mu\$s]&" "Ratio&" >>$tex
echo '\hline' >>$tex
printf 'Order     %16s%16s\n' "Time[us]" "Ratio" >$out
echo -e "\n" >>$out

for ord in `seq 2 4 32`; do
	putline asyoulik.txt o $ord "$tex" "$out" $size2
done

echo '\hline' >>$tex


