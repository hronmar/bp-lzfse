set logscale y
unset logscale x
set key top right
#set key off
set terminal postscript eps color

set xlabel "Corpus file"
set grid
set style data histogram

### -------
### |times|
### -------
set ylabel "Compression time"
set title "Compression times on files from Canterbury corpus"
set xrange[-1 : 11 ]

set xtics rotate by -45 \
	("alice29.txt" 0, "asyoulik.txt" 1, "cp.html" 2, "fields.c" 3,\
	"grammar.lsp" 4, "kennedy.xls" 5, "lcet10.txt" 6, "plrabn12.txt" 7,\
	"ptt5" 8, "sum" 9, "xargs.1" 10)
set yrange[400:3e8]
set ytics("1 ms" 1000, "10 ms" 10000, "100 ms" 100000, "1 s" 1000000, "10 s" 10000000, "100 s" 1e8)
#unset logscale y

set output "comp_time-col.eps"
set style fill solid 1.0 border -1
plot 'time_all.txt' index 1 using 2 title 'ACB' lc rgb "#D00000",\
     '' index 1 using 3 title 'DCA' lc rgb '#00CC00',\
     '' index 1 using 4 title 'PPM' lc rgb '#1F71E5'

set output "comp_time-bw.eps"
set style fill solid 1.0 border -1
plot 'time_all.txt' index 1 using 2 title 'ACB' lc rgb "#000000",\
     '' index 1 using 3 title 'DCA' lc rgb '#777777',\
     '' index 1 using 4 title 'PPM' lc rgb '#cccccc'

set ylabel "Decompression time"
set title "Decompression times on files from Canterbury corpus"

set output "decomp_time-col.eps"
set style fill solid 1.0 border -1
plot 'time_all.txt' index 1 using 5 title 'ACB' lc rgb "#D00000",\
     '' index 1 using 6 title 'DCA' lc rgb '#00CC00',\
     '' index 1 using 7 title 'PPM' lc rgb '#1F71E5'

set output "decomp_time-bw.eps"
set style fill solid 1.0 border -1
plot 'time_all.txt' index 1 using 5 title 'ACB' lc rgb "#000000",\
     '' index 1 using 6 title 'DCA' lc rgb '#777777',\
     '' index 1 using 7 title 'PPM' lc rgb '#cccccc'



