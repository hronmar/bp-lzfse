#set logscale y
set logscale x
set key off
set terminal postscript eps color

set xlabel "Memory [B]"
set ylabel "Time [s]"
set grid
set style data linespoints


### --------
### |memory|
### --------
set title "PPM: Compression time depending on memory size\nfile kennedy.xls"
set xrange[14000 : 4800000 ]
set xtics("16KiB" 16384, "64KiB" 65536, "256KiB" 262144,\
	"1MiB" 1048576, "4MiB" 4194304)
unset logscale y

set output "ppm_memory-col.eps"
#fun(t) = a * exp(-t / b)
#a=15
#b=2000
##fit fun(x) 'ppm_mem_kennedy.xls.txt' index 1 using 1:($2/1000000) via a, b
plot 'ppm_mem_kennedy.xls.txt' index 1 using 1:($2/1000000) w points
##, fun(x)
##  lc rgb "#1F71E5"

set output "ppm_memory-col2.eps"
plot 'ppm_mem_kennedy.xls.txt' index 1 using 1:3 w points

### |order|

set key bottom right
set title "PPM: Compression time depending on method order\nfile asyoulik.txt"
unset logscale x
set xrange[0:32]
set xtics(2,6,10,14,18,22,26,30)
set ylabel "Time [us]"
set xlabel "Method order"

ordfun(t) = a/t + c
a=130000
c=-94000
fit ordfun(x) 'ppm_ord_asyoulik.txt.txt' index 1 using 1:2 via a,c
set yrange[30000:190000]
set output "ppm_order-col.eps"
plot 'ppm_ord_asyoulik.txt.txt' index 1 using 1:2 title 'PPM' w points lw 3,\
	ordfun(x) title '-a/x + b' w lines lw 3

set output "ppm_order-bw.eps"
plot 'ppm_ord_asyoulik.txt.txt' index 1 using 1:2 title 'PPM' w points lw 3 lt 0 lc rgb "#000000",\
	ordfun(x) title '-a/x + b' w lines lw 3 lt 0 lc rgb "#000000"


set key top right
set title "PPM: Compression ratio depending on method order\nfile asyoulik.txt"
set ylabel "Ratio"
unset logscale y
set yrange [0.2:0.4]
set ytics(0.22,0.24,0.26,0.28,0.30,0.32,0.34,0.36,0.38)

ordratfun(t) = 0.2893057
set output "ppm_order2-col.eps"
plot 'ppm_ord_asyoulik.txt.txt' index 1 using 1:3 title 'PPM' w points lw 3,\
	ordratfun(x) title 'c' w lines lw 3

set output "ppm_order2-bw.eps"
plot 'ppm_ord_asyoulik.txt.txt' index 1 using 1:3 title 'PPM' w points lw 3 lt 0 lc rgb '#000000',\
	ordratfun(x) title 'c' w lines lw 3 lt 0 lc rgb "#000000"

exit

