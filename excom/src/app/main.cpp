#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#ifdef HAVE_CONFIG_H
  #include <config.h>
#endif
#ifdef HAVE_GETOPT_H
  #ifndef _GNU_SOURCE
    #define _GNU_SOURCE
    #include <getopt.h>
    #undef _GNU_SOURCE
  #else
    #include <getopt.h>
  #endif
#endif

#include <excom.h>

#define OUTCTX "hist.ctx"
#define OUTCONF "hist.conf"

#define EXCAPI(call, handler)\
	do {\
	res = call;\
	if (res != EXCOM_ERR_OK) {\
		fprintf (stderr, "%s at line %d failed with code %d (%s)\n", #call, __LINE__, res, translateCode(res));\
		goto handler; }\
	} while(0)

const char* translateCode(int err)
{
	switch(err) {
	case EXCOM_ERR_OK: return "EXCOM_ERR_OK";
	case EXCOM_ERR_OPEN: return "EXCOM_ERR_OPEN";
	case EXCOM_ERR_MEMORY: return "EXCOM_ERR_MEMORY";
	case EXCOM_ERR_METHOD: return "EXCOM_ERR_METHOD";
	case EXCOM_ERR_OPERATION: return "EXCOM_ERR_OPERATION";
	case EXCOM_ERR_HANDLE: return "EXCOM_ERR_HANDLE";
	case EXCOM_ERR_PARAM: return "EXCOM_ERR_PARAM";
	case EXCOM_ERR_VALUE: return "EXCOM_ERR_VALUE";
	case EXCOM_ERR_DUPLICATE: return "EXCOM_ERR_DUPLICATE";
	case EXCOM_ERR_CHAIN: return "EXCOM_ERR_CHAIN";
	case EXCOM_ERR_THREAD: return "EXCOM_ERR_THREAD";
	case EXCOM_ERR_MEASURE: return "EXCOM_ERR_MEASURE";
	case EXCOM_ERR_BUSY: return "EXCOM_ERR_BUSY";
	case EXCOM_ERR_LATER: return "EXCOM_ERR_LATER";
	case EXCOM_ERR_ALLOC: return "EXCOM_ERR_ALLOC";
	case EXCOM_ERR_NOT_NOW: return "EXCOM_ERR_NOT_NOW";
	case EXCOM_ERR_FORMAT: return "EXCOM_ERR_FORMAT";
	case EXCOM_ERR_INCOMPLETE: return "EXCOM_ERR_INCOMPLETE";
	case EXCOM_ERR_SEEKABLE: return "EXCOM_ERR_SEEKABLE";
	default:
		return "unknown error";
	}
}

void printHelp(const char* progname)
{
#ifdef HAVE_GETOPT_H
	static const char *lo_decompress = ", --decompress   ";
	static const char *lo_except     = ", --except=<path>";
	static const char *lo_ignore     = ", --ignore-first ";
	static const char *lo_help       = ", --help         ";
	static const char *lo_input      = ", --input=<path> ";
	static const char *lo_method     = ", --method=<met> ";
	static const char *lo_output     = ", --output=<path>";
	static const char *lo_param      = ", --param=<prm>  ";
	static const char *lo_quiet      = ", --quiet        ";
	static const char *lo_repeat     = ", --repeat=<T>   ";
	static const char *lo_timing     = ", --timing       ";
#else
	static const char *lo_decompress = "                 ";
	static const char *lo_except     = " <path>          ";
	static const char *lo_ignore     = "                 ";
	static const char *lo_help       = "                 ";
	static const char *lo_input      = " <path>          ";
	static const char *lo_method     = " <met>           ";
	static const char *lo_output     = " <path>          ";
	static const char *lo_param      = " <prm>           ";
	static const char *lo_quiet      = "                 ";
	static const char *lo_repeat     = " <T>             ";
	static const char *lo_timing     = "                 ";
#endif
	printf ("Usage: %s [options]\n", progname);
	printf ("where options may be:\n");
	printf ("  -d%s decompress input file (default is to compress)\n", lo_decompress);
	printf ("  -e%s path to exceptions' file (required for DCA, ignored for other)\n", lo_except);
	printf ("  -f%s don't count first run to the overall timing\n", lo_ignore);
	printf ("                      The first run may be way off because of empty cache\n");
	printf ("  -h%s print this help\n", lo_help);
	printf ("  -i%s path to the input file\n", lo_input);
	printf ("  -m%s select method <met>, use ? for a list\n", lo_method);
	printf ("  -o%s path to the output file\n", lo_output);
	printf ("  -p%s <prm> is a comma separated list of parameters\n                      of the method, use ? for a list\n", lo_param);
	printf ("  -q%s don't output anything except errors\n", lo_quiet);
	printf ("  -r%s repeat the process T times\n", lo_repeat);
	printf ("  -t%s measure time spent by the process\n", lo_timing);
}

void printMethods()
{
	printf ("Supported compression methods:\n");
	printf ("  copy    Just copies input to output\n");
	printf ("  acb     Associative coder of Buyanovsky\n");
       printf ("  arith   Arithmetic coding\n");
	printf ("  bwt     Burrows-Wheeler transform\n");
	printf ("  dca     Data compression using antidictionaries\n");
	printf ("  dhuff   Dynamic Huffman coding\n");
	printf ("  integer Integer compression methods\n");
	printf ("  lz77    Lempel-Ziv compression method from 1977\n");
	printf ("  lz78    Lempel-Ziv compression method from 1978\n");
	printf ("  lzap    Variant of LZW based on LZMW by Storer from 1988\n");
	printf ("  lzfse   Lempel-Ziv style algorithm using Finite State Entropy coding\n");
	printf ("  lzmw    Lempel-Ziv-Miller-Wegman, variant of LZW from 1985\n");
	printf ("  lzss    Lempel–Ziv–Storer–Szymanski compression method from 1982\n");
	printf ("  lzw     Lempel–Ziv–Welch compression method from 1984\n");
	printf ("  lzy     Variant of LZW by Dan Bernstein\n");
	printf ("  mtf     Move-to-front transform\n");
	printf ("  ppm     Prediction by partial matching\n");	
	printf ("  rle_n   RLE-N compression method\n");
	printf ("  sfano   Shannon-Fano coding\n");
	printf ("  shuff   Static Huffman coding\n");
}

void printParams(int method)
{
	switch(method) {
	case EXCOM_MET_COPY:
		printf ("Compression module 'copy' has no parameters\n");
		break;
	case EXCOM_MET_SHUFF:
		printf ("Compression module 'shuff' has no parameters\n");
		break;
	case EXCOM_MET_DHUFF:
		printf ("Compression module 'dhuff' has no parameters\n");
		break;
	case EXCOM_MET_SFANO:
		printf ("Compression module 'sfano' has no parameters\n");
		break;
	case EXCOM_MET_ACB:
		printf ("Parameters available for compression method 'acb':\n");
		printf ("  t        Produce a histogram for the first two fields of the triplet.\n");
		printf ("           One will be saved in hist.ctx, the other in hist.conf.\n");
		printf ("  b=<N>    Logarithm of maximum dictionary size.\n");
		printf ("           The actual size will be 2**N elements.\n");
                printf ("  c=<N>    Logarithm of dictionary size to start monitor compression ratio.\n");
		printf ("           The actual size will be 2**N elements.\n");
		printf ("  d=<N>    Logarithm of maximum context - content distance.\n");
		printf ("           The actual distance size will be 2**N contents.\n");
		printf ("  l=<N>    Logarithm of maximum content length.\n");
		printf ("           The actual length size will be 2**N.\n");
                printf ("  h        Use huffman coding. Default is arithmetic.\n");
		break;
	case EXCOM_MET_ARITH:
		printf ("Parameters available for compression method 'arith':\n");
		printf ("  s        Use static encoder instead of adaptive \n");
                break;
	case EXCOM_MET_DCA:
		printf ("Parameters available for compression method 'dca':\n");
		printf ("  m=<N>    Maximal antiword length will be N bits (1 <= N <= 255)\n");
		printf ("  r        Raw output\n");
		break;
	case EXCOM_MET_LZ77:
		printf ("Parameters available for compression method 'lz77':\n");
		printf ("  s=<N>    The size of the Search Buffer (1 <= N <= 65536)\n");
		printf ("  l=<N>    The size of the Look-ahead Buffer (1 <= N <= 65536)\n");
		break;
	case EXCOM_MET_LZSS:
		printf ("Parameters available for compression method 'lzss':\n");
		printf ("  s=<N>    The size of the Search Buffer (1 <= N <= 65536)\n");
		printf ("  l=<N>    The size of the Look-ahead Buffer (1 <= N <= 65536)\n");
		break;
	case EXCOM_MET_LZ78:
		printf ("Parameters available for compression method 'lz78':\n");
		printf ("  d=<N>    The size of the dictionary (1 <= N <= 65536)\n");
		break;
	case EXCOM_MET_LZW:
		printf ("Parameters available for compression method 'lzw':\n");
		printf ("  s=<N>    The size of input character in bits (1 <= N <= 31). Default value is 8.");
		printf ("  d=<N>    The size of the dictionary (2^s <= N < ~0u). Default value is 4096.\n");
		break;
	case EXCOM_MET_LZMW:
		printf ("Parameters available for compression method 'lzmw':\n");
		printf ("  s=<N>    The size of input character in bits (1 <= N <= 31). Default value is 8.");
		printf ("  d=<N>    The size of the dictionary (2^s <= N < ~0u). Default value is 4096.\n");
		break;
	case EXCOM_MET_LZAP:
		printf ("Parameters available for compression method 'lzap':\n");
		printf ("  s=<N>    The size of input character in bits (1 <= N <= 31). Default value is 8.");
		printf ("  d=<N>    The size of the dictionary (2^s <= N < ~0u). Default value is 4096.\n");
		break;
	case EXCOM_MET_LZY:
		printf ("Parameters available for compression method 'lzy':\n");
		printf ("  s=<N>    The size of input character in bits (1 <= N <= 31). Default value is 8.");
		printf ("  d=<N>    The size of the dictionary (2^s <= N < ~0u). Default value is 4096.\n");
		break;
	case EXCOM_MET_PPM:
		printf ("Parameters available for compression method 'ppm':\n");
		printf ("  m=<N>    Maximal amount of memory the method will use (in bytes, min. 2KiB)\n");
		printf ("  o=<N>    Order of the method (2 <= N <= 32)\n");
		printf ("  r        Raw output\n");
		break;
	case EXCOM_MET_INTEGER:
		printf ("Parameters available for compression method 'integer compression':\n");
		printf ("  m=<N>    Integer compression method\n");
		printf ("  p=<N>    Optional parameter\n");
		break;
	case EXCOM_MET_BWT:
		printf ("Parameters available for compression method 'bwt':\n");
		printf ("  s=<N>    The block size in bytes (2 <= N <= 255). Default value is 10.\n");
		break;
	case EXCOM_MET_MTF:
		printf ("Compression module 'mtf' has no parameters\n");
		break;
	case EXCOM_MET_RLE_N:
		printf ("Parameters available for compression method 'rle_n':\n");
		printf ("  N=<N>    Threshold (4 <= N <= ~0u). Default value is 4.\n");
		break;
	case EXCOM_MET_LZFSE:
		printf ("Parameters available for compression method 'lzfse':\n");
		printf ("  g=<N>    Match length in bytes to cause immediate emission (4 <= N <= ~0u). Default value is 40.\n");
		printf ("  h=<N>    Number of bits for hash function to produce (10 <= N <= 16). Default value is 14.\n");
		break;
	}
}

char *extractArg(char *param, int *pos)
{
	char *start;
	int x = *pos;
	x++;
	if (param[x] != '=') return NULL;
	start = &param[x + 1];
	do {
		x++;
		if (param[x] == ',') {
			param[x] = 0;
			x++;
			break;
		} else if (param[x] == 0) {
			break;
		}
	} while(true);
	*pos = x;

	return start;
}

#define UNKNOWN(method) \
	fprintf (stderr, "Unknown parameter '%c' for method " method "\n", c);

#define UINT_ARG(err) \
	t = extractArg(param, &pos);\
	if (t == NULL) {\
		fprintf (stderr, "%s\n", err);\
		return -1;\
	}\
	ui = (unsigned int) atoi(t);\
	arg = &ui;

#define VOID_ARG(err) \
	pos++;\
	if (param[pos] == ',') pos++;\
	else if (param[pos] != 0) {\
		fprintf (stderr, "%s\n", err);\
		return -1;\
	}\
	arg = NULL;

int parseParams(char *param, int method, unsigned int comp)
{
	if (param == NULL) return 0;
	if (method == EXCOM_MET_COPY) {
		fprintf (stderr, "The 'copy' method does not accept any parameters, but \"%s\" given.\n", param);
		return -1;
	}
	int pos = 0, res = 0, x;
	char c, *t;
	int p;
	void *arg;
	unsigned int ui;

	while (param[pos] != 0) {
		c = param[pos];
		p = -1;
		arg = NULL;
		switch (method) {
		case EXCOM_MET_ACB:
			if (c == 't') {
				res = 1;
				pos++;
			} else if (c == 'b') {
				UINT_ARG("Missing argument to parameter 'b'");
				p = EXCOM_PARAM_ACB_SEARCH_BUFFER_SIZE;
			} else if (c == 'c') {
				UINT_ARG("Missing argument to parameter 'c'");
				p = EXCOM_PARAM_ACB_CHECK_SIZE;
			} else if (c == 'd') {
				UINT_ARG("Missing argument to parameter 'd'");
				p = EXCOM_PARAM_ACB_DISTANCE;
			} else if (c == 'l') {
				UINT_ARG("Missing argument to parameter 'l'");
				p = EXCOM_PARAM_ACB_LENGTH;
                        } else if (c == 'h') {
				VOID_ARG("Extra argument to parameter 'h'");
				p = EXCOM_PARAM_ACB_CODING;
			} else {
				UNKNOWN("ACB");
				return -1;
			}
			break;
                case EXCOM_MET_ARITH:
			if (c == 's') {
				VOID_ARG("Extra argument to parameter 's'");
				p = EXCOM_PARAM_ARITH_STATIC;
			} else {
				UNKNOWN("ARITH");
				return -1;
			}
			break;
		case EXCOM_MET_DCA:
			if (c == 'm') {
				UINT_ARG("Missing argument to parameter 'm'");
				p = EXCOM_PARAM_DCA_MAXLENGTH;
			} else if (c == 'r') {
				VOID_ARG("Extra argument to parameter 'r'");
				p = EXCOM_PARAM_DCA_RAW;
				ui = 1;
				arg = &ui;
			} else {
				UNKNOWN("DCA");
				return -1;
			}
			break;
		case EXCOM_MET_LZ77:
			if (c == 's') {
				UINT_ARG("Missing argument to parameter 's'");
				p = EXCOM_PARAM_LZ77_SEARCH_BUFFER_SIZE;
			} else if (c == 'l') {
				UINT_ARG("Missing argument to parameter 'l'");
				p = EXCOM_PARAM_LZ77_LOOKAHEAD_BUFFER_SIZE;
			} else {
				UNKNOWN("LZ77");
				return -1;
			}
			break;
		case EXCOM_MET_LZSS:
			if (c == 's') {
				UINT_ARG("Missing argument to parameter 's'");
				p = EXCOM_PARAM_LZSS_SEARCH_BUFFER_SIZE;
			} else if (c == 'l') {
				UINT_ARG("Missing argument to parameter 'l'");
				p = EXCOM_PARAM_LZSS_LOOKAHEAD_BUFFER_SIZE;
			} else {
				UNKNOWN("LZSS");
				return -1;
			}
			break;
		case EXCOM_MET_LZ78:
			if (c == 'd') {
				UINT_ARG("Missing argument to parameter 'd'");
				p = EXCOM_PARAM_LZ78_DICTIONARY_SIZE;
			} else {
				UNKNOWN("LZ78");
				return -1;
			}
			break;
		case EXCOM_MET_LZW:
			if (c == 'd') {
				UINT_ARG("Missing argument to parameter 'd'");
				p = EXCOM_PARAM_LZW_DICTIONARY_SIZE;
			} else if (c == 's') {
				UINT_ARG("Missing argument to parameter 's'");
				p = EXCOM_PARAM_LZW_CHAR_BITSIZE;
			} else {
				UNKNOWN("LZW");
				return -1;
			}
			break;
		case EXCOM_MET_LZMW:
			if (c == 'd') {
				UINT_ARG("Missing argument to parameter 'd'");
				p = EXCOM_PARAM_LZMW_DICTIONARY_SIZE;
			} else if (c == 's') {
				UINT_ARG("Missing argument to parameter 's'");
				p = EXCOM_PARAM_LZMW_CHAR_BITSIZE;
			} else {
				UNKNOWN("LZMW");
				return -1;
			}
			break;
		case EXCOM_MET_LZAP:
			if (c == 'd') {
				UINT_ARG("Missing argument to parameter 'd'");
				p = EXCOM_PARAM_LZAP_DICTIONARY_SIZE;
			} else if (c == 's') {
				UINT_ARG("Missing argument to parameter 's'");
				p = EXCOM_PARAM_LZAP_CHAR_BITSIZE;
			} else {
				UNKNOWN("LZAP");
				return -1;
			}
			break;
		case EXCOM_MET_LZY:
			if (c == 'd') {
				UINT_ARG("Missing argument to parameter 'd'");
				p = EXCOM_PARAM_LZY_DICTIONARY_SIZE;
			} else if (c == 's') {
				UINT_ARG("Missing argument to parameter 's'");
				p = EXCOM_PARAM_LZY_CHAR_BITSIZE;
			} else {
				UNKNOWN("LZY");
				return -1;
			}
			break;
		case EXCOM_MET_PPM:
			if (c == 'm') {
				UINT_ARG("Missing argument to parameter 'm'");
				p = EXCOM_PARAM_PPM_MEMORY_SIZE;
			} else if (c == 'o') {
				UINT_ARG("Missing argument to parameter 'o'");
				p = EXCOM_PARAM_PPM_ORDER;
			} else if (c == 'r') {
				VOID_ARG("Extra argument to parameter 'r'");
				p = EXCOM_PARAM_PPM_RAW;
				ui = 1;
				arg = &ui;
			} else {
				UNKNOWN("PPM");
				return -1;
			}
			break;
		case EXCOM_MET_INTEGER:
			if (c == 'm') {
				UINT_ARG("Missing argument to parameter 'm'");
				p = EXCOM_PARAM_INTEGER_METHOD;
			} else if (c == 'p') {
				UINT_ARG("Missing argument to paramater 'p'");
				p = EXCOM_PARAM_INTEGER_PARAM;
			} else {
				UNKNOWN("Integer compression");
				return -1;
			}
			break;
		case EXCOM_MET_BWT:
			if (c == 's') {
				UINT_ARG("Missing argument to parameter 's'");
				p = EXCOM_PARAM_BWT_BLOCK_SIZE;
			} else {
				UNKNOWN("BWT");
				return -1;
			}
			break;
		case EXCOM_MET_RLE_N:
			if (c == 'N') {
				UINT_ARG("Missing argument to parameter 'N'");
				p = EXCOM_PARAM_RLE_N_THRESHOLD;
			} else {
				UNKNOWN("RLE-N");
				return -1;
			}
			break;
		case EXCOM_MET_LZFSE:
			if (c == 'g') {
				UINT_ARG("Missing argument to parameter 'g'");
				p = EXCOM_PARAM_LZFSE_ENCODE_GOOD_MATCH;
			} else if (c == 'h') {
				UINT_ARG("Missing argument to parameter 'h'");
				p = EXCOM_PARAM_LZFSE_ENCODE_HASH_BITS;
			} else {
				UNKNOWN("LZFSE");
				return -1;
			}
			break;
		}

		// try to set the parameter to the module
		if (p != -1) {
			x = exc_setParameter(comp, p, arg);
			if (x != EXCOM_ERR_OK) {
				fprintf (stderr, "Setting parameter failed. Parameter '%c', error code %d (%s)\n", c, x, translateCode(x));
				return -1;
			}
		}
	}; // while param[pos] != 0

	return res;
}

int main (int argc, char **argv)
{
	bool decomp = false;
	bool ign_first = false;
	char *input = NULL;
	char *output = NULL;
	char *met_name = (char*)"copy";
	int method = EXCOM_MET_COPY;
	char *param = NULL;
	char *except = NULL;
	unsigned int repeat = 1;
	bool timing = false;
	bool quiet = false;

	static const char *argstring = "de:fhi:m:o:p:qr:t";
	int opt;
	while(1) {
#ifdef HAVE_GETOPT_H
		static struct option long_options[] = {
			{"decompress", 0, 0, 'd'},
			{"except", 1, 0, 'e'},
			{"ignore-first", 0, 0, 'f'},
			{"help", 0, 0, 'h'},
			{"input", 1, 0, 'i'},
			{"method", 1, 0, 'm'},
			{"output", 1, 0, 'o'},
			{"param", 1, 0, 'p'},
			{"quiet", 0, 0, 'q'},
			{"repeat", 1, 0, 'r'},
			{"timing", 0, 0, 't'},
			{0, 0, 0, 0}
		};
		opt = getopt_long(argc, argv, argstring, long_options, 0);
#else
		opt = getopt(argc, argv, argstring);
#endif
		if (opt == -1) break;
		switch(opt) {
		case 'd':
			decomp = true;
			break;
		case 'e':
			except = optarg;
			break;
		case 'f':
			ign_first = true;
			break;
		case 'h':
			printHelp(argv[0]);
			exit(EXIT_SUCCESS);
		case 'i':
			input = optarg;
			break;
		case 'm':
			if (!strcmp(optarg, "?")) {
				printMethods();
				exit(EXIT_SUCCESS);
			} else if (!strcmp(optarg, "copy")) {
				method = EXCOM_MET_COPY;
			} else if (!strcmp(optarg, "shuff")) {
				method = EXCOM_MET_SHUFF;
			} else if (!strcmp(optarg, "dhuff")) {
				method = EXCOM_MET_DHUFF;
			} else if (!strcmp(optarg, "sfano")) {
				method = EXCOM_MET_SFANO;
			} else if (!strcmp(optarg, "lz77")) {
				method = EXCOM_MET_LZ77;
			} else if (!strcmp(optarg, "lz78")) {
				method = EXCOM_MET_LZ78;
			} else if (!strcmp(optarg, "lzss")) {
				method = EXCOM_MET_LZSS;
			} else if (!strcmp(optarg, "lzw")) {
				method = EXCOM_MET_LZW;
			} else if (!strcmp(optarg, "lzmw")) {
				method = EXCOM_MET_LZMW;
			} else if (!strcmp(optarg, "lzap")) {
				method = EXCOM_MET_LZAP;
			} else if (!strcmp(optarg, "lzy")) {
				method = EXCOM_MET_LZY;
			} else if (!strcmp(optarg, "acb")) {
				method = EXCOM_MET_ACB;
                     } else if (!strcmp(optarg, "arith")) {
				method = EXCOM_MET_ARITH;
			} else if (!strcmp(optarg, "dca")) {
				method = EXCOM_MET_DCA;
			} else if (!strcmp(optarg, "ppm")) {
				method = EXCOM_MET_PPM;
			} else if (!strcmp(optarg, "integer")) {
				method = EXCOM_MET_INTEGER;
			} else if (!strcmp(optarg, "bwt")) {
				method = EXCOM_MET_BWT;
			} else if (!strcmp(optarg, "mtf")) {
				method = EXCOM_MET_MTF;
			} else if (!strcmp(optarg, "rle_n")) {
				method = EXCOM_MET_RLE_N;
			} else if (!strcmp(optarg, "lzfse")) {
				method = EXCOM_MET_LZFSE;
			} else {
				fprintf (stderr, "Invalid compression method: \"%s\".\nUse -m? for a list of supported methods.\n", optarg);
				exit(EXIT_FAILURE);
			}
			met_name = optarg;
			break;
		case 'o':
			output = optarg;
			break;
		case 'p':
			param = optarg;
			if (!strcmp(param, "?")) {
				printParams(method);
				exit(EXIT_SUCCESS);
			}
			break;
		case 'q':
			quiet = true;
			break;
		case 'r':
			repeat = (unsigned int) atoi(optarg);
			break;
		case 't':
			timing = true;
			break;
		default:
			printHelp(argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	if (input == NULL) {
		fprintf(stderr, "No input file specified.\n");
		exit(EXIT_FAILURE);
	}
	if (output == NULL) {
		fprintf(stderr, "No output file specified.\n");
		exit(EXIT_FAILURE);
	}
	if (method == EXCOM_MET_DCA && except == NULL) {
		fprintf(stderr, "No exceptions' file specified for the DCA method.\n");
		exit(EXIT_FAILURE);
	}
	if (!quiet) {
		printf ("Summary:\n");
		printf ("  Method: %s\n", met_name);
		printf ("  Operation: %s\n", decomp?"decompression":"compression");
		if (param == NULL) {
			printf ("  No parameters\n");
		} else {
			printf ("  Parameters: %s\n", param);
		}
		printf ("  Repeat %d time(s)\n", repeat);
		printf ("  Input file: %s\n", input);
		printf ("  Output file: %s\n", output);
		if (method == EXCOM_MET_DCA) {
			printf ("  Exceptions' file: %s\n", except);
		}
		printf ("  Measure time? %s\n", timing?"yes":"no");
	}

	char *xparam = NULL;
	if (param != NULL) {
		int l = strlen(param);
		xparam = new char[l+1];
	}

	unsigned int chain=0, comp=0,
		histctx=0, histconf=0,
		ioin=0, ioout=0, ioexc=0,
		iopipe1=0, iopipe2=0,
		ioctx=0, ioconf=0;
	int res;
	unsigned int v;
	int u;
	int hist;

	// cumulative timers
	unsigned long tmchain = 0, tmmethod = 0, tmhistctx = 0, tmhistconf = 0;
	unsigned long tm;

	if (timing) {
		printf ("run\tchain\t%s\thistctx\thistconf\n", met_name);
	}

	// do given number of times
	for (unsigned int i = 0; i < repeat; i++) {

	// construct chain
	// The entire chain has to be reconstructed each run, because the chain
	// can only be used once.
	EXCAPI(exc_compressionChain(&chain), fail);
	// compression module
	EXCAPI(exc_compModule((enum exc_method_e)method, decomp?EXCOM_OP_DECOMPRESS:EXCOM_OP_COMPRESS, chain, &comp), fail_chain);

	// parse parameters
	if (param != NULL) strcpy(xparam, param);
	hist = parseParams(xparam, method, comp);
	if (hist == -1) {
		goto fail_chain;
	}
	else if (hist == 1) {
		EXCAPI(exc_compModule(EXCOM_MET_HISTOGRAM, EXCOM_OP_HISTOGRAM, chain, &histctx), fail_chain);
		EXCAPI(exc_compModule(EXCOM_MET_HISTOGRAM, EXCOM_OP_HISTOGRAM, chain, &histconf), fail_chain);

		// histograms' parameters
		v = 4;
		EXCAPI(exc_setParameter(histctx, EXCOM_PARAM_HIST_BYTES_PER_CHAR, &v), fail_chain);
		u = 0;
		EXCAPI(exc_setParameter(histctx, EXCOM_PARAM_HIST_RANGE_MIN, &u), fail_chain);
		u = 8192;
		EXCAPI(exc_setParameter(histctx, EXCOM_PARAM_HIST_RANGE_MAX, &u), fail_chain);
		v = 64;
		EXCAPI(exc_setParameter(histctx, EXCOM_PARAM_HIST_INTERVALS, &v), fail_chain);

		v = 4;
		EXCAPI(exc_setParameter(histconf, EXCOM_PARAM_HIST_BYTES_PER_CHAR, &v), fail_chain);
		u = 0;
		EXCAPI(exc_setParameter(histconf, EXCOM_PARAM_HIST_RANGE_MIN, &u), fail_chain);
		u = 128;
		EXCAPI(exc_setParameter(histconf, EXCOM_PARAM_HIST_RANGE_MAX, &u), fail_chain);
		v = 32;
		EXCAPI(exc_setParameter(histconf, EXCOM_PARAM_HIST_INTERVALS, &v), fail_chain);
	}

	// IO modules and connections
	EXCAPI(exc_fileIOModule(input, EXCOM_IO_READ, &ioin), fail_chain);
	EXCAPI(exc_fileIOModule(output, EXCOM_IO_WRITE, &ioout), fail_io1);
	if (hist) {
		EXCAPI(exc_fileIOModule(OUTCTX, EXCOM_IO_WRITE, &ioctx), fail_io2);
		EXCAPI(exc_fileIOModule(OUTCONF, EXCOM_IO_WRITE, &ioconf), fail_io3);
		EXCAPI(exc_memIOModule(NULL, 0, EXCOM_IO_PIPE, &iopipe1), fail_io4);
		EXCAPI(exc_memIOModule(NULL, 0, EXCOM_IO_PIPE, &iopipe2), fail_io5);

		EXCAPI(exc_connectModules(iopipe2, comp, EXCOM_CONN_ACB_CONFORMITY), fail_io6);
		EXCAPI(exc_connectModules(iopipe2, histconf, EXCOM_CONN_DATA_INPUT), fail_io5);
		EXCAPI(exc_connectModules(iopipe1, comp, EXCOM_CONN_ACB_CONTEXT), fail_io5);
		EXCAPI(exc_connectModules(iopipe1, histctx, EXCOM_CONN_DATA_INPUT), fail_io4);
		EXCAPI(exc_connectModules(ioconf, histconf, EXCOM_CONN_DATA_OUTPUT), fail_io4);
		EXCAPI(exc_connectModules(ioctx, histctx, EXCOM_CONN_DATA_OUTPUT), fail_io3);
	}
	if (method == EXCOM_MET_DCA) {
		if (decomp) {
			EXCAPI(exc_fileIOModule(except, EXCOM_IO_READ, &ioexc), fail_io2);
			EXCAPI(exc_connectModules(ioexc, comp, EXCOM_CONN_DCA_EXC_INPUT), fail_io7);
		} else {
			EXCAPI(exc_fileIOModule(except, EXCOM_IO_WRITE, &ioexc), fail_io2);
			EXCAPI(exc_connectModules(ioexc, comp, EXCOM_CONN_DCA_EXC_OUTPUT), fail_io7);
		}
	}
	EXCAPI(exc_connectModules(ioout, comp, EXCOM_CONN_DATA_OUTPUT), fail_io2);
	EXCAPI(exc_connectModules(ioin, comp, EXCOM_CONN_DATA_INPUT), fail_io1);

	// run
	EXCAPI(exc_run(chain), fail_chain);
	EXCAPI(exc_wait(chain), fail_chain);
	EXCAPI(exc_getValue(comp, EXCOM_VALUE_COMP_RESULT, &v), fail_chain);
	if (v != 0) {
		fprintf (stderr, "Compression result: %u (%s)\n", v, translateCode(v));
	}
	if (timing) {
		printf ("%u\t", i+1);
		EXCAPI(exc_getPerformanceData(chain, &tm), fail_chain);
		tmchain += (ign_first && i==0)?0:tm;
		printf ("%lu\t", tm);
		EXCAPI(exc_getPerformanceData(comp, &tm), fail_chain);
		tmmethod += (ign_first && i==0)?0:tm;
		printf ("%lu", tm);
		if (hist) {
			EXCAPI(exc_getPerformanceData(histctx, &tm), fail_chain);
			tmhistctx += (ign_first && i==0)?0:tm;
			printf ("\t%lu\t", tm);
			EXCAPI(exc_getPerformanceData(histconf, &tm), fail_chain);
			tmhistconf += (ign_first && i==0)?0:tm;
			printf ("%lu\n", tm);
		} else {
			printf ("\n");
		}
	}

	EXCAPI(exc_destroyChain(chain), fail);
	} // repeat given number of times

	if (ign_first) repeat--;
	if (timing && (repeat > 0)) {
		if (hist) {
			fprintf (stderr, "Total\t%u\t%lu\t%lu\t%lu\t%lu\n", repeat,
				tmchain, tmmethod, tmhistctx, tmhistconf);
			fprintf (stderr, "Avg\t%.2f\t%.2f\t%.2f\t%.2f\n",
				((double)tmchain)/repeat, ((double)tmmethod)/repeat,
				((double)tmhistctx)/repeat, ((double)tmhistconf)/repeat);
		} else {
			fprintf (stderr, "Total\t%u\t%lu\t%lu\n", repeat, tmchain, tmmethod);
			fprintf (stderr, "Avg\t%.2f\t%.2f\n", ((double)tmchain)/repeat,
				((double)tmmethod)/repeat);
		}
	}

	if (xparam != NULL) delete [] xparam;
	return EXIT_SUCCESS;

fail_io7:
	exc_destroyIOModule(ioexc);
	goto fail_io2;
fail_io6:
	exc_destroyIOModule(iopipe2);
fail_io5:
	exc_destroyIOModule(iopipe1);
fail_io4:
	exc_destroyIOModule(ioconf);
fail_io3:
	exc_destroyIOModule(ioctx);
fail_io2:
	exc_destroyIOModule(ioout);
fail_io1:
	exc_destroyIOModule(ioin);
fail_chain:
	exc_destroyChain(chain);
fail:
	if (xparam != NULL) delete [] xparam;
	return EXIT_FAILURE;
}

