#!/bin/sh

source 'common.sh'

#putline <corpus_file> <method> <tex_file> <tab_file> <met_index>
putline() {
	m=`getmem "$d"/_2$1.$2.txt`
	xm=`separators $m`
	printf '%8s%16s\\\\\n' "${names[$5]} &" "$xm" >>"$3"
	printf '%8s%16s\n' "$2" "$m" >>"$4"
}

#putunline <corpus_file> <method> <tex_file> <tab_file> <met_index>
putunline() {
	m=`getmem "$d"/_2$1.$2.un.txt`
	xm=`separators $m`
	printf '%8s%16s\\\\\n' "de-${names[$5]} &" "$xm" >>"$3"
	printf '%8s%16s\n' "de-$2" "$m" >>"$4"
}

for f in corpus/*; do
	g=`basename "$f"`
	out=mem_"$g".txt
	tex=mem_"$g".tex
	# write header
	echo '\hline' >$tex
	printf 'Method &%16s\\\\\n' "Memory[B]" >>$tex
	echo '\hline' >>$tex
	printf 'Method  %16s\n' "Memory[B]" >$out
	echo -e "\n" >>$out

	j=0
	for met in acb dca ppm; do
		putline "$g" $met "$tex" "$out" $j
		j=$(($j+1))
	done

	echo '\hline' >>$tex
	echo -e "\n" >>$out
	
	j=0
	for met in acb dca ppm; do
		putunline "$g" $met "$tex" "$out" $j
		j=$(($j+1))
	done
	echo '\hline' >>$tex
done

