names[0]='ACB'
names[1]='DCA'
names[2]='PPM'

get_orig_sizes() {
	# determine sizes of the original files of the corpus and store
	# the values in 'sizes' array
	i=0
	for f in corpus/*; do
		g=`basename "$f"`
		sizes[$i]=`wc -c "$f" | grep -o '^[0-9][0-9]*'`
		i=$(($i+1))
	done
}

separators() {
	# this is everything but flexible; well, ...
	if `echo "$1" | grep '\.' >/dev/null 2>/dev/null`; then
		# decimal number
		echo -n "$1" | sed 's/^\./0./;
		s/\.\([0-9][0-9][0-9]\)\([0-9]\)/.\1 \2/;
		s/ \([0-9][0-9][0-9]\)\([0-9]\)/ \1 \2/;
		s/\([0-9]\)\([0-9][0-9][0-9]\)\./\1 \2./;
		s/\([0-9]\)\([0-9][0-9][0-9]\) /\1 \2 /'
	else
		# integer
		echo -n "$1" | sed 's/\([0-9]\)\([0-9][0-9][0-9]\)$/\1 \2/;s/\([0-9]\)\([0-9][0-9][0-9]\) /\1 \2 /'
	fi
}

mintime() {
        # find minimal time per iteration
	min=-1
	read l
	if [ $? -ne 0 ]; then echo -n 0; return ; fi
	while read l; do
		v=`echo "$l" | sed 's/^.*[ |\t]\([0-9][0-9]*\)$/\1/'`
		if [ "$min" -eq -1 ] ; then min=$v
		elif [ "$min" -gt "$v" ] ; then min=$v
		fi
        done
	echo -n "$min"
}

getmem() {
	grep 'heap peak' "$1" | sed 's/.*heap peak: \([0-9][0-9]*\),.*/\1/'
}

# handle arguments
if [ $# -lt 1 ]; then
	echo "Usage $0 <dir to process>"
	exit 1
fi
d="$1"
export d

