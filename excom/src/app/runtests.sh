#!/bin/sh

resdir=`date +"%d-%m_%H-%M-%S"`

mkdir $resdir

for f in korpus/*; do
	met=ppm
	g=`basename $f`.$met
	./app -m $met -f -r 21 -q -t -i $f -o $resdir/$g >$resdir/_1$g.txt 2>$resdir/_2$g.txt
	./app -d -m $met -f -r 21 -q -t -i $resdir/$g -o $resdir/$g.un >$resdir/_1$g.un.txt 2>$resdir/_2$g.un.txt
	met=dca
	g=`basename $f`.$met
	./app -m $met -f -r 21 -q -t -i $f -o $resdir/$g -e $resdir/$g.exc >$resdir/_1$g.txt 2>$resdir/_2$g.txt
	./app -d -m $met -f -r 21 -q -t -i $resdir/$g -o $resdir/$g.un -e $resdir/$g.exc >$resdir/_1$g.un.txt 2>$resdir/_2$g.un.txt
	met=acb
	g=`basename $f`.$met
	./app -m $met -f -r 21 -q -t -i $f -o $resdir/$g -p s=10,l=8 >$resdir/_1$g.txt 2>$resdir/_2$g.txt
	./app -d -m $met -f -r 21 -q -t -i $resdir/$g -o $resdir/$g.un -e $resdir/$g.exc >$resdir/_1$g.un.txt 2>$resdir/_2$g.un.txt
done

