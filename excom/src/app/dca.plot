unset logscale y
unset logscale x
set key top left
set terminal postscript eps color

set xlabel "Maximal antiword length in bits"
set ylabel "Time"
set yrange[4e5: 6e5]
set ytics("400 ms" 4e5, "450 ms" 45e4, "500 ms" 5e5, "550 ms" 55e4)
set grid

set title "DCA: Compression time depending on maximal antiword length\nfile kennedy.xls"
set xrange[2 : 22]
set xtics(4,8,12,16,20)

set output "dca_max-tim-col.eps"
mltfun(t) = a * t**3 + b * t ** 2 + c
a=7.4728
b=74.7283
c=450326
#mltfun(t) = a * t**2 + b * t + c
#a=352
#b=-2940
#c=458754
fit mltfun(x) 'dca_max_kennedy.xls.txt' index 1 using 1:2 via a, b, c
plot 'dca_max_kennedy.xls.txt' index 1 using 1:2 title 'DCA' w points lw 3,\
	mltfun(x) title 'a.x^3 + b.x^2 + c' w lines lw 3

set output "dca_max-tim-bw.eps"
plot 'dca_max_kennedy.xls.txt' index 1 using 1:2 title 'DCA' w points lw 3 lt 0 lc rgb "#000000",\
	mltfun(x) title 'a.x^3 + b.x^2 + c' w lines lw 3 lt 0 lc rgb "#000000"

# | ratio |
set key top right

set ylabel "Ratio"
set yrange[0.7: 1.01]
set grid

set title "DCA: Compression ratio depending on antiword length\nfile kennedy.xls"
set ytics(0.75, 0.8, 0.85, 0.9, 0.95, 1.0)

mlrfun(t) = a * t**3 + b * t ** 2 + c
a=-0.0000525408
b=0.0004243956
c=0.9965741831
fit mlrfun(x) 'dca_max_kennedy.xls.txt' index 1 using 1:3 via a, b, c

set output "dca_max-rat-col.eps"
plot 'dca_max_kennedy.xls.txt' index 1 using 1:3 title 'DCA' w points lw 3,\
	'' index 1 using 1:4 title 'DCA inc. exceptions' w points lw 3,\
	mlrfun(x) title 'a.x^3 + b.x^2 + c' w lines lw 3

set output "dca_max-rat-bw.eps"
plot 'dca_max_kennedy.xls.txt' index 1 using 1:3 title 'DCA' w points lw 3 lt 0 lc rgb "#000000",\
	'' index 1 using 1:4 title 'DCA inc. exceptions' w points lw 3 lt 1 lc rgb "#000000",\
	mlrfun(x) title 'a.x^3 + b.x^2 + c' w lines lw 3 lt 0 lc rgb "#000000"


exit

set output "acb_ss-rat-col.eps"
ssratfun(t) = a * exp(-b*t)
a=3.175
b=0.1041
fit ssratfun(x) 'acb_ss_asyoulik.txt.txt' index 1 using 1:3 via a, b
plot 'acb_ss_asyoulik.txt.txt' index 1 using 1:3 w points lw 3,\
	ssratfun(x) w lines lw 3

set output "acb_ss-rat-bw.eps"
plot 'acb_ss_asyoulik.txt.txt' index 1 using 1:3 w points lw 3 lt 0 lc rgb "#000000",\
	ssratfun(x) w lines lw 3 lt 0 lc rgb "#000000"


