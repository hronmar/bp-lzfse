#!/bin/sh

source 'common.sh'

#putline <corpus_file> <method> <tex_file> <tab_file> <corpus_index> <met_index>
putline() {
	t=`mintime <"$d"/_1$1.$2.txt`
	xt=`separators $t`
	bps=`echo "scale=4; ${sizes[$5]} / ($t / 1000000)" | bc`
	xbps=`separators $bps`
	o=`wc -c "$d"/$1.$2 | grep -o '^[0-9][0-9]*'`
	if [ x"$2" = "xdca" ]; then
		exc=`wc -c "$d"/$1.$2.exc | grep -o '^[0-9][0-9]*'`
		rat=`echo "scale=7; $o / ${sizes[$5]}" | bc`
		xrat=`separators $rat`
		excrat=`echo "scale=7; ($o+$exc) / ${sizes[$5]}" | bc`
		xexcrat=`separators $excrat`
		printf '%8s%16s%16s%16s%16s\\\\\n' "${names[$6]} &" "$xt &" "$xbps &" "$xrat &" "$xexcrat" >>"$3"
		printf '%8s%16s%16s%16s%16s\n' "$2"   "$t"    "$bps"    "$rat"    "$excrat" >>"$4"
	else
		rat=`echo "scale=7; $o / ${sizes[$5]}" | bc`
		xrat=`separators $rat`
		printf '%8s%16s%16s%16s&\\\\\n' "${names[$6]} &" "$xt &" "$xbps &" "$xrat" >>"$3"
		printf '%8s%16s%16s%16s\n' "$2" "$t" "$bps" "$rat" >>"$4"
	fi
}

#putunline <corpus_file> <method> <tex_file> <tab_file> <met_index>
putunline() {
	t=`mintime <"$d"/_1$1.$2.un.txt`
	xt=`separators $t`
	printf '%8s%16s& & & \\\\\n' "de-${names[$5]} &" "$xt" >>"$3"
	printf '%8s%16s\n' "de-$2" "$t" >>"$4"
}

# obtain sizes of the original corpus files to be able to compute ratio
get_orig_sizes
export sizes

i=0
for f in corpus/*; do
	g=`basename "$f"`
	out=res_"$g".txt
	tex=res_"$g".tex
	# write header
	echo '\hline' >$tex
	printf 'Method &%16s%16s%16s %s\\\\\n' "Time[\$\\mu\$s]&" "B/s&" "Ratio&" "Ratio inc. exceptions" >>$tex
	echo '\hline' >>$tex
	printf 'Method  %16s%16s%16s  %s\n' "Time[us]" "B/s" "Ratio" "Ratio inc. exceptions" >$out
	echo -e "\n" >>$out

	j=0
	for met in acb dca ppm; do
		putline "$g" $met "$tex" "$out" $i $j
		j=$(($j+1))
	done

	echo '\hline' >>$tex
	echo -e "\n" >>$out
	
	j=0
	for met in acb dca ppm; do
		putunline "$g" $met "$tex" "$out" $j
		j=$(($j+1))
	done
	echo '\hline' >>$tex

	i=$(($i+1))
done

