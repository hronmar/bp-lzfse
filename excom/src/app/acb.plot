set logscale y
unset logscale x
set key bottom right
set terminal postscript eps color

set xlabel "Logarithm of search buffer size"
set ylabel "Time"
set yrange[1e5: 7e7]
set ytics("100 ms" 1e5, "500 ms" 5e5, "1 s" 1e6, "5 s" 5e6, "10 s" 1e7, "50 s" 5e7)
set grid

set title "ACB: Compression time depending on search buffer size\nfile asyoulik.txt"
set xrange[4 : 15]
set xtics(5,6,7,8,9,10,11,12,13,14)

set output "acb_ss-col.eps"
ssfun(t) = a * exp(b*t)
a=3100
b=0.707
#ssfun(t) = a * t**3 + c
#a=23463
#b=-953761
#c=-2773752
fit ssfun(x) 'acb_ss_asyoulik.txt.txt' index 1 using 1:2 via a, b
plot 'acb_ss_asyoulik.txt.txt' index 1 using 1:2 title 'ACB' w points lw 3,\
	ssfun(x) title 'a.e^(b.x)' w lines lw 3

set output "acb_ss-bw.eps"
plot 'acb_ss_asyoulik.txt.txt' index 1 using 1:2 title 'ACB' w points lw 3 lt 0 lc rgb "#000000",\
	ssfun(x) title 'a.e^(b.x)' w lines lw 3 lt 0 lc rgb "#000000"


set key bottom left
set ylabel "Ratio"
set yrange[0.68: 2.0]
set grid

set title "ACB: Compression ratio depending on search buffer size\nfile asyoulik.txt"
set xrange[4 : 15]
set xtics(5,6,7,8,9,10,11,12,13,14)
set ytics(0.7, 0.8, 1.0, 1.3, 1.6, 1.9)

set output "acb_ss-rat-col.eps"
ssratfun(t) = a * exp(-b*t)
a=3.175
b=0.1041
fit ssratfun(x) 'acb_ss_asyoulik.txt.txt' index 1 using 1:3 via a, b
plot 'acb_ss_asyoulik.txt.txt' index 1 using 1:3 title 'ACB' w points lw 3,\
	ssratfun(x) title 'a.e^(-b.x)' w lines lw 3

set output "acb_ss-rat-bw.eps"
plot 'acb_ss_asyoulik.txt.txt' index 1 using 1:3 title 'ACB' w points lw 3 lt 0 lc rgb "#000000",\
	ssratfun(x) title 'a.e^(-b.x)' w lines lw 3 lt 0 lc rgb "#000000"


