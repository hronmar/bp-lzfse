#!/bin/sh

resdir=`date +"%d-%m_%H-%M-%S"`_dca
f=korpus/kennedy.xls

mkdir $resdir

for m in `seq 4 4 20`; do
	g=m$m
	libtool --mode=execute memusage ./app -m dca -f -r 11 -q -t -i $f -o $resdir/$g -p m=$m -e $resdir/$g.exc >$resdir/${g}_1.txt 2>$resdir/${g}_2.txt
	libtool --mode=execute memusage ./app -d -m dca -f -r 11 -q -t -i $resdir/$g -o $resdir/$g.un -e $resdir/$g.exc >$resdir/${g}_1.un.txt 2>$resdir/${g}_2.un.txt
done

