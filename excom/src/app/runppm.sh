#!/bin/sh

resdir=`date +"%d-%m_%H-%M-%S"`_ppm
f=korpus/kennedy.xls

mkdir $resdir

for m in 2048 16384 65536 262144 1048576 4194304; do
	g=m$m
	./app -m ppm -f -r 11 -q -t -i $f -o $resdir/$g -p m=$m >$resdir/${g}_1.txt 2>$resdir/${g}_2.txt
	./app -d -m ppm -f -r 11 -q -t -i $resdir/$g -o $resdir/$g.un -p m=$m >$resdir/${g}_1.un.txt 2>$resdir/${g}_2.un.txt
done

f=korpus/asyoulik.txt

for o in `seq 2 4 32`; do
	g=o$o
	./app -m ppm -f -r 11 -q -t -i $f -o $resdir/$g -p o=$o >$resdir/${g}_1.txt 2>$resdir/${g}_2.txt
	./app -d -m ppm -f -r 11 -q -t -i $resdir/$g -o $resdir/$g.un >$resdir/${g}_1.un.txt 2>$resdir/${g}_2.un.txt
done

