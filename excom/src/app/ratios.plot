unset logscale y
unset logscale x
set key top left
#set key off
set terminal postscript eps color

set xlabel "Corpus file"
set grid
set style data histogram

### --------
### |ratios|
### --------
set ylabel "Compression ratio"
set title "Compression ratios on files from Canterbury corpus"
set xrange[-1 : 11 ]

set xtics rotate by -45 \
	("alice29.txt" 0, "asyoulik.txt" 1, "cp.html" 2, "fields.c" 3,\
	"grammar.lsp" 4, "kennedy.xls" 5, "lcet10.txt" 6, "plrabn12.txt" 7,\
	"ptt5" 8, "sum" 9, "xargs.1" 10)
set yrange[0.0:1.6]

set output "ratio-col.eps"
set style fill solid 1.0 border -1
plot 'ratio_all.txt' index 1 using 2 title 'ACB' lc rgb "#D00000",\
     '' index 1 using 3 title 'DCA' lc rgb '#00CC00',\
     '' index 1 using 4 title 'DCA inc. exceptions' lc rgb '#003C00',\
     '' index 1 using 5 title 'PPM' lc rgb '#1F71E5'

set output "ratio-bw.eps"
set style fill solid 1.0 border -1
plot 'ratio_all.txt' index 1 using 2 title 'ACB' lc rgb "#000000",\
     '' index 1 using 3 title 'DCA' lc rgb '#dddddd',\
     '' index 1 using 4 title 'DCA inc. exceptions' lc rgb '#555555',\
     '' index 1 using 5 title 'PPM' lc rgb '#aaaaaa'



