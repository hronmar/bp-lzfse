#ifndef __EXCOM_H__
#define __EXCOM_H__

/*
 * File:       excom.h
 * Purpose:    Application interface to the ExCom library
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file excom.h
 *  \brief Main header file for the Extensible Compression Library (libexcom).
 *
 *  This header file contains declarations of the outer API functions provided
 *  by the libexcom library.
 */

/*!
 *  \mainpage ExCom Library overview
 *
 *  \author Filip Simek <simekf1@fel.cvut.cz>
 *  \date 2009
 *
 *  \section purpose Purpose of this library
 *  The purpose of the ExCom library is to provide framework for implementing
 *  various compression methods with a common interface.
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NULL
#define NULL 0
#endif

/*! \defgroup general General
 *  @{*/

/*! \brief List of error codes that can be returned by ExCom library functions */
enum exc_error_e {
	EXCOM_ERR_OK = 0,     /*!< All was OK, the operation was successful */
	EXCOM_ERR_OPEN,       /*!< Unable to open file (file may not exist or the process may have insufficient permissions) */
	EXCOM_ERR_MEMORY,     /*!< Invalid memory pointer */
	EXCOM_ERR_METHOD,     /*!< Invalid compression method (a nonexistent method or a method that was disabled upon compilation) */
	EXCOM_ERR_OPERATION,  /*!< Unsupported operation */
	EXCOM_ERR_HANDLE,     /*!< Invalid module handle */
	EXCOM_ERR_PARAM,      /*!< Invalid parameter */
	EXCOM_ERR_VALUE,      /*!< Invalid parameter value */
	EXCOM_ERR_DUPLICATE,  /*!< Attempt to perform an operation that was already performed before or to set a value that has already been set */
	EXCOM_ERR_CHAIN,      /*!< Invalid or broken compression chain */
	EXCOM_ERR_THREAD,     /*!< Problem with multithreading (p.e. a new thread cannot be created) */
	EXCOM_ERR_MEASURE,    /*!< Problem with performance measurement (i.e. performance measurement was disabled upon compilation) */
	EXCOM_ERR_BUSY,       /*!< The operation cannot succeed, because the object is doing something else */
	EXCOM_ERR_LATER,      /*!< No result yet, try again later */
	EXCOM_ERR_ALLOC,      /*!< Memory allocation failed */
	EXCOM_ERR_NOT_NOW,    /*!< The operation would generally be acceptable, but not right now */
	EXCOM_ERR_FORMAT,     /*!< Data is in wrong format */
	EXCOM_ERR_INCOMPLETE, /*!< An operation was not completed */
	EXCOM_ERR_SEEKABLE    /*!< Some I/O module is not seekable but it needs to be */
};

/*! \brief Sets a parameter of an I/O or compression module.
 *  \param mod_handle Handle to the module to set parameter to
 *  \param param Symbolic constant of the parameter to be set. See the description of the
 *         particular module for a list of its parameters.
 *  \param value Pointer to the actual value of the parameter to be set. Type and size of
 *         the memory area pointed to by \c value depends on the actual type of the module
 *         and the value of \c param.
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_HANDLE - supplied \c mod_handle is invalid
 *         - \c EXCOM_ERR_MEMORY - selected parameter needs an actual value, but the \c value pointer is invalid
 *         - \c EXCOM_ERR_PARAM - supplied \c param is invalid for given \c mod_handle
 *         - \c EXCOM_ERR_VALUE - supplied \c value is invalid for given \c mod_handle and \c param
 *         - \c EXCOM_ERR_NOT_NOW - the parameter cannot be set right now (p.e. you can't change buffer size
 *           when the buffer is not empty)
 *
 *  \todo Define symbolic constants for param for each module.
 */
int exc_setParameter(unsigned int mod_handle, int param, void *value);

/*! \brief Requests a value from an I/O or compression module.
 *  \param mod_handle Handle to the module which should provide the value
 *  \param param Symbolic constant of the parameter to be retrieved. See the description of the
 *         particular module for a list of values provided by that module.
 *  \param value Pointer to a memory area to store the value into. The pointer must be valid and
 *         there must be enough room to store the data to. Actual memory size required to store the
 *         value depends on the actual type of the module and the value of \c param.
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_HANDLE - supplied \c mod_handle is invalid
 *         - \c EXCOM_ERR_MEMORY - \c value is an invalid pointer
 *         - \c EXCOM_ERR_PARAM - supplied \c param is invalid for given \c mod_handle
 *
 *  \todo Define symbolic constants for param for each module.
 */
int exc_getValue(unsigned int mod_handle, int param, void *value);

/*! @}*/

/*! \defgroup io_modules I/O modules
 *
 *  These functions create new I/O module instances. I/O modules provide compression modules with
 *  interface to files in the filesystem or to memory areas of the calling process. All compression
 *  methods only interfere with the filesystem or process' memory through I/O modules. A single I/O
 *  module only provides access to one file or one memory area, either for reading or for writing.
 *  So a basic compression method would need at least two I/O modules to operate (one for reading
 *  input data and one for storing output data). Some compression methods may make use of more than
 *  two data streams.
 *
 *  Each I/O module is of one of three different types, that are enumerated in #exc_iotype_e.
 *
 *  When an I/O module is created, the library assigns an \c unsigned \c int handle to it. This is a unique
 *  identifier of the module, which is used in all functions that need to operate on the module.
 *
 *  In order to be useful, an I/O module has to be connected to a compression module (I/O module
 *  of type \c EXCOM_IO_PIPE should be connected to two compression modules). This is done
 *  using the #exc_connectModules function. When the I/O module is connected to a compression
 *  module it becomes part of the \ref chain that the compression module belongs to.
 *
 *  I/O module is automatically destroyed (and its handle becomes invalid) when the entire compression
 *  chain is destroyed using the #exc_destroyChain function. Calling this function is the only way to
 *  destroy the I/O module and to free resources that it holds.
 *
 *  @{*/

/*! \brief Available types of I/O modules.
 *
 *  Each I/O module must be of one of these types. Selected I/O module type
 *  determines the direction of operations which are performed by the module.
 */
enum exc_iotype_e {
	EXCOM_IO_READ,   /*!< I/O module reads data from a file or memory */
	EXCOM_IO_WRITE,  /*!< I/O module writes data to a file or memory */
	EXCOM_IO_PIPE    /*!< I/O module interconnects two compression modules.
			      Moreover it may write all the data that pass through
			      to a file or memory. */
};

/*| \brief List of symbolic constants that represent parameters of I/O modules.
 *
 *  These are the values that can be used as the \c param argument when setting
 *  parameters to an I/O module using #exc_setParameter. Note that not all parameters
 *  may be supported by all variants of I/O modules. */
enum exc_param_io_e {
	/*! Size of a buffer that the module uses for better performance.
	 *  Value of this parameter is of type \c unsigned \c int. */
	EXCOM_PARAM_IO_BUFFER_SIZE,
	/*! Size of a buffer that the pipe module uses for piping data.
	 *  Value of this parameter is of type \c unsigned \c int. */
	EXCOM_PARAM_IO_PIPE_SIZE,
	/*! Dummy constant. Its value can be used as the first
	 *  PARAM constant for compression modules. Make sure
	 *  this is the last member of exc_param_io_e! */
	EXCOM_PARAM_IO_LAST
};

/*| \brief List of symbolic constants that represent values, which may be requested from I/O modules.
 *
 *  These are the values that can be used as the \c param argument when getting
 *  values from an I/O module using #exc_getValue. Note that not all parameters
 *  may be supported by all variants of I/O modules. */
enum exc_values_io_e {
	/*! Size of a buffer that the module uses for better performance.
	 *  Value of this parameter is of type \c unsigned \c int. */
	EXCOM_VALUE_IO_BUFFER_SIZE,
	/*! Current position in the input (number of bytes read) or output (number of bytes written) stream.
	 *  Value of this parameter is of type \c unsigned \c int. */
	EXCOM_VALUE_IO_POSITION,
	/*! Size of the buffer that is used to pipe data in classes inherited from IOPipe.
	 *  Value of this parameter is of type \c unsigned \c int. */
	EXCOM_VALUE_IO_PIPE_SIZE,
	/*! Dummy constant. Its value can be used as the first
	 *  VALUE constant for compression modules. Make sure this
	 *  is the last member of exc_values_io_e! */
	EXCOM_VALUE_IO_LAST
};

/*! \brief Creates I/O module which will operate on given file.
 *  \param filename Path to the file to open. This parameter may be set to \c NULL,
 *         but only if the value of \c type is \c EXCOM_IO_PIPE. In such case,
 *         data is piped through the module without being written to a file.
 *  \param type Type of the IO Module. See #exc_iotype_e.
 *  \param mod_handle Pointer to an \c unsigned \c int variable to hold a unique module identifier (handle).
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_OPEN - unable to open \c filename, the file either doesn't exist (and
 *           can't be created if that would make any sense) or the process has insufficient
 *           permissions to access the file.
 *         - \c EXCOM_ERR_MEMORY - \c filename is an invalid pointer
 *         - \c EXCOM_ERR_HANDLE - module handle couldn't have been allocated. Either \c mod_handle
 *           is an invalid pointer, or there are no free handles left.
 *         - \c EXCOM_ERR_PARAM - the \c type parameter is invalid.
 *         - \c EXCOM_ERR_ALLOC - memory allocation failed when allocating the object
 */
int exc_fileIOModule(const char *filename, enum exc_iotype_e type, unsigned int *mod_handle);

/*! \brief Creates I/O module which will operate on given memory area.
 *  \param start Pointer to the beginning of the memory area. This parameter may be set to \c NULL,
 *         but only if the value of \c type is \c EXCOM_IO_PIPE. In such case,
 *         data is piped through the module without being written to memory.
 *  \param length Length of the memory area. The module is not allowed to read from or write to
 *         memory addresses beyond and including \c start + \c length.
 *  \param type Type of the IO Module. See #exc_iotype_e.
 *  \param mod_handle Pointer to an \c unsigned \c int variable to hold a unique module identifier (handle).
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_MEMORY - the pointer provided as the \c start parameter is invalid
 *         - \c EXCOM_ERR_HANDLE - module handle couldn't have been allocated. Either \c mod_handle
 *           is an invalid pointer, or there are no free handles left
 *         - \c EXCOM_ERR_PARAM - the \c type parameter is invalid
 *         - \c EXCOM_ERR_ALLOC - memory allocation failed when allocating the object
 */
int exc_memIOModule(void *start, unsigned int length, enum exc_iotype_e type, unsigned int *mod_handle);

/*! \brief Destroys an I/O module if it can't be destroyed using #exc_destroyChain.
 *  \param mod_handle Handle to the I/O module to be destroyed
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_HANDLE - the supplied \c mod_handle is invalid
 *
 *  This function destroys a given I/O module. You should NOT call this function if the
 *  I/O module has been successfully connected to a compression module, because it will be
 *  destroyed automatically with the whole compression chain when you call #exc_destroyChain.
 *  If you'll destroy a connected I/O module using the \c exc_destroyIOModule function,
 *  you'll most likely break the connection chain which will lead to an unpredictable error.
 *
 *  The only purpose of this function is to allow the programmer destroy an I/O module
 *  (and release the resources that it holds) when the module cannot be connected to
 *  a compression chain (i.e. creation of the compression chain or of a compression module
 *  has failed).
 */
int exc_destroyIOModule(unsigned int mod_handle);

/*! @}*/


/*! \defgroup compression Compression modules
 *
 *  Compression and decompression methods are implemented in compression modules. Both compression and
 *  decompression may be understood as a process that takes input data and transforms it to output
 *  data. When we imagine a compression or a decompression module as a blackbox, they look both the same
 *  and have the same interface. Because of that, we will use the term compression module/method for
 *  both compression and decompression. To create a module which will decompress data, you should call
 *  the same function as for creating a module to compress data (#exc_compModule). The main difference is
 *  in the \c operation parameter.
 *
 *  Each compression module can only perform one operation (i.e. compression or decompression, not both).
 *  If you need both to compress and to decompress data using the same compression method, you need
 *  to create two compression modules.
 *
 *  When a compression module is created, it automatically belongs to the \ref chain specified
 *  by a given chain handle (and can never be reconnected to another compression chain). Therefore
 *  you have to create a compression chain before you create a compression module.
 *
 *  The compression module can only be destroyed together with the rest of the compression chain. To do
 *  that, call the #exc_destroyChain function.
 *
 *  @{*/

/*! \brief List of compression methods provided by the library
 *
 *  No other value than those enumerated here may be used as the \c method parameter to #exc_compModule
 *  function call. Nevertheless, even with one of these values, the function may fail, because the
 *  selected method might have been disabled when this library was being compiled. This situation can
 *  only be detected at runtime so one may never take the success of \c exc_compModule for granted.
 */
enum exc_method_e {
	EXCOM_MET_DCA,  /*!< Data compression using antidictionaries */
	EXCOM_MET_ACB,  /*!< Associative coder of Buyanovsky */
	EXCOM_MET_PPM,  /*!< Prediction by partial matching */
	EXCOM_MET_COPY, /*!< A dummy "compression method" which only copies input data to output */
	EXCOM_MET_DHUFF,  /*!< Adaptive Huffman Coding */
	EXCOM_MET_SHUFF,  /*!< Static Huffman Coding */
	EXCOM_MET_LZ77,  /*!< Lempel-Ziv compression method from 1977 */
	EXCOM_MET_LZ78,  /*!< Lempel-Ziv compression method from 1978 */
	EXCOM_MET_LZSS,  /*!< Lempel–Ziv–Storer–Szymanski compression method from 1982 */
	EXCOM_MET_LZW,  /*!< Lempel-Ziv-Welch compression method from 1984 */
	EXCOM_MET_LZMW,  /*!< Lempel-Ziv-Miller-Wegman, variant of LZW from 1985 */
	EXCOM_MET_LZAP,  /*!< Variant of LZW based on LZMW by Storer from 1988 */
	EXCOM_MET_LZY,  /*!< Variant of LZW by Dan Bernstein */
	EXCOM_MET_SFANO,  /*!< Shannon-Fano coding */
	EXCOM_MET_HISTOGRAM, /*!< Module which gathers statistical information from input data */
	EXCOM_MET_ARITH, /*!< Arithmetic codind, both normal and adaptive */
	EXCOM_MET_INTEGER, /*!< Integer compression methods */
	EXCOM_MET_BWT,	/*!< Burrows-Wheeler transform */
	EXCOM_MET_MTF,	/*!< Move-to-front transform */
	EXCOM_MET_RLE_N,	/*!< RLE-N compression method */
	EXCOM_MET_LZFSE  /*!< Lempel-Ziv style algorithm using Finite State Entropy coding */
};

/*! List of compression module operations
 *
 *  All of the standard compression methods should provide at least the compression and decompression
 *  operations (although it is possible to imagine special modules that may only provide one of those).
 *  To provide greater flexibility, design of the library doesn't impose any mandatory operations
 *  on the compression modules. Moreover, every compression module may provide additional operations
 *  apart from compression and decompression.
 */
enum exc_oper_e {
	EXCOM_OP_COMPRESS,    /*!< Perform data compression */
	EXCOM_OP_DECOMPRESS,  /*!< Perform data decompression */
	/*! Count frequencies of values read from input.
	 *  For module EXCOM_MET_HISTOGRAM */
	EXCOM_OP_HISTOGRAM
};

/*! List of symbollic constants that represent values, which may be requested from compression modules.
 *
 *  These are the values that can be used as the \c param argument when getting
 *  values from a compression module using #exc_getValue. This enum only
 *  contains constants for values that are common to all compression modules.
 *  Particular compression methods may define their own constants for accessing
 *  other values. Please see documentation for respective compression methods
 *  for further information. */
enum exc_values_comp_e {
	/*! Result of the last operation, that the module performed.
	 *  If you run a compression chain by calling #exc_run and then
	 *  wait for it with #exc_wait (or #exc_tryWait), none of these
	 *  functions will tell you, whether the operation actually
	 *  succeeded because return values of these functions only
	 *  reflect success or failure in starting or stopping program
	 *  threads.
	 *
	 *  Other reason for not returning the result of the operation
	 *  is that the compression chain may consist of more than one
	 *  compression module, so the library can't know, value of which
	 *  module it should report.
	 *
	 *  In order to get the actual result of the operation of a particular
	 *  compression module, request \c EXCOM_VALUE_COMP_RESULT from
	 *  the desired module using #exc_getValue function.
	 *
	 *  When the compression module hasn't performed any operation, yet,
	 *  the result will be \c EXCOM_ERR_LATER.
	 *
	 *  The value is of type \c int.
	 */
	EXCOM_VALUE_COMP_RESULT = EXCOM_VALUE_IO_LAST,
	/*! Dummy constant. Its value can be used as the first
	 *  VALUE constant for particular compression modules.
	 *  Make sure this is the last member of exc_values_comp_e! */
	EXCOM_VALUE_COMP_LAST
};

/*! \brief Creates a compression or decompression module.
 *  \param method Selected compression method. See #exc_method_e.
 *  \param operation Desired operation, see #exc_oper_e.
 *  \param chain_handle Handle to the \ref chain that this compression module will belong to.
 *  \param mod_handle Pointer to an \c unsigned \c int variable to hold a unique module identifier (handle).
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_METHOD - selected compression method is invalid. That either means that
 *           there is no such method or that the method wasn't compiled into the library.
 *         - \c EXCOM_ERR_OPERATION - selected operation isn't provided by the selected compression method.
 *         - \c EXCOM_ERR_HANDLE - module handle couldn't have been allocated. Either \c mod_handle
 *           is an invalid pointer, or there are no free handles left.
 *         - \c EXCOM_ERR_CHAIN - \c chain_handle is invalid
 *         - \c EXCOM_ERR_ALLOC - memory allocation failed when allocating the object
 */
int exc_compModule(enum exc_method_e method, enum exc_oper_e operation, unsigned int chain_handle, unsigned int *mod_handle);

/*!  @}*/


/*! \defgroup chain Compression chain
 *
 *  For greater robustness, the ExCom library allows the user to create compression chains
 *  by connecting I/O modules and compression modules. A simple compression module would contain
 *  one compression module connected with I/O modules that the compression method needs (typically
 *  one I/O module for input and one for output). But it is also possible to connect multiple
 *  compression modules into a stream using piping I/O modules. An example of a more complicated
 *  compression chain is illustrated at the following image.
 *
 *  \image html compression_chain.png
 *  \image latex compression_chain.eps "An example compression chain" width=14cm
 *
 *  A new compression chain is created using the #exc_compressionChain function. You have to create
 *  an empty compression chain before you can create compression modules. When a compression module
 *  is created, using the #exc_compModule function, it belongs to the compression chain that was
 *  referenced in a parameter to that function. On the other hand, I/O modules are created
 *  independently on compression modules and therefore also independently on compression chains.
 *  In order to be useful, I/O modules have to be connected to compression modules, which is done
 *  with the #exc_connectModules function. Once the I/O module is connected to a compression module,
 *  it becomes member of the same compression chain as the compression module.
 *
 *  Every I/O module may only be connected to one compression module, except for I/O modules of
 *  type \c EXCOM_IO_PIPE, which should be connected to two (both of which must belong to the
 *  same compression chain, otherwise the connection will fail with \c EXCOM_ERR_CHAIN error code).
 *
 *  Once the compression chain is complete, it can be run using the #exc_run function. This
 *  function creates a separate program thread for each compression module involved, so that
 *  they can all run in parallel. The \c exc_run function returns immediately after all the
 *  threads are started, therefore the compression process may not have finished by the time this
 *  function returns. This allows the calling application to do something else while the library
 *  is performing compression (which can lead to better performance on multiprocessor or multicore
 *  systems).
 *
 *  To ensure that the compression chain has finished its operation, one must call the #exc_wait
 *  function. This function blocks the calling thread until the operation is done, therefore
 *  the operation certainly has finished once this function returns. Another function, #exc_tryWait,
 *  provides similar functionality, but doesn't block the calling thread when the operation is
 *  not done. The application may check whether the compression chain has finished using the
 *  \c exc_tryWait function, and if it hasn't it can carry on doing something else (which is not
 *  possible to do with \c exc_wait function) and call either \c exc_tryWait or \c exc_wait again later.
 *
 *  To destroy the complete compression chain and free all the resources that are bonded to it,
 *  call the #exc_destroyChain function. This destroys all the involved I/O and compression modules
 *  and the handles to them as well as the handle to the compression chain become invalid.
 *  It is not possible to destroy single modules alone, because it might leave the compression
 *  chain in a broken state.
 *
 *  You should not destroy a compression chain which is currently performing a compression in another
 *  program thread. You should wait for the compression chain to finish before you destroy it.
 *
 *  @{*/

#define __EXC_CONN_INPUT 0
#define __EXC_CONN_OUTPUT 1
/*! \brief List of connection types
 *
 *  This enum only contains common types of connection between I/O modules and compression modules.
 *  Particular compression modules may define other connection types which are specific for the
 *  compression method that they implement (p.e. a semiadaptive compression method may want to read
 *  a precreated model from a file).
 *
 *  \attention The actual value of every connection type must have its lowest bit set to
 *  \c __EXC_CONN_INPUT or \c __EXC_CONN_OUTPUT, so that the library can easilly tell, wheather
 *  the I/O module will be used for reading or writing. With this information, the library can
 *  check, whether an appropriate I/O module is used, and find out which end of a I/O pipe is being
 *  connected.
 */
enum exc_connection_e {
	EXCOM_CONN_DATA_INPUT = (1<<1) | __EXC_CONN_INPUT,    /*!< This connection serves for transferring input compressed or decompressed data */
	EXCOM_CONN_DATA_OUTPUT= (1<<1) | __EXC_CONN_OUTPUT,   /*!< This connection serves for transferring output compressed or decompressed data */
	EXCOM_CONN_DCA_EXC_INPUT = (2<<1) | __EXC_CONN_INPUT, /*!< dynamic DCA method: for reading exceptions */
	EXCOM_CONN_DCA_EXC_OUTPUT= (2<<1) | __EXC_CONN_OUTPUT,/*!< dynamic DCA method: for writing exceptions */
	EXCOM_CONN_ACB_CONTEXT = (3<<1) | __EXC_CONN_OUTPUT,  /*!< output for abs(context-content) */
	EXCOM_CONN_ACB_CONFORMITY = (4<<1) | __EXC_CONN_OUTPUT/*!< output for length of conforming contents */
};

/*! \brief Creates a new compression chain.
 *  \param chain_handle pointer to an \c unsigned \c int variable to hold a unique identifier of the chain (a handle)
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_HANDLE - chain handle couldn't have been allocated. Either \c chain_handle
 *           is an invalid pointer, or there are no free handles left.
 *         - \c EXCOM_ERR_ALLOC - memory allocation failed when allocating the object
 */
int exc_compressionChain(unsigned int *chain_handle);

/*! \brief Destroys a previously created compression chain and all compression and I/O modules that are connected in this chain.
 *  \param chain_handle Handle to the chain to be destroyed. It's the dereferenced value of the
 *         parameter to the \c exc_compressionChain function. After calling \c exc_destroyChain, consider this handle
 *         and handles to all the modules connected to this chain invalid.
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_HANDLE - supplied \c chain_handle is invalid
 *         - \c EXCOM_ERR_BUSY - the chain cannot be destroyed, because it's running. You have
 *           to wait for it with #exc_wait or #exc_tryWait first.
 */
int exc_destroyChain(unsigned int chain_handle);

/*! \brief Connects an I/O module with a compression module.
 *  \param iomod_handle Handle to the I/O module to be connected
 *  \param compmod_handle Handle to the compression module to be connected
 *  \param connection_type Symbolic constant describing type of the connection being made.
 *         This value tells the compression module how to use the I/O module (basically for
 *         reading or for writing). For common values see #exc_connection_e. Furtermore, each
 *         compression method may define its own connection types. See the definition of
 *         a particular compression method for a list of its custom connection types.
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_HANDLE - supplied \c iomod_handle doesn't represent a valid I/O module
 *           or supplied \c compmod_handle doesn't represent a valid compression module
 *         - \c EXCOM_ERR_PARAM - supplied \c connection_type is invalid for given compression
 *           and I/O modules
 *         - \c EXCOM_ERR_DUPLICATE - given compression module already has a connection of
 *           the given connection type with another I/O module or the I/O module represented by
 *           \c iomod_handle has already been connected to another compression module (or the
 *           I/O module of type \c EXCOM_IO_PIPE has already been connected to two compression
 *           modules)
 *         - \c EXCOM_ERR_CHAIN - I/O module referred to by \c iomod_handle is of type \c EXCOM_IO_PIPE
 *           and each of its ends is connected to a different compression chain. (Both ends of the pipe
 *           must be connected to compression modules in the same compression chain)
 *
 *  \todo Define symbolic constants for connection types for each module.
 */
int exc_connectModules(unsigned int iomod_handle, unsigned int compmod_handle, int connection_type);

/*! \brief Runs the compression chain (i.e. all the compression modules in the chain start working)
 *  \param chain_handle Handle to the chain which is being run
 *  \return
 *         - \c EXCOM_ERR_OK - success. This only means, that the operation started successfully,
 *           but provides no information on how the operation should finish.
 *         - \c EXCOM_ERR_CHAIN - either the \c chain_handle is an invalid chain handle, or the
 *           respective chain is broken.
 *         - \c EXCOM_ERR_THREAD - the function was unable to create new program thread
 *         - \c EXCOM_ERR_ALLOC - memory allocation failed when allocating new threads
 *         - \c EXCOM_ERR_BUSY - the chain is already running and hasn't been properly waited for
 *         - \c EXCOM_ERR_FORMAT - input data is in wrong format for this compression
 *           method and operation (most likely an unpredicted symbol appeared in the
 *           input stream)
 *         - \c EXCOM_ERR_INCOMPLETE - the operation was not completed (i.e. not all input data
 *           was compressed, not all data was uncompressed, or there is not enough input data
 *           to complete decompression)
 *         - \c EXCOM_ERR_SEEKABLE - the compression method needs some of their I/O modules seekable
 *           (allowing random access), but at least one of them isn't
 *
 *   \todo TODO: specify in the documentation, which I/O modules are seekable and which compression
 *   methods need what I/O modules to be seekable:
 *     - seekable I/O: MemWriter, FileWriter
 *     - methods: DCA: data output module when operation == compress
 */
int exc_run(unsigned int chain_handle);

/*! \brief Blocks the calling thread until the compression chain finishes its operation.
 *  \param chain_handle Handle to the chain to wait for
 *  \return
 *         - \c EXCOM_ERR_OK - compression chain has finished successfully
 *         - \c EXCOM_ERR_CHAIN - \c chain_handle doesn't refer to a valid compression chain
 *         - \c EXCOM_ERR_NOT_NOW - the chain is not running, so it can't be waited for
 *  \todo What other errors may occur?
 */
int exc_wait(unsigned int chain_handle);

/*! \brief Checks whether the compression chain has finished. If not, this function returns immediately.
 *  \param chain_handle Handle to the chain to wait for
 *  \return
 *         - \c EXCOM_ERR_OK - compression chain has finished successfully
 *         - \c EXCOM_ERR_CHAIN - \c chain_handle doesn't refer to a valid compression chain
 *         - \c EXCOM_ERR_NOT_NOW - the chain is not running, so it can't be waited for
 *         - \c EXCOM_ERR_LATER - compression chain hasn't finished yet. The program should call
 *           \c exc_wait or \c exc_trywait again later.
 *  \todo What other errors may occur?
 */
int exc_tryWait(unsigned int chain_handle);

/*! @}*/

/*! \defgroup performance Performance measurement
 *
 *  If the library was compiled with performance measuring support, every time a compression chain is run,
 *  each of its compression modules, as well as the compression chain as a whole, measures the time it needs
 *  to complete the operation. The modules record a timestamp at the time they start working and at the time
 *  they've just finished. Difference of the two values is the reported result. Timestamps are taken per
 *  program thread using the \c clock_gettime function. This function might not be portable, even to all
 *  POSIX systems, but should work at least in Linux v2.6. For more info see man clock_gettime(3)).
 *
 *  The measurement results may be accessed from the program using #exc_getPerformanceData function. This
 *  function will return the time recorded by one selected compression module or compression chain.
 *  However, the function may fail with an \c EXCOM_ERR_MEASURE error code if performance measurement
 *  wasn't compiled into the library.
 *
 *  @{*/

/*! \brief Retrieves performance data from a module after the compression has finished
 *  \param mod_handle Handle to a compression module or a compression chain to retrieve data from
 *  \param time Pointer to a variable to which the function will write user+system time of the operation in microseconds
 *  \return
 *         - \c EXCOM_ERR_OK - success
 *         - \c EXCOM_ERR_HANDLE - \c mod_handle is not a valid module handle, or the object, that the handle is
 *           referring to, doesn't provide performance measurement
 *         - \c EXCOM_ERR_MEMORY - \c time is an invalid pointer
 *         - \c EXCOM_ERR_MEASURE - performance measurement is disabled in the library
 */
int exc_getPerformanceData(unsigned int mod_handle, unsigned long *time);

/*! @}*/

/* FIXME: consider moving these declarations to their own header files */

/*! \defgroup ACB Associative Coder of Buyanovsky
 *  @{*/
/*! List of acceptable parameters of the ACB method */
enum excom_param_acb_e {
	/*! Distance in which content is search. Center of this distance
	 *  is best context position. Distance is calculated as 2 ^ distance.
	 *
	 *  This parameter is of type \c unsigned \c int, its value
	 *  must be greater than zero.
         */
	EXCOM_PARAM_ACB_DISTANCE = EXCOM_PARAM_IO_LAST,
	/*! Maximum content length.
	 *  Maximum length is calculated as 2 ^ length.
	 *
	 *  This parameter is of type \c unsigned \c int, its value
	 *  must be greater than zero.
	 */
	EXCOM_PARAM_ACB_LENGTH,
	/*! Tells how big dictionary ACB should use.
	 *  Dictionary size is calculated as 2 ^ size.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Value of this parameter may be greater 1.
	 *  Default value is 20.
	 */
	EXCOM_PARAM_ACB_SEARCH_BUFFER_SIZE,
        /*! Tells how big dictionary ACB should be before
         *  compress ratio starts to be checked.
	 *  Check size is calculated as 2 ^ size.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Value of this parameter may be greater 1.
	 *  Default value is (EXCOM_PARAM_ACB_SEARCH_BUFFER_SIZE - 3).
	 */
        EXCOM_PARAM_ACB_CHECK_SIZE,
        /*! Tells what coding should be used for coding
         *  triplets. Default value is to use arithmetic coding.
         *  If you want to use huffman coding use this switch.
	 */
        EXCOM_PARAM_ACB_CODING
};

/*! List of values that an ACB module provides. These are the actual
 *  values of the parameters of an ACB module. See #excom_param_acb_e
 *  for their respective descriptions. */
enum excom_value_acb_e {
	EXCOM_VALUE_ACB_DISTANCE = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_ACB_LENGTH,
	EXCOM_VALUE_ACB_SEARCH_BUFFER_SIZE,
        EXCOM_VALUE_ACB_CHECK_SIZE,
        EXCOM_VALUE_ACB_CODING
};
/* @}*/

/*! \defgroup Arith Arithmetic coding
 *  @{*/
/*! List of acceptable parameters of the ARITH method */
enum excom_param_arith_e {
	/*! Use static encoding instead of adaptive encoding.
         */
	EXCOM_PARAM_ARITH_STATIC = EXCOM_PARAM_IO_LAST
};

/*! List of values that an ARITH module provides. These are the actual
 *  values of the parameters of an ARITH module. See #excom_param_arith_e
 *  for their respective descriptions. */
enum excom_value_arith_e {
	EXCOM_VALUE_ARITH_STATIC = EXCOM_VALUE_COMP_LAST
};
/* @}*/

/*! \defgroup DCA Data Compression using Antidictionaries
 *  @{*/
/*! List of acceptable parameters of the DCA method */
enum excom_param_dca_e {
	/*! Maximal length of an antiword in bits.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Only values from 1 to 255 (inclusive) are accepted.
	 */
	EXCOM_PARAM_DCA_MAXLENGTH = EXCOM_PARAM_IO_LAST,
	/*! Tells, whether DCA should operate on raw compressed data or
	 *  whether the stream should contain header. When compressing,
	 *  the algorithm can either only write out compressed output,
	 *  or it can place a short header to the beginning of the stream
	 *  with values of the parameters that it'll use during compression.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Value of this parameter may be either 0 (the coder will output
	 *  a header and the decoder will read this header from the stream
	 *  and set its parameters accordingly) or 1 (use raw input/output).
	 */
	EXCOM_PARAM_DCA_RAW
};

/*! List of values that a DCA module provides. These are the actual
 *  values of the parameters of a DCA module. See #excom_param_dca_e
 *  for their respective descriptions. */
enum excom_value_dca_e {
	EXCOM_VALUE_DCA_MAXLENGTH = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_DCA_RAW
};
/*  @}*/

/*! \defgroup LZ77
 *  @{*/
/*! List of acceptable parameters of the LZ77 method */
enum excom_param_lz77_e {
	/*! The size of the search buffer.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Only values from 1 to 65536 (inclusive) are accepted.
	 */
	EXCOM_PARAM_LZ77_SEARCH_BUFFER_SIZE = EXCOM_PARAM_IO_LAST,
	/*! The size of the lookahead buffer.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Only values from 1 to 65536 (inclusive) are accepted.
	 */
	EXCOM_PARAM_LZ77_LOOKAHEAD_BUFFER_SIZE
};

/*! List of values that a LZ77 module provides. These are the actual
 *  values of the parameters of a LZ77 module. See #excom_param_lz77_e
 *  for their respective descriptions. */
enum excom_value_lz77_e {
	EXCOM_VALUE_LZ77_SEARCH_BUFFER_SIZE = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_LZ77_LOOKAHEAD_BUFFER_SIZE
};
/*  @}*/

/*! \defgroup LZSS
 *  @{*/
/*! List of acceptable parameters of the LZSS method */
enum excom_param_lzss_e {
	/*! The size of the search buffer.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Only values from 1 to 65536 (inclusive) are accepted.
	 */
	EXCOM_PARAM_LZSS_SEARCH_BUFFER_SIZE = EXCOM_PARAM_IO_LAST,
	/*! The size of the lookahead buffer.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Only values from 1 to 65536 (inclusive) are accepted.
	 */
	EXCOM_PARAM_LZSS_LOOKAHEAD_BUFFER_SIZE
};

/*! List of values that a LZSS module provides. These are the actual
 *  values of the parameters of a LZSS module. See #excom_param_lzss_e
 *  for their respective descriptions. */
enum excom_value_lzss_e {
	EXCOM_VALUE_LZSS_SEARCH_BUFFER_SIZE = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_LZSS_LOOKAHEAD_BUFFER_SIZE
};
/*  @}*/

/*! \defgroup LZ78
 *  @{*/
/*! List of acceptable parameters of the LZ78 method */
enum excom_param_lz78_e {
	/*! The size of the dictionary.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Only values from 1 to 65536 (inclusive) are accepted.
	 */
	EXCOM_PARAM_LZ78_DICTIONARY_SIZE = EXCOM_PARAM_IO_LAST
};

/*! List of values that a LZ78 module provides. These are the actual
 *  values of the parameters of a LZ78 module. See #excom_param_lz78_e
 *  for their respective descriptions. */
enum excom_value_lz78_e {
	EXCOM_VALUE_LZ78_DICTIONARY_SIZE = EXCOM_VALUE_COMP_LAST
};
/*  @}*/

/*! \defgroup LZW
 *  @{*/
/*! List of acceptable parameters of the LZW method */
enum excom_param_lzw_e {
	/*! The size of the dictionary.
	 */
	EXCOM_PARAM_LZW_DICTIONARY_SIZE = EXCOM_PARAM_IO_LAST,
	/*! Bitsize of input character
	 */
	EXCOM_PARAM_LZW_CHAR_BITSIZE
};

/*! List of values that a LZW module provides. These are the actual
 *  values of the parameters of a LZW module. See #excom_param_lzw_e
 *  for their respective descriptions. */
enum excom_value_lzw_e {
	EXCOM_VALUE_LZW_DICTIONARY_SIZE = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_LZW_CHAR_BITSIZE
};
/*  @}*/

/*! \defgroup LZMW
 *  @{*/
/*! List of acceptable parameters of the LZMW method */
enum excom_param_lzmw_e {
	/*! The size of the dictionary.
	 */
	EXCOM_PARAM_LZMW_DICTIONARY_SIZE = EXCOM_PARAM_IO_LAST,
	/*! Bitsize of input character
	 */
	EXCOM_PARAM_LZMW_CHAR_BITSIZE
};

/*! List of values that a LZMW module provides. These are the actual
 *  values of the parameters of a LZMW module. See #excom_param_lzmw_e
 *  for their respective descriptions. */
enum excom_value_lzmw_e {
	EXCOM_VALUE_LZMW_DICTIONARY_SIZE = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_LZMW_CHAR_BITSIZE
};
/*  @}*/

/*! \defgroup LZAP
 *  @{*/
/*! List of acceptable parameters of the LZAP method */
enum excom_param_lzap_e {
	/*! The size of the dictionary.
	 */
	EXCOM_PARAM_LZAP_DICTIONARY_SIZE = EXCOM_PARAM_IO_LAST,
	/*! Bitsize of input character
	 */
	EXCOM_PARAM_LZAP_CHAR_BITSIZE
};

/*! List of values that a LZAP module provides. These are the actual
 *  values of the parameters of a LZAP module. See #excom_param_lzap_e
 *  for their respective descriptions. */
enum excom_value_lzap_e {
	EXCOM_VALUE_LZAP_DICTIONARY_SIZE = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_LZAP_CHAR_BITSIZE
};
/*  @}*/

/*! \defgroup LZY
 *  @{*/
/*! List of acceptable parameters of the LZY method */
enum excom_param_lzy_e {
	/*! The size of the dictionary.
	 */
	EXCOM_PARAM_LZY_DICTIONARY_SIZE = EXCOM_PARAM_IO_LAST,
	/*! Bitsize of input character
	 */
	EXCOM_PARAM_LZY_CHAR_BITSIZE
};

/*! List of values that a LZY module provides. These are the actual
 *  values of the parameters of a LZY module. See #excom_param_lzy_e
 *  for their respective descriptions. */
enum excom_value_lzy_e {
	EXCOM_VALUE_LZY_DICTIONARY_SIZE = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_LZY_CHAR_BITSIZE
};
/*  @}*/


/*! \defgroup PPM Prediction by Partial Matching
 *  @{*/
/*! List of acceptable parameters of the PPM method */
enum excom_param_ppm_e {
	/*! Maximal amount of memory for the method (in bytes).
	 *
	 *  NOTE: it's necessary to set this parameter to the same value
	 *  to both encoder and decoder (as with other parameters, but this
	 *  one may not be obvious), or else decompression will fail!
	 *  Value of the parameter is automatically written in file's
	 *  header, if the output is not raw.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 */
	EXCOM_PARAM_PPM_MEMORY_SIZE = EXCOM_PARAM_IO_LAST,
	/*! Maximal PPM method order.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 */
	EXCOM_PARAM_PPM_ORDER,
	/*! Tells, whether PPM should operate on raw compressed data or
	 *  whether the stream should contain header. When compressing,
	 *  the algorithm can either only write out compressed output,
	 *  or it can place a short header to the beginning of the stream
	 *  with values of the parameters that it'll use during compression.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Value of this parameter may be either 0 (the coder will output
	 *  a header and the decoder will read this header from the stream
	 *  and set its parameters accordingly) or 1 (use raw input/output).
	 */
	EXCOM_PARAM_PPM_RAW
};

/*! List of values that a PPM module provides. These are the actual
 *  values of the parameters of a PPM module. See #excom_param_ppm_e
 *  for their respective descriptions. */
enum excom_value_ppm_e {
	EXCOM_VALUE_PPM_MEMORY_SIZE = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_PPM_ORDER,
	EXCOM_VALUE_PPM_RAW
};
/*  @}*/

/*! \defgroup Histogram
 *  @{*/

/*! Parameters of a histogram module */
enum excom_param_hist_e {
	/*! Number of bytes per input unit. The module will
	 *  read that many bytes from the input stream and
	 *  will consider the data one value. (p.e. if you
	 *  want to count frequencies of different bytes,
	 *  set this to 1, if you want to count frequencies
	 *  of dwords, set this to 4).
	 *
	 *  This parameter is of type \c unsigned \c int, its
	 *  value may be 1, 2, 3 or 4.
	 */
	EXCOM_PARAM_HIST_BYTES_PER_CHAR = EXCOM_PARAM_IO_LAST,
	/*! Number of output intervals. There are \c 2^(8*bpc)
	 *  different values (if \c bpc is the number of bytes
	 *  per input character). The module will divide the
	 *  entire range into intervals and output frequencies
	 *  will be the numbers of values, which fall to the
	 *  respective intervals.
	 *
	 *  This parameter is of type \c unsigned \c int and
	 *  must be greater than 0. If the number of intervals
	 *  is bigger than the number range of values,
	 *  this parameter is ignored and the module counts
	 *  occurrencies of the distinct values.
	 */
	EXCOM_PARAM_HIST_INTERVALS,
	/*! Output format.
	 *
	 *  This parameter is of type \c int. For possible
	 *  values see #excom_hist_output_e.
	 */
	EXCOM_PARAM_HIST_OUTPUT,
	/*! Minimal value to be monitored. Values smaller than this
	 *  will fall outside the range.
	 *
	 *  This parameter is of type \c int. If the value
	 *  is -1, lower bound will be determined automatically
	 *  as the minimal value possible.
	 */
	EXCOM_PARAM_HIST_RANGE_MIN,
	/*! Maximal value to be monitored. Values bigger than this
	 *  will fall outside the range.
	 *
	 *  This parameter is of type \c int. If the value
	 *  is -1, upper bound will be determined automatically
	 *  as the maximal value possible.
	 */
	EXCOM_PARAM_HIST_RANGE_MAX
};

/*! Different types of output of the histogram module */
enum excom_hist_output_e {
	/*! Outputs data as binary data
	 *  \todo TODO: specify format
	 */
	EXCOM_HIST_OUTPUT_BINARY,
	/*! Outputs data in human readable format */
	EXCOM_HIST_OUTPUT_HUMAN
};
/*  @}*/

/*! \defgroup integer Integer compression methods
 *  @{*/
/*! List of acceptable parameters of the Integer compression methods */
enum excom_param_integer_e {
	/*! Used integer compression method
	 */
	EXCOM_PARAM_INTEGER_METHOD = EXCOM_PARAM_IO_LAST,
	/*! Optional parameter for some integer compression methods (ex. Fibonacci order)
	 */
	EXCOM_PARAM_INTEGER_PARAM
};

/*! List of values that an Integer compression module provides. These are
 *  the actual values of the parameters of an Integer compression  module.
 *  See #excom_param_integer_e for their respective descriptions. */
enum excom_value_integer_e {
	EXCOM_VALUE_INTEGER_METHOD = EXCOM_VALUE_COMP_LAST,
	EXCOM_VALUE_INTEGER_PARAM
};
/*  @}*/

/*! \defgroup BWT Burrows-Wheeler transform
 *  @{*/
/*! List of acceptable parameters of the Burrows-Wheeler transform */
enum excom_param_bwt_e {
	/*! Block size
	 */
	EXCOM_PARAM_BWT_BLOCK_SIZE = EXCOM_PARAM_IO_LAST
};

/*! List of values that a Burrows-Wheeler transform module provides. These are
 *  the actual values of the parameters of an Burrows-Wheeler transform module.
 *  See #excom_param_bwt_e for their respective descriptions. */
enum excom_value_bwt_e {
	EXCOM_VALUE_BWT_BLOCK_SIZE = EXCOM_VALUE_COMP_LAST
};
/*  @}*/

/*! \defgroup rlen RLE-N compression method
 *  @{*/
/*! List of acceptable parameters of the RLE-N method */
enum excom_param_rle_n_e {
	/*! Threshold
	 */
	EXCOM_PARAM_RLE_N_THRESHOLD = EXCOM_PARAM_IO_LAST
};

/*! List of values that a RLE-N module provides. These are
 *  the actual values of the parameters of an RLE-N module.
 *  See #excom_param_rle_n_e for their respective descriptions. */
enum excom_value_rle_n_e {
	EXCOM_VALUE_RLE_N_THRESHOLD = EXCOM_VALUE_COMP_LAST
};
/*  @}*/

/*! \defgroup lzfse LZFSE compression method
 *  @{*/
/*! List of acceptable parameters of the LZFSE method */
enum excom_param_lzfse_e {
	/*! Match length in bytes to cause immediate emission.
	 *
	 *  LZFSE maintains multiple candidate matches and waits to decide which match
	 *  to emit until more information is available. When a match exceeds this
	 *  threshold, it is emitted immediately. Thus, smaller values may give
	 *  somewhat better performance, and larger values may give somewhat better
	 *  compression ratios.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Only values from 4 to 65536 (inclusive) are accepted.
	 *  Default value is 40.
	 */
	EXCOM_PARAM_LZFSE_ENCODE_GOOD_MATCH = EXCOM_PARAM_IO_LAST, 

	/*! Number of bits for hash function to produce.
	 *
	 *  Should be in the range [10, 16]. Larger values reduce the number 
	 *  of false-positive found during the match search, and expand the history table, 
	 *  which may allow additional matches to be found, generally improving 
	 *  the achieved compression ratio. Larger values also increase the workspace size, 
	 *  and make it less likely that the history table will be present in cache, 
	 *  which reduces performance.
	 *
	 *  This parameter is of type \c unsigned \c int.
	 *  Only values from 10 to 16 (inclusive) are accepted.
	 *  Default value is 14.
	 */
	EXCOM_PARAM_LZFSE_ENCODE_HASH_BITS
};

/*! List of values that a LZFSE module provides. These are
 *  the actual values of the parameters of a LZFSE module.
 *  See #excom_param_lzfse_e for their respective descriptions. */
enum excom_value_lzfse_e {
	EXCOM_VALUE_LZFSE_ENCODE_GOOD_MATCH = EXCOM_VALUE_COMP_LAST, 
	EXCOM_VALUE_LZFSE_ENCODE_HASH_BITS
};
/*  @}*/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* ifndef __EXCOM_H__ */

