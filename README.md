# Compression method LZFSE

This repository contains source codes for my bachelor’s thesis 'Compression method LZFSE'. 
Excluding this readme file, the contents of this repository exactly matches contents of the CD enclosed in the printed version of the thesis. 
The [excom](excom) folder contains ExCom library with implemented LZFSE module. 

## Text

[pdf](text/thesis.pdf)

## Implementation

### Building ExCom (with LZFSE module)

```bash
cd excom/
autoreconf -i -s -f
mkdir bin
cd bin
../configure -C --enable-perf-measure
make
```

