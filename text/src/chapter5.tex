\chapter{Implementation}

The goal of this thesis was to integrate LZFSE method into the ExCom library. This chapter describes the steps taken to achieve it. LZFSE was implemented as a new ExCom module. 
The process of adding new methods into ExCom is described in detail in~\cite{Simek2009}, only changes from this procedure and important steps will be listed here. 

\section{Implementation of LZFSE module}

The LZFSE module was implemented as a new class \verb|CompLZFSE| extending the \verb|CompModule| class from the ExCom library. The source code for this class is present in \path{lzfse.hpp} and \path{lzfse.cpp} files. 

As there exists an open source reference implementation of LZFSE written in C language (described in previous chapter), it was possible to use this implementation and integrate it into ExCom by writing a wrapper over it. 
The reference implementation provides an interface for block compression. There are two methods \verb|lzfse_encode_buffer| and \verb|lzfse_decode_buffer| that performs compression and decompression respectively. They take a source buffer as an input and write their output to a given output buffer. 

The LZFSE module was implemented as a wrapper over these methods. It performs the required operation (i.e.\ either compression or decompression) by executing the following steps:

\begin{enumerate}
  \item Load the input into a source buffer.
  \item Allocate an output buffer\footnote{Because it is not known in advance how big the output of encode/decode operation will be, it is assumed that for compression the output will not be larger than the input. For decompression, the output buffer size is chosen to be four times the input size. If the output buffer is too small, the operation fails. In such case, the output buffer is enlarged and the operation is run again. }.
  \item Call the appropriate method that will process data from the input buffer and write the result into the output buffer.
  \item Write contents of the output buffer as an output of the operation.
\end{enumerate}

The ExCom library API supports reading/writing by bytes or by whole blocks. Initially, the first variant was used and the input was loaded byte by byte using a loop and also written in similar way. However, this proved to be a significant bottleneck. By rewriting the I/O operations using the block functions from the API (\verb|readBlock()| and \verb|writeBlock()|), the compression time was improved (lowered) by nearly 15\% in average. The improvement of decompression time was even higher, using block I/O operations decreased it by approximately 40\% in average on Prague Corpus files. 

The modified LZFSE reference implementation is placed in a subfolder named \verb|lzfse| inside the LZFSE module. The changes made to the code of the reference implementation are listed below in Section~\ref{sec:ref_impl_changes}. 

Furthermore, unit tests were created for the LZFSE module to validate the implementation. 
They test the compression and decompression operations and other functions of the module. 

\subsection{Implemented parameters}
\label{subsec:impl_implemented_parameters}

The following adjustable parameters were implemented for the LZFSE module (for details of their implementation see Section~\ref{sec:ref_impl_changes}): 

\begin{itemize}
\item \emph{good match (g)} -- This parameter sets the threshold for immediately emitting matches. As described in Section~\ref{subsec:lzfse_comp_frontend} when a found match is bigger than this threshold, it is immediately emitted. Default value of this parameter is $40$. Only values equal or larger than $4$ are accepted. %Value of this parameter has an impact on compression ratio and speed. 

\item \emph{hash bits (h)} -- This parameter controls the number of bits produced by hash function that is used in frontend to search for matches (see Section~\ref{subsec:lzfse_comp_frontend}). Integer values between $10$ and $16$ (inclusively) are possible, $14$ is the default. 

\end{itemize}

\subsection{Changes in reference implementation}
\label{sec:ref_impl_changes}
All changes made to the reference implementation are described here. The version of LZFSE reference implementation from 22 May 2017 as published at \cite{LZFSE_github} was used.

As mentioned in Chapter~\ref{chap:LZFSE}, the reference implementation contains a heuristic that uses a different algorithm called LZVN for small inputs. Because the goal was to implement and measure performance of the LZFSE algorithm, the LZVN compression was removed here by commenting out the corresponding part of the code. While the LZVN compression was removed from the encoding function, the ability to decompress LZVN blocks was preserved in the decoding function to maintain compatibility with the reference implementation\footnote{Therefore, LZVN compressed block will never be produced by the LZFSE module, but the module is able to decode them (so it can decode files compressed by the LZFSE reference implementation).}. 

Further changes were necessary in order to implement the adjustable parameters described above. Since these parameters affect only the compression operation, only the encoder was needed to be modified. 

The reference implementation contains four adjustable parameters in the \path{lzfse_tunables.h} header. However, they are implemented as \emph{preprocessor constants} there, so it is not possible to change their value at runtime. 

The \verb|LZFSE_ENCODE_GOOD_MATCH| and \verb|LZFSE_ENCODE_HASH_BITS| preprocessor constants were replaced by parameters that are part of structure defined in \path{lzfse_params.h}. This structure is kept by the LZFSE module and its fields~(the parameters) are updated when parameter change is requested through the module API. When the compression operation is run, this structure is passed to the appropriate method and saved inside the compression state. Its fields are then used instead of the constants. 

Because the \verb|LZFSE_ENCODE_HASH_BITS| affects the size of the history table, it was also necessary to modify the history table allocation. It is now allocated dynamically with size depending on the parameter value. 

The \verb|LZFSE_ENCODE_LZVN_THRESHOLD| constant was used to control the LZVN compression mentioned before. But since LZVN is not used here, this constant is unnecessary and was removed. 

The last constant called \verb|LZFSE_ENCODE_HASH_WIDTH| controls the number of entries stored in each set of the history table. Implementing this parameter would require even more complex allocation of the history table and various additional changes in code where the history table is manipulated with. Furthermore, this parameter may take only two values: $4$ or $8$. Because of this, it was left as a preprocessor constant and may be modified by changing its definition in the \path{lzfse_tunables.h} header file and recompiling the module.

\subsection{Testing}
\textit{Unit tests} were implemented for the LZFSE module. The unit tests are similar to unit tests for other modules in ExCom. The functionality of the module is tested: input/output operations, setting of parameters, and finally the compression and decompression operations. 
The \textit{cxxtest} framework is used for unit tests in ExCom, as described in~\cite{Simek2009}.

Additionally, when running benchmarks, all files from Prague Corpus were always tested if they were decompressed correctly by the method (i.e.\ whether the decompressed file was identical to the original file). 

\subsection{Other changes}
Additional minor changes outside of the LZFSE module were necessary in order to compile the ExCom library using a modern g++ compiler. 

Also the explicit call to search buffer object destructor present in LZ77 and LZSS modules was removed, because it was causing double delete (as the object is deleted using \verb|delete| later) and the methods could not be run. 

