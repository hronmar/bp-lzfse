\chapter{Reference implementation}
\label{appendix_LZFSE}

The reference implementation of LZFSE is available from GitHub at~\cite{LZFSE_github}. It is licensed under the open source 3-Clause BSD License.
The file structure of the reference implementation and important functions are described here. All details pertain to the version from 22 May 2017. 

\section{Source files}
The source codes are present in the \path{src} subfolder of the reference implementation. It contains the following files (listed alphabetically): 

\begin{itemize}
\item \path{lzfse.h} -- This header file contains the declarations of the API functions for both the encode and decode operations. See Section~\ref{appendix_LZFSE_API} for details of the API functions. 

\item \path{lzfse_decode.c} -- The implementation of the decode function declared in the \path{lzfse.h} file is present here. The actual LZFSE decompression is not implemented in this file. Here, it is only assured that an auxiliary buffer is allocated and an initialization is performed. An internal function called \verb|lzfse_decode| is then invoked to perform the actual decoding. 

\item \path{lzfse_decode_base.c} -- The \verb|lzfse_decode| function is implemented here. The decoding of the compressed blocks is done here. Functions implemented in the \path{lzfse_fse.h} and \path{lzfse_fse.c} files are used for the FSE decoding. 

\item \path{lzfse_encode.c} -- Similarly to \path{lzfse_decode.c}, this file contains an implementation of the API function that performs compression. Same as in the case of decompression, initialization is done here and another function is called to do the actual encoding. However, the heuristic that chooses which function will be called to encode the input is implemented here. If the input is very small, it is copied uncompressed. Otherwise, if it is smaller than a given threshold, it is encoded using LZVN. If it is not, it is encoded using LZFSE by calling the appropriate function implemented in \path{lzfse_encode_base.c} file.

\item \path{lzfse_encode_base.c} -- This source file contains an implementation of the LZFSE compression. It contains both the implementation of the frontend and the backend, both were described in Chapter~\ref{chap:LZFSE}. For the FSE encoding, functions from \path{lzfse_fse.h} and \path{lzfse_fse.c} are called. 

\item \path{lzfse_encode_tables.h} -- This file contains the tables used to map values to their base values in a technique described in Section~\ref{subsec:lzfse_comp_backend}, which is used in the LZFSE compression backend.

\item \path{lzfse_fse.c} -- This file contains an implementation of the finite state entropy. 

\item \path{lzfse_fse.h} -- This header file contains declarations of the finite state entropy functions (and also an inline implementation of some of them). 

\item \path{lzfse_internal.h} -- This header file contains definitions for the functions implemented in \path{lzfse_decode_base.c} and \path{lzfse_encode_base.c}. It also contains definitions of structures used in other files and preprocessor constants. Unlike the constants in \path{lzfse_tunables.h}, values defined here should not be changed. 

\item \path{lzfse_main.c} -- An implementation of the LZFSE command line tool. 

\item \path{lzfse_tunables.h} -- Adjustable LZFSE parameters are defined in this header file (as preprocessor constants). 

\item \path{lzvn_decode_base.c}, \path{lzvn_decode_base.h}, \path{lzvn_encode_base.c} and \path{lzvn_encode_base.h} -- These files contain an implementation of the LZVN algorithm, which is used as a fallback for small files. 

\end{itemize}

\section{The API functions}
\label{appendix_LZFSE_API}

\subsection{Encoding}

To compress a block of data using LZFSE, a function called \verb|lzfse_encode_buffer| has to be called. This function is declared as: 

\begin{samepage}
\begin{verbatim}
LZFSE_API size_t lzfse_encode_buffer(uint8_t * dst_buffer,
                                     size_t dst_size,
                                     const uint8_t * src_buffer,
                                     size_t src_size,
                                     void * scratch_buffer);
\end{verbatim}
\end{samepage}

The \verb|dst_buffer| is a destination buffer, which must be provided to this function and which will contain the result of the compression operation. 
The \verb|src_buffer| is an array containing the input block. 

The \verb|scratch_buffer| is is an auxiliary buffer, it is a pointer to memory that should be allocated by the caller and passed to the \verb|lzfse_encode_buffer| function.
This will be used during compression for storing the encoder state. If it is not provided (i.e.\ a \verb|NULL| is given), then the function will allocate the needed memory itself. 
The function \verb|lzfse_encode_scratch_size()| should be called to get the required \verb|scratch_buffer| (i.e.\ the auxiliary buffer) size.

If the compression succeeds, \verb|lzfse_encode_buffer| function returns the number of bytes written to the destination buffer. If the provided destination buffer is too small or if the compression fails from other reason, zero is returned. The contents of the destination buffer is undefined in such case. 

\subsection{Decoding}

LZFSE decompression has a similar interface as the compression described above. The function is declared as: 

\begin{samepage}
\begin{verbatim}
LZFSE_API size_t lzfse_decode_buffer(uint8_t * dst_buffer,
                                     size_t dst_size,
                                     const uint8_t * src_buffer,
                                     size_t src_size,
                                     void * scratch_buffer);
\end{verbatim}
\end{samepage}

The \verb|src_buffer| is the input block and the \verb|dst_buffer| is the output buffer. 
A scratch buffer should also be passed to this function. To find out the required size, the \verb|lzfse_decode_scratch_size()| function should be called. 

Unlike \verb|lzfse_encode_buffer|, this function always returns the number of bytes written to the output. Even if the entire output will not fit into the output buffer, it will contain a portion of decoded data.


\chapter{Prague Corpus files}
\label{chap:prague_corpus_files}
\input{prague_corpus.tex}

\chapter{Building the ExCom library}
The process of building the ExCom library with all its modules is described in detail in~\cite[Appendix~E]{Simek2009}.  

\begin{flushleft}
To build the ExCom library on Unix system, execute the following commands in shell from the \path{excom} directory: 
\end{flushleft}

\begin{verbatim}
autoreconf -i -s -f
mkdir bin
cd bin
../configure -C --enable-perf-measure
make
\end{verbatim}

\begin{flushleft}
Additionally, to generate the Doxygen documentation, run:
\begin{verbatim}
make doxygen-doc
\end{verbatim}
\end{flushleft}

\begin{flushleft}
The compiled testing application is located in \path{bin/src/app} directory, execute \verb|./app -h| for its usage. 
\end{flushleft}

\subsubsection*{Unit tests}
\begin{flushleft}
Unit tests require the Python interpreter to be installed. 
To enable unit tests for the library and its modules, add the \verb|--enable-test-cases| parameter to the configure command when building the library:% run in step~5:
\begin{verbatim}
../configure -C --enable-perf-measure --enable-test-cases
\end{verbatim}
\end{flushleft}

\begin{flushleft}
The tests may then be run by invoking:
\begin{verbatim}
make tests-run
\end{verbatim}
\end{flushleft}


\chapter{Scripts used for benchmarking}
\label{appendix_scripts}

Two \textit{bash} scripts were developed to automate the process of running multiple methods on given corpus files and measuring their performance. 

The first script called \texttt{benchmarks.sh} was used to compare performance of implemented compression methods with each other. The second script called \texttt{benchmarks\_params.sh} was used to measure impact of different settings of LZFSE method adjustable parameters. 

These two scripts perform compression and decompression multiple times on all given files and measure the minimal time taken by each operation. Compression ratio is also computed for each file. 
Usage of these scripts is described below. Both scripts are present on the CD attached to this thesis. 

\section*{Script \texttt{benchmarks.sh}}
This script will run compression and decompression of given methods on all corpus files and measure the time taken by these operations and resulting compression ratio. Each operation is run multiple times (default is 50) and the minimum from measured times is taken. 

This script produces three files in the folder from which it is run. The \path{benchmarks_comp.csv} and \path{benchmarks_decomp.csv} contain compression and decompression times measured in microseconds. Compression ratios are computed as percentage (by default) and output into \path{benchmarks_ratio.csv} file. 
All three files are \textit{csv} files containing data in form of comma-separated values. If files with these names already exist in the folder, they will be overwritten. 

The script should be called from a directory containing both the \mbox{ExCom} testing application~(\texttt{app}) and a folder containing corpus files (\path{PragueCorpus} by default). If it is run elsewhere, locations of these files must be given as arguments (see below). 

\begin{flushleft}
\subsection*{Usage}
\verb|./benchmarks.sh [options] [-l method...]|

Possible options are: 
\begin{description}[leftmargin=!,labelwidth=\widthof{\bfseries -c APP}]
\item[-h] Show help. 

\item[-r T] Each operation will be repeated $T$ times. Default is 50. 

\item[-f] Do not exit on failure. All files are tested for equality with the original when they are decompressed. Whenever a method fails to compress and decompress a file correctly, the benchmarks are stopped by default. This option disables this behaviour, a '?' is written into results instead of compression ratio and the benchmarking continues. 

\item[-s, -b] These flags control how much information is output during the process. The \verb|-b| flag means \textit{brief} and disables messages for individual files. The \verb|-s| flag stands for \textit{silent} and disables all output. 

\item[-d DIR] The benchmarks will be performed on all files from \path{DIR} directory. Default is \path{PragueCorpus}. If the corpus files are on different location, it must be given with this parameter. 

\item[-c APP] Relative path to the ExCom testing application. Default is \path{app}. 

\item[-n] Disables using percentage for compression ratio. 

\item[-l] If this option is used, a list of methods must be given as script arguments following the options. The benchmarks will be run for given methods. Methods must be given by the names ExCom uses for them, use \verb|./app -m?| for list of possible methods.

\end{description}

\subsection*{Example}

\begin{verbatim}
./benchmarks.sh -r 30 -d ~/PragueCorpus -l lzfse lz78 shuff
\end{verbatim}

This command will perform benchmarks on LZFSE, LZ78 and static Huffman coding methods. It will run each operation 30 times for each file from the PragueCorpus folder in user home directory and save results in the folder from where it is run. 

\end{flushleft}

\section*{Script \texttt{benchmarks\_params.sh}}
This script is similar to \texttt{benchmarks.sh} but measures the impact of different parameter settings on method speed and compression ratio. This script produces same files as \texttt{benchmarks.sh} script.

\subsection*{Usage}
\verb|./benchmarks_params.sh [options] method param min max|

\begin{flushleft}

Four arguments must be given after the optional arguments:
\begin{description}[leftmargin=!,labelwidth=\widthof{\bfseries min, max}]
\item[method] The name of the method to run benchmarks for. Use \verb|./app -m?| for list.

\item[param] Name of the parameter. Use \verb|./app -m method -p?| for list.

\item[min, max] These two arguments define the range of tested values of the parameter. By default all parameter values from $\left[ min, max \right]$ range (inclusive) are tested. 
\end{description}

All options available for \texttt{benchmarks.sh} (with the exception of \verb|-l| and \verb|-f|) can also be used here with the same effect. Additional options are: 

\begin{description}[leftmargin=!,labelwidth=\widthof{\bfseries -i INC}]
\item[-i INC] Parameter value increment. First tested value of parameter is defined by $min$, then the value is incremented by \verb|INC| every step until it is larger than $max$. 

\item[-a] Apply parameters to both compression and decompression operations. The parameter is used \emph{only} for compression by default. 
\end{description}

\subsection*{Example}

\begin{verbatim}
./benchmarks_params.sh -i 10 lzfse g 10 130
\end{verbatim}

This command will perform benchmarks on LZFSE and measure the impact of \textit{good match} parameter on compression/decompression times and compression ratio. Values $10, 20, \ldots , 130$ will be tested. 

\end{flushleft}

\chapter{Detailed benchmark results}
%\section{Tables}
\input{benchmarks_dict_comp.tex}
\input{benchmarks_dict_decomp.tex}
\input{benchmarks_dict_ratio.tex}

%\section{Non-dictionary methods}
\input{benchmarks_other_comp.tex}
\input{benchmarks_other_decomp.tex}
\input{benchmarks_other_ratio.tex}

%\clearpage
%compression time graph
\begin{landscape}
\section{Additional graphs}
\vfill
\begin{figure}[ht]
\includegraphics[scale=1.3]{graph_dict_comp_full.pdf}
\caption[Comparison of compression time of LZFSE, LZ78 and LZW on all files]{Comparison of compression time of LZFSE, LZ78 and LZW dictionary methods on all files from the Prague Corpus}
\end{figure}
\end{landscape}

%decompression time graph
\begin{landscape}
\begin{figure}
\includegraphics[scale=1.4]{graph_dict_decomp_full.pdf}
\caption[Comparison of decompression time of LZFSE, LZ78 and LZW on all files]{Comparison of decompression time of LZFSE, LZ78 and LZW dictionary methods on all files from the Prague Corpus}
\end{figure}
\end{landscape}

%compression ratio graph (LZ78 and LZMW, all files)
\begin{landscape}
\begin{figure}
\includegraphics[scale=1.4]{graph_dict_ratio_full.pdf}
\caption[Comparison of compression ratio of LZFSE, LZ78 and LZMW on all files]{Comparison of compression ratio of LZFSE, LZ78 and LZMW dictionary methods on all files from the Prague Corpus}
\end{figure}
\end{landscape}
