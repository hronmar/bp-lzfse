\chapter{Benchmarks}

The results of benchmarks made on the newly added LZFSE module and other ExCom modules are presented in this chapter. 
The benchmarks were made using the ExCom library benchmarking and time measurement capabilities as described in~\cite{Simek2009}. 

\section{Methodology}

The Prague Corpus files were used for benchmarking, see Appendix~\ref{chap:prague_corpus_files} for list of files and file details. 
Every method was run $50$ times for both compression and decompression operations on each file. The minimal time from those $50$ runs was taken for each operation to eliminate bias caused by other running processes in the system. 

ExCom supports multiple runs of a method on one file, but it does not support computing minimum time or compression ratio~\cite{Simek2009}. 
Two \textit{bash} scripts were written to automate the process of measuring performance of methods. Both scripts are part of this work, they are described in Appendix~\ref{appendix_scripts}. 

\section{Testing platform specifications}
The ExCom library was built and the benchmarks were made on a system with the following specifications: 

\begin{table}[ht!]
\centering
\begin{tabular}{ll}
\begin{tabular}[c]{@{}l@{}}Operating system\\\end{tabular}  & Fedora 26 (Linux kernel version 4.14.18)                                             \\
Processor                                                   & \begin{tabular}[c]{@{}l@{}}Intel\textsuperscript{\textregistered} Core\textsuperscript{TM}2 Duo CPU T6570 @ 2.10GHz $\times$ 2 \\\end{tabular}  \\
Architecture                                                & \begin{tabular}[c]{@{}l@{}}64 bit\\\end{tabular}                                     \\
\begin{tabular}[c]{@{}l@{}}L2 cache size\\\end{tabular}     & 2048 kB                                                                              \\
\begin{tabular}[c]{@{}l@{}}Memory size (RAM)\\\end{tabular} & \begin{tabular}[c]{@{}l@{}}4 GB\\\end{tabular}                                       \\
Compiler                                                    & \begin{tabular}[c]{@{}l@{}}g++ 7.3.1\\\end{tabular}                                                    
\end{tabular}
\end{table}


\section{Results}

\subsection{Comparison with other compression methods}
\label{subsec:benchmarks_comparison}
Because LZFSE was designed as a balanced method and intended for general use cases, it was expected to be among above average methods both in terms of compression ration and speed. Furthermore, its compression ratio would likely benefit from the ANS entropy coder. The benchmarks proved those assumptions. 

For all benchmarks presented in this subsection, default values were used for all adjustable LZFSE parameters. 

\subsubsection{Dictionary methods}

LZFSE was compared to LZ77, LZ78, LZAP, LZMW, LZW and LZY dictionary methods implemented in ExCom. Unfortunately, most files from Prague Corpus were not decompressed correctly using the LZSS method (i.e.\ the decompressed file was not identical to the original), and so it was not used in comparisons. 

\paragraph*{Compression time}\mbox{}\\
Because LZFSE compression operation is relatively complex (as described in Chapter~\ref{chap:LZFSE}) and it is considerably more complex than its decompression operation, it was not expected to be the fastest method in this regard. 
Nevertheless, LZFSE had the lowest compression time on 23 from 30 tested files and so on most files, it was the fastest from tested methods. 
On each of the \path{abbot}, \path{firewrks} and \path{nightsht} files, LZFSE had around 50\% lower compression time than LZW -- the second fastest method on most files. 
LZFSE generally performed well on larger files, especially on audio and graphic files. On the largest tested file \path{nightsht}, LZFSE had nearly 53\% lower compression time than LZW, the absolute difference being more than 642 milliseconds.

However, LZFSE struggled on very small files, where compression time was negatively impacted by its complex compression algorithm. On the three smallest files \path{collapse}, \path{xmlevent} and \path{usstate}, which are all source codes, LZFSE had significantly worse compression time than LZW. 
The largest relative difference occurred on the smallest file \path{collapse}, where LZFSE was almost 2 times slower than LZW (0.313~ms versus 0.167~ms) and was the fourth slowest method. On \path{xmlevent} and \path{usstate} files, LZFSE had around 20\% larger compression time than LZW. However, apart from \path{collapse} and \path{usstate} files, LZFSE was always at worst the second fastest method. 

Based on these observations, it seems only logical to use a simpler algorithm instead of LZFSE for compressing very small files. Still, in absolute values, LZFSE was at most by 1.3 milliseconds slower than LZW (on file \path{modern}, where relative difference was only approx.\ 6\%).
%Nevertheless, the compression time on such files would usually be at most few milliseconds and so this might not be a problem in common usage. 

%\pagebreak

\begin{figure}[ht!]
\centering
\includegraphics[scale=1.0]{graph_dict_comp.pdf}
\caption[Comparison of compression time of LZFSE, LZ78 and LZW]{Comparison of compression time of LZFSE, LZ78 and LZW dictionary methods}

\vspace{2cm} 

\includegraphics[scale=1.0]{graph_dict_decomp.pdf}
\caption[Comparison of decompression time of LZFSE, LZ78 and LZW]{Comparison of decompression time of LZFSE, LZ78 and LZW dictionary methods}
\end{figure}
\clearpage

\paragraph*{Decompression time}\mbox{}\\
For decompression operation, LZFSE proved to be significantly faster than all dictionary methods implemented in ExCom that it was compared to. It had the lowest decompression time on all files and on most of them (27 from 30 files) it was more than 60\% faster than any other dictionary method. 

LZ78 was the dictionary method with fastest decompression so far. 
LZFSE achieved approximately 65\% lower decompression time than LZ78 in average. 
The only two files where the relative difference was significantly lower than~60\% were \path{collapse} and \path{mailfldr} text files, where LZFSE had only 42\% and 45\% respectively lower decompression time than LZ78. 

The largest relative difference occurred on \path{cyprus} and \path{hungary} files, which contain data in XML format. There, LZFSE had approximately 78\% better~(lower) decompression time than LZ78. 
The largest absolute difference was on \path{nightsht} and \path{venus} image files. 
These files took LZFSE approximately 264 milliseconds less than LZ78 to decompress (151.06~ms versus 415.21~ms on \path{nightsht}), having more than 66\% lower decompression time on both. 

\paragraph*{Compression ratio}\mbox{}\\
In terms of compression ratio, LZFSE proved to be the best from all tested dictionary methods. Thanks to its efficient compressor and robust entropy encoder, LZFSE achieved more than 20\% lower compression ratio than the second best method in average. It had the lowest compression ratio on all but one files from corpus and its compression ratio was always lower than $1$ (i.e.\ negative compression never occurred using LZFSE). On \path{hungary} XML file, LZFSE even had 55\% lower compression ratio than the second best dictionary method LZMW. The only file where LZFSE ranked second was \path{wnvcrdt} database file, where it had $6.31\%$ ratio and LZMW had $5.74$\%. 

The strength of LZFSE compressor was demonstrated on \path{abbot}, \path{firewrks} and \path{nightsht} files, where it was the only method to have compression ratio below $1$. On \path{nightsht} and \path{firewrks} graphical files, all other tested dictionary methods produced negative compression, as it is hard for them to find matches in these files. However, thanks to its ANS entropy coder, LZFSE had compression ratio of 92\% on both files. 

The file \path{abbot} is a ZIP archive and contains data compressed using the DEFLATE algorithm. % mentioned in the beginning of Chapter~\ref{chap:LZFSE}. 
The contents of this file was originally compressed with 54.3\% compression ratio using the said algorithm. While other dictionary methods failed to compress this archive further (each of them had at least 110\% compression ratio), LZFSE achieved compression ratio of 91.4\%. This indicates that the LZFSE compression method is slightly more sophisticated than the version of DEFLATE used to compress this ZIP archive. 

%compression ratio graph (all methods, selected files)
\begin{landscape}
\begin{figure}
\includegraphics[scale=1.4]{graph_dict_ratio.pdf}
\caption[Comparison of compression ratio of dictionary methods]{Comparison of compression ratio of LZFSE and other dictionary methods}
\end{figure}
\end{landscape}

\subsubsection{Statistical methods}

\paragraph*{Compression time}\mbox{}\\
In terms of compression speed, the fastest statistical method was static Huffman coding. It was also faster than LZFSE on more than half of corpus files. However, LZFSE was always at worst the second fastest method and it had the lowest compression time on $11$ from $30$ files from the Prague Corpus. 

LZFSE achieved comparable compression time to static Huffman coding in most cases, as the average relative difference was fewer than 5 percent. The biggest relative difference was on \path{modern} file, where compression time of LZFSE was 64\% larger. The biggest absolute difference was, unsurprisingly, on large graphic files. On \path{flower} and \path{venus} the difference was substantial -- it was more than 200 milliseconds on \path{flower} and nearly 170 milliseconds for \path{venus}. 
In contrast, on \path{nightsht}, LZFSE had comparable compression time to static Huffman coding and was only approximately one millisecond slower. % (the relative difference being fewer than 1\%). 

LZFSE performed best mainly on database and XML files. It did exceptionally well on \path{cyprus} and \path{hungary} XML files, where it had around 40\% lower compression time than static Huffman coding. 

\paragraph*{Decompression time}\mbox{}\\
LZFSE decompression was decisively faster than that of any tested statistical method. It outperformed statistical methods on all files and always had at least 90\% lower decompression time than the second fastest method (usually static Huffman coding, Shannon-Fano coding on some files). LZFSE did especially well on \path{hungary} and \path{cyprus} XML files, where it was around 36 times faster than the closest contender. Additionally, on \path{corilis} graphic file, it was more than 33 times faster than the second best static Huffman coding. 

This difference is particularly noticeable on large graphic files. On each of the \path{nightsht}, \path{flower} and \path{venus} images, LZFSE had at least 95\% lower decompression time than the second fastest method. In case of \path{nightsht} file, this difference was more than $3$ seconds (0.15 s versus 3.48 s static Huffman). 

\paragraph*{Compression ratio}\mbox{}\\
LZFSE achieved better compression ratio than any statistical method on 29 from 30 files. The only exception was \path{nightsht} file, where it had compression ratio of 92.3\% while arithmetic coding had 91.8\%. 

On 7 files from corpus, the LZFSE method had at least 70\% lower compression ratio than the second best compressing method on that file. On \path{hungary} XML file, it had 3.38\% compression ratio while the closest arithmetic coding had 56.82\%. Similarly, on \path{cyprus} XML file, LZFSE had ratio of 3.98\% and arithmetic coding 56.27\%, which is more than 14 times larger. 

\begin{figure}[ht!]
\centering
\includegraphics[scale=1.0]{graph_other_comp.pdf}
\caption[Comparison of compression time of LZFSE and non-dictionary methods]{Comparison of compression time of LZFSE, statistical methods and contextual method PPM}

\vspace{2cm} 

\includegraphics[scale=1.0]{graph_other_decomp.pdf}
\caption[Comparison of decompression time of LZFSE and non-dictionary methods]{Comparison of decompression time of LZFSE, statistical methods and contextual method PPM}
\end{figure}
%\clearpage

%compression ratio graph
\begin{landscape}
\begin{figure}
\includegraphics[scale=1.1]{graph_other_ratio.pdf}
\caption[Comparison of compression ratio of LZFSE and non-dictionary methods]{Comparison of compression ratio of LZFSE, statistical methods and contextual method PPM}
\end{figure}
\end{landscape}

\subsubsection{Prediction by partial matching}
Prediction by partial matching (PPM) from the contextual methods category was generally the best compressing method from all those tested. 
Excluding LZFSE, it had lower compression ratio than other tested methods on all files except one. 

Because compression ratio is not the main focus of the LZFSE method, but rather it seeks balance between compression quality and speed, it could be expected to be outperformed by PPM in this regard. PPM had better compression ratio than LZFSE on 26 from 30 corpus files. 
The largest difference was on \path{bovary} PDF document, where LZFSE had almost 55\% larger compression ratio than PPM (34.53\% ratio versus 22.34\% ratio) -- and so produced 1.55 times larger compressed file. Also on \path{emission}, \path{flower} and \path{modern} files, LZFSE had more than 1.4 times worse compression ratio. However, apart from these four files, the relative difference between LZFSE and PPM ratios was always lower than 35\%. On 22 from 30 corpus files, LZFSE had at most 25\% larger compression ratio than PPM. 

On four files from the Prague Corpus, LZFSE even achieved better compression ratio than PPM. When compressing the \path{hungary} file, LZFSE achieved ratio of 3.38\%, which is by 20\% lower than the 4.23\% ratio produced by PPM. Also on \path{cyprus} file, LZFSE had approximately 15\% lower compression ratio (3.98\% versus 4.68\%). On \path{abbot} and \path{firewrks} files, LZFSE had only slightly better compression ratio (by no more than 2\%).

However, in terms of compression and decompression speed, LZFSE was substantially better than PPM. LZFSE had lower both compression and decompression time on all files from corpus. On 25 files, LZFSE had at least 65\% lower compression time than PPM. The largest difference was on \path{firewrks} audio file, where LZFSE had more than 26 times smaller compression time than PPM (59~ms versus 1564~ms). On \path{nightsht} image, the largest file from corpus, PPM compression took 13.68 seconds longer and was 25 times slower~(574~ms LZFSE versus 14372~ms PPM). 

In case of decompression time, the difference between these two methods was even greater. On all files from corpus, LZFSE had more than 85\% lower decompression time than PPM, the average relative difference being approximately 95\%. The largest difference was again on the \path{firewrks} file, where LZFSE had more than 131 times lower decompression time (13~ms versus 1733~ms). The \path{nightsht} file was decompressed by LZFSE in 151~ms, while the same operation took PPM 15546~ms, which is more than 100 times longer.

In summary, LZFSE usually achieved slightly worse compression ratio but with significantly better speed than PPM.
So unless compression ratio is greatly important, LZFSE would almost always be better choice. 
Figures~\ref{fig:rel_perf_comp} and \ref{fig:rel_perf_decomp} contain comparison of relative performance of all tested methods. From these graphs, LZFSE seems to be best choice for general applications.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.6]{flower_comp_s.pdf}
\caption[Comparison of compression time and compression ratio of all tested methods on flower file]{Comparison of compression time and compression ratio of all tested methods on \path{flower} file. This graph shows the tradeoff between compression ratio and speed. Lower values are better for both axis, so methods closer to the origin have better performance.}
\label{fig:rel_perf_comp}

\vspace{1cm} 

\includegraphics[scale=0.6]{flower_decomp_s.pdf}
\caption[Comparison of decompression time and compression ratio of all tested methods on flower file]{Comparison of decompression time and compression ratio of all tested methods on \path{flower} file}
\label{fig:rel_perf_decomp}
\end{figure}
\clearpage

\subsection{Adjustable parameters}

\subsubsection{The \textit{good match} parameter}

As stated before, this parameter controls the minimum size for matches to be emitted immediately in the frontend~(see sections \ref{subsec:impl_implemented_parameters} and \ref{subsec:lzfse_comp_frontend}). 
Value of this parameter has an impact on compression ratio and speed. Setting it to higher value may yield better compression ratio as the matches are not emitted prematurely and longer matches may be found. However, this also increases the compression time. 

Increasing the value of this parameter always slightly increased the compression time, because it prolonged the search for matches in the frontend. However, larger values of this parameter did not always improve the achieved compression ratio. On \path{nightsht} file, in which it is hard to find matches, the compression ratio was exactly the same for $10$ and $100$ values of this parameter. Similarly, on \path{venus} and \path{flower} image files, only very small values (smaller than $20$) of the \textit{good match} parameter produced different compression ratios. 

The largest impact on compression ratio was on the \path{emission} database file, as shown in the graph below. This file contains many matches and some of them are very long. Therefore, when the parameter had lower values, the long matches were not found and the compression ratio was worse (higher). 

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.6]{graph_param_g.pdf}
\caption[Impact of good match parameter on compression time and ratio]{The impact of \textit{good match} parameter on compression time and compression ratio on the \path{emission} file}
\end{figure}

\subsubsection{The \textit{hash bits} parameter}

This parameter controls the number of bits produced by hash function when searching for matches~(see sections \ref{subsec:impl_implemented_parameters} and \ref{subsec:lzfse_comp_frontend}). 
Larger values reduce the probability of hash collisions and increase the size of the history table. 
Because the history table contains a history set for each possible hash value, the size grows exponentially. 
Increased size allows more match candidates to be present in the history table and potentially improves achieved compression ratio. However, it also causes more cache misses and subsequently larger compression time. 

Increasing this parameter always caused an increase in compression time. In case of \path{nightsht} and \path{flower} graphic files, the time increased significantly. 
The impact on compression ratio differed for each file. On \path{nightsht}, compression ratio remained basically the same (92.15\% for 10 vs. 92.23\% for 16). 
In contrast, the compression ratio changed significantly on the \path{flower} image~(see the graph below). 

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.6]{graph_param_h.pdf}
\caption[Impact of hash bits parameter on compression time and ratio]{The impact of \textit{hash bits} parameter on compression time and compression ratio on the \path{flower} file}
\end{figure}


\subsubsection{The \textit{hash width} parameter}

The \textit{hash width} parameter controls the number of entries for each set of the history table. Its value is defined in the \path{lzfse_tunables.h} header file as a preprocessor constant called \verb|LZFSE_ENCODE_HASH_WIDTH|. This parameter was not implemented for the LZFSE module, so its value can not be controlled through the ExCom API. To change the value of this parameter, its definition was modified and the LZFSE module was recompiled. 

Possible values for this parameter are 4 and 8, the default is 4. If it is set to 8, the history table size doubles. Having more match candidates for each position increases the chance of finding (longer) matches~\cite{LZFSE_github}. However, similarly to the \textit{hash bits} parameter, this results in more cache misses and slower compression speed. 

Benchmarks showed that the impact on compression time is significant. Compression time for \textit{hash bits} set to 8 was in average about 70\% larger than for value of 4. Biggest impact was on \path{collapse} file, where the compression time was 86\% larger. 

The impact on compression ratio was barely noticeable on most files. Setting the parameter value to 8 decreased compression ratio by approximately 1.4\% in average. The largest impact occurred on \path{hungary} and \path{cyprus} files\footnote{Both these files are XML files containing air quality monitoring data (see Appendix~\ref{chap:prague_corpus_files}).}, where the ratio decreased by 7\% and 6\% respectively. 

\subsubsection{Impact on decompression time}
Only compression time and compression ratio were discussed for all three parameters. All three parameters apply only to the compression operation. Therefore, decompression time is only affected indirectly by parameter settings. Compressing with different parameter values may produce slightly different output. 

However, the actual measured impact on decompression time was negligible. It changed by no more than 5\% in average for various settings of the three tested parameters. 

