\chapter{Asymmetric numeral systems}
\label{chap:ANS}

\emph{Asymmetric numeral systems} (ANS) is a family of entropy coding methods based on the work of Jarosław Duda. A variant of asymmetric numeral systems called \emph{finite state entropy} (FSE) is also part of the LZFSE compression method. 

This chapter will introduce important entropy coding concepts and some of the most common methods. Asymmetric numeral systems and their FSE variant will be described further in the chapter. 

\section{Entropy coding}
\emph{Entropy coding} (or entropy encoding) is a lossless data compression scheme. It achieves compression by representing frequently occurring symbols with fewer bits and rarely occurring elements with more bits. 

Huffman coding and arithmetic coding are two of the most common entropy coding methods and will be briefly described below. 

Asymmetric numeral systems is another family of entropy coding methods. 
ANS methods are increasingly used in compression as they combine compression ratio of arithmetic coding with compression speed similar to that of Huffman coding method~\cite{Duda2013}. ANS is described in Section~\ref{sec:ANS} and finite state entropy, which is their variant, in Section~\ref{sec:FSE}. 

Entropy coding methods are commonly used in combination with some dictionary compression scheme. 
For instance, the algorithm \mbox{DEFLATE}\footnote{DEFLATE is a lossless compression algorithm originally designed for the ZIP file format.} uses a combination of LZ77 (see Section~\ref{sec:LZ77}) and Huffman coding. LZFSE also belongs to this category as it combines a LZ-style compression algorithm with finite state entropy coding~\cite{LZFSE_github}. 

\subsection{Huffman coding}
\emph{Huffman coding} is an entropy coding technique developed by David A. Huffman. Huffman coding approximates probability of symbol occurrence as a (negative) power of $2$. Unlike more complex methods, Huffman coding always uses integral number of bits to represent a symbol and it encodes each symbol separately.

Every occurrence of one symbol is always encoded into same code word. 
Code words are assigned to symbols in a way that no code word is prefix of any other (such code is called a \emph{prefix code}). Shorter codes are assigned to common symbols. 

When the probabilities of the symbols are negative powers of two, Huffman coding produces the best results~\cite[p.~74]{Salomon2007}. 

Huffman coding is a simple compression method with low procession cost. Other entropy coding methods, such as arithmetic coding or ANS, may achieve considerably better compression ratio.

\subsection{Arithmetic coding}
\emph{Arithmetic coding} is another entropy coding technique. It encodes the input data as an interval of real numbers between $0$ and $1$. This interval becomes smaller as the input is encoded and number of bits needed to specify it grows~\cite{Witten1987}.

Unlike Huffman coding, which approximates symbol probabilities by powers of $2$, arithmetic coding is very precise but it is also more complex. Description of arithmetic coding is not the focus of this work and may be found in~\cite{Witten1987}.

\section{Asymmetric numeral systems}
\label{sec:ANS}
\emph{Asymmetric numeral systems} is an entropy coding technique introduced by Jarosław (Jarek) Duda. 

Apart from ANS, most common approaches to entropy coding are Huffman coding and arithmetic coding (both were briefly described in previous section). 
Huffman coding approximates symbol probabilities with powers of $2$ and so it generally does not achieve as good compression ratio. 
Arithmetic coding uses almost exact probabilities and so it achieves compression ratio close to the theoretical limit (see Section~\ref{subsec:shannon_theorem}), 
but it has larger computational cost than Huffman coding. 
According to its author, asymmetric numeral systems achieve comparable compression ratio as arithmetic coding while having procession cost similar to that of Huffman coding.~\cite{Duda2013}

While arithmetic coding uses two numbers (states) to represent a range, asymmetric numeral systems uses only one state -- a single natural number. 
ANS works by encoding information into this single natural number. To add new information to the information already stored in this number (state), new digits could be appended either in the most or the least significant position. Arithmetic coding is an example of the first approach. ANS uses the second option, new digits are added in the least significant position.~\cite{Duda2015}

In the standard binary numeral system a sequence of $n$ bits 
$\left(s_0, s_1, \ldots, s_{n-1}\right)$ would be encoded as a natural number 
$ x = \sum_{i=0}^{n-1} s_i 2^i $. 
To add information from a symbol~$s \in \{0, 1\}$, all bits of $x$ are shifted left and the least significant bit value is changed to $s$ (i.e.\ $s$ is appended to $x$). 
This changes $x$ to $ x\prime = C\left(x, s\right) = 2x + s $, where $C$ is a \emph{coding function}. 
The reverse process of decoding value of $s$ and previous state $x$ would 
use a \emph{decoding function} $ D(x\prime) = \left(x, s\right)
 = \left( \lfloor x\prime / 2 \rfloor, {x\prime \bmod 2} \right) $. 
Encoding a symbol sequence would be done by starting with some initial state and repeatedly using the coding function until all symbols are encoded. To decode, decoding function would be applied until state $x$ is equal to the initial state. Symbols are recovered in \emph{reverse} order. 

The scheme with coding and decoding functions presented above is optimal for any input with uniform probability distribution of symbols $ P(s_i=0)=P(s_i=1)=\frac{1}{2} $. 
The basic concept of ANS is to change this behaviour to make it optimal for any chosen asymmetric probability distribution. In the above example, $x\prime$ is either even or odd, based on the value of $s$. Therefore, $x\prime$ is $x$-th even or odd number, depending on $s$. As stated before, this scheme is optimal for storing uniformly distributed symbols. In ANS this division into even and odd subsets of natural numbers is replaced with division into subsets whose densities correspond with the chosen distribution~\cite{Duda2015}.

To optimize the coding procedure for the assumed probability 
distribution: $ P(s_i=s) = p_s $, subsets are redefined in such way, that their densities correspond to this probability distribution. %The number of occurrences of symbol $s$ up to the $x$-th position should be approximately $ x \cdot p_s $. 
The rule to add information from symbol $s$ to the information that is already stored in number $x$ is: $ x\prime = C\left(s,x\right) $. 
The coding function $ C\left(s,x\right) $ returns the $x$-th appearance of $s$ in the corresponding subset~\cite{Duda2015}.

\begin{figure}[ht!]

\def\svgwidth{\textwidth}
\input{ans_figure.pdf_tex}
\caption[Standard numeral system and ANS]{Adding information to state $x$ in standard binary numeral system~(up) and ANS~(bottom)~\cite{Duda2015}. The new state becomes $x$-th element of the $s$-th subset. In ANS subsets are defined so that their densities correspond to the assumed probability distribution\footnotemark.}
\label{fig:ANS_example}
\end{figure}

Figure~\ref{fig:ANS_example} shows example of asymmetric binary system for probability distribution: $ P(s_i=0) = \frac{1}{4}, P(s_i=1) = \frac{3}{4} $. Encoding a binary sequence $1101$ using tables shown in the figure and starting with initial state $x=1$ would produce: 

\begin{align*}
\text{standard binary system: } & 1 \xrightarrow{1} 3 \xrightarrow{1} 7 \xrightarrow{0} 14 \xrightarrow{1} 29 \\
\text{asymmetric binary system: } & 1 \xrightarrow{1} 2 \xrightarrow{1} 3 \xrightarrow{0} 12 \xrightarrow{1} 17
\end{align*}

As seen in this example, encoding the sequence using ANS produces smaller final state (number) than using the standard binary system, because it adapts the subsets to given probability distribution of input symbols. This difference would grow with the input size. 

Decoding would work similarly by following the steps in opposite direction.  
As seen in the example figure, the previous state and symbol are unambiguous for each state. It is always possible to get previous state and symbol from the table, for instance for state $x=17$, the previous state is 12 and the symbol is $1$. This way, previous state would be retrieved until the initial state is reached, decoding the whole sequence in the process. The symbols are retrieved in reverse order. 

%note for ANS figure
\footnotetext{In the original figure, the probabilities of symbols are defined as $ Pr(0) = 3/4 $ and $ Pr(1) = 1/4 $, but here they were changed to correspond to the subsets.}

In practice it is preferable to avoid operations on very large numbers (states), as such operations may be very demanding. When a state is larger than some maximum value during encoding, some of its least significant bits are transferred to the output stream. This way the state will always remain in some fixed range. Naturally, decoding has to be modified accordingly and it must be ensured that encoding and decoding steps are inverse of each other. The exact mechanisms to achieve that are described in~\cite{Duda2015}.

Also, because encoding works in opposite direction than decoding, it might be preferable to encode backwards starting from the last symbol so that decoding would start from the beginning and proceed forward. 

Concepts described in this section could also be generalized for numeral systems of different base than $2$ (i.e.\ for more possible input symbols). 
Several variants of ANS exists. An abstract description of finite state entropy (tANS), variant used in LZFSE, may be found in the next section (see Chapter~\ref{chap:LZFSE} for details of its implementation in~LZFSE).
Detailed description of asymmetric numeral systems and all their variants may be found in \cite{Duda2013} and \cite{Duda2015}. 

\section{Finite state entropy}
\label{sec:FSE}
\emph{Finite state entropy} is a variant of asymmetric numeral systems (described in previous section). It is also called \emph{tANS} (or table ANS) because the entire behaviour is put into a single coding table, yielding finite state machine. 

For the assumed probability distribution, given usually in form of symbol frequencies, symbols must be distributed into subsets (as shown in previous section). Densities of those subsets must correspond to given probability distribution. ``\textit{Unfortunately, finding the optimal symbol distribution is not an easy task.}'' It could be done by checking all possible symbol distributions. However, in practice some simple heuristic is usually used instead~\cite{Duda2013}. 
A~simple and fast method of pseudorandomly distributing symbols into subsets is described in detail in~\cite{Duda2015}. 

The table used for encoding is called \emph{encoding table}. For each symbol and state, the next state is stored in it. So the table stores results of coding function $C\left(x, s\right)$ for all possible $(x,s)$ pairs. This table is created based on symbol distribution into subsets described in previous paragraph. 
The encoding table may also be stored in one-dimensional array as it is better for memory handling efficiency~\cite{Duda2015}. 

The encoding process is similar to the ANS encoding described in Section~\ref{sec:ANS}, but instead of computing the coding function $C\left(s,x\right)$ in each step, the next state is obtained from the encoding table. 

Additionally, the mechanism of ensuring that states remain in some fixed range, which was discussed in previous section, is usually employed. If a state is bigger than given upper bound, its least significant bits are written to the output and the state is shifted right until it fits into the range. The number of bits that will have to be transferred to the output stream may be computed beforehand and stored for each possible transition (or symbol) to avoid additional computations during encoding. 

The table that is used for decoding is called \emph{decoding table}. For decoding to work, it is necessary to be able to get this table, and so some information must be added to the compressed data when encoding. This may be table of symbol frequencies, which was also used to construct the encoding table. This information is usually part of some \emph{header} and may itself be compressed. 

As with the encoding step, the decoding step of FSE is similar to the decoding step of general ANS (see Section~\ref{sec:ANS}).
Instead of computing the result of decoding function~$ D(x) $, the previous state and symbol are looked-up in the decoding table. 

This section contains only abstract description of finite state entropy, as there are several different possibilities of how to implement this method. 
More thorough description of this method along with exact pseudocodes for initialization and encoding and decoding steps may be found in~\cite{Duda2015}. Implementation of finite state entropy in LZFSE is described in Chapter~\ref{chap:LZFSE}.

